/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.builtin.library.base64.Base64;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.worksheets.ScreenCapturePanel;


public class ImageView extends BuiltinProcedure {

    private Map defaultOptions;
    
    public void plugIn(Environment aEnvironment)  throws Throwable
    {
	this.functionName = "ImageView";
	
        aEnvironment.getBuiltinFunctions().put("ImageView", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Base64?", false);
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
	
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if(! Utility.isSublist(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.

        Object imageFileArgument = arguments.car();
        

        Cons options = arguments.cdr();

        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        boolean isBase64 = (Boolean) userOptions.get("Base64?");
        
        Image image = null;
        
        if(isBase64)
        {
            if(! (imageFileArgument instanceof String))
            {
                LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
            }
            
            byte[] imageByte;

            imageByte =  Base64.decode((String) imageFileArgument);

            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        }
        else
        {
            if (!(imageFileArgument instanceof JavaObject) && !(((JavaObject) imageFileArgument).getObject() instanceof Image))
            {
                LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
            }
            
            image = (Image) ((JavaObject) imageFileArgument).getObject();
        }

        JLabel imageLabel = new JLabel();
        
        ImageIcon icon = new ImageIcon(image);

        imageLabel.setIcon(icon);


        JPanel screenCapturePanel = new ScreenCapturePanel();
        
        screenCapturePanel.add(imageLabel);
 
        JPanel jPanel = new JPanel();
        jPanel.setBackground(Color.WHITE);
        jPanel.add(screenCapturePanel);

        JavaObject response = new JavaObject(jPanel);

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));
    }//end method.


}//end class.





/*
%mathpiper_docs,name="ImageView",categories="Programming Procedures,Visualization"
*CMD ImageView --- display a Java Image

*CALL
    ImageView(expression, option, option, option...)

*PARMS
{image} -- a Java Image

{Options:}

{Base64?} -- if true the first argument is in Base64 format

*DESC
Display a Java Image.
 
*E.G.
In> Show(ImageView(image))
Result: java.awt.Component


*SEE Show
%/mathpiper_docs
*/



