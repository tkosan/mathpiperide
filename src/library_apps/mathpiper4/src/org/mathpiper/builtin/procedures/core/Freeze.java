/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

public class Freeze extends BuiltinProcedure
{

    private Freeze()
    {
    }

    public Freeze(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        String procedureName = (String) getArgument(aEnvironment, aStackTop, 0).car();
        
        Cons head = SublistCons.getInstance(AtomCons.getInstance(aEnvironment.getPrecision(), procedureName));
        ((Cons) head.car()).setCdr(getArgument(aEnvironment, aStackTop, 1).copy(false));
        setTopOfStack(aEnvironment, aStackTop, head);
    }
}

/*
%mathpiper_docs,name="Freeze",categories="Programming Procedures,Miscellaneous,Built In"
*CMD Freeze(expression) --- freezes the evaluation of an expression

*CALL
        Freeze(expression)

*PARMS

{expression} -- expression to freeze

*DESC

The expression that is passed to {Freeze} is returned unevaluated. This procedure is different from the 
{Hold} procedure in that it holds the expression that is its argument permanently.

*E.G.
In> Freeze(2 + 2)
Result: Freeze(2 + 2)

*SEE '', ', Hold
%/mathpiper_docs





%mathpiper_docs,name="''",categories="Operators"
*CMD ''expression --- freezes the evaluation of an expression

*CALL
        ''expression

*PARMS

{expression} -- expression to freeze

*DESC

The expression that is passed to {''} is returned unevaluated. The {''} freeze operator 
is different than the {'} operator in that {''} holds the expression that is its argument 
permanently. The {''} operator has a very low precedence so that it can often be used 
without needing to place parentheses around the expression
to be frozen.

*E.G.
In> x := 3
Result: 3

In> ''x
Result: ''x

In> ''2+2
Result: ''2 + 2

*SEE Freeze, ', Hold
%/mathpiper_docs
*/
