
package org.mathpiper.ui.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.Box;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.Cons;


public class VerticalLine extends LayoutTechnique {
    
    private Box box;
            
    public VerticalLine(boolean isBorder, Color color)
    {
        this.setLayout(new BorderLayout());
        
        box = Box.createVerticalBox();
        
        if(isBorder)
        {
            box.setBorder(BorderFactory.createLineBorder(color));
        }

        this.add(box);
    }
    
    public void setList(Environment aEnvironment, int aStackTop, Cons dataList) throws Throwable
    {
        box.removeAll();
        
        //box.add(new VariableWidthLine("LINE"));
        
        while (dataList != null)
        {
            Object object = dataList.car();

            if (!(object instanceof JavaObject))
            {
                LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
            }

            JavaObject javaObject = (JavaObject) object;

            object = javaObject.getObject();

            if (!(object instanceof Component))
            {
                LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
            }

            Component component = (Component) object;
            
            if (component.getName() != null && component.getName().equals("LINE")) {
                box.add(new VariableWidthLine("LINE"));
            }
            else if (component.getName() != null && component.getName().equals("LINE2")) {
                box.add(new VariableWidthLine("LINE2", 12));
            }
            else
            {
                box.add(component);
            }

            dataList = dataList.cdr();
        }
    }
    
}
