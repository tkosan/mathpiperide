/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Operator;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;


public class LeftSpacesGet extends BuiltinProcedure
{

    private LeftSpacesGet()
    {
    }

    public LeftSpacesGet(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons arg1 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 1);
	if (arg1 == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
	String operatorName = (String) arg1.car();
        
        Cons arg2 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 2);
        if( arg2 == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        LispError.checkIsString(aEnvironment, aStackTop, arg2, aStackTop);
        String xfix = (String) arg2.car();
        if( xfix == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        xfix = Utility.stripEndQuotesIfPresent(xfix);
        
        Operator operator = null;
        
        if(xfix.equals("Prefix"))
        {
            operator = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iPrefixOperators, 1);
            
            if (operator == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator " + operatorName + " is not a prefix operator.");
            }
        }
        else if (xfix.equals("Infix"))
        {
            operator = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iInfixOperators, 1);
            
            if (operator == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator " + operatorName + "  is not an infix operator.");
            }          
        }
        else if (xfix.equals("Postfix"))
        {
            operator = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iPostfixOperators, 1);
            
            if (operator == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator " + operatorName + " is not a postfix operator.");
            }
        }
        else
        {
            LispError.throwError(aEnvironment, aStackTop, "The symbol \"" + xfix + "\" is not a type of operator.");
        }
        
        
        operatorName = Utility.stripEndQuotesIfPresent(operatorName);
        
        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "\"" + operator.getSpaceLeft() + "\""));
    }
}



/*
%mathpiper_docs,name="LeftSpacesGet",categories="Programming Procedures,Miscellaneous,Built In"
*CMD LeftSpacesGet --- return the number of spaces that are placed to the left of an operator when it is unparsed
*CALL
    LeftSpacesGet(operatorName, operatorType)

*PARMS

{operatorName} -- string, the name of an operator
{operatorType} -- "Prefix", "Infix", or "Postfix"

*DESC
This procedure returns the number of spaces that are placed to the left of an operator when it is unparsed.

*EXAMPLES

*SEE RightSpacesGet,LeftPrecedenceGet,LeftPrecedenceSet,RightPrecedenceSet,RightAssociativeSet,OperatorsGet,OperatorSpacesSet
%/mathpiper_docs




%mathpiper,name="LeftSpacesGet",subtype="in_prompts"

LeftSpacesGet("+", "Infix") ~> " "

%/mathpiper
*/