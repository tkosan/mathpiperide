%mathpiper,def="<<;>>"

/* def file definitions
<<
>>
*/

/* Shifting operators */

n_Integer? << m_Integer? --> ShiftLeft(n,m);
n_Integer? >> m_Integer? --> ShiftRight(n,m);

%/mathpiper


%mathpiper_docs,name="<<;>>",categories="Operators"
*CMD << --- binary shift left operator
*CMD >> --- binary shift right operator
*STD
*CALL
        n<<m
        n>>m

*PARMS

{n}, {m} -- integers

*DESC

These operators shift integers to the left or to the right.
They are similar to the C shift operators. These are sign-extended
shifts, so they act as multiplication or division by powers of 2.

*EXAMPLES

*SEE +, -, *, ^
%/mathpiper_docs





%mathpiper,name="<<;>>",subtype="in_prompts"

1 << 10 -> 1024

-1024 >> 10 -> -1

%/mathpiper





%mathpiper,name="<<;>>",subtype="automatic_test"

Verify(1<<10,1024);
Verify(1024>>10,1);
Verify(-1024 >> 10, -1);

%/mathpiper