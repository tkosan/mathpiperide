package org.mathpiper.lisp.parsers.subl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.mathpiper.lisp.parsers.subl.parser.Parser;
import org.mathpiper.lisp.parsers.subl.lexer.Lexer;
import org.mathpiper.lisp.parsers.subl.parser.ParseTree;
import org.mathpiper.lisp.parsers.subl.parser.SExpression;
import org.mathpiper.lisp.parsers.subl.unparser.Unparser;

// Obtained from https://github.com/jtanderson/LispInterpreter

/*
MIT License

Copyright (c) 2020 Joseph Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
/**
 * File: LispInterpreter.java
 *
 * This is the driver file for the Lisp Interpreter project
 *
 * It takes input from stdin and evaluates it line by line. One should note that
 * if muliple statements are on the same line, their results will not be output
 * until all have executed successfully.
 *
 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
 * @since 2012-11-01
 * @version 2012-11-01
 */
class LispInterpreter {

    /**
     * Function: main
     *
     * The main function of the program.
     *
     * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
     * @since 2012-11-01
     * @version 2012-11-01
     *
     * @param args Any command line arguments. None are used as of this version.
     */
    public static void main(String[] args) throws Throwable {
        try {

            //"cyc-assert".matches(LexerPatterns.LITERAL);
            String code = "(PLUS 1 2)";

            code = "(cyc-assert '(#$isa #$LogicMOO #$Project) #$LogicMOOMt)";
            
            code = "(((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$ExtendedAirDefenseSimulationSystem))) ((?X #$EnterpriseForSystemFn #$USGroundBasedMidcourseDefenseSystem)) ((?X . #$ProductLinking-CycProject)) ((?X . #$ThePrototypicalSAPProject)) ((?X . #$GeneralCycKE)) ((?X #$EnterpriseForSystemFn #$UnitedStatesBallisticMissileDefenseSystem)) ((?X . #$ThePrototypicalDODEnterpriseProgram)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$USGroundBasedMidcourseDefenseSystem))) ((?X #$EnterpriseForSystemFn #$PhasedArrayTrackingRadarInterceptOnTargetDefenseSystem)) ((?X . #$SmallBusinessTechnologyTransferProgram)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$UnitedStatesDefenseSupportProgram))) ((?X . #$LogicMOO)) ((?X . #$AutomatedJCIDSProject)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$USTerminalHighAltitudeAreaDefenseSystem))) ((?X . #$AgentBasedSoftwareProject)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$GlobalInformationGridContentDeliveryServiceSystem-NCES))) ((?X #$EnterpriseForSystemFn #$ExtendedAirDefenseSimulationSystem)) ((?X #$EnterpriseForSystemFn (#$TextKAObjectFn (#$PCWSubSectionOfTypeFn (#$PCWSubSectionOfTypeFn #$MDATestDoc1 #$MDAReqSpecIntroduction-PCW) #$MDAReqSpecSubSection-TitlePage-PCW) (#$KATemplateFieldOfTextTypeFn #$MDAReqSpecSubSection-TitlePage-PCW (#$AgentAndInstrumentSystemOperationallyControlledByFn #$MissileDefenseAgency) 1) \"Core Truth Services System\"))) ((?X . #$ConventionGuideProject)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn (#$TheKAFieldValueMappedFn (#$KATemplateFieldOfTextTypeFn #$CDDIntroductionSubSection-TitlePage-PCW #$DODAgentAndInstrumentSystem 1))))) ((?X #$EnterpriseForSystemFn #$C2BMC-System)) ((?X #$EnterpriseForSystemFn #$JointTacticalGroundStation)) ((?X #$EnterpriseForSystemFn #$EnterpriseFileDeliveryServiceSystem-NCES)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$MarineCorpsIntelligenceSurveillanceReconnaissanceSystem))) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$PhasedArrayTrackingRadarInterceptOnTargetDefenseSystem))) ((?X #$EnterpriseForSystemFn #$SpaceTrackingAndSurveillanceSystem)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$Space-BasedInfraredSystem))) ((?X . #$NISTProject-TaskA)) ((?X . #$NISTProject-TaskD)) ((?X #$EnterpriseForSystemFn #$UnitedStatesDefenseSupportProgram)) ((?X . #$OpenDirectory-CycProject)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$USAegisShipBasedMissileDefenseSystem))) ((?X #$EnterpriseForSystemFn #$GlobalInformationGridContentDeliveryServiceSystem-NCES)) ((?X #$EnterpriseForSystemFn #$USAegisShipBasedMissileDefenseSystem)) ((?X #$EnterpriseForSystemFn #$EnterpriseMessagingSOAFServiceSystem-NCES)) ((?X #$DODDevelopmentProjectForFn #$MarineCorpsIntelligenceSurveillanceReconnaissanceEnterprise)) ((?X . #$HealthCareProject)) ((?X #$EnterpriseForSystemFn (#$TheKAFieldValueMappedFn (#$KATemplateFieldOfTextTypeFn #$CDDIntroductionSubSection-TitlePage-PCW #$DODAgentAndInstrumentSystem 1)))) ((?X . #$ManhattanProject)) ((?X . #$NISTProject-TaskC)) ((?X #$EnterpriseForSystemFn #$Space-BasedInfraredSystem)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$UnitedStatesBallisticMissileDefenseSystem))) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$EnterpriseMessagingSOAFServiceSystem-NCES))) ((?X . #$ThePrototypicalUSFederalGovernmentProgram)) ((?X . #$TextClassificationProject)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$SpaceTrackingAndSurveillanceSystem))) ((?X . #$Apollo11SpaceMission)) ((?X . #$HPKB-Year2-EndToEndEvaluation)) ((?X . #$RICASeedling)) ((?X . #$SystemsBiologyProject)) ((?X . #$MarineCorpsIntelligenceSurveillanceReconnaissanceEnterprise)) ((?X . #$OpenCycProject)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$JointTacticalGroundStation))) ((?X . #$SovietSmallPoxBWTest)) ((?X . #$SmallBusinessInnovationResearchProgram)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn (#$TextKAObjectFn (#$PCWSubSectionOfTypeFn (#$PCWSubSectionOfTypeFn #$MDATestDoc1 #$MDAReqSpecIntroduction-PCW) #$MDAReqSpecSubSection-TitlePage-PCW) (#$KATemplateFieldOfTextTypeFn #$MDAReqSpecSubSection-TitlePage-PCW (#$AgentAndInstrumentSystemOperationallyControlledByFn #$MissileDefenseAgency) 1) \"Core Truth Services System\")))) ((?X #$EnterpriseForSystemFn #$USTerminalHighAltitudeAreaDefenseSystem)) ((?X . #$NISTProject-TaskB)) ((?X . #$IntrinsicallySidedObjectWork)) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$EnterpriseFileDeliveryServiceSystem-NCES))) ((?X #$DODDevelopmentProjectForFn (#$EnterpriseForSystemFn #$C2BMC-System))))";

            //InputStream is = new ByteArrayInputStream(code.getBytes("UTF8"));
            Lexer lexer = new Lexer(code);
            Parser parser = new Parser(lexer.getTokens());
            // System.out.println(p.printParseTree());

            ParseTree parseTree = (ParseTree) parser.statements.get(0);
           
            Unparser unparser = new Unparser();

            System.out.println(unparser.unparse((SExpression) parseTree.root, false));

            //p.evaluate();
        } catch (IOException e) {
            System.out.println("End of input...");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(3);
        }
    }
}
