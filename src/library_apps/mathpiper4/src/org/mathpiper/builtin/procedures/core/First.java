/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class First extends BuiltinProcedure
{

    private First()
    {
    }

    public First(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
	Cons argumentCons = getArgument(aEnvironment, aStackTop, 1);
	
        setTopOfStack(aEnvironment, aStackTop, Utility.nth(aEnvironment, aStackTop, argumentCons, 1, true));
    }
}



/*
%mathpiper_docs,name="First",categories="Programming Procedures,Lists (Operations),Built In"
*CMD First --- the first element of a list
*CORE
*CALL
	First(list)

*PARMS

{list} -- a list

*DESC

This function returns the first element of a list. If it is applied to
a general expression, it returns the first operand. An error is
returned if "list" is an atom.

*EXAMPLES

*SEE Rest, Length
%/mathpiper_docs





%mathpiper,name="First",subtype="in_prompts"

First([_a,_b,_c]) ~> _a

f(a,b,c) := [a,b,c] ~> True

First(f(_a,3,4)) ~> _a

%/mathpiper





%mathpiper,name="First",subtype="automatic_test"

Verify(First('[a,b,c]),'a);

%/mathpiper

*/