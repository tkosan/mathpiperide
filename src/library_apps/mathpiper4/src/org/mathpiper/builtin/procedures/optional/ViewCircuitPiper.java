/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitPiperMain;

/**
 *
 *
 */
public class ViewCircuitPiper extends BuiltinProcedure {

    private Map defaultOptions;
    
    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "ViewCircuitPiper";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 0, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    
        defaultOptions = new HashMap();
        defaultOptions.put("Netlist", "");
        defaultOptions.put("Width", null);
        defaultOptions.put("Height", null);
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if (!Utility.isSublist(arguments))
        {
            LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
        }

        arguments = (Cons) arguments.car(); //Go to sublist.

        Map userOptions = null;
        
        if(arguments != null)
        {
            Cons options = arguments.cdr(); //Strip List tag.
            userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        }
        else
        {
            userOptions = defaultOptions;
        }
        
        String netlist = (String) userOptions.get("Netlist");
        
        JavaObject response = new JavaObject(showFrame(netlist, userOptions));

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));

    }//end method.

    
    public static Circuit showFrame(String netlist, Map userOptions) throws Throwable {
        final JFrame frame = new javax.swing.JFrame();

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        final CircuitPiperMain circuitPiperMain = new CircuitPiperMain();
        final CircuitPanel circuitPanel = circuitPiperMain.getPanel();
        Circuit circuit = circuitPanel.getCurrentCircuit();
        circuit.frame = frame;
        
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {

                circuitPiperMain.getPanel().isRunning = false;
                circuitPiperMain.getPanel().myTimer.stop();

                if (frame.getDefaultCloseOperation() == JFrame.EXIT_ON_CLOSE) {
                    System.exit(0);
                } else if (frame.getDefaultCloseOperation() == JFrame.DISPOSE_ON_CLOSE) {
                    frame.dispose();
                }
            }
        });
            
        if(! netlist.equals(""))
        {
            circuit.readCircuit(netlist);
        }

        Container contentPane = frame.getContentPane();
        contentPane.add(circuitPiperMain, BorderLayout.CENTER);

        frame.setTitle("CircuitPiper");
        frame.pack();
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = userOptions.get("Width") == null ? screenSize.width / 2 : ((BigNumber) userOptions.get("Width")).toInt();
        int height = userOptions.get("Height") == null ? screenSize.height / 2 : ((BigNumber) userOptions.get("Height")).toInt();
        frame.setSize(width, height);
        
        frame.setLocationRelativeTo(null); // added

        frame.setVisible(true);

        return circuit;
    }
}




/*
%mathpiper_docs,name="ViewCircuitPiper",categories="Programming Procedures,Built In"
*CMD ViewCircuitPiper --- display the CircuitPiper simulator
*CALL
    ViewCircuitPiper()

 *PARMS

 {Options:}

 {Netlist} -- the circuit's file
 {Width} -- the width of the window
 {Height} -- the height of the window

*DESC
Displays the CircutiPiper simulator.


%/mathpiper_docs
*/
