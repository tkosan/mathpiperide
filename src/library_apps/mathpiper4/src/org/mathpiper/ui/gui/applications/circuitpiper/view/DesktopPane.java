package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;


public class DesktopPane extends JDesktopPane
{
    public Dimension getPreferredSize()
    {
        JInternalFrame[] internalFrames = getAllFrames();
        
        int lowX = Integer.MAX_VALUE;
        int lowY = Integer.MAX_VALUE;
        int highX = Integer.MIN_VALUE;
        int highY = Integer.MIN_VALUE;
        
        for(JInternalFrame internalFrame : internalFrames)
        {
            Insets insets = internalFrame.getInsets();
            
            Rectangle bounds = internalFrame.getBounds();
            
            if(bounds.x - insets.left < lowX)
            {
                lowX = bounds.x - insets.left;
            }
            
            if(bounds.x + bounds.width + insets.right > highX)
            {
                highX = bounds.x + bounds.width + insets.right;
            }
            
            if(bounds.y - insets.top < lowY)
            {
                lowY = bounds.y - insets.top;
            }
            
            if(bounds.y + bounds.height + insets.bottom > highY)
            {
                highY = bounds.y + bounds.height + insets.bottom;
            }
        }
        
        Insets paneInsets = getInsets();
        
        return new Dimension((highX - lowX) + paneInsets.left + paneInsets.right, (highY - lowY) + paneInsets.top + paneInsets.bottom);
    }
    
}


