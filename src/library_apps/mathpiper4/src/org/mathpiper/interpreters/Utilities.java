package org.mathpiper.interpreters;

import java.util.ArrayList;
import java.util.List;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;

public class Utilities {

    public static Object associationListGet(Environment aEnvironment, int aStackTop, String key, Cons listCons) throws Throwable {

        Cons keyAtom = AtomCons.getInstance(aEnvironment.getPrecision(), Utility.toMathPiperString(aEnvironment, aStackTop, key));

        while (listCons != null) {
            if (listCons.car() instanceof Cons) {
                Cons sublist = (Cons) listCons.car();
                if (sublist != null) {
                    sublist = sublist.cdr();

                    if (Utility.equals(aEnvironment, aStackTop, keyAtom, sublist)) {
                        Object object = listCons.cdr().car();
                        return object;
                    }
                }
            }
            listCons = listCons.cdr();
        }

        return null;
    }

    public static List associationListKeys(Environment aEnvironment, int aStackTop, Cons listCons) throws Throwable {

        List keysList = new ArrayList();
        Cons associationList = ((Cons) listCons.car()).cdr();
        
        while (associationList != null) {
            Object object = Cons.cadar(associationList);
            keysList.add(object);
            associationList = associationList.cdr();
        }

        return keysList;
    }

    public static List associationListValues(Environment aEnvironment, int aStackTop, Cons listCons) throws Throwable {

        List valuesList = new ArrayList();
        Cons associationList = ((Cons) listCons.car()).cdr();

        while (associationList != null) {
            Object object = Cons.caddar(associationList);

            valuesList.add(object);

            associationList = associationList.cdr();
        }

        return valuesList;
    }

    public static String toJavaString(String aOriginal) {

        if (aOriginal == null) {
            return null;
        }

        // If there are not quotes on both ends of the string then return
        // without any changes.
        if (aOriginal.startsWith("\"") && aOriginal.endsWith("\"")) {
            aOriginal = aOriginal.substring(1, aOriginal.length());
            aOriginal = aOriginal.substring(0, aOriginal.length() - 1);
        }

        return aOriginal;
    }

}

