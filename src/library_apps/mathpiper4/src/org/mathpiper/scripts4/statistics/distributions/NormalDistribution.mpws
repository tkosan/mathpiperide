%mathpiper,def="NormalDistribution"

/* Guard against distribution objects with senseless parameters
   Anti-nominalism */
   
RulebaseHoldArguments("NormalDistribution",["x"]);

NormalDistribution( m_ , s2_RationalOrNumber?)::(s2 <=? 0) --> Undefined;

%/mathpiper




%mathpiper_docs,name="NormalDistribution",categories="Mathematics Procedures,Statistics & Probability"
*CMD NormalDistribution --- The normal distribution.
*STD
*CALL
        NormalDistribution(mean, sigma)

*PARMS

{mean} -- Number, the mean of the distribution
{sigma} -- Number, the standard deviation of the distribution

*DESC
The normal distribution.

*EXAMPLES

*SEE BinomialDistribution, BernoulliDistribution, ChiSquareDistribution, DiscreteUniformDistribution, ContinuousUniformDistribution, ExponentialDistribution, GeometricDistribution, NormalDistribution, PoissonDistribution, tDistribution
%/mathpiper_docs





%mathpiper,name="NormalDistribution",subtype="in_prompts"

NM(CDF(NormalDistribution(60,5),64.3)) ~> 0.8051055222;

%/mathpiper





%mathpiper,name="NormalDistribution",subtype="automatic_test"

Verify(NM(CDF(NormalDistribution(50,3),40.2)), 0.0005441691);

%/mathpiper