%mathpiper,def="Drop"

/* ���� Drop ���� */

/* Needs to check the parameters */

/*
 * Drop( list, n ) gives 'list' with its first n elements dropped
 * Drop( list, -n ) gives 'list' with its last n elements dropped
 * Drop( list, [m,n] ) gives 'list' with elements m through n dropped
 */

RulebaseHoldArguments("Drop", ["lst", "range"]);

RuleHoldArguments("Drop", 2, 1, List?(range))
    Concat(Take(lst,range[1]-1), Drop(lst, range[2]));

RuleHoldArguments("Drop", 2, 2, range >=? 0)
    Decide( range =? 0 |? lst =? [], lst, Drop( Rest(lst), range-1 ));

RuleHoldArguments("Drop", 2, 2, range <? 0)
    Take( lst, Decide(AbsN(range) <? Length(lst), Length(lst) + range, 0 ) );

%/mathpiper



%mathpiper_docs,name="Drop",categories="Programming Procedures,Lists (Operations)"
*CMD Drop --- drop a range of elements from a list

*STD

*CALL
        Drop(list, n)
        Drop(list, -n)
        Drop(list, [m,n])

*PARMS

{list} -- list to act on

{n}, {m} -- positive integers describing the entries to drop

*DESC

This command removes a sublist of "list" and returns a list
containing the remaining entries. The first calling sequence drops the
first "n" entries in "list". The second form drops the last "n"
entries. The last invocation drops the elements with indices "m"
through "n".

*EXAMPLES

*SEE Take, Select, Remove
%/mathpiper_docs





%mathpiper,name="Drop",subtype="in_prompts"

lst := [_a,_b,_c,_d,_e,_f,_g] ~> [_a,_b,_c,_d,_e,_f,_g]

Drop(lst, 2) ~> [_c,_d,_e,_f,_g]

Drop(lst, -3) ~> [_a,_b,_c,_d]

Drop(lst, [2,4]) ~> [_a,_e,_f,_g]

%/mathpiper





%mathpiper,name="Drop",subtype="automatic_test"

{

Local(list);

list := [_a,_b,_c,_d,_e,_f,_g];

Verify(Drop(list, 2), [_c,_d,_e,_f,_g]);

Verify(Drop(list, -3), [_a,_b,_c,_d]);

Verify(Drop(list, [2,4]), [_a,_e,_f,_g]);

}

%/mathpiper
