package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

public class Segment extends GraphicObject
{

    private Point point1;
    private Point point2;

    public Segment(String label, Point point1, Point point2)
    {
        this.label = label;

        this.point1 = point1;
        this.point2 = point2;
    }

    public void paint(ScaledGraphics sg, PlotPanel plotPanel)
    {
        if (isVisible)
        {
            Color oldColor = sg.getColor();

            sg.setColor(Color.BLACK);

            sg.setLineWidth(1.0);
            sg.drawLine(plotPanel.toPixelsX(point1.getXCoordinate()), plotPanel.toPixelsY(point1.getYCoordinate()),
                    plotPanel.toPixelsX(point2.getXCoordinate()), plotPanel.toPixelsY(point2.getYCoordinate()));

            sg.setColor(oldColor);
        }
    }

}
