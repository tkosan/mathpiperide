package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive;

import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.Stack;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.Point;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.TransformerElm;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.mathpiper.ui.gui.applications.circuitpiper.view.SchematicSymbol;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.NTerminalComponent;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class Transformer extends NTerminalComponent {
    
    public static int componentCounter = 1;

    public Transformer(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        calculatedTerminals = new Terminal[2];
        calculatedPoints = new Point2D.Double[2];
        
        if(uid == null)
        {
            componentSubscript = componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }

        init();
        primaryValue = 0.00234;
        enteredPrimaryValue = "" + 1/siToValue.get(siPrefix) * primaryValue;
    }
    
    public Transformer(int x, int y, String uid, Stack<String> attributes, Circuit circuit) throws Exception {
        super(x, y, circuit);
        
        calculatedTerminals = new Terminal[2];
        calculatedPoints = new Point2D.Double[2];
        
        if(uid == null)
        {
            componentSubscript = componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
 
        init();
         
        handleAttribute(attributes);
    }

    public void init() {
        symbol = new SchematicSymbol();
        symbol.type = "Transformer";
        
        symbol.templateHeadTerminalPoint = new Point2D.Double(0, 0);
        symbol.templateHeadLeadPoint = new Point2D.Double(0, 0);
        symbol.templateTailTerminalPoint = new Point2D.Double(30, 0);
        symbol.templateTailLeadPoint = new Point2D.Double(30, 0);
        
        symbol.template.put("1", SchematicSymbol.makeArc(new Point2D.Double(6,30), new Point2D.Double(9.75, 26.25), new Point2D.Double(6,22.5)));
        symbol.template.put("2", SchematicSymbol.makeArc(new Point2D.Double(6,22.5), new Point2D.Double(9.75, 18.75), new Point2D.Double(6,15)));
        symbol.template.put("3", SchematicSymbol.makeArc(new Point2D.Double(6,15), new Point2D.Double(9.75, 11.25), new Point2D.Double(6,7.5)));
        symbol.template.put("4", SchematicSymbol.makeArc(new Point2D.Double(6,7.5), new Point2D.Double(9.75, 3.75), new Point2D.Double(6,0)));

        symbol.template.put("5", SchematicSymbol.makeArc(new Point2D.Double(24,30), new Point2D.Double(20.25, 26.25), new Point2D.Double(24,22.5)));
        symbol.template.put("6", SchematicSymbol.makeArc(new Point2D.Double(24,22.5), new Point2D.Double(20.25, 18.75), new Point2D.Double(24,15)));
        symbol.template.put("7", SchematicSymbol.makeArc(new Point2D.Double(24,15), new Point2D.Double(20.25, 11.25), new Point2D.Double(24,7.5)));
        symbol.template.put("8", SchematicSymbol.makeArc(new Point2D.Double(24,7.5), new Point2D.Double(20.25, 3.75), new Point2D.Double(24,0)));

        // Short leads.
        symbol.template.put("LeadLine1", new Line2D.Double(0, 30, 6, 30));
        symbol.template.put("LeadLine2", new Line2D.Double(0, 0, 6, 0));
        symbol.template.put("LeadLine3", new Line2D.Double(24, 30, 30, 30));
        symbol.template.put("LeadLine4", new Line2D.Double(24, 0, 30, 0));

        // Iron core symbol.
        symbol.template.put("13", new Line2D.Double(12, 30, 12, 0));
        symbol.template.put("14", new Line2D.Double(18, 30, 18, 0));
        
        //symbol.template.add(new Line2D.Double(12, 30, 18, 30)); //Temporary.
        
        symbol.rotateSymbol(15, 15, -270);
        
        // todo:tk:check these values.
        setSecondary("Current");
        setPrimary("Inductance");
        setPrimaryUnit("Henry");
        setPrimaryUnitPlural("Henries");
        setPrimarySymbol("T");
        setSiPrefix("m");
        setPrimaryUnitSymbol("H");
        setSecondaryUnitSymbol("A");
        setPreselectedPrimaryPrefix(getSiPrefix() + getPrimaryUnitSymbol());
    }
    
    protected void calculateGeometry()
    {
        super.calculateGeometry();
        
        if(getHeadTerminal() == null || getTailTerminal() == null)
        {
            return;
        }
        
        divisor = Math.sqrt(distanceBetweenTerminalsSquared) / 30.0;
        
        calculatedPoints[0] = new Point2D.Double(headTerminalX + rise / divisor, headTerminalY - run / divisor);
        
        calculatedPoints[1] = new Point2D.Double(tailTerminalX + rise / divisor, tailTerminalY - run / divisor);
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        calculateGeometry();

        symbol.drawComponent(getHeadTerminal().getX(), getHeadTerminal().getY(), getTailTerminal().getX(), getTailTerminal().getY(), sg);

        sg.drawLine(calculatedPoints[0].x, calculatedPoints[0].y, symbol.headLeadPoint.x + rise / divisor, symbol.headLeadPoint.y - run / divisor);
        sg.drawLine(calculatedPoints[1].x, calculatedPoints[1].y, symbol.tailLeadPoint.x + rise / divisor, symbol.tailLeadPoint.y - run / divisor);
        
        if(this.circuit.isCirSim && circuit.circuitPanel.isNotMovingAny())
        {
           TransformerElm transformerElm = (TransformerElm) getCircuitElm();


           // ============== Left side of the transformer.
           transformerElm.drawDots(sg, new Point(headTerminalX, headTerminalY), new Point((int) Math.round(symbol.headLeadPoint.x), (int) Math.round(symbol.headLeadPoint.y)), transformerElm.curcount[0]);
           transformerElm.drawDots(sg, new Point((int) Math.round(symbol.tailLeadPoint.x), (int) Math.round(symbol.tailLeadPoint.y)), new Point(tailTerminalX, tailTerminalY), transformerElm.curcount[0]); 
           
           Path2D path2D1 = (Path2D) this.symbol.positioned.get("LeadLine1");
           PathIterator pathIterator1 = path2D1.getPathIterator(null);
           double point1a[] = new double[2];
           pathIterator1.currentSegment(point1a);
           pathIterator1.next();
           double point2a[] = new double[2];
           pathIterator1.currentSegment(point2a);
           
           Path2D path2D2 = (Path2D) this.symbol.positioned.get("LeadLine2");
           PathIterator pathIterator2 = path2D2.getPathIterator(null);
           double point3a[] = new double[2];
           pathIterator2.currentSegment(point3a);
           pathIterator2.next();
           double point4a[] = new double[2];
           pathIterator2.currentSegment(point4a);

           transformerElm.drawDots(sg, 
                   new Point((int) Math.round(point1a[0]), (int) Math.round(point1a[1])),
                   new Point((int) Math.round(point2a[0]), (int) Math.round(point2a[1])),
                   transformerElm.curcount[0]);
           
           // Left coil.
           transformerElm.drawDots(sg, 
                   new Point((int) Math.round(point2a[0]), (int) Math.round(point2a[1])),
                   new Point((int) Math.round(point4a[0]), (int) Math.round(point4a[1])),
                   transformerElm.curcount[0]);
           
           transformerElm.drawDots(sg, 
                   new Point((int) Math.round(point4a[0]), (int) Math.round(point4a[1])),
                   new Point((int) Math.round(point3a[0]), (int) Math.round(point3a[1])),
                   transformerElm.curcount[0]);
           
           
           // ============== Right side of the transformer.
           transformerElm.drawDots(sg, new Point((int) Math.round(symbol.headLeadPoint.x + rise / divisor), (int) Math.round(symbol.headLeadPoint.y - run / divisor)), new Point((int) Math.round(calculatedPoints[0].x), (int) Math.round(calculatedPoints[0].y)), -transformerElm.curcount[1]);
           transformerElm.drawDots(sg,  new Point((int) Math.round(calculatedPoints[1].x), (int) Math.round(calculatedPoints[1].y)), new Point((int) Math.round(symbol.tailLeadPoint.x + rise / divisor), (int) Math.round(symbol.tailLeadPoint.y - run / divisor)), -transformerElm.curcount[1]);
           
           Path2D path2D3 = (Path2D) this.symbol.positioned.get("LeadLine3");
           PathIterator pathIterator3 = path2D3.getPathIterator(null);
           double point1b[] = new double[2];
           pathIterator3.currentSegment(point1b);
           pathIterator3.next();
           double point2b[] = new double[2];
           pathIterator3.currentSegment(point2b);
           
           Path2D path2D4 = (Path2D) this.symbol.positioned.get("LeadLine4");
           PathIterator pathIterator4 = path2D4.getPathIterator(null);
           double point3b[] = new double[2];
           pathIterator4.currentSegment(point3b);
           pathIterator4.next();
           double point4b[] = new double[2];
           pathIterator4.currentSegment(point4b);

           transformerElm.drawDots(sg, 
                   new Point((int) Math.round(point2b[0]), (int) Math.round(point2b[1])),
                   new Point((int) Math.round(point1b[0]), (int) Math.round(point1b[1])),
                   transformerElm.curcount[1]);
           
           // Right coil.
           transformerElm.drawDots(sg, 
                   new Point((int) Math.round(point1b[0]), (int) Math.round(point1b[1])),
                   new Point((int) Math.round(point3b[0]), (int) Math.round(point3b[1])),
                   transformerElm.curcount[1]);
           
           transformerElm.drawDots(sg, 
                   new Point((int) Math.round(point3b[0]), (int) Math.round(point3b[1])),
                   new Point((int) Math.round(point4b[0]), (int) Math.round(point4b[1])),
                   transformerElm.curcount[1]);
        }
    }
    
    public int getLabelDistance()
    {
        return 30;
    }
    
    public String toString()
    {
        return super.toString() +  " " + this.getPrimaryValue();
    }
}
