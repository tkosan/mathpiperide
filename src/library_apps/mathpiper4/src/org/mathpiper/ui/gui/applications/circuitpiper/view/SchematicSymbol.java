package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class SchematicSymbol
{
    public String type = "None";
    public Point2D.Double templateHeadTerminalPoint;
    public Point2D.Double templateHeadLeadPoint;
    public Point2D.Double headLeadPoint;
    public Point2D.Double templateTailTerminalPoint;
    public Point2D.Double templateTailLeadPoint;
    public Point2D.Double tailLeadPoint;
    public Map<String, Shape> template = new HashMap();
    public Map<String, Shape> positioned = new HashMap();

    double dotProduct(Point2D.Double v1, Point2D.Double v2)
    {
        return v1.x * v2.x + v1.y * v2.y;
    }

    double crossProduct(double a, double b, double c, double d, double e, double f)
    {
        return a * e - b * d;
    }

    Point2D.Double rotatePoint(Point2D.Double rotationPoint, Point2D.Double rotatedPoint, double angleRadian)
    {
        Point2D.Double relativeVector = new Point2D.Double(rotatedPoint.x - rotationPoint.x, rotatedPoint.y - rotationPoint.y);

        double relativeDistance = rotatedPoint.distance(rotationPoint);

        if (relativeDistance == 0.0)
        {
            return rotatedPoint;
        }

        double currentAngle = Math.acos(dotProduct(relativeVector, new Point2D.Double(1.0, 0.0)) / relativeDistance);

        if (crossProduct(relativeVector.x, relativeVector.y, 0.0, 1.0, 0.0, 0.0) < 0.0)
        {
            currentAngle = -1 * currentAngle;
        }

        double newAngle = angleRadian + currentAngle;

        return new Point2D.Double(rotationPoint.x + Math.cos(newAngle) * relativeDistance, rotationPoint.y + Math.sin(newAngle) * relativeDistance);

    }
    
    public void rotateSymbol(double x, double y, double angleDegrees)
    {
        Map<String, Shape> newTemplate = new HashMap();
        
        for (Map.Entry<String, Shape> entry : template.entrySet())
        {
            Shape shape = entry.getValue();
            
            AffineTransform transform = new AffineTransform();

            transform.rotate(Math.toRadians(angleDegrees), x, y);

            Shape shape2 = transform.createTransformedShape(shape);

            newTemplate.put(entry.getKey(), shape2);
        }
        
        template = newTemplate;
    }

    public void drawComponent(double x1, double y1, double x2, double y2, ScaledGraphics sg) throws Exception
    {
        Point2D.Double placedComponentHeadPoint = new Point2D.Double(x1, y1);
        Point2D.Double placedComponentTailPoint = new Point2D.Double(x2, y2);

        double placedTerminalDistance = placedComponentHeadPoint.distance(placedComponentTailPoint);

        double templateTerminalDistance = templateHeadTerminalPoint.distance(templateTailTerminalPoint);

        //Check(placedTerminalDistance  >=? templateTerminalDistance, "There is not enough room to place a " + componentType + " between " + placedTerminalPointOne + ", " + placedTerminalPointTwo + ".");
        if (placedTerminalDistance >= templateTerminalDistance)
        {
            //todo:tk.
            //throw new Exception("There is not enough room to place a " + this.type + " between " + placedTerminalPointOne + ", " + placedTerminalPointTwo + ".");
        }

        double scale = (placedTerminalDistance - templateTerminalDistance) / (2.0 * placedTerminalDistance);
        
        Point2D.Double extensionVectorScaled = new Point2D.Double(scale * (placedComponentTailPoint.x - placedComponentHeadPoint.x), scale * (placedComponentTailPoint.y - placedComponentHeadPoint.y));
        
        Point2D.Double placedRotationPoint = new Point2D.Double(placedComponentHeadPoint.x + extensionVectorScaled.x, placedComponentHeadPoint.y + extensionVectorScaled.y);

        //Create placer vector and template vector
        
        // From 0,0 in the schematic to 0,0 in template.
        Point2D.Double placerVector = new Point2D.Double(placedComponentTailPoint.x - placedComponentHeadPoint.x, placedComponentTailPoint.y - placedComponentHeadPoint.y);
        
        // From 0,0 in the template coordinate system to some point in the template.
        Point2D.Double templateVector = new Point2D.Double(templateTailTerminalPoint.x - templateHeadTerminalPoint.x, templateTailTerminalPoint.y - templateHeadTerminalPoint.y);

        //Use the dot product to determine angle between the template vector and the placed vector.
        double angleRotated = Math.acos(dotProduct(templateVector, placerVector) / (placedTerminalDistance * templateTerminalDistance));
        

        //Use the cross product and the right hand rule to determine which direction to rotate.
        if (crossProduct(templateVector.x, templateVector.y, 0.0, placerVector.x, placerVector.y, 0.0) < 0.0)
        {
            angleRotated = 2.0 * Math.PI - angleRotated;
        }
        
        positioned.clear();
        
        Point2D.Double rotatedPoint;

        //Transform from template xy-plane (shifted so that the first terminal is [0,0]) into the placed xy-plane.
        
        for (Map.Entry<String, Shape> entry : template.entrySet())
        {
            Shape shape = entry.getValue();
            
            AffineTransform transform = new AffineTransform();
            
            transform.rotate(angleRotated, placedRotationPoint.x, placedRotationPoint.y);

            transform.translate(placedRotationPoint.x, placedRotationPoint.y);
            
            transform.scale(1, -1);
            
            Shape shape2 = transform.createTransformedShape(shape);

            positioned.put(entry.getKey(), shape2);
        }

        //Add first extension line.
        headLeadPoint = new Point2D.Double(templateHeadLeadPoint.x, templateHeadLeadPoint.y);
        rotatedPoint = rotatePoint(templateHeadTerminalPoint, headLeadPoint, angleRotated);
        headLeadPoint.x = placedRotationPoint.x + rotatedPoint.x;
        headLeadPoint.y = placedRotationPoint.y + rotatedPoint.y;
        positioned.put("ExtentionLine1", new Line2D.Double(placedComponentHeadPoint, headLeadPoint));

        //Add second extension line.
        tailLeadPoint = new Point2D.Double(templateTailLeadPoint.x, templateTailLeadPoint.y);
        rotatedPoint = rotatePoint(templateHeadTerminalPoint, tailLeadPoint, angleRotated);
        tailLeadPoint.x = placedRotationPoint.x + rotatedPoint.x;
        tailLeadPoint.y = placedRotationPoint.y + rotatedPoint.y;
        positioned.put("ExtentionLine2", new Line2D.Double(placedComponentTailPoint, tailLeadPoint));

        //repaint();
        for (Shape shape : positioned.values())
        {
            sg.draw(shape);
        }
    }
    
    
// https://stackoverflow.com/questions/1089719/how-to-draw-a-curve-that-passes-through-three-points
    public static Point2D.Double getCircleCenter(Point2D.Double a, Point2D.Double b, Point2D.Double c)
    {
        double ax = a.getX();
        double ay = a.getY();
        double bx = b.getX();
        double by = b.getY();
        double cx = c.getX();
        double cy = c.getY();

        double A = bx - ax;
        double B = by - ay;
        double C = cx - ax;
        double D = cy - ay;

        double E = A * (ax + bx) + B * (ay + by);
        double F = C * (ax + cx) + D * (ay + cy);

        double G = 2 * (A * (cy - by) - B * (cx - bx));
        if (G == 0.0)
        {
            return null; // a, b, c must be collinear
        }
        double px = (D * E - B * F) / G;
        double py = (A * F - C * E) / G;
        return new Point2D.Double(px, py);
    }

    public static double makeAnglePositive(double angleDegrees)
    {
        double ret = angleDegrees;
        if (angleDegrees < 0)
        {
            ret = 360 + angleDegrees;
        }
        return ret;
    }

    public static double getNearestAnglePhase(double limitDegrees, double sourceDegrees, int dir)
    {
        double value = sourceDegrees;
        if (dir > 0)
        {
            while (value < limitDegrees)
            {
                value += 360.0;
            }
        }
        else if (dir < 0)
        {
            while (value > limitDegrees)
            {
                value -= 360.0;
            }
        }
        return value;
    }

    public static Arc2D makeArc(Point2D.Double s, Point2D.Double mid, Point2D.Double e)
    {
        Point2D.Double c = getCircleCenter(s, mid, e);
        double radius = c.distance(s);

        double startAngle = makeAnglePositive(Math.toDegrees(-Math
                .atan2(s.y - c.y, s.x - c.x)));
        double midAngle = makeAnglePositive(Math.toDegrees(-Math.atan2(mid.y - c.y, mid.x
                - c.x)));
        double endAngle = makeAnglePositive(Math.toDegrees(-Math.atan2(e.y - c.y, e.x - c.x)));

        // Now compute the phase-adjusted angles begining from startAngle, moving positive and negative.
        double midDecreasing = getNearestAnglePhase(startAngle, midAngle, -1);
        double midIncreasing = getNearestAnglePhase(startAngle, midAngle, 1);
        double endDecreasing = getNearestAnglePhase(midDecreasing, endAngle, -1);
        double endIncreasing = getNearestAnglePhase(midIncreasing, endAngle, 1);

        // Each path from start -> mid -> end is technically, but one will wrap around the entire
        // circle, which isn't what we want. Pick the one that with the smaller angular change.
        double extent = 0;
        if (Math.abs(endDecreasing - startAngle) < Math.abs(endIncreasing - startAngle))
        {
            extent = endDecreasing - startAngle;
        }
        else
        {
            extent = endIncreasing - startAngle;
        }

        return new Arc2D.Double(c.x - radius, c.y - radius, radius * 2, radius * 2, startAngle, extent,
                Arc2D.OPEN);
    }

    public static void main(String[] args) throws Exception
    {
        
        SchematicSymbol symbol = new SchematicSymbol();
        symbol.type = "Resistor";
        symbol.templateHeadTerminalPoint = new Point2D.Double(0, 0);
        symbol.templateHeadLeadPoint = new Point2D.Double(0, 0);
        symbol.templateTailTerminalPoint = new Point2D.Double(30, 0);
        symbol.templateTailLeadPoint = new Point2D.Double(30, 0);

        /*
        // Zigzag.
        symbol.template.add(new Line2D.Double(0, 0, 2.5, 5));
        symbol.template.add(new Line2D.Double(2.5, 5, 7.5, -5));
        symbol.template.add(new Line2D.Double(7.5, -5, 12.5, 5));
        symbol.template.add(new Line2D.Double(12.5, 5, 17.5, -5));
        symbol.template.add(new Line2D.Double(17.5, -5, 22.5, 5));
        symbol.template.add(new Line2D.Double(22.5, 5, 27.5, -5));
        symbol.template.add(new Line2D.Double(27.5, -5, 30, 0));
        
        // Plus sign.
        symbol.template.add(new Line2D.Double(0, -7, 4, -7));
        symbol.template.add(new Line2D.Double(2, -5, 2, -9));
        
symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(6,30), new Point2D.Double(9.75, 26.25), new Point2D.Double(6,22.5)));
symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(6,22.5), new Point2D.Double(9.75, 18.75), new Point2D.Double(6,15)));
symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(6,15), new Point2D.Double(9.75, 11.25), new Point2D.Double(6,7.5)));
symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(6,7.5), new Point2D.Double(9.75, 3.75), new Point2D.Double(6,0)));

symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(24,30), new Point2D.Double(20.25, 26.25), new Point2D.Double(24,22.5)));
symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(24,22.5), new Point2D.Double(20.25, 18.75), new Point2D.Double(24,15)));
symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(24,15), new Point2D.Double(20.25, 11.25), new Point2D.Double(24,7.5)));
symbol.template.add(SchematicSymbol.makeArc(new Point2D.Double(24,7.5), new Point2D.Double(20.25, 3.75), new Point2D.Double(24,0)));

symbol.template.add(new Line2D.Double(0, 30, 6, 30));
symbol.template.add(new Line2D.Double(0, 0, 6, 0));
symbol.template.add(new Line2D.Double(24, 30, 30, 30));
symbol.template.add(new Line2D.Double(24, 0, 30, 0));

symbol.template.add(new Line2D.Double(12, 30, 12, 0));
symbol.template.add(new Line2D.Double(18, 30, 18, 0));
        */


        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        frame.setSize(width / 2, height / 2);

        frame.add(new JComponent()
        {
            public void paintComponent(Graphics g)
            {
                try
                {
                    //symbol.drawComponent(184.0, 34.0, 184.0, 124.0, new ScaledGraphics(g));
                    
                    symbol.drawComponent(184.0, 34.0, 274.0, 34.0, new ScaledGraphics(g));
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        
        frame.setVisible(true);
    }
}