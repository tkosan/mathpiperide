package org.mathpiper.ui.gui.applications.circuitpiper.model;

import org.mathpiper.ui.gui.applications.circuitpiper.view.instruments.InstrumentPanel;


public class DevicePart
{
    public Circuit circuit;
    
    public Circuit getCircuit() {
        return circuit;
    }

    public void setCircuit(Circuit circuit) {
        this.circuit = circuit;
    }

    public void addInstrumentPanel(InstrumentPanel instrumentPanel)
    {
        circuit.circuitPanel.instrumentsPanel.add(instrumentPanel);
        circuit.circuitPanel.instrumentsPanel.invalidate();
        circuit.circuitPanel.instrumentsPanel.revalidate();
        circuit.circuitPanel.instrumentsPanel.repaint();
    }

    public void removeInstrumentPanel(InstrumentPanel instrumentPanel)
    {
        circuit.circuitPanel.instrumentsPanel.remove(instrumentPanel);
        circuit.circuitPanel.instrumentsPanel.invalidate();
        circuit.circuitPanel.instrumentsPanel.revalidate();
        circuit.circuitPanel.instrumentsPanel.repaint();
    }

    public void removeInstrumentPanels()
    {
        if (circuit != null && circuit.circuitPanel != null && circuit.circuitPanel.instrumentsPanel != null)
        {
            circuit.circuitPanel.instrumentsPanel.removeAll();
            circuit.circuitPanel.instrumentsPanel.invalidate();
            circuit.circuitPanel.instrumentsPanel.revalidate();
            circuit.circuitPanel.instrumentsPanel.repaint();
        }
    }
    
}
