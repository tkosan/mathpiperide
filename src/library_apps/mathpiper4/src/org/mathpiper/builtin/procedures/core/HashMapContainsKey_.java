package org.mathpiper.builtin.procedures.core;

import java.util.HashMap;
import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;

/**
 *
 *  
 */
public class HashMapContainsKey_ extends BuiltinProcedure
{
    
    private HashMapContainsKey_()
    {
    }

    public HashMapContainsKey_(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons evaluated = getArgument(aEnvironment, aStackTop, 1);

        BuiltinContainer gen = (BuiltinContainer) evaluated.car();
        if( gen == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        Object o = gen.getObject();
        
        if(! (o instanceof HashMap)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        
        HashMap<Object,Cons> hashMap = (HashMap) o;

        Cons key = getArgument(aEnvironment, aStackTop, 2);

        if( key == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        //if(! (key.car() instanceof String)) LispError.checkArgument(aEnvironment, aStackTop, 2);

        Object key2 = key.car();
        
        boolean result = hashMap.containsKey(key2);
        
        if(result == true)
        {
            setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
        }
        else
        {
            setTopOfStack(aEnvironment, aStackTop, Utility.getFalseAtom(aEnvironment));
        }
    }
}



/*
%mathpiper_docs,name="HashMapContainsKey?",categories="Programming Procedures,Built In"
*CMD HashMapContainsKey --- checks if a key is in the given hash map

*CALL
	HashMapContainsKey?(hashMap, key)

*PARMS

{hashMap} -- a hash map
{key} -- an atom

*DESC
This procedure checks if a key is in the given hash map.

*SEE HashMapCreate

%/mathpiper_docs
*/
