package org.mathpiper.io.coursemodules;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.mathpiper.io.worksheets.MPWSFile2;
import org.mathpiper.lisp.Utility;

public class Course {

    private File moduleDir;
    
    public Course()
    { 
    }
    
    public Course(String sourcePath, String destinationPath) throws Throwable
    {
        publishCourse(sourcePath, destinationPath);
    }

    public void publishModule(String sourcePath, String destinationPath) throws Throwable {

        moduleDir = new java.io.File(sourcePath);

        if (moduleDir.exists()) {
            
            java.io.File[] figuresDirectory = moduleDir.listFiles(new java.io.FilenameFilter() {

                public boolean accept(java.io.File file, String name) {

                    if (name.equals("figures")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            
            if(figuresDirectory.length != 0)
            {
                final Path sourceDir = Paths.get(figuresDirectory[0].getAbsolutePath());

                Path modulePath = sourceDir.getParent();
                
                String zipFileName = destinationPath + File.separator + "figures_" + modulePath.getFileName() + ".zip";
                
                try {
                    final ZipOutputStream outputStream = new ZipOutputStream(new FileOutputStream(zipFileName));
                    Files.walkFileTree(sourceDir, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) {
                            try {
                                Path targetFile = sourceDir.relativize(file);
                                outputStream.putNextEntry(new ZipEntry(targetFile.toString()));
                                byte[] bytes = Files.readAllBytes(file);
                                outputStream.write(bytes, 0, bytes.length);
                                outputStream.closeEntry();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return FileVisitResult.CONTINUE;
                        }
                    });
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            java.io.File[] moduleDirectoryContentsArray = moduleDir.listFiles(new java.io.FilenameFilter() {

                public boolean accept(java.io.File file, String name) {
                    if (name.endsWith(".mpws")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            if(moduleDirectoryContentsArray.length == 0)
            {
                throw new Exception("There are no .mpws files in " + sourcePath);
            }
            
            Arrays.sort(moduleDirectoryContentsArray);

            for (int index = 0; index < moduleDirectoryContentsArray.length; index++) {

                File worksheetSourceFile = moduleDirectoryContentsArray[index];
                
                String worksheetSourceFilename = worksheetSourceFile.getName();
                
                String worksheetDestinationFilename = worksheetSourceFilename;
                
                MPWSFile2 mpwsFile2 = new MPWSFile2(new FileInputStream(worksheetSourceFile));
                
                String worksheetSourcePath = worksheetSourceFile.getPath();
            
                mpwsFile2.scanSourceFile(worksheetSourcePath);
                
                String versionLabel = mpwsFile2.getVersionLabel();
                
                if(versionLabel == null)
                {
                    throw new Exception("The worksheet \"" + worksheetSourcePath + "\" does not have a version label.");
                }
                
                if(worksheetSourceFilename.startsWith("problems") || worksheetSourceFilename.startsWith("practice"))
                {
                    worksheetDestinationFilename = worksheetSourceFilename.substring(0, worksheetSourceFilename.lastIndexOf(".")) + "_firstname_lastname_idnumber_" + versionLabel + ".lg.mpws";
                
                    if(worksheetSourceFilename.startsWith("problems"))
                    {
                        mpwsFile2.processFoldsPublish();
                    }
                    
                    mpwsFile2.publishWorksheet(destinationPath + File.separator + worksheetDestinationFilename, true);
                }
                else if(worksheetSourceFilename.startsWith("lecture"))
                {
                    worksheetDestinationFilename = worksheetSourceFilename.substring(0, worksheetSourceFilename.lastIndexOf(".")) + "_" + versionLabel + ".mpws";
                    mpwsFile2.publishWorksheet(destinationPath + File.separator + worksheetDestinationFilename, false);
                }
            }

        }

    }
    
    

    public void publishCourse(String sourcePath, String destinationPath) throws Throwable {

        moduleDir = new java.io.File(sourcePath);

        if (moduleDir.exists()) {

            java.io.File[] courseDirectoryContentsArray = moduleDir.listFiles(new java.io.FileFilter() {

                public boolean accept(java.io.File file) {
                    if (file.isDirectory()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            
            Arrays.sort(courseDirectoryContentsArray, new Comparator<File>() {
                public int compare(File s1, File s2) {
                    return Utility.compareNatural(s1.getName(), s2.getName());
                }
            });

            for (int index = 0; index < courseDirectoryContentsArray.length; index++) {

                File moduleDirectory = courseDirectoryContentsArray[index];
                
                File destination = new File(destinationPath + File.separator + moduleDirectory.getName());
                
                if(destination.exists())
                {
                    Path rootPath = Paths.get(destination.getAbsolutePath());

                    try (Stream<Path> walk = Files.walk(rootPath)) {
                        walk.sorted(Comparator.reverseOrder())
                            .map(Path::toFile)
                            .peek(System.out::println)
                            .forEach(File::delete);
                    }
                }
                
                destination.mkdir();
                
                System.out.println(moduleDirectory);
                
                publishModule(moduleDirectory.getAbsolutePath(),
                destination.getAbsolutePath());
                
            }
            
        }
        
    }

 

    public static void main(String[] args) {
        Course course = new Course();

	try {

	    String sourceDirectory = null;

	    String outputDirectory = null;
            
	    if (args.length == 2) {
		sourceDirectory = args[0];
		outputDirectory = args[1];

	    } else {
		throw new Exception("Two arguments must be submitted to the main method.");
	    }

	    course.publishCourse(sourceDirectory, outputDirectory);

	} catch (Throwable e) {
	    e.printStackTrace();
	}
    }
}
