/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import java.util.HashMap;
import java.util.Map;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.exceptions.EvaluationException;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *  
 */
public class ExceptionThrow extends BuiltinProcedure
{

    private Map defaultOptions;
    
    private ExceptionThrow()
    {
    }

    public ExceptionThrow(String functionName)
    {
        this.functionName = functionName;
        
	defaultOptions = new HashMap();
	defaultOptions.put("MetaData", null);
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons typeCons;
        typeCons = getArgument(aEnvironment, aStackTop, 1);
        LispError.checkIsString(aEnvironment, aStackTop, typeCons, 1);
        String typeString = Utility.toNormalString(aEnvironment, aStackTop, (String) typeCons.car());
        
        Cons message = ((Cons) getArgument(aEnvironment, aStackTop, 2).car()).cdr();;
        LispError.checkIsString(aEnvironment, aStackTop, message, 2);
        String errorMessage = " Error: " + Utility.toNormalString(aEnvironment, aStackTop, (String) message.car());
        
        Cons options = ((Cons) getArgument(aEnvironment, aStackTop, 2).car()).cdr().cdr();

        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);

        if(userOptions.get("MetaData") == null)
        {
            LispError.throwError(aEnvironment, aStackTop, typeString, errorMessage);
        }
        else
        {
            int lineNumber = -1;
            int startIndex = -1;
            int endIndex = -1;
            String sourceName = "Null";
            
            Object object = userOptions.get("MetaData");
            
            if(object instanceof Cons)
            {
                Cons cons = (Cons) object;
                
                if(cons instanceof SublistCons)
                {
                     cons = (Cons) cons.car();
                }
                
                Map map = cons.getMetadataMap();

                if (map != null) {
                    if (map.get("\"lineNumber\"") != null) {

                        lineNumber = (int) map.get("\"lineNumber\"");
                    }

                    if (map.get("\"startIndex\"") != null) {

                        startIndex = (int) map.get("\"startIndex\"");
                    }

                    if (map.get("\"endIndex\"") != null) {

                        endIndex = (int) map.get("\"endIndex\"");
                    }

                    if (map.get("\"sourceName\"") != null) {

                        sourceName = (String) map.get("\"sourceName\"");
                    }
                }
            }
            
            LispError.throwError(aEnvironment, aStackTop, typeString, errorMessage, lineNumber, startIndex, endIndex);
        }
    }
}



/*
%mathpiper_docs,name="ExceptionThrow",categories="Programming Procedures,Error Reporting,Built In"
*CMD ExceptionThrow --- throw an exception of a given type
*CORE
*CALL
	ExceptionThrow("type", "exceptionMessage")

*PARMS

{"type"} -- a string that contains the type of the expression

{"exceptionMessage"} -- string which holds the exception message

*DESC
The current operation is stopped and an exception will be thrown.

Exceptions that are thrown by this function can be caught by the {ExceptionCatch} function.



*E.G.

In> ExceptionThrowThrow("EXAMPLE", "This is an example exception.")
Result: Exception
Exception: The argument must be an integer.


*SEE ExceptionCatch, ExceptionGet, Check

%/mathpiper_docs
*/