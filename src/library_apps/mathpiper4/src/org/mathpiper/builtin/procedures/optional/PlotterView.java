/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.mathpiper.builtin.BigNumber;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.worksheets.ViewPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.mathpiper.ui.gui.worksheets.symbolboxes.SymbolBox;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import org.mathpiper.ui.gui.worksheets.GraphPanelController;
import org.mathpiper.ui.gui.worksheets.SignalPhasePanelController;


public class PlotterView extends BuiltinProcedure implements ResponseListener {

    private Map defaultOptions;
    private List<ResponseListener> responseListeners = new ArrayList<ResponseListener>();
    protected double viewScale = 1;
    
    public void plugIn(Environment aEnvironment)  throws Throwable
    {
	this.functionName = "PlotterView";
	
        aEnvironment.getBuiltinFunctions().put("PlotterView", new BuiltinProcedureEvaluator(this, 0, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Scale", new BigNumber("1.0", 10, 10));
        defaultOptions.put("Resizable", true);
        defaultOptions.put("ReturnList", false);
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
	
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if (!Utility.isSublist(arguments))
        {
            LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
        }

        arguments = (Cons) arguments.car(); //Go to sublist.

        Map userOptions = null;
        
        if(arguments != null)
        {
            Cons options = arguments.cdr(); //Strip List tag.
            userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        }
        else
        {
            userOptions = defaultOptions;
        }
        
        Double viewScale = ((BigNumber) userOptions.get("Scale")).toDouble();
        
        JPanel mathControllerPanel = new JPanel();
        mathControllerPanel.setLayout(new BorderLayout());
        org.mathpiper.ui.gui.worksheets.PlotPanel plotPanel = new org.mathpiper.ui.gui.worksheets.PlotPanel(aEnvironment, -1, viewScale, userOptions);
        plotPanel.addResponseListener(this);
        
        GraphPanelController mathPanelScaler = null;
        if (userOptions.containsKey("ShowSignalPhase?") && ((boolean) userOptions.get("ShowSignalPhase?")) == true)
        {
            mathPanelScaler = new SignalPhasePanelController(plotPanel, viewScale);
        }
        else
        {
            mathPanelScaler = new GraphPanelController(plotPanel, viewScale);
        }
        
        JScrollPane scrollPane = new JScrollPane(plotPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        mathControllerPanel.add(scrollPane);

        boolean includeSlider = (Boolean) userOptions.get("Resizable");
        if(includeSlider)
        {
            mathControllerPanel.add(mathPanelScaler, BorderLayout.NORTH);
        }

        //Box box = Box.createVerticalBox();

	//box.add(mathControllerPanel);
        
        JavaObject response = null;
                
        boolean returnList = (Boolean) userOptions.get("ReturnList");
        
        if(returnList)
        {
            List responseList = new ArrayList();
            responseList.add(mathControllerPanel);
            responseList.add(plotPanel);
            response = new JavaObject(responseList);
        }
        else
        {
           response = new JavaObject(plotPanel); 
        }

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));
    }
    
    

    public void response(EvaluationResponse response) {
        notifyListeners(response);
    }
    
    public boolean remove()
    {
        return false;
    }
    
    public void addResponseListener(ResponseListener listener) {
        responseListeners.add(listener);
    }

    public void removeResponseListener(ResponseListener listener) {
        responseListeners.remove(listener);
    }

    protected void notifyListeners(EvaluationResponse response) {
        for (ResponseListener listener : responseListeners) {
            listener.response(response);
        }
    }
}


class PlotPanel extends JPanel implements ViewPanel {

    protected SymbolBox symbolBox;
    protected double viewScale = 1;
    private Queue<SymbolBox> queue = new LinkedList();
    private int[] lastOnRasterArray = new int[10000];
    private int maxTreeY = 0;
    private boolean paintedOnce = false;

    public PlotPanel(double viewScale) {
        this.viewScale = viewScale;
        this.setBackground(Color.white);
    }

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));

        g2d.setStroke(new BasicStroke((float) (2), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2d.setColor(Color.black);
        g2d.setBackground(Color.white);
        ScaledGraphics sg = new ScaledGraphics(g2d);
        sg.setLineWidth(0);
        sg.setViewScale(viewScale);

        sg.drawOval(50, 50, 5, 5);
    }

    

    public Dimension getPreferredSize() {
        
        return(new Dimension(200, 200));

    }//end method.


    public void setViewScale(double viewScale) {
        this.viewScale = viewScale;
        this.revalidate();
        this.repaint();
    }
}



/*
%mathpiper_docs,name="PlotterView",categories="Programming Procedures,Visualization"
*CMD PlotterView --- display the MathPiper plotter

*CALL
    PlotterView(option, option, option...)

*PARMS

{Options:}

{Scale} -- a value that sets the initial size of the tree

{Resizable} -- if set to True, a resizing slider is displayed

*DESC
Display a the MathPiper plotter.

 
*E.G.
In> Show(PlotterView())
Result: class javax.swing.JFrame

%/mathpiper_docs
*/
