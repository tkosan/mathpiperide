package org.mathpiper.ui.gui.applications.circuitpiper.view.instruments;

import javax.swing.JPanel;
import org.mathpiper.ui.gui.VerticalFlow;

public class InstrumentsPanel extends JPanel
{
    public InstrumentsPanel()
    {
       this.setLayout(new VerticalFlow());
    }
}
