package org.mathpiper.builtin.procedures.optional;

import java.util.ArrayList;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;

public class Printf extends BuiltinProcedure {

    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "Printf";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {

        Cons cons = (Cons) getArgument(aEnvironment, aStackTop, 1).car();
        cons = cons.cdr();
        
        Object formatString = cons.car();
        
        if(! (formatString instanceof String)) LispError.raiseError("The first argument must be a string.", aStackTop, aEnvironment);
        
        ArrayList javaList = new ArrayList();
        
        cons = cons.cdr();
        
        while (cons != null) {
            
            if(cons instanceof NumberCons)
            {
                String item = (String) cons.car();
                item = Utility.stripEndQuotesIfPresent(item);
                
                Object number;
                
                if(item.contains("."))
                {
                    number = Double.parseDouble(item);
                }
                else
                {
                    number = Integer.parseInt(item);
                }
                
                javaList.add(number);
                
            }
            else if(cons instanceof AtomCons)
            {
                String item = (String) cons.car();
                item = Utility.stripEndQuotesIfPresent(item);
                javaList.add(item);
            }
            else
            {
                LispError.raiseError("The argument " + cons.toString() + " is not supported yet.", aStackTop, aEnvironment);
            }

            cons = cons.cdr();
        }
        
        String formattedString = String.format((String)formatString, javaList.toArray());
        
        formattedString = Utility.stripEndQuotesIfPresent(formattedString);

        aEnvironment.iCurrentOutput.write(formattedString);
        
        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}
/*
%mathpiper_docs,name="Printf",categories="Programming Procedures,Input/Output,Built In"
*CMD Printf --- print a formatted string

*CALL
Printf(formatString, expr, ...)

*PARMS

{formatString} -- a string that contains format specifiers and literals
{expr} -- expressions that are associated with the format specifiers

*DESC
Print a formatted string. A description of formatted strings
is in the documentation for the {StringFormat} procedure, which is
linked to below.

*EXAMPLES

*SEE StringFormat
%/mathpiper_docs




%mathpiper,name="Printf",subtype="in_prompts"

In> Printf("%d + %.3f == %f", 1, 2.2, 3.2) >>
Result: True >>
Side Effects: >>
"1 + 2.200 == 3.200000" >>

%/mathpiper





%mathpiper,name="Printf",subtype="automatic_test"

Verify(PipeToString()Printf("%d + %.3f == %f", 1, 2.2, 3.2), "1 + 2.200 == 3.200000");

%/mathpiper
 */
