
package org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors;

import java.awt.geom.Point2D;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.NTerminalComponent;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;


public abstract class Transistor extends NTerminalComponent {
    public static int componentCounter = 1;
    protected double radius = 14;
    protected double radius2;
    protected double shaftScale;
    protected double shaftScale2;
    protected double setback3;
    protected int circleOffset;
    
    public Transistor(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        calculatedTerminals = new Terminal[1];
        calculatedPoints = new Point2D.Double[1];
        postsConstraint = 2;
        
        if(uid == null)
        {
            componentSubscript = componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        
        init();
    }
    
    public void init()
    {
        setPrimarySymbol("Q");
    }
    
}
