package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive;

import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Stack;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.mathpiper.ui.gui.applications.circuitpiper.view.SchematicSymbol;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class Resistor extends Component {

    public static final String ohm = "\u03a9";
    public static int componentCounter = 1;

    public Resistor(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        if(uid == null)
        {
            componentSubscript = componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        init();
        primaryValue = 5.67;
        enteredPrimaryValue = "" + 1/siToValue.get(siPrefix) * primaryValue;
    }
    
    public Resistor(int x, int y, String uid, Stack<String> attributes, Circuit circuit) throws Exception {
        super(x, y, circuit);
        if(uid == null)
        {
            componentSubscript = componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        init();

        
        handleAttribute(attributes);
    }

    public void init() {
        symbol = new SchematicSymbol();
        symbol.type = "Resistor";
        symbol.templateHeadTerminalPoint = new Point2D.Double(0, 0);
        symbol.templateHeadLeadPoint = new Point2D.Double(0, 0);
        symbol.templateTailTerminalPoint = new Point2D.Double(30, 0);
        symbol.templateTailLeadPoint = new Point2D.Double(30, 0);

        // Zigzag.
        symbol.template.put("1", new Line2D.Double(0, 0, 2.5, 5));
        symbol.template.put("32", new Line2D.Double(2.5, 5, 7.5, -5));
        symbol.template.put("3", new Line2D.Double(7.5, -5, 12.5, 5));
        symbol.template.put("4", new Line2D.Double(12.5, 5, 17.5, -5));
        symbol.template.put("5", new Line2D.Double(17.5, -5, 22.5, 5));
        symbol.template.put("6", new Line2D.Double(22.5, 5, 27.5, -5));
        symbol.template.put("7", new Line2D.Double(27.5, -5, 30, 0));
        
        /*
        // Plus sign.
        symbol.template.put("8", new Line2D.Double(0, -7, 4, -7));
        symbol.template.put("9", new Line2D.Double(2, -5, 2, -9));
        */
        
        setPrimary("Resistance");
        setPrimaryUnit("Ohm");
        setPrimaryUnitPlural("Ohms");
        setPrimarySymbol("R");
        setSiPrefix("");
        setPrimaryUnitSymbol(ohm);
        setPreselectedPrimaryPrefix(getSiPrefix() + getPrimaryUnitSymbol());
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);

        symbol.drawComponent(getHeadTerminal().getX(), getHeadTerminal().getY(), getTailTerminal().getX(), getTailTerminal().getY(), sg);

        if(this.circuit.isCirSim && circuit.circuitPanel.isNotMovingAny())
        {
            this.getCircuitElm().doDots(sg);
        }
    }
    
    public int getLabelDistance()
    {
        return 15;
    }
    
    public String getTurtle()
    {
        StringBuilder sb = new StringBuilder();
                
        sb.append(super.getTurtle());
        sb.append(Circuit.turtleIndent + "cp:value " + getPrimaryValue() + " .");
        
        return sb.toString();
    }
    
    public String toString()
    {
        return super.toString() + " " + this.getPrimaryValue();
    }
}
