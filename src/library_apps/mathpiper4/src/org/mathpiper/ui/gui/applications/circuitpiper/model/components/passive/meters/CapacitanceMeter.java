package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class CapacitanceMeter extends Meter {
    
    private static String capacitanceStringInitial = "todo F"; //"??? F";
    protected String capacitanceString = capacitanceStringInitial;

    public CapacitanceMeter(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        if(uid != null)
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        primary = "capacitance";
    }
    
    public void reset() {
        super.reset();
        setCapacitanceString(CapacitanceMeter.capacitanceStringInitial);
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        String valueAndUnit = Component.addUnitOperator(getCapacitanceString(), isValueVisible(), isUnitVisible());
        
        if(isValueVisible())
        {
            sg.drawString(valueAndUnit.split("`")[0], Math.round((x1 + x2) * 0.5 + 3) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 3));
        }
        
        if(isUnitVisible())
        {
            sg.drawString(valueAndUnit.split("`")[1], Math.round((x1 + x2) * 0.5 + 16) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 16));
        }
    }

    public String getCapacitanceString() {
        return capacitanceString;
    }

    public void setCapacitanceString(String capacitanceString) {
        this.capacitanceString = capacitanceString;
    }
}
