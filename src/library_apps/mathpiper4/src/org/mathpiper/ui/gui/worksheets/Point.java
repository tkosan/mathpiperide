package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import java.awt.RenderingHints;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

public class Point extends GraphicObject implements Scalable
{
    private double arrowWidth;
    private double arrowHeight;
    private double arrowLength;
    private double smallCircleDiameter;
    private double xCoordinate;
    private double yCoordinate;
    private double diameter = 6.0;
    private double highlightDiameter = diameter * 1.6;

    public Point(String label, double x, double y)
    {
        this.label = label;
        this.xCoordinate = x;
        this.yCoordinate = y;
        
        resetScale();
    }
    
    public void scale(double scale)
    {
        arrowWidth *= scale;
        arrowHeight *= scale;
        arrowLength *= scale;
        
        smallCircleDiameter *= scale;
    }
    
    public void resetScale()
    {
        arrowWidth = 7;
        arrowHeight = 15;
        arrowLength = 15;
        
        smallCircleDiameter = 6;
    }

    public void paint(ScaledGraphics sg, PlotPanel plotPanel)
    {
        RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); 
        qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY); 
        sg.setRenderingHints(qualityHints); 

        if (isVisible)
        {
            double xPixels = plotPanel.toPixelsX(xCoordinate);
            double yPixels = plotPanel.toPixelsY(yCoordinate);

            sg.setColor(color);

            if (style == null)
            {
                if (isSelected)
                {
                    sg.setColor(highlightColor);
                    sg.fillOval(xPixels - highlightDiameter / 2.0, yPixels - highlightDiameter / 2.0, highlightDiameter, highlightDiameter);
                }

                sg.fillOval(xPixels - diameter / 2.0, yPixels - diameter / 2.0, diameter, diameter);
            }
            else if (style.equals("TriangleNorth"))
            {
                sg.setColor(Color.BLACK);

                drawArrowLine(sg, xPixels, yPixels + arrowLength, xPixels, yPixels, arrowWidth, arrowHeight);
            }
            else if (style.equals("SmallCircle"))
            {
                sg.setColor(Color.BLACK);
                sg.fillOval(xPixels - smallCircleDiameter / 2.0, yPixels - smallCircleDiameter / 2.0, smallCircleDiameter, smallCircleDiameter);
            }

            if (isShowLabel)
            {
                sg.drawScaledString(label, xPixels + 3, yPixels - 5, 1.5);
            }
        }
    }

    /**
     * Draw an arrow line between two points.
     *
     * @param g the graphics component.
     * @param x1 x-position of first point.
     * @param y1 y-position of first point.
     * @param x2 x-position of second point.
     * @param y2 y-position of second point.
     * @param h the width of the arrow.
     * @param w the height of the arrow.
     */
    
    // Obtained from https://stackoverflow.com/questions/2027613/how-to-draw-a-directed-arrow-line-in-java
    // todo:tk:duplicated in org/mathpiper/ui/gui/applications/circuitpiper/model/components/Component.java
    
    private void drawArrowLine(ScaledGraphics g, double x1, double y1, double x2, double y2, double w, double h)
    {
        double dx = x2 - x1, dy = y2 - y1;
        double D = Math.sqrt(dx * dx + dy * dy);
        double xm = D - h, xn = xm, ym = w, yn = -w, x;
        double sin = dy / D, cos = dx / D;

        x = xm * cos - ym * sin + x1;
        ym = xm * sin + ym * cos + y1;
        xm = x;

        x = xn * cos - yn * sin + x1;
        yn = xn * sin + yn * cos + y1;
        xn = x;

        double[] xpoints =
        {
            x2, xm, xn
        };
        
        double[] ypoints =
        {
            y2, ym, yn
        };
        
        g.drawLine(x1, y1, x2, y2);
        g.fillPolygon(xpoints, ypoints, 3);
    }

    public double getXCoordinate()
    {
        return xCoordinate;
    }

    public double getYCoordinate()
    {
        return yCoordinate;
    }

    public void select(boolean isSelected)
    {
        this.isSelected = isSelected;
    }

    public void toggleSelected()
    {
        if (isSelected)
        {
            isSelected = false;
        }
        else
        {
            isSelected = true;
        }
    }

    public void setXCoordinate(double xCoordinate)
    {
        this.xCoordinate = xCoordinate;
    }

    public void setYCoordinate(double yCoordinate)
    {
        this.yCoordinate = yCoordinate;
    }

    public void move(double xDifference, double yDifference)
    {
        xCoordinate += xDifference;
        yCoordinate += yDifference;
    }

    public void snapToInteger()
    {
        xCoordinate = Math.round(xCoordinate);
        yCoordinate = Math.round(yCoordinate);
    }

    public double getDiameter()
    {
        return diameter;
    }

    public void setDiameter(double diameter)
    {
        this.diameter = diameter;
    }

    public boolean isSelected()
    {
        return this.isSelected;
    }
}
