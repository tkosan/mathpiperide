package org.mathpiper.builtin.procedures.optional;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;


public class HTTPPostJSON extends BuiltinProcedure{

    public void plugIn(Environment aEnvironment) throws Throwable
    {
        this.functionName = "HTTPPostJSON";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        // Server URL.
        if (getArgument(aEnvironment, aStackTop, 1) == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 1);
        }

        Object argumen1 = getArgument(aEnvironment, aStackTop, 1).car();

        if (! (argumen1 instanceof String)) {
            LispError.raiseError("The argument to TellUser must be a string.", aStackTop, aEnvironment);
        }

        String serverURL = (String) argumen1;

        if (serverURL == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 1);
        }

        serverURL = Utility.stripEndQuotesIfPresent(serverURL);

        
        // JSON.
        if (getArgument(aEnvironment, aStackTop, 2) == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 2);
        }

        Object argument2 = getArgument(aEnvironment, aStackTop, 2).car();

        if (! (argument2 instanceof String)) {
            LispError.raiseError("The argument to TellUser must be a string.", aStackTop, aEnvironment);
        }

        String jsonInputString = (String) argument2;

        if (jsonInputString == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 2);
        }

        jsonInputString = Utility.stripEndQuotesIfPresent(jsonInputString);
        
        
        URL url = new URL(serverURL);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod( "POST");
        con.setRequestProperty( "Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput( true);

        
        try ( OutputStream os = con.getOutputStream())
        {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        
        try ( BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8")))
        {
            StringBuilder response = new StringBuilder();
            
            String responseLine = null;
            
            while ((responseLine = br.readLine()) != null)
            {
                response.append(responseLine.trim());
            }
            
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine scriptEngine = manager.getEngineByName("JavaScript");
            scriptEngine.put("jsonString", response.toString());
            scriptEngine.eval("result = JSON.stringify(JSON.parse(jsonString), null, 2)");
            String prettyPrintedJson = (String) scriptEngine.get("result");

            setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "\"" + prettyPrintedJson + "\""));
        }
    }
}