package org.mathpiper.ui.gui.worksheets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import javax.swing.JComponent;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

import com.foundationdb.sql.parser.SQLParser;
import com.foundationdb.sql.parser.StatementNode;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.Interpreters;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.interpreters.ResponseProvider;
import org.mathpiper.io.StringInputStream;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.parsers.MathPiperParser;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;

public class SoughtGivenPanel extends JComponent implements ViewPanel, MouseListener, ResponseProvider {
    static Interpreter interpreter;
    static
    {
        interpreter = Interpreters.newAsynchronousInterpreter();
    }

    protected Cons expression;
    protected double viewScale = 1;
    private Queue<SymbolNode> queue = new LinkedList();
    private int[] lastOnRasterArray = new int[10000];
    private int maxTreeY = 0;
    private SymbolNode mainRootNode = null;

    private int leftMostPosition = Integer.MAX_VALUE; // Initialize to maximum possible position.
    private int rightMostPosition = 0; // Initialize to minimum possible
    // position.
    private double defaultLineThickness = .6;
    public static int fontSize = 11;

    private static final Map<String, Color> colorMap = new HashMap();
    static
    {
        colorMap.put("\"RED\"", Color.RED);
        colorMap.put("\"BLACK\"", Color.BLACK);
        colorMap.put("\"BLUE\"", Color.BLUE);
        colorMap.put("\"CYAN\"", Color.CYAN);
        colorMap.put("\"DARKGRAY\"", Color.DARK_GRAY);
        colorMap.put("\"GRAY\"", Color.GRAY);
        colorMap.put("\"GREEN\"", Color.GREEN);
        colorMap.put("\"MAGENTA\"", Color.MAGENTA);
        colorMap.put("\"ORANGE\"", Color.ORANGE);
        colorMap.put("\"RED\"", Color.RED);
        colorMap.put("\"WHITE\"", Color.WHITE);
        colorMap.put("\"YELLOW\"", Color.YELLOW);
    }

    private int yPositionAdjust = 39; /*
     * Adjust y position of whole tree,
     * smaller numbers moves the tree down.
     */

    private int adjust = 1;

    private Map<String, Object> treeOptionsMap = new HashMap();

    private List<ResponseListener> responseListeners;

    private Environment environment;

    private String positionString = null;

    private String operatorString = null;

    private boolean isShowPositions = false;
    
    private boolean isShowDepths = false;

    private boolean isMouseClickedHandlerStillProcessing = false;

    private boolean isSelectSingleNode = false;

    private SymbolNode oldSelectedNode = null;

    private int sequenceNumber = 0;
    
    private boolean isArcsHighlight = false;
    
    private boolean isNodesHighlight = false;
    
    private boolean isSubtreeSelected = false;
    
	// Show(TreeView(a/b == 3))
    // 99 # UnparseLatex(_x / _y, _p)_( --> UnparseLatexBracketIf(p <?
    // PrecedenceGet("/"), ConcatStrings("\\frac{", UnparseLatex(x,
    // UnparseLatexMaxPrec()), "}{", UnparseLatex(y, UnparseLatexMaxPrec()),
    // "} ") );
    // Show(TreeView( '(a*(b+c) == a*b + a*c)))
    // Show(TreeView( '(a*(b+c) == a*b + a*c), Resizable -> True,
    // IncludeExpression -> True))
    // Show(TreeView( '(-500), Resizable -> True, IncludeExpression -> True))
    // Show(TreeView( '(-500 * a), Resizable -> True, IncludeExpression ->
    // True))
    // Show(TreeView( '(2*3+8-4), Resizable -> True, IncludeExpression -> True))
    // Show(TreeView( '(-50000000000000*a), Resizable -> True, IncludeExpression
    // -> True))
    public SoughtGivenPanel(Environment aEnvironment, Cons expressionCons, double viewScale, Map optionsMap) {

        super();

        this.environment = aEnvironment;

		// this.setBorder(new EmptyBorder(1,1,1,1));
        responseListeners = new ArrayList<ResponseListener>();

        if (optionsMap != null) {
            this.treeOptionsMap = optionsMap;
        }

        this.setLayout(null);

        this.expression = expressionCons;
        this.setOpaque(true);
        this.viewScale = viewScale;

        this.addMouseListener(this);

        for (int index = 0; index < lastOnRasterArray.length; index++) {
            lastOnRasterArray[index] = -1;
        }// end for.

        if (expressionCons != null) {
            mainRootNode = new SymbolNode();
            mainRootNode.setFontSize(viewScale * fontSize);
            mainRootNode.setViewScale(viewScale);

            try {
                // listToTree(rootNode, expressionCons, null, null);

                String operator;

                if (expressionCons.car() instanceof Cons) {
                    operator = (String) Cons.caar(expressionCons);
                } else {
                    operator = (String) expressionCons.car();
                }

                mainRootNode.setOperator(operator, (Boolean) treeOptionsMap.get("Code"), (int) treeOptionsMap.get("WordWrap"));

                handleSublistCons(mainRootNode, expressionCons, null, null, "", treeOptionsMap);

            } catch (Throwable e) {
                e.printStackTrace();
            }

            layoutLinear();
        }

    }

    public static void listToTree(SymbolNode rootNode, Cons rootCons, Color markAllColor, String markAllNodeShape, String position, Map treeOptionsMap) throws Throwable {

        Object object = rootCons.car();

        if (!(object instanceof Cons)) {
            return;
        }

        Cons cons = (Cons) object; // Go into sublist.

        Map optionsMap = cons.getMetadataMap();

        if (markAllColor != null) {
            rootNode.setHighlightColor(markAllColor);
        } else if (optionsMap != null) {

            if (optionsMap.containsKey("\"HighlightColor\"")) {

                Cons atomCons = (Cons) optionsMap.get("\"HighlightColor\"");

                if (atomCons != null) {
                    rootNode.setHighlightColor(colorMap.get(atomCons.car()));
                }
            }
        }

        if (markAllNodeShape != null) {
            rootNode.setHighlightNodeShape(markAllNodeShape);
        } else if (optionsMap != null) {

            if (optionsMap.containsKey("\"HighlightNodeShape\"")) {

                Cons atomCons = (Cons) optionsMap.get("\"HighlightNodeShape\"");

                if (atomCons != null) {
                    rootNode.setHighlightNodeShape(Utility.stripEndQuotesIfPresent((String) atomCons.car()));
                }
            }
        }

        rootNode.setPosition(position);

        String operator = "<ERROR>";
        if (cons.car() instanceof String) {
            operator = (String) cons.car();
        } else {
            operator = cons.car().toString();
        }

        rootNode.setOperator(operator, (Boolean) treeOptionsMap.get("Code"), (int) treeOptionsMap.get("WordWrap"));

        int positionInt = 0;

        while (cons.cdr() != null) {
            cons = cons.cdr();
            positionInt++;

            SymbolNode node2 = new SymbolNode();

            node2.setPosition(position + (position.length() == 0 ? "" : ",") + positionInt);

            if (cons instanceof SublistCons) {

                handleSublistCons(node2, cons, markAllColor, markAllNodeShape, position + (position.length() == 0 ? "" : ",") + positionInt, treeOptionsMap);

                rootNode.addChild(node2);

            } else {

                Map map = cons.getMetadataMap();

                if (markAllColor != null) {
                    node2.setHighlightColor(markAllColor);
                } else if (map != null) {

                    if (map.containsKey("\"HighlightColor\"")) {

                        Cons atomCons = (Cons) map.get("\"HighlightColor\"");

                        if (atomCons != null) {
                            node2.setHighlightColor(colorMap.get(atomCons.car()));
                        }
                    }
                }

                if (markAllNodeShape != null) {
                    node2.setHighlightNodeShape(markAllNodeShape);
                } else if (map != null) {

                    if (map.containsKey("\"HighlightNodeShape\"")) {

                        Cons atomCons = (Cons) map.get("\"HighlightNodeShape\"");

                        if (atomCons != null) {
                            node2.setHighlightNodeShape(Utility.stripEndQuotesIfPresent((String) atomCons.car()));
                        }
                    }
                }

                operator = (String) cons.car();

                node2.setOperator(operator, (Boolean) treeOptionsMap.get("Code"), (int) treeOptionsMap.get("WordWrap"));

                rootNode.addChild(node2);
            }
        }

    }// end method.

    public static void handleSublistCons(SymbolNode node2, Cons cons, Color markAllColor, String markAllNodeShape, String position, Map treeOptionsMap) throws Throwable {
        Color markSubtreeColor = null;

        Map map = cons.getMetadataMap();

        if (markAllColor != null) {
            markSubtreeColor = markAllColor;
        } else if (map != null) {

            if (map.containsKey("\"HighlightColor\"")) {

                Cons atomCons = (Cons) map.get("\"HighlightColor\"");

                if (atomCons != null) {
                    markSubtreeColor = colorMap.get(atomCons.car());
                }
            }
        }

        String markSubtreeNodeShape = null;

        if (markAllNodeShape != null) {
            markSubtreeNodeShape = markAllNodeShape;
        } else if (map != null) {

            if (map.containsKey("\"HighlightNodeShape\"")) {

                Cons atomCons = (Cons) map.get("\"HighlightNodeShape\"");

                if (atomCons != null) {
                    markSubtreeNodeShape = Utility.stripEndQuotesIfPresent((String) atomCons.car());
                }
            }
        }

        listToTree(node2, cons, markSubtreeColor, markSubtreeNodeShape, position, treeOptionsMap);
    }

	//Uncomment for debugging.
	
     public void paint(Graphics g) { 
        super.paint(g); Dimension d = getPreferredSize(); g.drawRect(0, 0, d.width - 1, d.height - 1); 
     }
     
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;

        layoutLinear();

        int xInset = getInsets().left;
        int yInset = getInsets().top;
        int w = getWidth() - getInsets().left - getInsets().right;
        int h = getHeight() - getInsets().top - getInsets().bottom;

        g2d.setColor(Color.white);
        g2d.fillRect(xInset, yInset, w, h);
        
        g2d.setColor(Color.black);
        
        if((Boolean) this.treeOptionsMap.get("Debug"))
        {
           g2d.drawRect(0, 0, getWidth(), getHeight()); 
        }

        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));

        g2d.setStroke(new BasicStroke((float) (2), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        
        


        ScaledGraphics sg = new ScaledGraphics(g2d);

        sg.setFontSize(viewScale * fontSize);

        sg.setViewScale(viewScale);

        /*
        queue.add(mainRootNode);
        paintHighlightLayer(sg);

        queue.add(mainRootNode);
        paintDrawingLayer(sg);
        */
        
        paintLinear(mainRootNode, sg);

    }

    
    // ==== paintLinear	
    void paintLinear(SymbolNode currentNode, ScaledGraphics sg) {	

            //var nodeLeftX = node.leftX;
            //var nodeTopY = yPosition;
            

            //drawNode(node, nodeLeftX, nodeTopY, scaledGraphics);
            
            double nodeX0 = currentNode.getTreeX();

            double nodeY0 = currentNode.getTreeY();

            sg.setLineWidth(defaultLineThickness);

            sg.setColor(Color.BLACK);

            
            sg.drawLatex(currentNode.getTexFormula(), 12, nodeX0, nodeY0);

            if(currentNode.getChildren().length != 0)
            {
                    for(SymbolNode child: currentNode.getChildren()) {

                            paintLinear(child, sg);
                    }
            }
    }


    public Dimension getPreferredSize() {

        //todo:tk:the +4 is a hack to prevent the bottom of the bottommost nodes from being clipped.
        int maxHeightScaled = (int) ((maxTreeY - yPositionAdjust + 4) * viewScale);  

        //Adjusts how far to the right the component will  extend.
        int maxWidth = rightMostPosition - 4; //todo:tk: the subtraction is a hack to move the right side of the component left.

        int maxWidthScaled = (int) ((maxWidth) * viewScale);

	// System.out.println("" + maxWidth + ", " + maxTreeY + ", " +
        // maxWidthScaled + ", " + maxHeightScaled);
        
        //return (new Dimension(maxWidthScaled, maxHeightScaled));
        
        return new Dimension(600,500); //todo:tk:needs to be calculated

    }// end method.

    
    public Dimension getMaximumSize() {
        return this.getPreferredSize();
    }

    public Dimension getMinimumSize() {
        return this.getPreferredSize();
    }

    public void setViewScale(double viewScale) {
        this.viewScale = viewScale;

		//this.adjust = (int) viewScale;
        //System.out.println(adjust);
        this.revalidate();
        this.repaint();
    }

    
    
//    public void layoutTree() {
//
//        rightMostPosition = 0;
//
//        for (int index = 0; index < lastOnRasterArray.length; index++) {
//            lastOnRasterArray[index] = -1;
//        }// end for.
//
//        maxTreeY = 0;
//
//        layoutTree(mainRootNode, 40/* yPosition */, 0/* position */, null, true); // todo:tk:add to 40 to stretch in the Y direction.
//
//    }

    public void layoutLinear()
    {
        rightMostPosition = 0;
        
        maxTreeY = 0;
        
        layoutLinear(mainRootNode, 40, 0, 5);
    }

	// ==== layoutLinear
    Map layoutLinear(SymbolNode node, int topY, int offset, int spacing)
    {	
        
        Map<String, Integer> lowerRightXY = new HashMap<String, Integer>();
        
        lowerRightXY.put("rightMost",  0);
        lowerRightXY.put("bottomMost",  0);
                 
        customOrder(node, offset, topY, lowerRightXY, spacing);

        return lowerRightXY;
    }
	
	void customOrder(SymbolNode node, int leftX, int topY, Map<String, Integer> lowerRightXY, int spacing)
	{
		node.setTreeY(topY);
		
		if(node.getChildren().length == 0)
		{
			node.setTreeLeftX(leftX);
                        
                        
			node.rightmost = leftX + node.getNodeWidth() + spacing;
		}
		else if(node.getOperator().equals("-") && node.getChildren().length == 1)
		{
			if(false /*node.parenthesesWidth*/)
			{
                            /*
				node.leftX = leftX + node.parenthesesWidth + spacing;
				customOrder(node.args[0], node.leftX + node.width + spacing, topY, lowerRightXY, spacing);
				node.rightmost = node.args[0].rightmost + node.parenthesesWidth + spacing;
				var leftParenthesesNode = {};
				leftParenthesesNode.name = "(";
				leftParenthesesNode.leftX = leftX;
				leftParenthesesNode.width = node.parenthesesWidth;
				leftParenthesesNode.height = node.parenthesesHeight;
				var rightParenthesesNode = {};
				rightParenthesesNode.name = ")";
				rightParenthesesNode.leftX = node.args[0].rightmost;
				rightParenthesesNode.width = node.parenthesesWidth;
				rightParenthesesNode.height = node.parenthesesHeight;
				node.args.unshift(leftParenthesesNode);
				node.args.push(rightParenthesesNode);
                            */
			}
			else
			{
				node.setTreeLeftX(leftX);
				customOrder(node.getChildren()[0], node.getTreeX() + node.getNodeWidth() + spacing, topY, lowerRightXY, spacing);
				node.rightmost = node.getChildren()[0].rightmost;
			}
		}
		else if(node.getOperator().equals("+") || node.getOperator().equals("-") || node.getOperator().equals("*") || node.getOperator().equals("/") || node.getOperator().equals("=="))
		{
			if(false /*node.parenthesesWidth*/)
			{
                            /*
				customOrder(node.args[0], leftX + node.parenthesesWidth + spacing, topY, lowerRightXY, spacing);
				node.leftX = node.args[0].rightmost;
				customOrder(node.args[1], node.leftX + node.width + spacing, topY, lowerRightXY, spacing);
				node.rightmost = node.args[1].rightmost + node.parenthesesWidth + spacing;
				var leftParenthesesNode = {};
				leftParenthesesNode.name = "(";
				leftParenthesesNode.leftX = leftX;
				leftParenthesesNode.width = node.parenthesesWidth;
				leftParenthesesNode.height = node.parenthesesHeight;
				var rightParenthesesNode = {};
				rightParenthesesNode.name = ")";
				rightParenthesesNode.leftX = node.args[1].rightmost;
				rightParenthesesNode.width = node.parenthesesWidth;
				rightParenthesesNode.height = node.parenthesesHeight;
				node.args.unshift(leftParenthesesNode);
				node.args.push(rightParenthesesNode);
                            */
			}
			else
			{
				customOrder(node.getChildren()[0], leftX, topY, lowerRightXY, spacing);
				node.setTreeLeftX(node.getChildren()[0].rightmost);
				customOrder(node.getChildren()[1], node.getTreeX()+ node.getNodeWidth() + spacing, topY, lowerRightXY, spacing);
				node.rightmost = node.getChildren()[1].rightmost;
			}
		}
		else if(node.getOperator().equals("^"))
		{
			customOrder(node.getChildren()[0], leftX, topY, lowerRightXY, spacing);
			node.setTreeLeftX(node.getChildren()[0].rightmost);
			customOrder(node.getChildren()[1], node.getTreeX() + node.getNodeWidth() + spacing, topY, lowerRightXY, spacing);
			node.rightmost = node.getChildren()[1].rightmost;
		}
		
		if(node.rightmost > lowerRightXY.get("rightMost")) 
                {
                    lowerRightXY.put("rightMost",  node.rightmost);
                }
                
                
		if(topY + node.getNodeHeight() > lowerRightXY.get("bottomMost")) 
                {
                    lowerRightXY.put("bottomMost",  topY + node.getNodeHeight());
                }
	}




    public SymbolNode getMainRootNode() {
        return mainRootNode;
    }

    public void setMainRootNode(SymbolNode mainRootNode) {
        this.mainRootNode = mainRootNode;
    }

    public static void main(String[] args) {
        try {
            SQLParser parser = new SQLParser();

            StatementNode stmt = parser.parseStatement("SELECT hotel_no, SUM(price) FROM room r WHERE room_no NOT IN (SELECT room_no FROM booking b, hotel h WHERE (date_from <= CURRENT_DATE AND date_to >= CURRENT_DATE) AND b.hotel_no = h.hotel_no) GROUP BY hotel_no");
            stmt.treePrint();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectedNodeGet(SymbolNode node, int x, int y, List<SymbolNode> nodeList) {

        double nodeX0 = (node.getTreeX() - (leftMostPosition)) * this.viewScale;

        double nodeY0 = (node.getTreeY() - (yPositionAdjust /* height of symbol */)) * this.viewScale;

        double nodeWidth = node.getNodeWidth() * this.viewScale;
        double nodeHeight = node.getNodeHeight() * this.viewScale;

        if (x >= nodeX0 && x <= nodeX0 + nodeWidth && y >= nodeY0 && y <= nodeY0 + nodeHeight) {
            nodeList.add(node);
        } else {
            node.select(false);
        }

        SymbolNode[] children = node.getChildren();

        if (children != null) {
            for (int childIndex = 0; childIndex < children.length; childIndex++) {
                selectedNodeGet(children[childIndex], x, y, nodeList);
            }

        }

    }

    private void selectedNodeGet(SymbolNode node, Position positionString, List<SymbolNode> nodeList) {

        
        if (node.getPosition().equals(positionString.toString())) {
            nodeList.add(node);
        }

        SymbolNode[] children = node.getChildren();

        if (children != null) {
            for (int childIndex = 0; childIndex < children.length; childIndex++) {
                selectedNodeGet(children[childIndex], positionString, nodeList);
            }

        }

    }

    public void mouseClicked(MouseEvent me) {

        if (isMouseClickedHandlerStillProcessing) {
            return;
        }

        isMouseClickedHandlerStillProcessing = true;

        // System.out.println(me.getX() + ", " + me.getY());
        List<SymbolNode> nodeList = new ArrayList();

        this.selectedNodeGet(mainRootNode, me.getX(), me.getY(), nodeList);

        if (!nodeList.isEmpty() && treeOptionsMap.containsKey("Process")) {
            selectNode(nodeList);
            
            EvaluationResponse notifyResponse = EvaluationResponse.newInstance();
            notifyResponse.setResult(positionString);
            notifyResponse.setSideEffects("NotCleared");
            try {
                notifyResponse.setResultList(Cons.deepCopy(environment, -1, this.expression));
                this.notifyListeners(notifyResponse);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        } else {
            // No part of a tree was selected. 

            clearTree();

            EvaluationResponse notifyResponse = EvaluationResponse.newInstance();
            notifyResponse.setResult(positionString);
            notifyResponse.setSideEffects("Cleared");
            try {
                notifyResponse.setResultList(Cons.deepCopy(environment, -1, this.expression));
                this.notifyListeners(notifyResponse);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        isMouseClickedHandlerStillProcessing = false;
    }

    public void clearTree() {
        this.oldSelectedNode = null;

        positionString = null;
        operatorString = null;

        redrawTree(this.expression);
        
        this.isSubtreeSelected = false;
    }

    private void selectNode(List<SymbolNode> nodeList) {

        try {
            SymbolNode selectedNode = nodeList.get(0);

            operatorString = selectedNode.toString();

            positionString = selectedNode.getPosition();

            EvaluationResponse selectionHighlightedResponse = highlightTree(environment, selectedNode, null);

            if (isSelectSingleNode && this.oldSelectedNode != null) {
                Position oldSelectedPosition = new Position("0," + oldSelectedNode.getPosition()); // "0" +

                Position selectedPosition = new Position("0," + selectedNode.getPosition()); // "0" +

                int distance = 0;

                int shortest = (selectedPosition.size() < oldSelectedPosition.size()) ? selectedPosition.size() : oldSelectedPosition.size();

                int index = 0;

                while (index < shortest) {
                    if (selectedPosition.positionAt(index) != oldSelectedPosition.positionAt(index)) {
                        break;
                    }

                    index++;
                }

                index--;

                Position oldSelectedSubPosition = oldSelectedPosition.subPosition(index);

                Position selectedSubPosition = selectedPosition.subPosition(index);

                distance = oldSelectedSubPosition.size() + selectedSubPosition.size() - 2;

                if (oldSelectedPosition.size() - 1 > index) {
                    // Highlight all nodes in the start branch.
                    for (int index2 = index; index2 < oldSelectedPosition.size(); index2++) {
                        nodeList = new ArrayList();

                        this.selectedNodeGet(mainRootNode, new Position(oldSelectedNode.getPosition()).subPosition(0, index2), nodeList);

                        if (!nodeList.isEmpty()) {
                            SymbolNode pathNode = nodeList.get(0);

                            selectionHighlightedResponse = highlightTree(environment, pathNode, selectionHighlightedResponse.getResultList());
                        }
                    }
                }

                if (selectedPosition.size() - 1 > index) {
                    // Highlight all nodes in the end branch.
                    for (int index2 = index; index2 < selectedPosition.size(); index2++) {
                        nodeList = new ArrayList();

                        this.selectedNodeGet(mainRootNode, new Position(selectedNode.getPosition()).subPosition(0, index2), nodeList);

                        if (!nodeList.isEmpty()) {
                            SymbolNode pathNode = nodeList.get(0);

                            selectionHighlightedResponse = highlightTree(environment, pathNode, selectionHighlightedResponse.getResultList());
                        }
                    }
                }

                selectionHighlightedResponse = highlightTree(environment, oldSelectedNode, selectionHighlightedResponse.getResultList());
            }

            relayoutTree(selectionHighlightedResponse.getResultList());

            if (isSelectSingleNode && this.oldSelectedNode != null) {
                Position oldSelectedPosition = new Position("0," + oldSelectedNode.getPosition());

                Position selectedPosition = new Position("0," + selectedNode.getPosition());

                int shortest = (selectedPosition.size() < oldSelectedPosition.size()) ? selectedPosition.size() : oldSelectedPosition.size();

                int index = 0;

                while (index < shortest) {
                    if (selectedPosition.positionAt(index) != oldSelectedPosition.positionAt(index)) {
                        break;
                    }

                    index++;
                }

                index--;

                this.sequenceNumber = 1;

                // Start branch.
                if (oldSelectedPosition.size() - 1 > index) {
                    for (int index2 = oldSelectedPosition.size() - 1; index2 >= index; index2--) {
                        setSequence(oldSelectedNode, index2);
                    }

                    this.sequenceNumber--;
                }

                // End branch.
                if (selectedPosition.size() - 1 > index) {
                    for (int index2 = index; index2 < selectedPosition.size(); index2++) {
                        setSequence(selectedNode, index2);
                    }
                }

                selectedNode = null;
            }

            this.oldSelectedNode = selectedNode;

            layoutLinear();
            this.repaint();
            this.invalidate();
            this.revalidate();

            if (!isSelectSingleNode) {
                EvaluationResponse notifyResponse = EvaluationResponse.newInstance();
                notifyResponse.setResult(positionString);
                notifyResponse.setSideEffects("Selected");
                
                this.isSubtreeSelected = true;

                notifyResponse.setResultList(Cons.deepCopy(environment, -1, selectionHighlightedResponse.getResultList()));
                this.notifyListeners(notifyResponse);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void selectNode(String positionString) {
        selectNode(new Position(positionString));
    }
    
    public void selectNode(Position positionString) {
        List<SymbolNode> nodeList = new ArrayList();

        this.selectedNodeGet(mainRootNode, positionString, nodeList);

        if (!nodeList.isEmpty() && treeOptionsMap.containsKey("Process")) {

            selectNode(nodeList);
        }
    }
    
    
    public void selectPath(String position1, String position2)
    {
        List<SymbolNode> nodeList = new ArrayList();

        this.selectedNodeGet(mainRootNode, new Position(position1), nodeList);

        if (!nodeList.isEmpty() ){
            this.oldSelectedNode = nodeList.get(0);
            selectNode(new Position(position2));
        }     
    }
        

    private void setSequence(SymbolNode node, int index2) {
        List<SymbolNode> nodeList = new ArrayList();

        this.selectedNodeGet(mainRootNode, new Position(node.getPosition()).subPosition(0, index2), nodeList);

        if (!nodeList.isEmpty()) {
            SymbolNode pathNode = nodeList.get(0);

            pathNode.setSequenceNumber(sequenceNumber++);
        }
    }

    public EvaluationResponse highlightTree(Environment environment, SymbolNode selectedNode, Cons existingExpression) throws Throwable {

        String operatorString = selectedNode.toString();

        String positionString = selectedNode.getPosition();

        Cons cons = (Cons) treeOptionsMap.get("Process");

        Cons from = AtomCons.getInstance(environment.getPrecision(), "\"expression\"");

        Cons to = null;
        if (existingExpression != null) {
            to = existingExpression;
        } else {
            to = this.expression;
        }

        org.mathpiper.lisp.astprocessors.ExpressionSubstitute behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(environment, from, to);
        cons = Utility.substitute(environment, -1, cons, behaviour);

        String ruleString;

        if (!isSelectSingleNode && (environment.isOperator(operatorString))) {
            if (selectedNode.getChildren().length == 2) {
                ruleString = "'(x_ " + operatorString + " y_);";
            } else if (selectedNode.getChildren().length == 1) {
                if (environment.iPostfixOperators.map.containsKey(operatorString)) {
                    ruleString = "'( x_ " + operatorString + ");";
                } else {
                    ruleString = "'(" + operatorString + " x_);";
                }
            } else {
                throw new Exception("Only unary and binary operators are currently supported.");
            }
        } 
        else if (!isSelectSingleNode && environment.isProcedure(operatorString))
        {
            if (selectedNode.getChildren().length == 2) {
                ruleString = "'(" + operatorString + "(x_, y_));";
            } else if (selectedNode.getChildren().length == 1) {
                ruleString = "'(" + operatorString + "(x_));";
            } else {
                throw new Exception("Only procedures with one or two arguments are supported.");
            }
        }
        else {
            ruleString = "ToAtom(\"" + operatorString + "\");";
        }

        StringInputStream newInput = new StringInputStream(ruleString, environment.iInputStatus);
        MathPiperParser parser = new MathPiperParser(new MathPiperTokenizer(), newInput, environment, environment.iPrefixOperators, environment.iInfixOperators, environment.iPostfixOperators, environment.iBodiedProcedures);

        Cons pattern = parser.parse(-1);
        from = AtomCons.getInstance(environment.getPrecision(), "\"pattern\"");
        to = pattern;
        behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(environment, from, to);
        cons = Utility.substitute(environment, -1, cons, behaviour);

        from = AtomCons.getInstance(environment.getPrecision(), "\"position\"");
        to = AtomCons.getInstance(environment.getPrecision(), "\"" + positionString + "\"");
        behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(environment, from, to);
        cons = Utility.substitute(environment, -1, cons, behaviour);



        return interpreter.evaluate(cons);
    }

    public void relayoutTree(Cons annotatedTree) {

        for (int index = 0; index < lastOnRasterArray.length; index++) {
            lastOnRasterArray[index] = -1;
        }// end for.

        if (annotatedTree != null) {
            mainRootNode = new SymbolNode();

            try {
                // listToTree(rootNode, expressionCons, null, null);

                String operator;

                if (annotatedTree.car() instanceof Cons) {
                    operator = (String) Cons.caar(annotatedTree);
                } else {
                    operator = (String) annotatedTree.car();
                }

                mainRootNode.setOperator(operator, (Boolean) treeOptionsMap.get("Code"), (int) treeOptionsMap.get("WordWrap"));

                handleSublistCons(mainRootNode, annotatedTree, null, null, "", treeOptionsMap);

            } catch (Throwable e) {
                e.printStackTrace();
            }

        }

    }

    public void redrawTree(Cons annotatedTree) {
        relayoutTree(annotatedTree);
        layoutLinear();;
        this.repaint();
        this.invalidate();
        this.revalidate();
    }

    public void mousePressed(MouseEvent me) {
    }

    public void mouseReleased(MouseEvent me) {
    }

    public void mouseEntered(MouseEvent me) {
    }

    public void mouseExited(MouseEvent me) {
    }

    public void addResponseListener(ResponseListener listener) {
        responseListeners.add(listener);
    }

    public void removeResponseListener(ResponseListener listener) {
        responseListeners.remove(listener);
    }

    protected void notifyListeners(EvaluationResponse response) {

        for (ResponseListener listener : responseListeners) {
            listener.response(response);
        }//end for.
        
    }//end method.

    public String getPositionString() {
        return positionString;
    }

    public String getOperatorString() {
        return operatorString;
    }

    public Cons getExpression(Environment aEnvironment, int aStackTop) throws Throwable {
        return Cons.deepCopy(aEnvironment, aStackTop, expression);
    }

    public void setExpression(Cons expression) {
        this.expression = expression;
    }

    public void showPositions(boolean isShowPositions) {
        this.isShowPositions = isShowPositions;
        this.repaint();
    }
    
    public void showDepths(boolean isShowDepths) {
        this.isShowDepths = isShowDepths;
        this.repaint();
    }

    public void setIsSelectSingleNode(boolean isSelectSingleNode) {
        this.isSelectSingleNode = isSelectSingleNode;
        this.oldSelectedNode = null;
    }
    
    public void setIsArcsHighlight(boolean isArcsHighlight) {
        this.isArcsHighlight = isArcsHighlight;
    }
    
    public void setIsNodesHighlight(boolean isNodesHighlight) {
        this.isNodesHighlight = isNodesHighlight;
    }
    
    public boolean isSubtreeSelected()
    {
        return this.isSubtreeSelected;
    }
    
    
public class Position
{
    private List<Integer> position = new ArrayList();
    
    public Position(String positionString)
    {
        String[] positions = positionString.split(",");
        
        for(String digitString: positions)
        {
            if(! digitString.equals(""))
            {
                position.add(Integer.parseInt(digitString));
            }
        }
    }
    
    public Position(List position)
    {
        this.position = position;
    }
    
    public Position subPosition(int start)
    {
        return new Position(position.subList(start, position.size()));
    }
    
    public Position subPosition(int start, int end)
    {
        return new Position(position.subList(start, end));
    }
    
    public int size()
    {
        return position.size();
    }
    
    public int positionAt(int index)
    {
        return position.get(index);
    }
    
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        
        int index = 0;
        
        while(index < position.size())
        {
            result.append(position.get(index++));
            
            if(index != position.size())
            {
                result.append(",");
            }
        }
        
        return result.toString();
    }
    }

}// end class.