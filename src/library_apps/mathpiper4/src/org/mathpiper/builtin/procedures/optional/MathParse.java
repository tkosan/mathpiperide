package org.mathpiper.builtin.procedures.optional;

import java.util.ArrayList;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.NumberCons;
import org.mathpiper.lisp.cons.SublistCons;


class OpStr {

    public static String ADD = "+";
    public static String SUB = "-";
    public static String MUL = "*";
    public static String TIMES = "times";
    public static String CDOT = "cdot";
    public static String DIV = "div";
    public static String FRAC = "/";
    public static String DFRAC = "dfrac";
    public static String EQL = "==";
    public static String GT = ">?";
    public static String LT = "<?";
    public static String GEQ = ">=?";
    public static String LEQ = "<=?";
    public static String ATAN2 = "atan2";
    public static String SQRT = "sqrt";
    public static String PM = "pm";
    public static String SIN = "sin";
    public static String COS = "cos";
    public static String TAN = "tan";
    public static String SEC = "sec";
    public static String COT = "cot";
    public static String CSC = "csc";
    public static String LN = "ln";
    public static String VAR = "var";
    public static String CST = "cst";
    public static String COMMA = ";";
    public static String POW = "^";
    public static String ABS = "Abs";
    public static String PAREN = "()";
    public static String HIGHLIGHT = "hi";
    public static String DERIV = "deriv";
    public static String NEQ = "!=";
    public static String NUM = "num";
    public static String EMP = "empty";
}

class Node {

    public String op;
    public List args = new ArrayList();
}

public class MathParse extends BuiltinProcedure {
    

    static final int TK_NONE = 0;
    static final int TK_ADD = 43;
    static final int TK_CARET = 94;
    static final int TK_DIV = 47;
    static final int TK_EQL = 61;
    static final int TK_LT = 60;
    static final int TK_GT = 62;
    static final int TK_LEFTBRACE = 123;
    static final int TK_LEFTBRACKET = 91;
    static final int TK_LEFTPAREN = 40;
    static final int TK_MUL = 42;
    static final int TK_NUM = 48;
    static final int TK_RIGHTBRACE = 125;
    static final int TK_RIGHTBRACKET = 93;
    static final int TK_RIGHTPAREN = 41;
    static final int TK_SUB = 45;
    static final int TK_VAR = 97;
    static final int TK_COEFF = 65;
    static final int TK_ABS = 124;
    static final int TK_COMMA = 44;
    static final int TK_DEC = 46;
    static final int TK_SPACE = 32;
    static final int TK_BACKSLASH = 92;
    static final int TK_AMP = 38;
    static final int TK_SHARP = 35;
    static final int TK_CDOT = 0x100;
    static final int TK_TIMES = 0x101;
    static final int TK_PM = 0x102;
    static final int TK_FRAC = 0x103;
    static final int TK_DFRAC = 0x104;
    static final int TK_LN = 0x105;
    static final int TK_COS = 0x106;
    static final int TK_SIN = 0x107;
    static final int TK_TAN = 0x108;
    static final int TK_SEC = 0x109;
    static final int TK_COT = 0x110;
    static final int TK_CSC = 0x111;
    static final int TK_SQRT = 0x112;
    static final int TK_LEQ = 0x113;
    static final int TK_GEQ = 0x114;
    static final int TK_NOT = 0x115;
    static final int TK_NEXT = 0x116;
    static final int TK_COLOR = 0x117;

    //var scan = scanner(src);
    Map<Integer, String> tokenToOp = new HashMap<Integer, String>();

    int T0 = TK_NONE, T1 = TK_NONE;

    String src;

    Scan scan;

    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "MathParse";

        aEnvironment.getBuiltinFunctions().put("MathParse", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        tokenToOp.put(TK_FRAC, OpStr.FRAC);
        tokenToOp.put(TK_DFRAC, OpStr.DFRAC);
        tokenToOp.put(TK_SQRT, OpStr.SQRT);
        tokenToOp.put(TK_ADD, OpStr.ADD);
        tokenToOp.put(TK_SUB, OpStr.SUB);
        tokenToOp.put(TK_PM, OpStr.PM);
        tokenToOp.put(TK_CARET, OpStr.POW);
        tokenToOp.put(TK_MUL, OpStr.MUL);
        tokenToOp.put(TK_TIMES, OpStr.TIMES);
        tokenToOp.put(TK_CDOT, OpStr.CDOT);
        tokenToOp.put(TK_DIV, OpStr.FRAC);
        tokenToOp.put(TK_SIN, OpStr.SIN);
        tokenToOp.put(TK_COS, OpStr.COS);
        tokenToOp.put(TK_TAN, OpStr.TAN);
        tokenToOp.put(TK_SEC, OpStr.SEC);
        tokenToOp.put(TK_COT, OpStr.COT);
        tokenToOp.put(TK_CSC, OpStr.CSC);
        tokenToOp.put(TK_LN, OpStr.LN);
        tokenToOp.put(TK_EQL, OpStr.EQL);
        tokenToOp.put(TK_LT, OpStr.LT);
        tokenToOp.put(TK_GT, OpStr.GT);
        tokenToOp.put(TK_GEQ, OpStr.GEQ);
        tokenToOp.put(TK_LEQ, OpStr.LEQ);
        tokenToOp.put(TK_ABS, OpStr.ABS);
    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {

        Cons argument = getArgument(aEnvironment, aStackTop, 1);
        
        LispError.checkIsString(aEnvironment, aStackTop, argument, 1);
        
        String src = (String) argument.car();
        
        src = Utility.stripEndQuotesIfPresent(src);

        src = src.replace("u2212", "-");
        
        src = src.replace("−", "-");
        
        src = src.replace("==", "=");
        
        src = src.replace("_", "");
        
        
        src = src + ";";

        scan = new Scan(src);
        
        Node node = expr();
        
        if(node != null)
        {
            Cons expression = nodesToList(node, aEnvironment);

            setTopOfStack(aEnvironment, aStackTop, expression);
        }
        else
        {
            LispError.throwError(aEnvironment, aStackTop, "Invalid argument.");
        }



    }//end method.

    	// === parse (copied from the Khan Academy math-model.js program.
    void start() {
        T0 = scan.start();
    }

    int hd() {
        return T0;
    }

    String lexeme() {
        return scan.lexeme();
    }

    boolean matchToken(int t) {
        if (T0 == t) {
            next();
            return true;
        }
        return false;
    }

    void next() {
        T0 = T1;
        T1 = TK_NONE;
        if (T0 == TK_NONE) {
            T0 = scan.start();
        }
    }

    void replace(int t) {
        T0 = t;
    }

    void eat(int tc) {
        int tk = hd();
        if (tk != tc) {

            System.out.println("Expecting " + tc + " found " + tk); // @Nolint

        }
        next();
    }

    boolean match(int tc) {
        int tk = hd();
        if (tk != tc) {
            return false;
        }
        next();
        return true;
    }

    Node primaryExpr() {
        Node returnExpr = new Node();

        List list = new ArrayList();
        
        
        int t;
        int op;
        
        switch ((t = hd())) {
            case 65:
            case 97:

                /*e = {
                 op : "var",
                 args : [ lexeme() ]
                 };*/

                returnExpr.op = "var";
                list = new ArrayList();
                list.add(lexeme());
                returnExpr.args = list;

                next();
                break;
            case TK_NUM:
                /*e = {
                 op : "num",
                 args : [ Number(lexeme()) ]
                 };*/
                returnExpr.op = "var";
                list.add(Integer.parseInt(lexeme()));
                returnExpr.args = list; 

                next();
                break;
            case TK_DEC:
                returnExpr.op = "var";
                list.add(Double.parseDouble(lexeme()));
                returnExpr.args = list;
                
                next();
                break;
            case TK_LEFTPAREN:
                returnExpr = parenExpr();
                break;
            case TK_ABS:
                returnExpr = absExpr();
                break;
            case TK_FRAC:
            case TK_DFRAC:
                next();
                /*e = {
                 op : tokenToOp[t],
                 args : [ braceExpr(), braceExpr() ]
                 };*/
                returnExpr.op = tokenToOp.get(t);
                list.add(braceExpr());
                list.add(braceExpr());
                returnExpr.args = list;

                break;
            case TK_SQRT:
                next();
                switch (hd()) {
                    case TK_LEFTBRACKET:
                        /*e = {
                         op : tokenToOp[TK_SQRT],
                         args : [ bracketExpr(), braceOptionalExpr() ]
                         };*/
                        returnExpr.op = tokenToOp.get(TK_SQRT);
                        list.add(braceExpr());
                        list.add(braceOptionalExpr());
                        returnExpr.args = list;

                        break;
                    default:
                        /*e = {
                         op : tokenToOp[TK_SQRT],
                         args : [ braceOptionalExpr() ]
                         };*/
                        returnExpr.op = tokenToOp.get(TK_SQRT);
                        list.add(braceOptionalExpr());
                        returnExpr.args = list;

                        break;
                }
                break;
            case TK_SIN:
            case TK_COS:
            case TK_TAN:
            case TK_SEC:
            case TK_COT:
            case TK_CSC:
            case TK_LN:
                next();
                if (hd() == TK_CARET) {
                    eat(TK_CARET);
                    Node expr2 = braceOptionalExpr();
                    /*e = {
                     op : tokenToOp[t],
                     args : [ unaryExpr() ]
                     };
                     e = {
                     op : OpStr.POW,
                     args : [ e, expr2 ]
                     };*/
                    
                    Node expr1 = new Node();
                    expr1.op = tokenToOp.get(t);
                    list.add(unaryExpr());
                    expr1.args = list;
                    
                    list = new ArrayList();
                    
                    returnExpr.op = OpStr.POW;
                    list.add(expr1);
                    list.add(expr2);
                    returnExpr.args = list;

                    
                } else {
                    /*e = {
                     op : tokenToOp[t],
                     args : [ unaryExpr() ]
                     };*/
                    returnExpr.op = tokenToOp.get(t);
                    list.add(unaryExpr());
                    returnExpr.args = list;

                }
                break;
            default:
                returnExpr = null;//e = void 0;
                break;
        }
        return returnExpr;
    }

    Node braceExpr() {
        eat(TK_LEFTBRACE);
        Node e = commaExpr();
        eat(TK_RIGHTBRACE);
        return e;
    }

    Node braceOptionalExpr() {
        if (hd() == TK_LEFTBRACE) {
            eat(TK_LEFTBRACE);
            Node e = commaExpr();
            eat(TK_RIGHTBRACE);
            return e;
        }
        return unaryExpr();
    }

    Node bracketExpr() {
        eat(TK_LEFTBRACKET);
        Node e = commaExpr();
        eat(TK_RIGHTBRACKET);
        return e;
    }

    Node parenExpr() {
        return surroundedExpr(TK_LEFTPAREN, TK_RIGHTPAREN, OpStr.PAREN);
    }

    Node absExpr() {
        return surroundedExpr(TK_ABS, TK_ABS, OpStr.ABS);
    }

    Node surroundedExpr(int leftTk, int rightTk, String op) {
        eat(leftTk);

        Node e = commaExpr();

        eat(rightTk);
        /*
         Node expr = {
         op : op,
         args : [ e ],
         };
         */
        Node returnExpr = new Node();
        returnExpr.op = op;
        List list = new ArrayList();
        list.add(e);
        returnExpr.args = list;

        return op.equals(OpStr.PAREN) ? e : returnExpr;
    }

    Node unaryExpr() {
        int t;

        Node returnExpr;

        switch (t = hd()) {

            case TK_ADD:
                next();
                returnExpr = unaryExpr();
                break;
            case TK_SUB:
                next();
                Node arg = unaryExpr();
				//if(arg == undefined) throw new Error("Invalid expression");
				/*expr = {
                 op : "-",
                 args : [ arg ]
                 };*/

                returnExpr = new Node();
                returnExpr.op = "-";
                List list = new ArrayList();
                list.add(arg);
                returnExpr.args = list;
                break;
            default:
                returnExpr = primaryExpr();
                break;
        }
        return returnExpr;
    }

    Node exponentialExpr() {
        Node expr = unaryExpr();
        Node returnExpr = null;
        //if(expr == undefined) throw new Error("Invalid expression");
        int t;
        
        boolean isExponential = false;
        
        while ((t = hd()) == TK_CARET) {
            
            isExponential = true;
            
            Node oldExpr = expr;
            expr = new Node();
            
            next();
            Node expr2 = braceOptionalExpr();
				//if(expr2 == undefined) throw new Error("Invalid expression");

            /*expr = {
             op : tokenToOp[t],
             args : [ expr, expr2 ]
             };*/
            expr.op = tokenToOp.get(t);
            List list = new ArrayList();
            list.add(oldExpr);
            list.add(expr2);
            expr.args = list;
            
            returnExpr = expr;
        }
        return isExponential ? returnExpr : expr;
    }

    Node multiplicativeExpr() {
        Node expr = exponentialExpr();
        
        Node returnExpr = null;
        //if(expr == undefined) throw new Error("Invalid expression");
        int t;
        t = hd();
        boolean isMultiplicative = false;
        while (isMultiplicative(t) || (t == TK_VAR) || (t == TK_LEFTPAREN) || (t == TK_DEC)) {
            
        	Node oldExpr = expr;
            expr = new Node();
                
            if(t != TK_DEC)
                isMultiplicative = true;
            
            if (isMultiplicative(t) || t == TK_DEC) {
                next();
            }
            
            Node expr2 = exponentialExpr();
				//if(expr2 == undefined) throw new Error("Invalid expression");
				/*expr = {
             op : tokenToOp[t],
             args : [ expr, expr2 ]
             };*/
             
            expr.op = tokenToOp.get(t);
            List list = new ArrayList();
            list.add(oldExpr);
            list.add(expr2);
            expr.args = list;

            if (t != TK_DIV && t != TK_DEC) {
                expr.op = "*";
            }
            
            t = hd();
            
            returnExpr = expr;
        }
        return isMultiplicative ? returnExpr : expr;

    }

    boolean isMultiplicative(int t) {
        return t == TK_MUL || t == TK_DIV || t == TK_TIMES
                || t == TK_CDOT;
    }

    Node additiveExpr() {
        Node expr = multiplicativeExpr();
        Node returnExpr = null;

        //if(expr == undefined) throw new Error("Invalid expression");
        int t;
        boolean isAdditive = false;
        
        while (isAdditive(t = hd())) {
        	
        	isAdditive = true;
            
            Node oldExpr = expr;
            expr = new Node();           
            
            next();
            Node expr2 = multiplicativeExpr();
				//if(expr2 == undefined) throw new Error("Invalid expression");

            /*expr = {
             op : tokenToOp[t],
             args : [ expr, expr2 ]
             };*/
            
            
            
            expr.op = tokenToOp.get(t);
            List list = new ArrayList();
            list.add(oldExpr);
            list.add(expr2);
            expr.args = list;
            
            returnExpr = expr;
        }
        return isAdditive ? returnExpr : expr;

    }

    boolean isAdditive(int t) {
        return t == TK_ADD || t == TK_SUB || t == TK_PM;
    }

    
    
    Node compareExpr() {
        Node expr = additiveExpr();
        Node returnExpr = null;
        
        if (expr == null) {
            /*expr = {
             op : OpStr.EMP,
             args : []
             };*/

            expr.op = OpStr.EMP;
            List list = new ArrayList();
            expr.args = list;
        }
        
        
        int t = hd();
        
        boolean compareExpr = false;
        
        while ((t == TK_EQL) || (t == TK_LT) || (t == TK_GT)
                || (t == TK_LEQ) || (t == TK_GEQ) || (t == TK_NOT)) {
            
            compareExpr = true;
            
            Node oldExpr = expr;
            expr = new Node();
            
            next();

            String op = tokenToOp.get(t);

            if (t == TK_NOT) {
                t = hd();
                if (t != TK_EQL) {
                    System.out.println("\\not can only be followed by = in this version");
                }
                next();
                op = OpStr.NEQ;
            }
            
            Node expr2 = additiveExpr();

            /*expr = {
             op : op,
             args : [ expr, expr2 ]
             };*/
            expr.op = op;
            List list = new ArrayList();
            list.add(oldExpr);
            list.add(expr2);
            expr.args = list;
            
            returnExpr = expr;

            t = hd();
        }
        
        return compareExpr ? returnExpr : expr;
    }

    Node commaExpr() {
        Node n = compareExpr();
        return n;
    }

    Node expr() {
        start();
        Node n = commaExpr();
        return n;
    }
    
    
    private Cons nodesToList(Node node, Environment aEnvironment) throws Throwable {
        Cons sublistCons = SublistCons.getInstance(null);

        nodesToListHelper(node, sublistCons, aEnvironment);

        return sublistCons.cdr();
    }
    
    private Cons nodesToListHelper(Node node, Cons cons, Environment aEnvironment) throws Throwable {
        
        if(node.op.equals("var") && node.args.size() == 1 && !(node.args.get(0) instanceof Node))
        {
            Object object = node.args.get(0);
            
            if(object instanceof Integer)
            {
                Cons numberCons = new NumberCons(((Integer)object).toString(), 10);
                
                cons.setCdr(numberCons);
            }
            else if(object instanceof Double)
            {
                Cons numberCons = new NumberCons(((Double)object).toString(), 10);
                
                cons.setCdr(numberCons);
            }
            else if(object instanceof String)
            {
                String variable = (String) object;
                
                Cons atomCons = AtomCons.getInstance(aEnvironment.getPrecision(), "_" + variable);
                
                cons.setCdr(atomCons);
            }
            else
            {
                throw new Exception("Internal error.");
            }
            
            cons = cons.cdr();
            return cons;
        }
        else if(node.op.equals("dec") && node.args.size() == 1 && !(node.args.get(0) instanceof Node))
        {
            if(node.args.get(0) instanceof String)
            {                
                Cons atomCons = AtomCons.getInstance(aEnvironment.getPrecision(), ".");
                
                cons.setCdr(atomCons);
            }
            else
            {
                throw new Exception("Internal error.");
            }
            
            cons = cons.cdr();
            
            return cons;
        }
        else
        {
            SublistCons sublistCons = SublistCons.getInstance(null);
            
            Cons cons2 = AtomCons.getInstance(aEnvironment.getPrecision(), ((Node)node).op);

            sublistCons.setCar(cons2);
            

            cons.setCdr(sublistCons);
            
            cons = cons.cdr();
            
            
            for(Object argNode : node.args) 
            {
                cons2 = nodesToListHelper((Node) argNode, cons2, aEnvironment);
            }
            
            return cons;
        }
        

    };

}

class Scan {

    static final int TK_NONE = 0;
    static final int TK_ADD = 43;
    static final int TK_CARET = 94;
    static final int TK_DIV = 47;
    static final int TK_EQL = 61;
    static final int TK_LT = 60;
    static final int TK_GT = 62;
    static final int TK_LEFTBRACE = 123;
    static final int TK_LEFTBRACKET = 91;
    static final int TK_LEFTPAREN = 40;
    static final int TK_MUL = 42;
    static final int TK_NUM = 48;
    static final int TK_RIGHTBRACE = 125;
    static final int TK_RIGHTBRACKET = 93;
    static final int TK_RIGHTPAREN = 41;
    static final int TK_SUB = 45;
    static final int TK_VAR = 97;
    static final int TK_COEFF = 65;
    static final int TK_ABS = 124;
    static final int TK_COMMA = 44;
    static final int TK_DEC = 46;
    static final int TK_SPACE = 32;
    static final int TK_BACKSLASH = 92;
    static final int TK_AMP = 38;
    static final int TK_SHARP = 35;
    static final int TK_CDOT = 0x100;
    static final int TK_TIMES = 0x101;
    static final int TK_PM = 0x102;
    static final int TK_FRAC = 0x103;
    static final int TK_DFRAC = 0x104;
    static final int TK_LN = 0x105;
    static final int TK_COS = 0x106;
    static final int TK_SIN = 0x107;
    static final int TK_TAN = 0x108;
    static final int TK_SEC = 0x109;
    static final int TK_COT = 0x110;
    static final int TK_CSC = 0x111;
    static final int TK_SQRT = 0x112;
    static final int TK_LEQ = 0x113;
    static final int TK_GEQ = 0x114;
    static final int TK_NOT = 0x115;
    static final int TK_NEXT = 0x116;
    static final int TK_COLOR = 0x117;

    private String src;

    Map lexemeToToken = new HashMap();

    public Scan(String src) {
        this.src = src;

        lexemeToToken.put("\\times", TK_TIMES);
        lexemeToToken.put("\\cdot", TK_CDOT);
        lexemeToToken.put("\\div", TK_DIV);
        lexemeToToken.put("\\frac", TK_FRAC);
        lexemeToToken.put("\\dfrac", TK_DFRAC);
        lexemeToToken.put("\\sqrt", TK_SQRT);
        lexemeToToken.put("\\pm", TK_PM);
        lexemeToToken.put("\\sin", TK_SIN);
        lexemeToToken.put("\\cos", TK_COS);
        lexemeToToken.put("\\tan", TK_TAN);
        lexemeToToken.put("\\sec", TK_SEC);
        lexemeToToken.put("\\cot", TK_COT);
        lexemeToToken.put("\\csc", TK_CSC);
        lexemeToToken.put("\\ln", TK_LN);
        lexemeToToken.put("\\leq", TK_LEQ);
        lexemeToToken.put("\\geq", TK_GEQ);
        lexemeToToken.put("\\not", TK_NOT);
        lexemeToToken.put("\\color", TK_COLOR);

    }

    int start() {

        int c;
        lexeme = "";

        while (curIndex < src.length()) {
            switch ((c = src.codePointAt(curIndex++))) {
                case TK_SPACE: // space
                case 9: // tab
                case 10: // new line
                case 13: // carriage return
                    continue;
                case TK_BACKSLASH: // backslash
                    lexeme += String.valueOf((char)c);
                    int token = latex();
                    if ((token == TK_TIMES) || (token == TK_CDOT)) {
                        int nc = src.codePointAt(curIndex);
                        while ((nc == TK_AMP) || (nc == TK_SPACE)) {
                            curIndex++;
                            if (nc == TK_AMP) {
                                //alignment[1]++;
                            }
                            nc = src.codePointAt(curIndex);
                        }
                    }
                    return token;
                case TK_AMP: // &
                    //alignment[0]++;
                    continue;
                case TK_LEFTPAREN:
                case TK_RIGHTPAREN:
                case TK_MUL:
                case TK_ADD:
                case TK_COMMA: // comma
                case TK_SUB:
                case TK_DIV:
                case TK_EQL:
                case TK_LEFTBRACKET:
                case TK_RIGHTBRACKET:
                case TK_CARET:
                case TK_ABS:
                    
                    lexeme += String.valueOf((char)c);
                    advance();
                    
                    return c; // char code is the token id
                    
                case TK_LT:
                    
                    if(src.codePointAt(curIndex++) == TK_EQL)
                    {
                        lexeme += String.valueOf((char)TK_LEQ);
                        advance();
                        return TK_LEQ;
                    }
                    else
                    {
                        lexeme += String.valueOf((char)c);
                    }
                    advance();
                    return c;
                    
                case TK_GT:
                    
                    if(src.codePointAt(curIndex++) == TK_EQL)
                    {
                        lexeme += String.valueOf((char)TK_GEQ);
                        advance();
                        return TK_GEQ;
                    }
                    else
                    {
                        lexeme += String.valueOf((char)c);
                    }
                    advance();
                    return c;
                    
                case TK_SHARP:
                    c = src.codePointAt(curIndex);
                    if (c == TK_LEFTBRACE) {
                        curIndex++;
                    }

                    continue;
                case TK_LEFTBRACE:

                    lexeme += String.valueOf((char)c);
                    return c;
                    
                case TK_RIGHTBRACE:

                    lexeme += String.valueOf((char)c);
                    return c;
                    
                default:
                    /*if (c >= 65 && c <= 90) {
                        lexeme += String.valueOf((char)c);
                        return TK_COEFF;
                    } else */ 
                    if (c >= 65 && c <= 90 || c >= 97 && c <= 122) {
                        lexeme += String.valueOf((char)c);
                        return TK_VAR;
                    } else if (c >= 48
                            && c <= 57) {
							// lexeme += String.valueOf((char)c);
                        // c = src.codePointAt(curIndex++);
                        // return TK_NUM;
                        return number(c);
                    } else {
                        //KhanUtil.assert(false, "scan.start(): c=" + c);
                        return 0;
                    }
            }
        }
        return 0;
    }
    
    void advance()
    {
        int nc = src.codePointAt(curIndex);
        while ((nc == TK_AMP) || (nc == TK_SPACE)) {
            curIndex++;
            if (nc == TK_AMP) {
                //alignment[1]++;
            }
            nc = src.codePointAt(curIndex);
        }
    }

    int number(int c) {
        boolean isDec = false;
        while (c >= 48 && c <= 57 || c == TK_DEC) {
            if(c == TK_DEC)
                isDec = true;
            lexeme += String.valueOf((char)c);
            c = src.codePointAt(curIndex++);
        }
        curIndex--;

        return isDec ? TK_DEC : TK_NUM;
    }

    int latex() {
        int c = src.codePointAt(curIndex++);
        while (c >= 97 && c <= 122) {
            lexeme += String.valueOf((char)c);
            c = src.codePointAt(curIndex++);
        }
        curIndex--;

        Integer tk = (Integer) lexemeToToken.get(lexeme);
        if (tk == null) {
            tk = TK_VAR; // e.g. \\theta
        }
        return tk;
    }

    char readChar() {
        return src.charAt(curIndex++);
    }

    int curIndex = 0;

    String lexeme = "";

    public String lexeme() {
        return lexeme;
    }
    
    
/*
    
%mathpiper_docs,name="MathParse",categories="Programming Procedures,Input/Output,Built In",access="experimental"
*CMD MathParse ---  parse expressions that are in linear traditional format
*CALL
	MathParse(string)

*PARMS
{string} -- a string that contains a math expression


*DESC
This function parse expressions that are in linear traditional format

*E.G.
In> MathParse("2x + 3y")
Result: 2 * _x + 3 * _y

%/mathpiper_docs
*/
    

}
