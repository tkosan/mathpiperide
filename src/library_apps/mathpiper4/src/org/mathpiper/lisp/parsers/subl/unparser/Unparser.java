package org.mathpiper.lisp.parsers.subl.unparser;

import org.mathpiper.lisp.parsers.subl.parser.Atom;
import org.mathpiper.lisp.parsers.subl.parser.Primitives;
import org.mathpiper.lisp.parsers.subl.parser.SExpression;
import org.mathpiper.lisp.parsers.subl.parser.TreeNode;

/*
MIT License

Copyright (c) 2020 Joseph Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

public class Unparser {

    String newLineCharacter = "";
    String spaceCharacter = "";


    protected boolean isMultilineArguments = false;
    
    public Unparser() {

    }

    //private List<Cons> visitedLists = new ArrayList<Cons>();
    
    public String unparse( SExpression aExpression,  boolean isCompact) throws Throwable
    {
        StringOutput out = new StringOutput();
        
        out.write("(");
        print(aExpression, out, isCompact);
        out.write(")");
        
        String output = out.toString();
        
        return output;
    }

    public void print(SExpression aExpression, StringOutput aOutput,  boolean isCompact) throws Throwable {
        if(isCompact)
        {
            newLineCharacter = "";
            spaceCharacter = "";
        }
        else
        {
            newLineCharacter = "\n";
            spaceCharacter = "   ";
        }

        
        printExpression(aExpression, aOutput, 0);

        //visitedLists.clear();
    }


    public void rememberLastChar(char aChar) {
    }


    void printExpression(SExpression aExpression, StringOutput aOutput,  int aDepth /* =0 */) throws Throwable {

        SExpression consWalker = aExpression;
        int item = 0;

        if(consWalker == null)
        {
            aOutput.write("<null>");
        }

        while (consWalker != null && Primitives.CDR(consWalker) != null) {
            
            if (item != 0) {
                indent(aOutput, aDepth + 1);
            }

            if (Primitives.CAR(consWalker) instanceof Atom) {
                TreeNode atom =  Primitives.CAR(consWalker);
                
                aOutput.write(atom.toString());

            } // else print "(", print sublist, and print ")"
            else if (Primitives.CAR(consWalker) instanceof SExpression) {

                aOutput.write("(");
                printExpression(((SExpression) Primitives.CAR(consWalker)), aOutput, aDepth + 1);
                aOutput.write(")");
                item = 0;
              


            } else {
                //aOutput.write("[BuiltinObject]");
            }

            consWalker = (SExpression) Primitives.CDR(consWalker); // print rest element
            
            if(consWalker != null && Primitives.CDR(consWalker) != null)
            {
               aOutput.putChar(' ');
            }

            item++;

        }

    }//end method.


    void indent(StringOutput aOutput, int aDepth) throws Throwable {
        aOutput.write(newLineCharacter);
        
        int i;
        for (i = aDepth; i > 0; i--) {
            aOutput.write(spaceCharacter);
        }
    }

};
