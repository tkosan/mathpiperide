%mathpiper,def="AssociationIndices"

AssociationIndices(associndiceslist_List?) -->
  Reverse!(MapSingle("First",associndiceslist));

%/mathpiper



%mathpiper_docs,name="AssociationIndices",categories="Programming Procedures,Lists (Operations)"
*CMD AssociationIndices --- return the keys in an association list
*STD
*CALL
        AssociationIndices(alist)

*PARMS

{alist} -- association list to examine

*DESC

All the keys in the association list "alist" are assembled in a list
and this list is returned.

*E.G.

In> writer := [];
Result: [];

In> writer["Iliad"] := "Homer";
Result: True;

In> writer["Henry IV"] := "Shakespeare";
Result: True;

In> writer["Ulysses"] := "James Joyce";
Result: True;

In> AssociationIndices(writer);
Result: ["Iliad","Henry IV","Ulysses"];

*SEE Association, AssociationDelete, AssociationValues
%/mathpiper_docs