package org.mathpiper.ui.gui.applications.circuitpiper.controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.event.MouseInputListener;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.EditDialog;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.CircuitElm;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.PotElm;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.SwitchElm;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.VCCSElm;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACCurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACVoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ammeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Block;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CapacitanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Capacitor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CurrentIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ControlledSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.DCVoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.NTerminalComponent;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.InductanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Inductor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ohmmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Resistor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.VoltageIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.Transistor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Potentiometer;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Voltmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Wire;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ChangeACParametersFrame;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ChangePrimaryValueFrame;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ControlledSourceDialog;
import org.mathpiper.ui.gui.applications.circuitpiper.view.DrawingPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.instruments.MultimeterPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.PhasePlane;
import org.mathpiper.ui.gui.applications.circuitpiper.view.PopupMenu;
import org.mathpiper.ui.gui.applications.circuitpiper.view.instruments.ScopePanel;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */
public final class MouseHandler implements MouseMotionListener, MouseInputListener {

    CircuitPanel circuitPanel;
    
    private boolean isJustDoneDragging = false;

    public MouseHandler(final CircuitPanel theParentCircuitEnginePanel) {
        circuitPanel = theParentCircuitEnginePanel;
    }

    //@Override
    public void mouseDragged(MouseEvent theMouseEvent)
    {
        //System.out.println("dragged");
        
        synchronized (circuitPanel.drawingPanel)
        {
            //System.out.println("begin drag");
            // TODO make more elegant
            circuitPanel.setMouseX(theMouseEvent.getX());
            circuitPanel.setMouseY(theMouseEvent.getY());
            
            if (!circuitPanel.isDrawing() && circuitPanel.isMovingPoint())
            {
                handleDragTerminal();
            }
            else if (!circuitPanel.isDrawing() && circuitPanel.isMovingComponent())
            {
                handleDragComponent();
            }
            else
            {
                this.circuitPanel.drawingPanel.setSelectX2(theMouseEvent.getX());
                this.circuitPanel.drawingPanel.setSelectY2(theMouseEvent.getY());
                this.circuitPanel.drawingPanel.repaint();
            }
            //System.out.println("end dragged");
        }
        
        this.isJustDoneDragging = true;
    }

    public void determineHint() {
        if (!circuitPanel.isDrawing()
                && !circuitPanel.isMovingPoint()) {
            if (circuitPanel.isNearTerminal()) {
                circuitPanel.hintNearTerminal();
                circuitPanel.myNearestComponent = null;
                // myParentCircuitEnginePanel.myNearestSwitch = null;
            } else if (circuitPanel.nearSwitch()) {
                circuitPanel.hintNearSwitch();
                circuitPanel.myNearestComponent = null;
            } else if (circuitPanel.isNearComponent()) {
                circuitPanel.hintNearComponent();
                // myParentCircuitEnginePanel.myNearestTerminal=null;
            } else {
                circuitPanel.myNearestComponent = null;
                circuitPanel.myNearestTerminal = null;
                circuitPanel.setHintStarting();
            }
        }
    }

    //@Override
    public void mouseMoved(MouseEvent theMouseEvent) {
        //System.out.println("moved");
    
        //System.out.println("XXX " + theMouseEvent);
    
        //System.out.println("waiting");

        synchronized (circuitPanel.drawingPanel) {
            // System.out.println("moved");
            circuitPanel.setMouseX(theMouseEvent.getX());
            
            circuitPanel.setMouseY(theMouseEvent.getY());
            
            if (circuitPanel.isDrawing()) {
                
                if(circuitPanel.myTempComponent instanceof NTerminalComponent)
                {
                    NTerminalComponent nTerminalComponent = (NTerminalComponent) circuitPanel.myTempComponent;
                    
                    if(circuitPanel.myTempComponent.getHeadTerminal().getX() == circuitPanel.nearestGridPointXPixels())
                    { // Vertical movement.
                        if((circuitPanel.nearestGridPointYPixels() - circuitPanel.myTempComponent.getHeadTerminal().getY()) % (circuitPanel.terminalYSpacing * nTerminalComponent.postsConstraint) == 0)
                        {
                            circuitPanel.myTempComponent.moveTail(circuitPanel.nearestGridPointXPixels(), circuitPanel.nearestGridPointYPixels());
                        }
                    }
                    
                    if(circuitPanel.myTempComponent.getHeadTerminal().getY() == circuitPanel.nearestGridPointYPixels())
                    { // Horizontal movement.
                        if((circuitPanel.nearestGridPointXPixels() - circuitPanel.myTempComponent.getHeadTerminal().getX()) % (circuitPanel.terminalXSpacing * nTerminalComponent.postsConstraint) == 0)
                        {
                            circuitPanel.myTempComponent.moveTail(circuitPanel.nearestGridPointXPixels(), circuitPanel.nearestGridPointYPixels());
                        }
                    }
                }
                else
                {
                    circuitPanel.myTempComponent.moveTail(circuitPanel.nearestGridPointXPixels(), circuitPanel.nearestGridPointYPixels());
                }
            } else {
                determineHint();
            }
            
            circuitPanel.repaint();
        }
    }

    //@Override
    public void mouseClicked(MouseEvent theMouseEvent) {

        /*
        if (theMouseEvent.getButton() == MouseEvent.BUTTON3) {
            circuitPanel.setIsDrawing(false);
            circuitPanel.setHintStarting();
            circuitPanel.repaint();
        }
        */


        if(this.isJustDoneDragging)
        {
            //Terminal.terminalCounter++;
            
            isJustDoneDragging = false;
        }
        
        synchronized (circuitPanel.drawingPanel) {
            //System.out.println("clicked");
            // TODO reduce redundancy
            if (circuitPanel.myNearestComponent == null && theMouseEvent.getClickCount() == 1 && theMouseEvent.getButton() == MouseEvent.BUTTON1) {
                if (circuitPanel.isMovingPoint()) {
                    // todo:tk:Ignore clicks if the mouse is moving.
                    //myParentCircuitEnginePanel.setIsMovingPoint(false);
                }

                if (!circuitPanel.isDrawing()
                        && !circuitPanel.isMovingPoint()
                        && !circuitPanel.isMovingComponent()) {
                    // todo:tk:legitimate first click in drawing mode.

                    if (!circuitPanel.isNearTerminal() && circuitPanel.nearSwitch()) {
                        // todo:tk:if the intent is to flip a switch, then leave.

                        //myParentCircuitEnginePanel.myNearestSwitch.flip();
                        //myParentCircuitEnginePanel.repaint();
                        return;
                    }
                    
                    if (circuitPanel.drawingPanel.isComponentHighlightingMode && circuitPanel.isNearTerminal()) {
                        final Terminal terminal = circuitPanel.myNearestTerminal ;
                        
                        if(terminal.isHighlight)
                        {
                            terminal.isHighlight = false;
                        }
                        else
                        {
                            terminal.isHighlight = true;
                        }
                        
                        return;
                    }
                    
                    circuitPanel.myNearestComponent = null;
                    circuitPanel.myNearestTerminal = null;

                    // clicked

                    // Create a new component.
                    if(circuitPanel.getNearTerminal() == null || ! circuitPanel.getNearTerminal().isInactive)
                    {
                        circuitPanel.setIsDrawing(true);

                        circuitPanel.setHintDrawing();

                        try {
                            circuitPanel.newTempComponent(circuitPanel.mySelectedComponent.replaceAll(" ", "").replace("(todo)", ""),
                                    circuitPanel.nearestGridPointXPixels(),
                                    circuitPanel.nearestGridPointYPixels());

                            circuitPanel.drawingPanel.clickPoint = new Point(circuitPanel.nearestGridPointXPixels(), circuitPanel.nearestGridPointYPixels());

                            circuitPanel.repaint();
                        } catch (Throwable ex) {
                            ex.printStackTrace();
                        }
                    }
                    

                    // Click another point on the canvas to create a component, or click
                    // cancel
                    // g.fillOval((int)(x1+alpha*deltax-4),(int)(y1+beta*deltay-4), 8, 8);
                    
                } else if (circuitPanel.isDrawing()) {
                    // todo:tk:second click, the tail of a new component is being placed.

                    if (circuitPanel.myTempComponent.isBadSize()) {
                        return;
                    }

                    Point headPoint = circuitPanel.myTempComponent.getHeadTerminal().getPosition();

                    Point nearestPoint = new Point(circuitPanel.nearestGridPointXPixels(), circuitPanel.nearestGridPointYPixels());

                    if (circuitPanel.currentCircuit.terminals.containsKey(nearestPoint)
                            && circuitPanel.currentCircuit.terminals.containsKey(headPoint)
                            && circuitPanel.currentCircuit.terminals.get(headPoint).isConnectedTo(circuitPanel.currentCircuit.terminals.get(nearestPoint))) {
                        //todo:tk:is it possible for this condition to be true?
                        return;

                    }

                    circuitPanel.setIsDrawing(false);

                    try
                    {
                        circuitPanel.currentCircuit.addComponent(circuitPanel.myTempComponent, circuitPanel.nearestGridPointXPixels(), circuitPanel.nearestGridPointYPixels());

                        if(circuitPanel.currentCircuit.isCirSim)
                        {
                            circuitPanel.currentCircuit.cirSim.elmList.add(circuitPanel.myTempComponent.getCircuitElm());
                            
                            circuitPanel.currentCircuit.cirSim.needAnalyze();
                        }

                    }
                    catch(Throwable e)
                    {
                        e.printStackTrace(); // todo:tk:maybe show an error dialog here.
                    }
                    
                    determineHint();
                    
                    circuitPanel.drawingPanel.clickPoint = null;
                    
                    circuitPanel.repaint();
                    
                }
            } else if (theMouseEvent.getClickCount() == 1 && theMouseEvent.getButton() == MouseEvent.BUTTON3 ||
                    circuitPanel.myNearestComponent != null && theMouseEvent.getClickCount() == 2 && theMouseEvent.getButton() == MouseEvent.BUTTON1) {

                if (!circuitPanel.isDrawing()
                        && !circuitPanel.isMovingPoint()
                        && !circuitPanel.isMovingComponent()) {
                    
                    if (circuitPanel.isNearTerminal()) {
                     
                        final Terminal nearestTerminal = circuitPanel.myNearestTerminal;
                        
                        JPopupMenu jPopupMenu = new JPopupMenu();
                        
                        JMenuItem deleteMenuItem = new JMenuItem("Delete Terminal");
                        deleteMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                circuitPanel.deleteTerminal(nearestTerminal);
                            }
                        });
                        jPopupMenu.add(deleteMenuItem);
                        
                        JMenuItem nameMenuItem = new JMenuItem("Name Terminal");
                        nameMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                String name = JOptionPane.showInputDialog(null, "New name");
                                nearestTerminal.name = name;
                                circuitPanel.drawingPanel.repaint();
                            }
                        });
                        jPopupMenu.add(nameMenuItem);
                        
                        
                        JMenuItem showInstrumentMenuItem = new JMenuItem("Show Multimeter");
                        showInstrumentMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                
                                MultimeterPanel multimeterPanel = new MultimeterPanel(nearestTerminal);
                                
                                nearestTerminal.addInstrumentPanel(multimeterPanel);
                                
                                multimeterPanel.refresh();
                            }
                        });
                        jPopupMenu.add(showInstrumentMenuItem);
                        
                        
                        jPopupMenu.show(circuitPanel, circuitPanel.getMouseX(),
                                circuitPanel.getMouseY());
                        
                    } else if (circuitPanel.isNearComponent()) {
                        final Component ec = circuitPanel.myNearestComponent;
                        
                        JPopupMenu jPopupMenu = new JPopupMenu();
                        
                        if(circuitPanel.currentCircuit.isCirSim)
                        {
                            JMenuItem change = new JMenuItem("Edit Parameters");
                            change.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {

                                    EditDialog editDialog = new EditDialog(ec.getCircuitElm(), circuitPanel.currentCircuit.cirSim);
                                    editDialog.setModal(true);
                                    editDialog.setVisible(true);
                                }
                            });
                            jPopupMenu.add(change);
                        }
                        else
                        {
                            if (ec instanceof Voltmeter || ec instanceof Ammeter
                                    || ec instanceof CurrentIntegrator || ec instanceof VoltageIntegrator || ec instanceof Ohmmeter
                                    || ec instanceof CapacitanceMeter || ec instanceof InductanceMeter) {

                                JMenuItem graphItem = new JMenuItem("Graph this " + ec.getPrimary().substring(0, 1).toUpperCase() + ec.getPrimary().substring(1));
                                graphItem.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {

                                        ScopePanel scopePanel = new ScopePanel(ec, 0, 0, ec.getID(), ec.getPrimary(), ec.getPrimaryUnitSymbol());
                                        ec.addInstrumentPanel(scopePanel);
                                        //JOptionPane.showMessageDialog(null, "Not available in demo");
                                    }
                                });
                                jPopupMenu.add(graphItem);

                                JMenuItem phasePlaneYItem = new JMenuItem("Make this " + ec.getPrimary().substring(0, 1).toUpperCase()
                                        + ec.getPrimary().substring(1) + " the Y-axis of a phase plane");
                                phasePlaneYItem.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {
                                        // /*demo  
                                        if (circuitPanel.phasePlanes.size() == 0
                                                || (circuitPanel.phasePlanes.getLast().yComponent != null
                                                && circuitPanel.phasePlanes.getLast().xComponent != null)) {
                                            PhasePlane phasePlane = new PhasePlane(circuitPanel, 0, 0);
                                            phasePlane.setY(ec);
                                            circuitPanel.phasePlanes.add(phasePlane);
                                        } else {//if (myParentCircuitEnginePanel.phasePlanes.getLast().xComponent==null){
                                            circuitPanel.phasePlanes.getLast().setY(ec);
                                        }
                                        // */
                                        //      JOptionPane.showMessageDialog(null, "Not available in demo");
                                    }
                                });
                                jPopupMenu.add(phasePlaneYItem);

                                JMenuItem phasePlaneXItem = new JMenuItem("Make this " + ec.getPrimary().substring(0, 1).toUpperCase()
                                        + ec.getPrimary().substring(1) + " the X-axis of a phase plane");
                                phasePlaneXItem.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {
                                        // /*demo   
                                        if (circuitPanel.phasePlanes.size() == 0
                                                || (circuitPanel.phasePlanes.getLast().yComponent != null
                                                && circuitPanel.phasePlanes.getLast().xComponent != null)) {
                                            PhasePlane phasePlane = new PhasePlane(circuitPanel, 0, 0);
                                            phasePlane.setX(ec);
                                            circuitPanel.phasePlanes.add(phasePlane);
                                        } else {//if (myParentCircuitEnginePanel.phasePlanes.getLast().yComponent==null){
                                            circuitPanel.phasePlanes.getLast().setX(ec);
                                        }
                                        //  */
                                        //   JOptionPane.showMessageDialog(null, "Not available in demo");
                                    }
                                });
                                jPopupMenu.add(phasePlaneXItem);
                            }
                            else if (ec instanceof DCVoltageSource || ec instanceof Capacitor || ec instanceof Resistor
                                    || ec instanceof CurrentSource || ec instanceof Inductor) {

                                JMenuItem change = new JMenuItem("Change " + ec.getPrimary());
                                change.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {

                                        if(circuitPanel.currentCircuit.isCirSim)
                                        {
                                            EditDialog editDialog = new EditDialog(ec.getCircuitElm(), circuitPanel.currentCircuit.cirSim);
                                            editDialog.setModal(true);
                                            editDialog.setVisible(true);
                                        }
                                        else
                                        {
                                            new ChangePrimaryValueFrame(circuitPanel, 0, 0, ec);
                                        }
                                    }
                                });
                                jPopupMenu.add(change);
                            }
                            else if (ec instanceof ACVoltageSource || ec instanceof ACCurrentSource) {
                                JMenuItem change = new JMenuItem("Adjust Paramaters");
                                change.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {

                                        new ChangeACParametersFrame(circuitPanel, 0, 0, ec);
                                        //JOptionPane.showMessageDialog(null, "Not available in demo");
                                    }
                                });
                                jPopupMenu.add(change);
                            }
                            else if (ec instanceof ControlledSource) {
                                JMenuItem multipler = new JMenuItem("Expression");
                                multipler.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {

                                        // todo:tk:support for CircuitPiper has been disabled for now.

                                        //ControlledSource controlledSource = (ControlledSource) ec;

                                        VCCSElm controlledElm = (VCCSElm) ec.getCircuitElm();

                                        String valueString = JOptionPane.showInputDialog(null, "Expression", controlledElm.exprString);

                                        if(valueString != null)
                                        {
                                            try
                                            {
                                                //double value = Double.parseDouble(valueString);

                                                controlledElm.setExpr(valueString);
                                                //controlledSource.setMultiplier(value);
                                            }
                                            catch(Throwable nfe)
                                            {
                                                JOptionPane.showMessageDialog(null, "The expression was not formatted correctly.", "", JOptionPane.ERROR_MESSAGE);
                                            }
                                        }
                                    }
                                });
                                jPopupMenu.add(multipler);

                                JMenuItem change = new JMenuItem("Controlling Component");
                                change.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {
                                        new ControlledSourceDialog((ControlledSource) ec, circuitPanel.currentCircuit.components);
                                    }
                                });
                                jPopupMenu.add(change);
                            }
                            else if (ec instanceof CurrentIntegrator || ec instanceof Capacitor
                                    || ec instanceof Inductor || ec instanceof VoltageIntegrator) {
                                JMenuItem resetMenuItem = new JMenuItem("Reset " + ec.getSecondary() + " to 0 " + ec.getSecondaryUnitSymbol());
                                resetMenuItem.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {
                                        ec.setSecondaryValue(0);
                                        if (ec instanceof CurrentIntegrator) {
                                            CurrentIntegrator ci = (CurrentIntegrator) ec;
                                            ci.setChargeString("0.00 C");
                                            circuitPanel.repaint();
                                        }
                                        if (ec instanceof VoltageIntegrator) {
                                            VoltageIntegrator ci = (VoltageIntegrator) ec;
                                            ci.setMagneticFluxString("0.00 Wb");
                                            circuitPanel.repaint();
                                        }
                                    }
                                });
                                jPopupMenu.add(resetMenuItem);
                            }
                            else if (!(ec instanceof Wire) && ! (ec instanceof Resistor)) {
                                JMenuItem reverseOrientationMenuItem = new JMenuItem("Reverse Orientation");
                                reverseOrientationMenuItem.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {
                                        ec.reverse();
                                    }
                                });
                                jPopupMenu.add(reverseOrientationMenuItem);
                            }
                            else if (ec instanceof Transistor) {
                                JMenuItem reverseOrientationMenuItem = new JMenuItem("Mirror");
                                reverseOrientationMenuItem.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {
                                        Transistor transistor = (Transistor) ec;
                                        transistor.mirror();
                                    }
                                });
                                jPopupMenu.add(reverseOrientationMenuItem);
                            }
                            else if (ec instanceof Block) {
                                JMenuItem enterTextMenuItem = new JMenuItem("Text");
                                enterTextMenuItem.addActionListener(new ActionListener() {
                                    public void actionPerformed(final ActionEvent e) {
                                        final JTextArea textArea = new JTextArea(4, 10);

                                        JOptionPane pane = new JOptionPane(textArea, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION) {
                                            @Override
                                            public void selectInitialValue() {
                                                textArea.requestFocusInWindow();
                                            }
                                        };
                                        JDialog dialog = pane.createDialog(null, "Enter text");
                                        dialog.setVisible(true);
                                        dialog.dispose();

                                        Object object = pane.getValue();
                                        if(object instanceof Integer)
                                        {
                                            if(((Integer) object) == JOptionPane.OK_OPTION)
                                            {

                                                Block block = (Block) ec;
                                                block.setText(textArea.getText());
                                            }
                                        }
                                    }
                                });
                                jPopupMenu.add(enterTextMenuItem);
                            }
                        }

                        
                        JMenuItem changeNumberMenuItem = new JMenuItem("Change Subscript");
                        changeNumberMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                String subscript = JOptionPane.showInputDialog(null, "New Subscript");
                                
                                if(subscript.contains(" ") || subscript.contains("\t") || subscript.contains("\n"))
                                {
                                    JOptionPane.showMessageDialog(null, "Subscripts cannot contain whitespace characters.");
                                    return;
                                }
                                
                                String newID = ec.getPrimarySymbol() + subscript;

                                if(circuitPanel.currentCircuit.components.keySet().contains(newID))
                                {
                                    JOptionPane.showMessageDialog(null, "The ID " + newID + " already exists.");
                                }
                                else
                                {
                                    circuitPanel.currentCircuit.components.remove(ec.getID(), ec);
                                    ec.setComponentSubscript(subscript);
                                    circuitPanel.currentCircuit.components.put(newID, ec);
                                    circuitPanel.drawingPanel.repaint();
                                }
                            }
                        });
                        jPopupMenu.add(changeNumberMenuItem);
                        
                        
                        JMenuItem deleteMenuItem = new JMenuItem("Delete");
                        deleteMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                circuitPanel.deleteComponent(ec);
                            }
                        });
                        jPopupMenu.add(deleteMenuItem);
                        
                        
                        jPopupMenu.addSeparator();

                        
                        JMenuItem isHideLabelCheckBox = new JCheckBoxMenuItem("Label Visible");
                        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
                        isHideLabelCheckBox.setSelected(ec.isLabelVisible());
                        isHideLabelCheckBox.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if(e.getStateChange() == ItemEvent.SELECTED)
                                {
                                    ec.setIsLabelVisible(true);
                                }
                                else
                                {
                                    ec.setIsLabelVisible(false);
                                }

                                circuitPanel.drawingPanel.repaint();;
                            }
                        });
                        jPopupMenu.add(isHideLabelCheckBox);

                        
                        JMenuItem isHideValueCheckBox = new JCheckBoxMenuItem("Value Visible");
                        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
                        isHideValueCheckBox.setSelected(ec.isValueVisible());
                        isHideValueCheckBox.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if(e.getStateChange() == ItemEvent.SELECTED)
                                {
                                    ec.setIsValueVisible(true);
                                }
                                else
                                {
                                    ec.setIsValueVisible(false);
                                }

                                circuitPanel.drawingPanel.repaint();;
                            }
                        });
                        jPopupMenu.add(isHideValueCheckBox);
                        
                        
                        JMenuItem isHideUnitCheckBox = new JCheckBoxMenuItem("Unit Visible");
                        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
                        isHideUnitCheckBox.setSelected(ec.isUnitVisible());
                        isHideUnitCheckBox.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if(e.getStateChange() == ItemEvent.SELECTED)
                                {
                                    ec.setIsUnitVisible(true);
                                }
                                else
                                {
                                    ec.setIsUnitVisible(false);
                                }

                                circuitPanel.drawingPanel.repaint();;
                            }
                        });
                        jPopupMenu.add(isHideUnitCheckBox);
                        
                        
                        JMenuItem isHighlightCheckBox = new JCheckBoxMenuItem("Highlight");
                        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
                        isHighlightCheckBox.setSelected(ec.isHighlight);
                        isHighlightCheckBox.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if(e.getStateChange() == ItemEvent.SELECTED)
                                {
                                    ec.isHighlight = true;
                                }
                                else
                                {
                                    ec.isHighlight = false;
                                }

                                circuitPanel.drawingPanel.repaint();
                            }
                        });
                        jPopupMenu.add(isHighlightCheckBox);
                        
                        
                        jPopupMenu.addSeparator();
                        
                        
                        JMenuItem showInstrumentMenuItem = new JMenuItem("Show Multimeter");
                        showInstrumentMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                
                                MultimeterPanel multimeterPanel = new MultimeterPanel(ec);
                                ec.addInstrumentPanel(multimeterPanel);
                                multimeterPanel.refresh();
                            }
                        });
                        jPopupMenu.add(showInstrumentMenuItem);
                        
                        
                        if (ec instanceof Potentiometer)
                        {
                            PotElm potElm = (PotElm) ((Potentiometer) ec).circuitElm;
                            
                            JMenuItem sliderMenuItem = new JMenuItem("Show Adjuster");
                            sliderMenuItem.addActionListener(new ActionListener()
                            {
                                public void actionPerformed(final ActionEvent e)
                                {
                                    circuitPanel.adjustersPanel.add(potElm.sliderPanel);
                                    circuitPanel.adjustersPanel.invalidate();
                                    circuitPanel.adjustersPanel.revalidate();
                                    circuitPanel.adjustersPanel.repaint();
                                }
                            });
                            jPopupMenu.add(sliderMenuItem);

                        }
                        
                        
                        jPopupMenu.addSeparator();
                        
                        
                        showInstrumentMenuItem = new JMenuItem("Show Voltage Oscilloscope");
                        showInstrumentMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                
                                ScopePanel scopePanel = new ScopePanel(ec, 0, 0, ec.getID(), "Voltage", "V");
                                ec.addInstrumentPanel(scopePanel);
                            }
                        });
                        jPopupMenu.add(showInstrumentMenuItem);
                        
                        
                        showInstrumentMenuItem = new JMenuItem("Show Current Oscilloscope");
                        showInstrumentMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                
                                ScopePanel scopePanel = new ScopePanel(ec, 0, 0, ec.getID(), "Current", "A");
                                ec.addInstrumentPanel(scopePanel);
                            }
                        });
                        jPopupMenu.add(showInstrumentMenuItem);
                        
                        
                        showInstrumentMenuItem = new JMenuItem("Show Power Oscilloscope");
                        showInstrumentMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                
                                ScopePanel scopePanel = new ScopePanel(ec, 0, 0, ec.getID(), "Power", "W");
                                ec.addInstrumentPanel(scopePanel);
                            }
                        });
                        jPopupMenu.add(showInstrumentMenuItem);                                                
                                                
                        // ===============================
                        jPopupMenu.show(circuitPanel, circuitPanel.getMouseX(),
                                circuitPanel.getMouseY());
                    }
                }
                else
                {
                    PopupMenu popupMenu = new PopupMenu(circuitPanel);

                    popupMenu.show(circuitPanel, theMouseEvent.getX(), theMouseEvent.getY());
                }
            } 
            else if (circuitPanel.drawingPanel.isComponentHighlightingMode && circuitPanel.isNearComponent()) {
                final Component ec = circuitPanel.myNearestComponent;
                
                if(ec.isHighlight)
                {
                    ec.isHighlight = false;
                }
                else
                {
                    ec.isHighlight = true;
                }
            }
        }
    }

    //@Override
    public void mouseEntered(MouseEvent theMouseEvent) {
        // System.out.println("entered");
        java.awt.Component awtComponent = theMouseEvent.getComponent();
        
        if(! (awtComponent instanceof DrawingPanel))
        {
            circuitPanel.setMouseEntered(true);
            circuitPanel.repaint();
        }
    }

    //@Override
    public void mouseExited(MouseEvent arg0) {
        circuitPanel.setMouseEntered(false);
        if (!circuitPanel.isDrawing()
                && !circuitPanel.isMovingPoint()
                && !circuitPanel.isMovingComponent()) {
            circuitPanel.myNearestComponent = null;
            circuitPanel.myNearestTerminal = null;
            circuitPanel.setHintStarting();
        }
        circuitPanel.repaint();
        // System.out.println("exited");
    }

    //@Override
    public void mousePressed(MouseEvent theMouseEvent) {

        synchronized (circuitPanel.drawingPanel)
        {
            //System.out.println("pressed");
            if (theMouseEvent.getButton() == MouseEvent.BUTTON1)
            {

                circuitPanel.setButtonState(true);

                if (!circuitPanel.isDrawing() && !circuitPanel.isMovingPoint() && !circuitPanel.isMovingComponent())
                {
                    if (!circuitPanel.isNearTerminal() && circuitPanel.nearSwitch())
                    {
                        circuitPanel.myNearestSwitch.flip();
                        
                        if (circuitPanel.currentCircuit.isCirSim)
                        {
                            ((SwitchElm) circuitPanel.myNearestSwitch.getCircuitElm()).toggle();
                            circuitPanel.currentCircuit.cirSim.needAnalyze();
                        }
                        circuitPanel.repaint();
                    }
                    else if (circuitPanel.isNearTerminal())
                    {
                        circuitPanel.setIsMovingPoint(true);
                        circuitPanel.draggedTerminal = circuitPanel.myNearestTerminal;
                        circuitPanel.drawingPanel.repaint();
                    }
                    else if (circuitPanel.isNearComponent())
                    {
                        //myParentCircuitEnginePanel.nearTerminal();//to set Nearest
                        circuitPanel.setIsMovingComponent(true);
                        circuitPanel.myDraggedComponent = circuitPanel.myNearestComponent;
                        circuitPanel.draggedTerminal = circuitPanel.myNearestTerminal;
                    }
                    else
                    {
                        this.circuitPanel.drawingPanel.setSelectX1(theMouseEvent.getX());
                        this.circuitPanel.drawingPanel.setSelectY1(theMouseEvent.getY());
                    }
                }
            }
        }
    }

    //@Override
    public void mouseReleased(MouseEvent theMouseEvent) {

        synchronized (circuitPanel.drawingPanel)
        {
            if (theMouseEvent.getButton() == MouseEvent.BUTTON1)
            {
                circuitPanel.setButtonState(false);
                
                if (circuitPanel.isMovingPoint())
                {
                    Point point = new Point(circuitPanel.draggedTerminal.getX(), circuitPanel.draggedTerminal.getY());

                    for (Component component : circuitPanel.currentCircuit.components.values())
                    {
                        if (component.getHeadTerminal() == circuitPanel.draggedTerminal)
                        {
                            component.setHead(circuitPanel.currentCircuit.terminals.get(point));
                        }
                        else if (component.getTailTerminal() == circuitPanel.draggedTerminal)
                        {
                            component.setTail(circuitPanel.currentCircuit.terminals.get(point));
                        }
                    }

                    circuitPanel.draggedTerminal = null;
                    circuitPanel.myDraggedComponent = null;
                    circuitPanel.setIsMovingPoint(false);
                    circuitPanel.setIsMovingComponent(false);
                    circuitPanel.drawingPanel.terminalsDrag = null;
                    if (circuitPanel.currentCircuit.isCirSim)
                    {
                        circuitPanel.currentCircuit.cirSim.needAnalyze();
                    }
                }
                else if (circuitPanel.isMovingComponent())
                {
                    circuitPanel.draggedTerminal = null;
                    circuitPanel.myDraggedComponent = null;
                    circuitPanel.setIsMovingPoint(false);
                    circuitPanel.setIsMovingComponent(false);
                    if (circuitPanel.currentCircuit.isCirSim)
                    {
                        circuitPanel.currentCircuit.cirSim.needAnalyze();
                    }
                }
                else
                {
                    circuitPanel.drawingPanel.terminalsDrag = new HashMap();
                    
                    for(Terminal terminal : circuitPanel.currentCircuit.terminals.values())
                    {
                        if(circuitPanel.drawingPanel.isTerminalInSelection(terminal))
                        {
                            circuitPanel.drawingPanel.terminalsDrag.put(terminal.getPosition(), terminal);
                        }
                    }
                    
                    circuitPanel.drawingPanel.setSelectX1(-1);
                    circuitPanel.drawingPanel.setSelectY1(-1);
                    circuitPanel.drawingPanel.setSelectX2(-1);
                    circuitPanel.drawingPanel.setSelectX2(-1);
                }
                
                determineHint();
                
                circuitPanel.repaint();
            }
            //System.out.println("released");
        }
    }

    public void handleDragTerminal()
    {
        if(circuitPanel.draggedTerminal.isCalculated == true)
        {
            return;
        }
        
        boolean isDragged = false;
        
        List nTerminalComponents = circuitPanel.draggedTerminal.getConnectedNTerminalComponents();
        
        if ((circuitPanel.nearestGridPointXPixels() != circuitPanel.draggedTerminal.getX() ||
            circuitPanel.nearestGridPointYPixels() != circuitPanel.draggedTerminal.getY()) &&
            !(nTerminalComponents.size() > 1) // todo:tk:moving a terminal that has more than 1 transistor attached to it is too complex to handle for now.
                )
        {// If a terminal has been dragged to a different grid point.
            Point newGridlocation = new Point(circuitPanel.nearestGridPointXPixels(), circuitPanel.nearestGridPointYPixels());
            
            Point oldGridLocation = new Point(circuitPanel.draggedTerminal.getX(), circuitPanel.draggedTerminal.getY());

            if (!circuitPanel.currentCircuit.terminals.containsKey(newGridlocation))
            {// If we are dragging to an unoccupied location.
                
                if(nTerminalComponents.size() == 1)
                {
                    NTerminalComponent nTerminalComponent = circuitPanel.draggedTerminal.getConnectedNTerminalComponents().get(0);

                    Terminal otherTerminal = (circuitPanel.draggedTerminal == nTerminalComponent.getTailTerminal()) ? nTerminalComponent.getHeadTerminal(): nTerminalComponent.getTailTerminal();

                    if(otherTerminal.getX() == circuitPanel.nearestGridPointXPixels())
                    { // Vertical movement.
                        if((circuitPanel.nearestGridPointYPixels() - otherTerminal.getY()) % (circuitPanel.terminalYSpacing * nTerminalComponent.postsConstraint) == 0)
                        {
                            circuitPanel.currentCircuit.terminals.remove(oldGridLocation);
                            circuitPanel.draggedTerminal.setX(circuitPanel.nearestGridPointXPixels());
                            circuitPanel.draggedTerminal.setY(circuitPanel.nearestGridPointYPixels());
                            circuitPanel.currentCircuit.terminals.put(newGridlocation, circuitPanel.draggedTerminal);
                            
                            for(Terminal calculatedTerminal : nTerminalComponent.calculatedTerminals)
                            {
                                circuitPanel.currentCircuit.terminals.remove(calculatedTerminal.getPosition());
                                nTerminalComponent.setNTerminalPositions();
                                circuitPanel.currentCircuit.terminals.put(calculatedTerminal.getPosition(), calculatedTerminal);
                            }
                        }
                    }

                    if(otherTerminal.getY() == circuitPanel.nearestGridPointYPixels())
                    { // Horizontal movement.
                        if((circuitPanel.nearestGridPointXPixels() - otherTerminal.getX()) % (circuitPanel.terminalXSpacing * nTerminalComponent.postsConstraint) == 0)
                        {
                            circuitPanel.currentCircuit.terminals.remove(oldGridLocation);
                            circuitPanel.draggedTerminal.setX(circuitPanel.nearestGridPointXPixels());
                            circuitPanel.draggedTerminal.setY(circuitPanel.nearestGridPointYPixels());
                            circuitPanel.currentCircuit.terminals.put(newGridlocation, circuitPanel.draggedTerminal);
                            
                            for(Terminal calculatedTerminal : nTerminalComponent.calculatedTerminals)
                            {
                                circuitPanel.currentCircuit.terminals.remove(calculatedTerminal.getPosition());
                                nTerminalComponent.setNTerminalPositions();
                                circuitPanel.currentCircuit.terminals.put(calculatedTerminal.getPosition(), calculatedTerminal);
                            }
                        }
                    }
                }
                else
                {
                    //if (circuitPanel.currentCircuit.terminals.get(oldGridLocation) == circuitPanel.draggedTerminal)
                    //{// If there is no point left behind.
                    circuitPanel.currentCircuit.terminals.remove(oldGridLocation);
                    //}
                    circuitPanel.draggedTerminal.setX(circuitPanel.nearestGridPointXPixels());
                    circuitPanel.draggedTerminal.setY(circuitPanel.nearestGridPointYPixels());
                    circuitPanel.currentCircuit.terminals.put(newGridlocation, circuitPanel.draggedTerminal);
                }
                
                isDragged = true;
            }
            else
            {//if there is already a point here
                
                if(circuitPanel.draggedTerminal.getConnectedNTerminalComponents().size() != 0)
                {
                    //todo:tk:do not support dragging of three terminal components at this time.
                    return;
                }
                
                if (!circuitPanel.draggedTerminal.isConnectedTo(circuitPanel.currentCircuit.terminals.get(newGridlocation))
                        && !circuitPanel.draggedTerminal.isConnectedToOrder2(circuitPanel.currentCircuit.terminals.get(newGridlocation)))
                {//that is, if the new point at the new location isn't connectod to the old point
                    if (circuitPanel.currentCircuit.terminals.get(oldGridLocation) == circuitPanel.draggedTerminal)
                    {//that is, if there is no point left behind after drag
                        circuitPanel.currentCircuit.terminals.remove(oldGridLocation);
                    }
                    
                    circuitPanel.draggedTerminal.setX(circuitPanel.nearestGridPointXPixels());
                    circuitPanel.draggedTerminal.setY(circuitPanel.nearestGridPointYPixels());
                }
                
                isDragged = true;
            }
            
            
            if(isDragged && this.circuitPanel.drawingPanel.terminalsDrag != null)
            {
                List<Terminal> newTerminalsList = new ArrayList<>();
                newTerminalsList.add(circuitPanel.draggedTerminal);
                
                Map<Point, Terminal> oldAndNewPositionsMap = new HashMap<>();

                int xDifference = circuitPanel.draggedTerminal.getX() - oldGridLocation.x;
                int yDifference = circuitPanel.draggedTerminal.getY() - oldGridLocation.y;

                for(Terminal terminal : circuitPanel.drawingPanel.terminalsDrag.values())
                {
                    if(terminal != circuitPanel.draggedTerminal)
                    {
                        Point oldPosition = terminal.getPosition();
                        circuitPanel.currentCircuit.terminals.remove(oldPosition);
                        terminal.setX(terminal.getX() + xDifference);
                        terminal.setY(terminal.getY() + yDifference);
                        newTerminalsList.add(terminal);
                        oldAndNewPositionsMap.put(oldPosition, terminal);
                    }
                }

                for(Terminal terminal : newTerminalsList)
                {
                    circuitPanel.currentCircuit.terminals.put(terminal.getPosition(), terminal);
                }

                if(circuitPanel.currentCircuit.isCirSim)
                {
                    
                    for(CircuitElm cirElm : circuitPanel.currentCircuit.cirSim.elmList)
                    {
                       Terminal headTerminal = oldAndNewPositionsMap.get(new Point(cirElm.x1, cirElm.y1));
                        
                        if(headTerminal != null)
                        {
                            cirElm.x1 = headTerminal.getX();
                            cirElm.y1 = headTerminal.getY();
                            cirElm.setPoints();
                        }

                       Terminal tailTerminal = oldAndNewPositionsMap.get(new Point(cirElm.x2, cirElm.y2));
                        
                        if(tailTerminal != null)
                        {
                            cirElm.x2 = tailTerminal.getX();
                            cirElm.y2 = tailTerminal.getY();
                            cirElm.setPoints();
                        }
                    }
                }
            }
            
            circuitPanel.repaint();
        }
    }

    public void handleDragComponent() {
        
        if(circuitPanel.myDraggedComponent instanceof NTerminalComponent)
        {
            //todo:tk:do not support dragging of three terminal components at this time.
            return;
        }
        
        if (circuitPanel.nearestGridPointXPixels() != circuitPanel.draggedTerminal.getX()
                || circuitPanel.nearestGridPointYPixels() != circuitPanel.draggedTerminal.getY()) 
        {
            
            //System.out.println("dragged comp");
            Point point = new Point(circuitPanel.nearestGridPointXPixels(),
                    circuitPanel.nearestGridPointYPixels());
            
            Point oldTerminalPoint = new Point(circuitPanel.draggedTerminal.getX(),
                    circuitPanel.draggedTerminal.getY());
            
            if (circuitPanel.isNearTerminal()) {//must be called because this method has side effects
                //System.out.println("connected");
            }
            
            Integer oldTerminalNumber = null; 
            
            if (circuitPanel.draggedTerminal.myConnectedTo.size() == 1
                    && !(circuitPanel.currentCircuit.terminals.containsKey(point)
                    && ((circuitPanel.myDraggedComponent.getHeadTerminal() == circuitPanel.draggedTerminal
                    && (circuitPanel.myDraggedComponent.getTailTerminal() == circuitPanel.myNearestTerminal
                    || circuitPanel.myDraggedComponent.getTailTerminal().isConnectedTo(circuitPanel.myNearestTerminal)))
                    || (circuitPanel.myDraggedComponent.getTailTerminal() == circuitPanel.draggedTerminal
                    && (circuitPanel.myDraggedComponent.getHeadTerminal() == circuitPanel.myNearestTerminal
                    || circuitPanel.myDraggedComponent.getHeadTerminal().isConnectedTo(circuitPanel.myNearestTerminal))))) //!(myParentCircuitEnginePanel.myGridPoints.containsKey(point)&&
                    //  (myParentCircuitEnginePanel.myDraggedTerminal
                    // .isConnectedTo(myParentCircuitEnginePanel.myGridPoints.get(point))||
                    // myParentCircuitEnginePanel.myDraggedTerminal
                    //.isConnectedToOrder2(myParentCircuitEnginePanel.myGridPoints.get(point))))
                    ) {
                //System.out.println("r2");
                //System.out.prntln();
                circuitPanel.currentCircuit.terminals.remove(oldTerminalPoint);
                oldTerminalNumber = circuitPanel.draggedTerminal.terminalNumber;
            }
            
            if (!circuitPanel.currentCircuit.terminals.containsKey(point)) {//that is if there is no point at this location yet
            
                try
                {
                    Terminal newTerminal = new Terminal(circuitPanel.nearestGridPointXPixels(),
                        circuitPanel.nearestGridPointYPixels(), circuitPanel.currentCircuit, oldTerminalNumber);

                    circuitPanel.currentCircuit.terminals.put(point, newTerminal);//then put a point there

                    if (circuitPanel.myDraggedComponent.getHeadTerminal()
                            == circuitPanel.draggedTerminal) {//and set the location of the head
                        circuitPanel.myDraggedComponent
                                .setHead(circuitPanel.currentCircuit.terminals.get(point));
                        circuitPanel.draggedTerminal = circuitPanel.myDraggedComponent.getHeadTerminal();
                    } else {//or tail of the dragged component accordingly
                        circuitPanel.myDraggedComponent
                                .setTail(circuitPanel.currentCircuit.terminals.get(point));
                        //myParentCircuitEnginePanel.myDraggedComponent.setTail(newTerminal);
                        circuitPanel.draggedTerminal = circuitPanel.myDraggedComponent.getTailTerminal();
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            } else {//if there is a point here already
                //myParentCircuitEnginePanel.myGridPoints.put(point, newTerminal);
                // System.out.println("4");
                if ((circuitPanel.myDraggedComponent.getHeadTerminal() == circuitPanel.draggedTerminal
                        && (circuitPanel.myDraggedComponent.getTailTerminal() == circuitPanel.myNearestTerminal
                        || circuitPanel.myDraggedComponent.getTailTerminal().isConnectedTo(circuitPanel.myNearestTerminal)))//if this is the head and is connectedorder2 to nearesttiepoint 
                        || (circuitPanel.myDraggedComponent.getTailTerminal() == circuitPanel.draggedTerminal
                        && (circuitPanel.myDraggedComponent.getHeadTerminal() == circuitPanel.myNearestTerminal
                        || circuitPanel.myDraggedComponent.getHeadTerminal().isConnectedTo(circuitPanel.myNearestTerminal))) //myParentCircuitEnginePanel.myDraggedTerminal
                        //.isConnectedTo(myParentCircuitEnginePanel.myGridPoints.get(point))||
                        //myParentCircuitEnginePanel.myDraggedTerminal
                        //.isConnectedToOrder2(myParentCircuitEnginePanel.myGridPoints.get(point))
                        // myParentCircuitEnginePanel.myNearestTerminal.isConnectedTo(
                        // myParentCircuitEnginePanel.myDraggedTerminal)||
                        //myParentCircuitEnginePanel.myNearestTerminal.isConnectedToOrder2(
                        //myParentCircuitEnginePanel.myDraggedTerminal)
                        ) {
                    //System.out.println("connected");
                    //System.out.println(myParentCircuitEnginePanel.myNearestTerminal.getY());
                    //System.out.println(myParentCircuitEnginePanel.myDraggedTerminal.getY());
                    return;//then don't do anything, we can't move here
                }
                
                // If we reach this point, it means that we are moving the component to a location that already has a terminal.
                if (circuitPanel.myDraggedComponent.getHeadTerminal()
                        == circuitPanel.draggedTerminal) {//if we're moving the head of draggedcomp
                    circuitPanel.myDraggedComponent
                            .setHead(circuitPanel.currentCircuit.terminals.get(point));//then move the head
                    circuitPanel.draggedTerminal = circuitPanel.myDraggedComponent.getHeadTerminal();
                } else {
                    circuitPanel.myDraggedComponent
                            .setTail(circuitPanel.currentCircuit.terminals.get(point));
                    //myParentCircuitEnginePanel.myDraggedComponent.setTail(newTerminal);
                    circuitPanel.draggedTerminal = circuitPanel.myDraggedComponent.getTailTerminal();
                }
            }
            circuitPanel.myNearestTerminal = circuitPanel.draggedTerminal;
            circuitPanel.repaint();
        }

    }
}
