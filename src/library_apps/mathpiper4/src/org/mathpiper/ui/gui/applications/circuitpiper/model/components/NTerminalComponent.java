package org.mathpiper.ui.gui.applications.circuitpiper.model.components;

import java.awt.geom.Point2D;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.Point;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

public abstract class NTerminalComponent extends Component
{
    
    protected int mirrorOrientation = 1;
    public Terminal[] calculatedTerminals;
    public int postsConstraint = 1;
    protected int headTerminalX;
    protected int headTerminalY;
    protected int tailTerminalX;
    protected int tailTerminalY;
    protected double centerX;
    protected double centerY;
    protected double headX;
    protected double headY;
    protected double tailX;
    protected double tailY;
    protected Point2D.Double[] calculatedPoints;
    protected int rise;
    protected int run;
    protected double divisor;
    protected int distanceBetweenTerminalsSquared;
    protected double distanceBetweenTerminals;
    protected double setback;
    protected double setback2;
    protected double sine;
    protected double cosine;
    protected double hypotenuse; // Distance between terminals.

    public NTerminalComponent(int x, int y, Circuit circuit)
    {
        super(x, y, circuit);
    }
    
    public NTerminalComponent(int x, int y, String uid, Circuit circuit)
    {
        super(x, y, circuit);
    }
    

    protected void calculateGeometry()
    {
        if(getHeadTerminal() == null || getTailTerminal() == null)
        {
            return;
        }
        
        headTerminalX = getHeadTerminal().getX();
        headTerminalY = getHeadTerminal().getY();
        tailTerminalX = getTailTerminal().getX();
        tailTerminalY = getTailTerminal().getY();
        rise = tailTerminalY - headTerminalY;
        run = tailTerminalX - headTerminalX;
        distanceBetweenTerminalsSquared = (rise * rise + run * run);
        distanceBetweenTerminals = Math.sqrt(distanceBetweenTerminalsSquared);
        centerX = (headTerminalX + tailTerminalX) / 2;
        centerY = (headTerminalY + tailTerminalY) / 2;
        hypotenuse = Math.sqrt(rise * rise + run * run); // Distance between terminals.
        sine = run / hypotenuse;
        cosine = rise / hypotenuse;
    }

    public void setHeadTerminal(Terminal headTerminal)
    {
        super.setHeadTerminal(headTerminal);
        if (calculatedTerminals != null)
        {
            setNTerminalPositions();
        }
    }

    public void setTailTerminal(Terminal tailTerminal)
    {
        super.setTailTerminal(tailTerminal);
        setNTerminalPositions();
    }
    
    public void setCalculatedTerminal(Terminal calculatedTerminal, int index)
    {
        calculatedTerminals[index] = calculatedTerminal;
    }

    public void setNTerminalPositions()
    {
        int index = 0;
        
        if (calculatedTerminals != null)
        {
            for(Point calculatedPoint : this.calculatedPointsToGridPoints())
            {
                if(this.calculatedTerminals[index] != null)
                {
                    this.calculatedTerminals[index].setX(calculatedPoint.x);
                    this.calculatedTerminals[index].setY(calculatedPoint.y);
                }
                
                index++;
            }
        }
    }

    public void connectHeadAndTail()
    {
        super.connectHeadAndTail();
        
        if (calculatedTerminals != null)
        {
            for(Terminal terminal : calculatedTerminals)
            {
                if(terminal != null)
                {
                    terminal.myConnectedTo.add(getHeadTerminal());
                }
            }
        }
    }

    public void disconnectHeadAndTail()
    {
        super.disconnectHeadAndTail();
        
        if (calculatedTerminals != null)
        {            
            for(Terminal terminal : calculatedTerminals)
            {
                if(terminal != null)
                {
                    terminal.myConnectedTo.remove(getHeadTerminal());
                }
            }
        }
    }

    public Point[] calculatedPointsToGridPoints()
    {
        calculateGeometry();
        
        Point[] gridPoints = new Point[calculatedPoints.length];
        
        int index = 0;
        
        while(index < calculatedPoints.length)
        {
            gridPoints[index] = new Point(circuit.circuitPanel.nearestGridPointXPixels((int) Math.round(calculatedPoints[index].x)), circuit.circuitPanel.nearestGridPointXPixels((int) Math.round(calculatedPoints[index].y)));
            index++;
        }
        
        return gridPoints;
    }
    
    public void draw(ScaledGraphics sg) throws Exception
    {
        super.draw(sg);
    }

    public int getLabelDistance()
    {
        return 20;
    }

    public void mirror()
    {
        mirrorOrientation = mirrorOrientation * -1;
    }
    
}
