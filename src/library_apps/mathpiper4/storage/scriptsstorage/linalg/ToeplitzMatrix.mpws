%mathpiper,def="ToeplitzMatrix"

// The arguments of the following functions should be checked
ToeplitzMatrix(N):=BuildMatrix([[i,j],N[Abs(i-j)+1]], Length(N), Length(N) );

%/mathpiper



%mathpiper_docs,name="ToeplitzMatrix",categories="Mathematics Functions;Matrices (Special)"
*CMD ToeplitzMatrix --- create a Toeplitz matrix
*STD
*CALL
        ToeplitzMatrix(N)
*PARMS

{N} -- an $n$-dimensional row vector

*DESC

The procedure {ToeplitzMatrix} calculates the Toeplitz matrix given
an $n$-dimensional row vector. This matrix has the same entries in
all diagonal columns, from upper left to lower right.

*E.G.

In> UnparseMath2D(ToeplitzMatrix([1,2,3,4,5]))

        /                                \
        | ( 1 ) ( 2 ) ( 3 ) ( 4 ) ( 5 )  |
        |                                |
        | ( 2 ) ( 1 ) ( 2 ) ( 3 ) ( 4 )  |
        |                                |
        | ( 3 ) ( 2 ) ( 1 ) ( 2 ) ( 3 )  |
        |                                |
        | ( 4 ) ( 3 ) ( 2 ) ( 1 ) ( 2 )  |
        |                                |
        | ( 5 ) ( 4 ) ( 3 ) ( 2 ) ( 1 )  |
        \                                /
%/mathpiper_docs