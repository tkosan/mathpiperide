package vmm.xm3d;

import java.awt.Graphics2D;
import vmm.core.Display;
import vmm.core.View;
import vmm.core3D.Exhibit3D;
import vmm.core3D.Transform3D;
import vmm.core3D.Vector3D;
import vmm.core3D.View3D;
import vmm.spacecurve.SpaceCurveView;

public class Test3 extends Exhibit3D
{

    private double Diameter = 10.0;

    private Vector3D Xaxis = new Vector3D(10, 0, 0);
    private Vector3D Yaxis = new Vector3D(0, 10, 0);
    private Vector3D Zaxis = new Vector3D(0, 0, 10);

    protected void doDraw3D(Graphics2D g, View3D view, Transform3D transform)
    {

        Vector3D origin = new Vector3D(0, 0, 0);
        view.drawDot(origin, Diameter);
        view.drawString("Origin", origin);

        view.drawDot(Xaxis, Diameter);
        view.drawString("X-Axis", Xaxis);

        view.drawDot(Yaxis, Diameter);
        view.drawString("Y-Axis", Yaxis);

        view.drawDot(Zaxis, Diameter);
        view.drawString("Z-Axis", Zaxis);

        view.drawLine(origin, Xaxis);
        view.drawLine(origin, Yaxis);
        view.drawLine(origin, Zaxis);
    }

    public static void main(String[] args)
    {
        // you can put as many points in the array list as warranted

        Test3 exhibit = new Test3();

        View view = exhibit.getDefaultView();

        WindowXM window = new WindowXM(Menus.SINGLE_GALLERY);

        //window.getMenus();
        if (exhibit != null)
        {
            window.getMenus().install(view, exhibit);

        }

        //Support support = ((UserExhibit) exhibit).getUserExhibitSupport();
        Display display = window.getDisplay();

        exhibit.computeDrawData(view, true, exhibit.previousTransform3D, exhibit.previousTransform3D);
        display.install(view, exhibit);

        window.setVisible(true);

    }//end main.
}
