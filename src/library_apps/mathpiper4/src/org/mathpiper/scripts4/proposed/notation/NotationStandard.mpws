%mathpiper,def="NotationStandard"

10 ## NotationStandard(value_Number?`unit_) -->
{
     /*
        NotationStandard() takes either a real number or a real number with units.
        
        If a unitless number is passed to NotationStandard(), the procedure will:
            -Removes all exponents
            
        If a number with a unit is passed to NotationStandard(), the procedure will:
            -Removes all exponents
            -Reduces all SI to their congruent SI equivalents, and multiplies the value by the correct power of 10 which was represented by any SI prefixes. 
     */
     
    Local(finalProcessing, result);
     
    finalProcessing :=
    [
        ((x_Number? * y_) * z_) :: Not?(Number?(z)) |? Not?(Number?(y)) -> (x * (y * z)),
        ((s_Number? * u_) * (r_Number? * v_)) :: Not?(Number?(u)) |? Not?(Number?(v)) ->   ((s * r) * (u * v)),
        x_ * (y_Number? * z_) :: Not?(Number?(x)) &? Not?(Number?(z)) -> y * (x *z),
        (x_Number? * y_) / z_ :: Not?(Number?(y)) &? Not?(Number?(z)) -> /` '(@x * (@y / @z)),
        (x_Number? * y_)^z_Number? :: Not?(Number?(y)) -> NM(x^z) * (y^z),
    ];

    value := NM(value);
    unit := NM(unit);
    
    //Remove exponents:
    value := NotationDecimal(value);
          
    
    If(unit !=? 1)
    {
        unit := UnitsReduce(unit /:: ?sizeConversions); 
        If(Not?(Number?(unit) |? Rational?(unit)))
        {
            result := NM(value`unit);
        }
        Else
        {
            result := NM(value*unit);
        }
    }
    Else
    {
       result := value;
    }
    
    Eval(result /:: finalProcessing);
}

15 ## NotationStandard(value_UnderscoreConstant?`unit_) --> value`unit;

20 ## NotationStandard(expression_) --> (expression /: [vv_`uu_ -> NotationStandard(vv`uu)]);

%/mathpiper





%mathpiper_docs,name="NotationStandard",categories="Mathematics Procedures,Units"
*CMD NotationStandard --- takes a magnitude and replaces all SI units with their prefixless counterparts.

*CALL
        NotationStandard(magnitude)

*PARMS

{magnitude}    -- A number or a number with units


*DESC

The {NotationStandard} procedure takes a {magnitude}, a number or a 
number with a unit, and converts all SI units, if they are present, to
their prefixless form by removing all prefixed units and multiplying the
number part of the {magnitude} by the appropriate factor. All SI  units 
will be reduced to a prefixless. All non-SI units will remain  unchanged.
If the {magnitude} passed to {NotationStandard} has no SI  units or no 
units at all, then the {magnitude} will be returned unchanged

*EXAMPLES

*SEE NotationCongruent, NotationScientific, NotationEngineering

%/mathpiper_docs




%mathpiper,name="NotationStandard",subtype="in_prompts"

ToString(NotationStandard(15`(mA))) ~> "0.015`A"
ToString(NotationStandard(15.3)) ~> "15.3"
ToString(NotationStandard(42`ft)) ~> "42.0`ft"
ToString(NotationStandard(31231`(g*mA/(nm * hour)))) ~> "31231000000`((A * g) / (m * hour))"
ToString(NotationStandard(4`dag)) ~> "40.0`g"

%/mathpiper





%mathpiper,name="NotationStandard",subtype="automatic_test"

Verify(ToString(NotationStandard(1.3`kilotesla)), "1300.0`tesla");
Verify(ToString(NotationStandard(1.3`(dekatesla/T))), "13.0`(tesla / T)");
Verify(ToString(NotationStandard(4`(dag/kg))), "0.04");

%/mathpiper
