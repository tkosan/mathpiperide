/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.util.HashMap;
import java.util.Map;
import org.hipparchus.analysis.UnivariateFunction;
import org.hipparchus.analysis.integration.RombergIntegrator;
import org.hipparchus.analysis.integration.SimpsonIntegrator;
import org.hipparchus.analysis.integration.TrapezoidIntegrator;
import org.hipparchus.analysis.integration.UnivariateIntegrator;
import org.hipparchus.analysis.integration.gauss.GaussIntegrator;
import org.hipparchus.analysis.integration.gauss.GaussIntegratorFactory;
import org.hipparchus.exception.LocalizedCoreFormats;
import org.hipparchus.exception.MathIllegalArgumentException;
import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;
import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *
 */
public class IntegrateN extends BuiltinProcedure {

    private Map defaultOptions;

    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "IntegrateN";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 4, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Method", "Romberg");
        defaultOptions.put("MaxPoints", new BigNumber("200", 10, 10));
        defaultOptions.put("MaxIterations", new BigNumber("500", 10, 10));
    }
    
    /*
    todo:tk
    In> IntegrateN(_t^2, 0, 1)
    Result: Exception
    Exception: java.lang.NullPointerException: null. Starting at index 0.
    */

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        
        
        Cons expWithUnits = getArgument(aEnvironment, aStackTop, 1);
        Cons notationStandardExp = Utility.lispEvaluate(aEnvironment, aStackTop, "NotationStandard", expWithUnits);
        Cons expWithoutUnits = Utility.lispEvaluate(aEnvironment, aStackTop, "UnitsStripAll", notationStandardExp);

        Cons varWithUnits = getArgument(aEnvironment, aStackTop, 2);
        Cons varWithoutUnits = Utility.lispEvaluate(aEnvironment, aStackTop, "UnitsStripAll", varWithUnits);
        
        BigNumber min = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 3);
        BigNumber max = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 4);
        
        Cons lastArgument = getArgument(aEnvironment, aStackTop, 4);
        
        Cons options = null;

        if(Utility.isSublist(lastArgument))
        {
            lastArgument = (Cons) lastArgument.car(); //Go to sub list.

            lastArgument = lastArgument.cdr(); //Strip List tag.

            options = lastArgument.cdr();
        }
            
        final Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        
        if(! (userOptions.get("Method") instanceof String)) LispError.throwError(aEnvironment, aStackTop, "The value of the \"Method\" option must be a string.");
        String method = (String) userOptions.get("Method");
        
        if((! (userOptions.get("MaxPoints") instanceof BigNumber))  &&  (! ((BigNumber) userOptions.get("MaxPoints")).isInteger())) LispError.throwError(aEnvironment, aStackTop, "The value of the \"MaxPoints\" option must be an integer.");
        int maxPoints = ((BigNumber) userOptions.get("MaxPoints")).toInt();

        if((! (userOptions.get("MaxIterations") instanceof BigNumber))  &&  (! ((BigNumber) userOptions.get("MaxIterations")).isInteger())) LispError.throwError(aEnvironment, aStackTop, "The value of the \"MaxIterations\" option must be an integer.");
        int maxIterations = ((BigNumber) userOptions.get("MaxIterations")).toInt();

        
        UnivariateFunction objectiveFunction = new Procedure(aEnvironment, aStackTop, expWithoutUnits, varWithoutUnits);
        
        Double result = null;

        UnivariateIntegrator integrator = null;

        if (method.equals("Simpson")) {
            integrator = new SimpsonIntegrator();
        } else if (method.equals("Romberg")) {
            integrator = new RombergIntegrator();
        } else if (method.equals("Trapezoid")) {
            integrator = new TrapezoidIntegrator();
        } else if (method.equals("LegendreGauss")) {
            if (maxPoints > 1000) {
                // See https://github.com/Hipparchus-Math/hipparchus/issues/61
                throw new MathIllegalArgumentException(LocalizedCoreFormats.NUMBER_TOO_LARGE, maxPoints, 1000);
            }
            GaussIntegratorFactory factory = new GaussIntegratorFactory();
            GaussIntegrator integ = factory.legendre(maxPoints, min.toDouble(), max.toDouble());
            result = integ.integrate(objectiveFunction);
        } else {
            LispError.throwError(aEnvironment, aStackTop, "Invalid method: " + method);
        }

        if(integrator != null)
        {
            result = integrator.integrate(maxIterations, objectiveFunction, min.toDouble(), max.toDouble());
        }
        
        BigNumber resultValue = new BigNumber(aEnvironment.getPrecision());

        resultValue.setTo(result);
        
        Cons resultCons = new org.mathpiper.lisp.cons.NumberCons(resultValue);
        
        if(Utility.toString(aEnvironment, aStackTop, expWithUnits).contains("`"))
        {
            Cons unitsOperatorCons = AtomCons.getInstance(10, "`");
            
            unitsOperatorCons.setCdr(resultCons);
            
            resultCons.setCdr(AtomCons.getInstance(10, "_"));
            
            resultCons = SublistCons.getInstance(unitsOperatorCons);
        }

        setTopOfStack(aEnvironment, aStackTop, resultCons);
    }
}

class Procedure implements UnivariateFunction {

    Cons exp;
    Environment aEnvironment;
    int aStackTop;
    Cons from;
    BigNumber bnum = new BigNumber(10);

    public Procedure(Environment aEnvironment, int aStackTop, Cons exp, Cons from) throws Throwable {
        this.aEnvironment = aEnvironment;
        this.aStackTop = aStackTop;
        Cons head0 = SublistCons.getInstance(AtomCons.getInstance(aEnvironment.getPrecision(), "NM"));
        ((Cons) head0.car()).setCdr(exp);

        this.exp = head0;
        this.from = from;
    }

    public double value(double d) {
        double resultValue = 0;
        try {
            bnum.setTo(d);

            Cons number = new NumberCons(bnum);

            org.mathpiper.lisp.astprocessors.ExpressionSubstitute behaviour = new org.mathpiper.lisp.astprocessors.ExpressionSubstitute(aEnvironment, from, number);

            Cons sub = Utility.substitute(aEnvironment, aStackTop, exp, behaviour);
            Cons result = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, sub);

            if (result instanceof NumberCons) {
                NumberCons numberCons = (NumberCons) result;
                resultValue = numberCons.toDouble();
            } else {
                throw new Exception("Internal error in 'IntegrateN'.");
            }

            return resultValue;
        } catch (Throwable e) {
            RuntimeException rte = new RuntimeException();
            rte.initCause(e);
            throw rte;
        }

    }
}

/*
%mathpiper_docs,name="IntegrateN",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD IntegrateN --- Numerical integration

*CALL
	IntegrateN(expression, variable, lowValue, highValue)

 *PARMS
{expression} -- expression to integrate

{variable} -- atom, variable to integrate over

{lowerValue} -- lower value of definite integration

{higherValue} -- higher value of definite integration

{Options:}

 {todo} -- todo

*DESC

This procedure integrates the expression {expression} with respect to the
variable {variable}.

%/mathpiper_docs
 */
