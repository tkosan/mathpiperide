%mathpiper,def="Tan"

RulebaseHoldArguments("Tan",["x"]);

5 ## Tan(x_Number?)::NumericMode?() --> TanD(x);
5 ## Tan(x_Number?`1)::NumericMode?() --> TanD(x);

10 ## Tan(x_Number?`rad)::NumericMode?() --> TanD(x);
10 ## Tan(x_Number?`radian)::NumericMode?() --> TanD(x);

15 ## Tan(x_Number?`deg)::NumericMode?() --> TanD(x * Pi/180);
15 ## Tan(x_Number?`degree)::NumericMode?() --> TanD(x * Pi/180);

20 ## Tan(xx_Number?`uu_)::NumericMode?() --> TanD(UnitsStripAll(NotationStandard(xx`uu)));

%/mathpiper





%mathpiper_docs,name="Tan",categories="Mathematics Procedures,Trigonometry (Symbolic)"

*CMD Tan --- trigonometric function tangent

*CALL
        Tan(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure represents the trigonometric function tangent.


*EXAMPLES
*SEE Sin, Cos, Cot, Sec, Csc, ArcSin, ArcCos, ArcTan, ArcCot, ArcSec, ArcCsc
%/mathpiper_docs





%mathpiper,name="Tan",subtype="in_prompts"

Tan(2) ~> Tan(2)

NM(Tan(4`rad)) ~> 1.157821282

NM(Tan(7`deg)) ~> 0.1227845609

%/mathpiper





%mathpiper,name="Tan",subtype="automatic_test"

Verify(NM(Tan(1)), 1.557407725);

%/mathpiper