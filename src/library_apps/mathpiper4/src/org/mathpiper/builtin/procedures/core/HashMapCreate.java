package org.mathpiper.builtin.procedures.core;

import java.util.HashMap;
import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.NumberCons;

public class HashMapCreate extends BuiltinProcedure
{
    
    private HashMapCreate()
    {
    }

    public HashMapCreate(String functionName)
    {
        this.functionName = functionName;
    }

    
    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        HashMap hashMap = new HashMap();
        
        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, new JavaObject(hashMap)));
    }
}//end class.



/*
%mathpiper_docs,name="HashMapCreate",categories="Programming Procedures,Built In"
*CMD HashMapCreate --- creates a hash map

*CALL
	HashMapCreate()

*DESC
This procedure creates a hash map.

*EXAMPLES

*SEE HashMapGet, HashMapPut, HashMapRemove, HashMapKeys, HashMapValues, HashMapContainsKey?

%/mathpiper_docs





%mathpiper,name="HashMapCreate",subtype="in_prompts"

hashMap := HashMapCreate() ~> class java.util.HashMap

HashMapContainsKey?(hashMap, "aa") ~> False

HashMapGet(hashMap, "aa") ~> Null

HashMapPut(hashMap, "aa", 3) ~> True

HashMapContainsKey?(hashMap, "aa") ~> True

HashMapKeys(hashMap) ~> ["aa"]

HashMapValues(hashMap) ~> [3]

HashMapGet(hashMap, "aa") ~> 3

HashMapRemove(hashMap, "aa") ~> 3

HashMapKeys(hashMap) ~> []

HashMapValues(hashMap) ~> []

HashMapPut(hashMap, 'bb, 4) ~> True

HashMapKeys(hashMap) ~> ['bb]

HashMapGet(hashMap, 'bb) ~> 4

HashMapValues(hashMap) ~> [4]

HashMapRemove(hashMap, 'bb) ~> 4

HashMapKeys(hashMap) ~> []

HashMapValues(hashMap) ~> []

%/mathpiper





%mathpiper,name="HashMapCreate",subtype="automatic_test"

{
    Local(hashMap);

    hashMap := HashMapCreate();

    Verify(HashMapContainsKey?(hashMap, "aa"), False);
    Verify(HashMapGet(hashMap, "aa"), Null);

    Verify(HashMapPut(hashMap, "aa", 3), True);
    Verify(HashMapContainsKey?(hashMap, "aa"), True);
    Verify(HashMapKeys(hashMap), ["aa"]);
    Verify(HashMapValues(hashMap), [3]);
    Verify(HashMapGet(hashMap, "aa"), 3);
    Verify(HashMapRemove(hashMap, "aa"), 3);
    Verify(HashMapKeys(hashMap), []);
    Verify(HashMapValues(hashMap), []);

    Verify(HashMapPut(hashMap, 'bb, 3), True);
    Verify(HashMapKeys(hashMap), ['bb]);
    Verify(HashMapGet(hashMap, 'bb), 3);
    Verify(HashMapValues(hashMap), [3]);
    Verify(HashMapRemove(hashMap, 'bb), 3);
    Verify(HashMapKeys(hashMap), []);
    Verify(HashMapValues(hashMap), []);
}

%/mathpiper
*/


