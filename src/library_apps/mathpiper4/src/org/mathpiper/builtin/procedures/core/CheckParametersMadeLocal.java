package org.mathpiper.builtin.procedures.core;

import java.util.ArrayList;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.astprocessors.PatternProcess;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;

public class CheckParametersMadeLocal extends BuiltinProcedure {

    private CheckParametersMadeLocal() {
    }

    public CheckParametersMadeLocal(String functionName) {
        this.functionName = functionName;
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        Cons argument = getArgument(aEnvironment, aStackTop, 1);
        LispError.checkIsString(aEnvironment, aStackTop, argument, 1);
        String procedureName = (String) argument.car();

        Cons arguments = getArgument(aEnvironment, aStackTop, 2);
        
        arguments = (Cons) arguments.car();
        arguments = arguments.cdr();
        
        java.util.List argumentsList = new ArrayList();

        while (arguments != null) {
            String item = (String) arguments.car();
            argumentsList.add(Utility.stripEndQuotesIfPresent(item));
            arguments = arguments.cdr();
        }

        Cons body = getArgument(aEnvironment, aStackTop, 3);

        String s = "[\n"
                + "[\"track\",[]],\n"
                + "[\"function\",\n"
                + "Lambda([trackingList,positionString,node],\n"
                + "{\n"
                + "Insert!(trackingList[\"track\"], Length(trackingList[\"track\"])+1, positionString);\n"
                + "node;\n"
                + "})\n"
                + "]\n"
                + "];\n";

        Cons associationList = Utility.mathPiperParse(aEnvironment, aStackTop, s);

        Cons pattern = Utility.mathPiperParse(aEnvironment, aStackTop, "Local(aa__);");

        PatternProcess patternVisitor = new org.mathpiper.lisp.astprocessors.PatternProcess(aEnvironment, pattern, null, associationList);

        Utility.substitute(aEnvironment, aStackTop, body, patternVisitor);

        associationList = (Cons) associationList.car();

        associationList = associationList.cdr();

        Cons cons = Utility.associationListGet(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "\"track\""), associationList);

        cons = (Cons) cons.car();
        cons = cons.cdr();
        cons = cons.cdr();
        cons = (Cons) cons.car();
        Cons positionList = cons;
        positionList = positionList.cdr();

        while (positionList != null) {
            String localPosition = (String) positionList.car();

            localPosition = Utility.stripEndQuotesIfPresent(localPosition);

            Cons expression = body;

            String[] indexStrings = localPosition.split(",");

            for (String indexString : indexStrings) {
                int index = Integer.parseInt(indexString);
                expression = Utility.nth(aEnvironment, aStackTop, expression, index, false);
            }

            expression = (Cons) expression.car();
            expression = expression.cdr();

            while (expression != null) {
                String item = (String) expression.car();
                item = Utility.stripEndQuotesIfPresent(item);
                
                if(argumentsList.contains(item))
                {
                    LispError.throwError(aEnvironment, aStackTop, "", "The variable \"" + item + "\" is a parameter in procedure " + procedureName + ", and parameters cannot be made local.", expression.getMetadataMap());
                }
                
                expression = expression.cdr();
            }

            positionList = positionList.cdr();
        }

        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}




/*
%mathpiper_docs,name="CheckParametersMadeLocal",categories="Programming Procedures,Error Reporting,Built In"
*CMD CheckParametersMadeLocal --- checks for procedure parameters being made local

*CALL

    CheckParametersMadeLocal(procedureName, parametersList, body)

*PARMS
{procedureName} -- the name of the procedure being defined

{parameterssList} -- the procedure's parameters

{body} -- the procedure's body

*DESC
Checks for procedure parameters being made local.

*EXAMPLES

%/mathpiper_docs




%mathpiper,name="CheckParametersMadeLocal",subtype="in_prompts"

CheckParametersMadeLocal("foo", ["one", "two", "three"], '{ Local(one); }) ~~> Exception
Exception: The variable "one" is a parameter in procedure "foo", and parameters cannot be made local.  Starting at index 0.

%/mathpiper




%mathpiper,name="CheckParametersMadeLocal",subtype="automatic_test"

Verify(ExceptionCatch(CheckParametersMadeLocal("foo", ["one", "two", "three"], '{ Local(one); }), "", Exception), Exception);

%/mathpiper
*/
