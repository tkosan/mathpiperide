## Welcome to the MathPiper and MathPiperIDE project.

MathPiper is a new education-oriented programming
language which is simple enough to be learned as a first
programming language and yet powerful enough to be useful in
any science, technology, engineering, or mathematics related career.
MathPiper is also a Computer Algebra System (CAS) which is
similar in function to the CAS which is included in the
TI-89 and TI-Nspire CAS calculators.

MathPiperIDE is an Integrated Development Environment (IDE)
for MathPiper programming which includes powerful text
editing and interactive graphics capabilities.

More information about MathPiper and MathPiperIDE can be
found at http://mathpiper.org.



## Obtaining and Building MathPiperIDE

MathPiperIDE and MathPiper are written in Java. A Java JDK
(Java Development Kit) needs to be used to build the
software instead of just a JRE (Java Runtime Environment).

Apache Ant is the build tool that is used to build the
software. It can be obtained from http://ant.apache.org/.

If you are using a version of Linux, your package manager
should be able to install a JDK and Apache Ant.

The following are the instructions for obtaining MathPiperIDE 
and building it:

1. Clone MathPiperIDE from the git repository that is at
https://bitbucket.org/tkosan/mathpiperide

2. Change into the top-level MathPiperIDE directory (it
contains a src directory, a build.xml file and this
README.md file).

3.  Download lib.zip (which contains the library files that
are needed to build MathPiperIDE) from
http://patternmatics.org/mathpiper/developer_releases/lib.zip , and
extract this archive into the top-level directory. A
directory named "lib" will be placed into the top-level
directory after lib.zip has been extracted.

4. Build MathPiperIDE using the command `ant *all`

5. After the build is done, a directory named "build" should
have been created. Change into build/mathpiperide and run
MathPiperIDE using either the win_run.bat or the unix_run.sh
scripts.



## Contents of Files and Directories

build.xml - Apache ant build file. 

lib/ - Library directory. Holds Java .jar files which are used 
by MathPiperIDE. 

src/ - Contains all of the source code that 
is used to create MathPiper and MathPiperIDE.



### src/

doc/ - Holds the MathPiper books and other MathPiper and
MathPiperIDE-related documentation.

examples/ - Contains the various example .mpw and .mpi files
which are contained in the MathPiper distribution (.mpi
files are explained below.)

jedit_core/ - Contains the base jEdit distribution which
MathPiperIDE is built upon. The macros/ and modes/
directories in this directory contain various
MathPiper-specific macros and modes which are used to
enhance jEdit to support the extra capabilities that
MathPiperIDE needs. The settings/properties file is used to
configure jEdit to work as MathPiperIDE.

library_apps/mathpiper4 - Contains all of the MathPiper CAS code (which
is explained in more detail below.).

plugins/ - Contains all of the source code for the various
plugins that are specifically designed for MathPiperIDE. 
Each plugin has its own subdirectory in this directory and
also its own apache ant build.xml file which the main
build.xml file uses to build the plugin.

scripts/ - Contains miscellaneous beanshell scripts which are
used by MathPiperIDE



### src/library_apps/mathpiper4

The core MathPiper CAS system is located in this directory.
The following is a description of the directories it
contains:

build.xml - Apache ant build file which is used to build the
MathPiper CAS.

docs/ - An empty directory which might eventually contain
MathPiper documentation.

examples - Mostly contains examples from Yacas that are
being stored here for reference purposes.

misc - Contains miscellaneous files which are being used as
a starting point for enhancing MathPiper.

resources - Contains resources which are placed into the
mathpiper.jar file.

src - Contains all of the MathPiper source code (and is
further described below).

tests - Contains miscellaneous manual tests which are useful
for testing various parts of MathPiper.



### src/library_apps/mathpiper/src/org/mathpiper

builtin/ - Contains all of the MathPiper functions which are
implemented in Java.

exceptions/ - Contains Java exception classes.

interpreters/ - Contains the Java interpreter classes which
are used to access the CAS.

io/ - Contains Java Input/Out classes.

lisp/ - Contains the Java-based lisp interpreter on top of
which MathPiper runs.

scripts4/ - Contains the .mpw MathPiper worksheet files which
hold the MathPIper "source code" and the MathPiper
documentation "source code" which is processed to create the
MathPiper function documentation.

test - Contains the class that is used to build MathPiper
and the class which runs the test suite.

ui - Contains a number of user interface-related classes
such as the GUI console and the function help application.

Version.java - The Java class which holds the MathPiper
version number.



### MathPiperIDE Build Process

The top-level build.xml file is the master build.xml file
for MathPiperIDE and it calls all of the other area-specific
build.xml files which build the system. When the ant *all
command is executed, a directory named build/ is created
in the same directory which contains the top-level build.xml
file. If the /build/ directory already exists, it is deleted
and then recreated.

During the MathPiperIDE build process, the following three
directories are created in the build/ directory:

library_apps/ - Contains the compiled and processed MathPiper
CAS code.

mathpiperide/ - Contains the complete MathPiperIDE
application which has the contents of the library_apps/
directory and the and "plugins" directory copied into it. It
is this directory which is archived and distributed to
users.

plugins/ - Contains the compiled code for all of the plugins.



### MathPiper Build Process

During the MathPiper build process, all of the Java source
code which is in the src/library_apps/mathpiper/src/
directory is compiled and placed into the
build/library_apps/mathpiper/classes/ directory. This is just
normal compilation of Java code and is not an unusual part
of the MathPiper build process. However, what is unusual is
that the MathPiper script .mpw files which are in
src/library_apps/mathpiper/src/org/mathpiper/scripts4/ are
processed by a special MathPiper build tool (which is written 
in Java) and the processed files are placed into
build/library_apps/mathpiper4/src/org/mathpiper/Scripts.java

The documentation code in the .mpw file %mathpiper_docs
folds is assembled into documentation files which are copied
into the
build/library_apps/mathpiper/classes/org/mathpiper/ui/gui/help/data/
directory. The information in these data files are converted
into html format when function help is requested by the user
during runtime, and the html is then displayed by the
function help application.



### Adding Experimental Functions To The Library

Now that the above information has been covered, the
procedure for adding your own experimental functions to the
system is fairly straight forward. Experimental functions
are usually placed into the
src/library_apps/mathpiper/src/org/mathpiper/scripts4/proposed/
directory. Simply create a directory in the proposed
directory and then place your experimental .mpw files into
it. When the system is built, your .mpw files will be
processed by the build system along with the rest of the
.mpw files that are in scripts4/.



### The Attributes of %mathpiper Folds

The "def" attribute in the header of a %mathpiper fold is
used to insert the function into the MathPiper function
lookup system. If you don't place a "def" attribute into a
%mathpiper fold header, the functions it specifies will not
be inserted into the function lookup system.

The "scope" attribute is used to customize the behavior of
the build system. A fold which has a "scope" attribute which
is set to "nobuild" will be ignored by the build
system and its contents will not be copied to the build/
directory. This is useful for placing manual testing folds
into the .mpw file which are not meant to become part of the
distributed system.

The "access" attribute is used to set the visibility of a
function in the help system. 

Note: robust parsing of fold headers has not
been implemented yet and so small typos such as missing
commas in the headers will cause strange problems.



### %mathpiper_docs Folds

Text that is placed in braces ({}) will be made bold, and
text which is placed in matching $$ characters (like $x^2$)
will be rendered as math LaTeX.

If <shift><enter> is pressed inside of a %mathpiper_docs
fold above the \*E.G. section, the fold contents will be
converted into html and displayed in a window.



#### THE *CMD, *CALL, *PARMS, *DESC, *E.G., AND *SEE COMMANDS

The \*CMD, \*CALL, \*PARMS, \*DESC, \*E.G., and \*SEE commands are
transformed by the documentation system into html markup
which supports the intent of the section.

The \*CMD section is used to give the function name and a
short description of what it does. Note: the description
cannot contain any commas.

The \*CALL section shows the calling formats for the
function.

The \*DESC section gives a more complete description of the
function.

The \*E.G. section contains examples which show how to use
the function. These can either be console examples or
%mathpiper fold examples (both of which should be left
justified to make it easier for users to copy the examples
and execute them). If %mathpiper fold examples are included,
forward slashes must be placed in front of each fold
header and footer % character so that the documentation system
does not think it is a live fold. Here is an example:

```
/%mathpiper

<...>

/%/mathpiper

    /%output,preserve="false" 
        Result: True 
        Side Effects:
        <...> 
.   /%/output
```


The \*SEE section contains links to functions which are
related to this function.



#### The "name" and "categories" Attributes in the Headers of %mathpiper_docs Folds

The "name" attribute informs the documentation
processing system what the name of the function is that is
being documented in the fold.

The "categories" attribute is more sophisticated and it
consists of parameters separated by semicolons. The
first parameter is the overall category that the
function belongs in. There are then one or more
subcategories listed after the main category, and the
function will be placed into each of these subcategories.
The .mpw files in "scripts4" provide good examples of how to
configure %mathpiper_docs headers.


## The Old Source Code Repository

The old source code repository for MathPiper and
MathPiperIDE is at https://code.google.com/p/mathpiper/ .



<!--- :wrap=hard:maxLineLen=60: -->