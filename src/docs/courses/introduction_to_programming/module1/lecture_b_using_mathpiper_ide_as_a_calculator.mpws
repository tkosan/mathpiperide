v.11


USING MathPiperIDE AS A NUMERIC (SCIENTIFIC) CALCULATOR

MathPiperIDE worksheets are text files that have a
.mpws extension and which are able to execute
multiple types of code in a single text area.
(Note: a new .mpws file needs to be saved
immediately after it is created, because
MathPiperIDE will not recognize it as a MathPiper
worksheet until it has been saved.)

When the cursor is placed anywhere in a fold that
contains code and then <shift><enter> is pressed,
the code is evaluated and the result of the
evaluation is placed into an output fold that is
beneath the original fold. For example, when
<shift><enter> was pressed in the following fold,
2 + 2 was read into MathPiper for evaluation and
the result was 4. The numeral 4 is the value that
was returned by evaluating 2 + 2.

%mathpiper

2 + 2;

%/mathpiper



In addition to addition, MathPiper can also
perform subtraction, multiplication, exponents,
and division.

%mathpiper

5 - 2;

%/mathpiper



%mathpiper

3 * 4;

%/mathpiper



%mathpiper

2^3;

%/mathpiper



%mathpiper

12/6;

%/mathpiper



Notice that the multiplication symbol is an
asterisk (*), the exponent symbol is a caret (^),
and the division symbol is a forward slash (/).
These symbols (along with addtion (+), subtraction
(−), and ones we will talk about later) are called
OPERATORS because they tell MathPiper to perform
an operation such as addition or division.



MathPiper can also work with decimal numbers.

%mathpiper

.5 + 1.2;

%/mathpiper



%mathpiper

3.7 - 2.6;

%/mathpiper



%mathpiper

2.2 * 3.9;

%/mathpiper



%mathpiper

2.2^3;

%/mathpiper



%mathpiper

1 / 2;

%/mathpiper

In the last example, MathPiper returned the
fraction unevaluated. This sometimes happens due
to MathPiper's symbolic nature, but a result in
numeric form can be obtained by using the NM()
procedure (which is discussed in the next
section).

%mathpiper

NM(1 / 2);

%/mathpiper

As can be seen here, when a result is given in
numeric form, it means that it is given as a
decimal number.



USING PROCEDURES

NM() is an example of a procedure. A PROCEDURE can
be thought of as a "black box" that accepts input,
processes the input, and returns a result. Each
procedure has a name and in this case, the name of
the procedure is NM, which stands for "Numeric
Mode".

To the right of a procedure's name there is always
a set of parentheses, and information that is sent
to the procedure is placed inside of them. The
purpose of the NM() procedure is to make sure that
the information that is sent to it is processed
numerically instead of symbolically. Procedures
are used by EVALUATING them, and this happens when
<shift><enter> is pressed. Another name for
evaluating a procedure is CALLING it.



THE Sqrt() SQUARE ROOT PROCEDURE

The Sqrt() procedure calcuates the square root of
a number. For example, this is how to calculate the
square root of 9.

%mathpiper

Sqrt(9);

%/mathpiper


Calls to procedures can also be nested. The following
code will first calculate the square root of 81 then
pass the 9 that results from this calculation to the
Sqrt() procedure again to obtain 3

%mathpiper

Sqrt(Sqrt(81));

%/mathpiper


The following two folds show the Sqrt()
procedure being used without and with
the NM() procedure.

%mathpiper

Sqrt(8);

%/mathpiper



%mathpiper

NM(Sqrt(8));

%/mathpiper

Notice that Sqrt(9) returned 3 as expected but
Sqrt(8) returned Sqrt(8). We needed to use the
NM() procedure to force the square root procedure
to return a numeric result. The reason that
Sqrt(8) does not appear to have done anything is
because computer algebra systems are designed to
work with expressions that are as exact as
possible. In this case the symbolic value Sqrt(8)
represents the number that is the square root of 8
more accurately than any decimal number can. For
example, the following four decimal numbers all
represent the square root of 8, but none of them
represent it more accurately than Sqrt(8) does:

2.828427125
2.82842712474619
2.82842712474619009760337744842
2.8284271247461900976033774484193961571393437507539

Whenever MathPiper returns a symbolic result and a
numeric result is desired, simply use the NM()
procedure to obtain one. The ability to work with
symbolic values are one of the things that make
computer algebra systems so powerful.



THE Even?() PROCEDURE

An example of a simple procedure is Even?(). The
Even?() procedure takes a number as input and
returns True if the number is even and False if it
is not even.

%mathpiper

Even?(4);

%/mathpiper



%mathpiper

Even?(5);

%/mathpiper


MathPiper has a large number of procedures, and
complete list of them is contained in the
MathPiperDocs plugin.



SYNTAX ERRORS

An expression's syntax is related to whether it is
typed correctly or not. If input is sent to
MathPiper that has one or more typing errors in
it, MathPiper will return an error message which
is meant to be helpful for locating the error. For
example, if a backwards slash (\) is entered for
division instead of a forward slash (/), MathPiper
returns the following error message.

%mathpiper

12 \ 6;

%/mathpiper


To fix this problem, change the \ to a /, and
reevaluate the expression. This section provided a
short introduction to using MathPiper as a numeric
calculator. The next section contains a short
introduction to using MathPiper as a symbolic
calculator.



USING MathPiperIDE AS A SYMBOLIC CALCULATOR

MathPiper is good at numeric computation, but it
is great at symbolic computation. If you have
never used a system that can do symbolic
computation, you are in for a treat! As a first
example, let's try adding fractions (which are
also called rational numbers). Add 1/2 and 1/3 in
the MathPiper console.

%mathpiper

1/2 + 1/3;

%/mathpiper

Instead of returning a numeric result like
0.83333333333333333333 (which is what a scientific
calculator would return) MathPiper added these two
rational numbers symbolically and returned 5/6.



VARIABLES AND THE VARIABLE STATE

Up to this point, the result values that have been
produced could be viewed but not used as inputs to
further work. It would be useful if MathPiper
provided a way to store results so they could be
used later. Fortunately, MathPiper and most
programming languages have this capability! Names
that can be associated with values are called
VARIABLES. Variable names must start with an upper
or lower case letter and be followed by zero or
more upper case letters, lower case letters, or
numbers. Examples of variable names include:

aa, bb, xx, yy, answer, totalAmount, and index.

All variable names should be at least two
characters long so they don't conflict with
single digit units constants such as 'm' (which
is the SI symbol for meters).

Even though variable names can start with
an upper case letter, by convention all variables
should begin with a lower case letter. If the name
is composed of more than one word, the first
letter of each word after the first word should be
capitalized as shown in these examples:

numberOfDoors, seatsInRoom6, and averageTemperature.

Note: the underscore '_' character cannot be used
in variable names. One or more underscore
characters in a name identify it as a constant. A
CONSTANT is a name that always evaluates to
itself, and it is discussed below.

The process of associating a value with a variable
is called ASSIGNING the value to the variable, and
this consists of placing the name of a variable
you would like to create on the left side of the
assignment operator ':=' and an expression on the
right side of this operator. This expression is
evaluated, and the value it returns is assigned to
the variable. For example, the following code
assigns the value 5 to the variable 'aa'.

%mathpiper

aa := 7;

%/mathpiper


The assignment operator ':=' is read as "becomes",
and therefore the above expression reads "'a'
becomes 5". If the variable 'a' is evaluated by
itself, it returns the value that is currently
assigned to it.

%mathpiper

aa;

%/mathpiper


The assignment operator ':=' is meant to look like
an arrow that points from right to left like ← in
order to emphasize the right-to-left assignment of
variables.

The following code evaluates 1/2 + 1/3 and assigns 
the result to the variable 'aa'.

%mathpiper

aa := (1/2 + 1/3);

%/mathpiper



%mathpiper

aa;

%/mathpiper


After a result value is assigned to a variable, it
is available for further work. The following code
determines the numerator and the denominator of the
rational number that has been assigned to 'aa'.

%mathpiper

Numerator(aa);

%/mathpiper



%mathpiper

Denominator(aa);

%/mathpiper


In this example, the assignment operator ':=' was
used to assign the result value 5/6 to the variable
'aa'. When 'aa' was evaluated by itself, the value
that was most recently assigned to it (in this
case 5/6) was returned. This value will stay assigned
to the variable 'aa' as long as MathPiper is
running, unless 'aa' is unassigned with the
Unassign() procedure, or 'aa' has another value
assigned to it. This is why we were able to
determine both the numerator and the denominator
of the rational number assigned to 'aa' using two
procedures in turn. (Note: there can be no spaces
between the ':' and the '=' in the ':=' operator)



THE GLOBAL VARIABLE STATE

The global variable state is the list of all of
the global variables that are currently assigned,
along with the values that have been assigned to
them. A global variable is a variable that is
accessible by all the code in the system. The
other main kind of variable is a local variable.
Local variables (which are covered in a later
section) are accessible to limited sections of
code. All variables that we are using are global
variables. The State() procedure can be used to
obtain a copy of the global variable state.

%mathpiper

aa := 1;

%/mathpiper



%mathpiper

bb := 2;

%/mathpiper



%mathpiper

State();

%/mathpiper


The GlobalState plugin that is on the right side
of MathPiperIDE also shows all of the global
variables that have values assigned to them along
with these values. It is a good idea to keep the
GlobalState plugin open while programming because
it makes it easier to see the effects that the
code is having on these variables.



EVALUATING AN UNASSIGNED VARIABLE THROWS AN EXCEPTION

If an unassigned variable is evaluated, an
exception is thrown.

%mathpiper

Unassign(aa);

%/mathpiper


%mathpiper

aa;

%/mathpiper


The Unassign() procedure unassigns a variable, and
it returns the value True as a result to indicate
that the variable that was sent to it was
successfully unassigned. Many procedures return
either return True or False to indicate whether or
not the operation they performed succeeded. True
and False are constants, and constants are
discussed in the next section.



CONSTANTS

A constant is a name that evaluates to itself. The
following is list of some constants that are
predefined in MathPiper:

True
False
Infinity
Undefined
All
None

The constant Infinity evaluates to itself.

%mathpiper

Infinity;

%/mathpiper


If an attempt is made to assign a value to a
constant, an exception is thrown.

%mathpiper

Infinity := 3;

%/mathpiper



As mentioned earlier, some procedures return a
predefined constant as a value. For example, the
Assigned?() procedure returns True if a variable
is assigned, and it returns False if it is
unassigned.

%mathpiper

aa := 3;

%/mathpiper



%mathpiper

aa + 1;

%/mathpiper



%mathpiper

Assigned?(aa);

%/mathpiper


All currently assigned variables can be unassigned
by passing the constant 'All' to Unassign.

%mathpiper

bb := 2;

%/mathpiper



%mathpiper

State();

%/mathpiper



%mathpiper

Unassign(All);

%/mathpiper



%mathpiper

State();

%/mathpiper



One way to indicate that a name is a constant is
to use an underscore character '_' as the first
letter in the name:

_xx, _yy, _heavy

Constants that start with an underscore evaluate
to themselves.

%mathpiper

_xx;

%/mathpiper

Values cannot be assigned to these constants
either.

%mathpiper

_xx := 3;

%/mathpiper


Numbers are also constants because they evaluate
to themselves.

%mathpiper

3;

%/mathpiper



CALCULATING WITH CONSTANTS

Constants may not appear to be very useful, but
they provide the flexibility needed for computer
algebra systems to perform symbolic calculations.
In order to demonstrate this flexibility, let's
first factor some numbers using the Factor()
procedure.

%mathpiper

Factor(8);

%/mathpiper



%mathpiper

Factor(14);

%/mathpiper



%mathpiper

Factor(2343);

%/mathpiper


Now let's factor an expression that contains the
constant '_x':

%mathpiper

aa := Factor(_x^2 + 24*_x + 80);

%/mathpiper



%mathpiper

Expand(aa);

%/mathpiper


Factor() uses the rules of algebra to manipulate
the algebraic expression that is sent to it into
factored form. The Expand() procedure was then
able to take the factored expression
(_x+4)*(_x+20) and manipulate it until it was
expanded. One way to remember what the procedures
Factor() and Expand() do is to look at the second
letters of their names. The 'a' in Factor can be
thought of as adding parentheses to an expression,
and the 'x' in Expand can be thought of xing out
or removing parentheses from an expression.



VARIABLE AND CONSTANT NAMES ARE CASE SENSITIVE

MathPiper variable and constant names are case
sensitive. This means MathPiper takes into account
the case of each letter in a variable name when it
is deciding if two or more variable names are the
same variable or not. For example, the variable
name Box and the variable name box are not the
same variable because the first variable name
starts with an upper case 'B' and the second
variable name starts with a lower case 'b'.

%mathpiper

Box := 1;

%/mathpiper




%mathpiper

box := 2;

%/mathpiper



%mathpiper

Box;

%/mathpiper



%mathpiper

box;

%/mathpiper



USING MORE THAN ONE VARIABLE

Programs are able to have more than one variable.
The following example shows three variables being
used.

%mathpiper

aa := 2;

bb := 3;

aa + bb; // This value is lost because it is assigned to a variable.


answer := (aa + bb + 3);

%/mathpiper



%mathpiper

answer;

%/mathpiper


The part of an expression that is on the right
side of an assignment operator is always evaluated
first, and the result value is then assigned to
the variable that is on the left side of the
operator. Now that you have seen how to use the
MathPiper console as both a symbolic and a numeric
calculator, our next step is to take a closer look
at the procedures that are included with
MathPiper. As you will soon discover, MathPiper
contains numerous procedures that deal with a wide
range of mathematics.
