%mathpiper,def="PolynomialCurveFit"

PolynomialCurveFit(data, var, numberOfCoefficients) :=
{
    Local(weightedObservedPoints, weightedObservedPointsList, fitter, coefficientList, terms, index, result, configurationList);
    
    data := NotationStandard(data);
    
    data := UnitsStripAll(data);
    
    var := UnitsStripAll(var);
    
    numberOfCoefficients := UnitsStripAll(numberOfCoefficients);

    weightedObservedPoints := JavaNew("org.hipparchus.fitting.WeightedObservedPoints");
    
    ForEach(numberPair, data)
    {
        JavaCall(weightedObservedPoints, "add", NotationDecimal(numberPair[1]), NotationDecimal(numberPair[2]));
    }
    
    weightedObservedPointsList := JavaCall(weightedObservedPoints, "toList");
    
    fitter := JavaCall("org.hipparchus.fitting.PolynomialCurveFitter", "create", numberOfCoefficients);
    
    coefficientList := JavaAccess(fitter, "fit", weightedObservedPointsList);
    
    terms := [];
    
    index := 0;
    
    While(index <=? numberOfCoefficients)
    {
        If(AbsN(coefficientList[index + 1]) >? .00000000001)
        {
            Append!(terms,  coefficientList[index + 1] * var ^ index);
        }
        
        index++;
    }
    
    result := UnFlatten(terms, "+");
}

%/mathpiper





%mathpiper_docs,name="PolynomialCurveFit",categories="Mathematics Procedures,Polynomials (Operations)"

*CMD PolynomialCurveFit --- create a polynomial that approximates a set of data points

*CALL
        PolynomialCurveFit(data, var, numberOfCoefficients)

*PARMS

{data} -- the data in [[x1, y1], [x2, y2], ..., [xn, yn]] format
{var} -- the polynomial variable (example: _x)
{numberOfEoefficients} -- the number of coefficients in the polynomial.

*DESC
This procedure creates a polynomial that approximates a set of data points.

*E.G.

In> aa := PolynomialCurveFit(NM(BuildList([xx,Sin(xx`rad)], xx, 0, 3, .1)), _x, 2)
Result: -0.04662836748 + 1.31216376 * _x +  -0.418997742 * _x^2

%/mathpiper_docs





%mathpiper,name="PolynomialCurveFit",subtype="in_prompts"

PolynomialCurveFit(NM(BuildList([xx, Sin(xx`rad)], xx, 0, 3, .1)), _x, 2) ~> '(-0.04662836748 + 1.31216376 * _x +  -0.418997742 * _x^2)

%/mathpiper