%mathpiper,def="Im"

/* Imaginary parts */
110 ## Im(Complex(r_,i_)) --> i;
120 ## Im(Undefined) --> Undefined;
300 ## Im(x_) --> 0;

%/mathpiper



%mathpiper_docs,name="Im",categories="Mathematics Procedures,Numbers (Complex)"
*CMD Im --- imaginary part of a complex number
*STD
*CALL
        Im(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure returns the imaginary part of the complex number "x".

*E.G.

In> Im(5)
Result: 0;

In> Im(I)
Result: 1;

In> Im(Complex(3,4))
Result: 4;

*SEE Complex, Re
%/mathpiper_docs





%mathpiper,name="Im",subtype="automatic_test"

/* Bug #7 */
Verify(Im(3+I*Infinity), Infinity); /* resolved */
Verify(Im(3+I*Undefined), Undefined);

%/mathpiper