package org.mathpiper.builtin.procedures.core;

import java.util.Collection;
import java.util.HashMap;
import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;

import org.mathpiper.lisp.cons.SublistCons;


public class HashMapKeys extends BuiltinProcedure
{
    
    private HashMapKeys()
    {
    }

    public HashMapKeys(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons evaluated = getArgument(aEnvironment, aStackTop, 1);

        BuiltinContainer gen = (BuiltinContainer) evaluated.car();
        if( gen == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        Object object = gen.getObject();
        
        if(! (object instanceof HashMap)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        
        HashMap<Object,Cons> hashMap = (HashMap) object;

        Collection keys = hashMap.keySet();
        
        Cons head = Utility.iterableToList(aEnvironment, aStackTop, keys);
        
        setTopOfStack(aEnvironment, aStackTop, SublistCons.getInstance(head));
    }
}



/*
%mathpiper_docs,name="HashMapKeys",categories="Programming Procedures,Built In"
*CMD HashMapKeys --- return the given hash map's keys

*CALL
	HashMapKeys(hashMap)

*PARMS

{hashMap} -- a hash map

*DESC
This procedure returns the given hash map's keys in a list.

*SEE HashMapCreate

%/mathpiper_docs
*/
