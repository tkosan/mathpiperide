%mathpiper,def="Sin"

RulebaseHoldArguments("Sin",["x"]);

5 ## Sin(x_Number?)::NumericMode?() --> SinD(x);
5 ## Sin(x_Number?`1)::NumericMode?() --> SinD(x);

10 ## Sin(x_Number?`rad)::NumericMode?() --> SinD(x);
10 ## Sin(x_Number?`radian)::NumericMode?() --> SinD(x);

15 ## Sin(x_Number?`deg)::NumericMode?() --> SinD(x * Pi/180);
15 ## Sin(x_Number?`degree)::NumericMode?() --> SinD(x * Pi/180);

20 ## Sin(xx_Number?`uu_)::NumericMode?() --> SinD(UnitsStripAll(NotationStandard(xx`uu)));

%/mathpiper






%mathpiper_docs,name="Sin",categories="Mathematics Procedures,Trigonometry (Symbolic)"

*CMD Sin --- trigonometric function sine

*CALL
        Sin(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure represents the trigonometric function sine.


*EXAMPLES
*SEE Cos, Tan, Cot, Sec, Csc, ArcSin, ArcCos, ArcTan, ArcCot, ArcSec, ArcCsc
%/mathpiper_docs





%mathpiper,name="Sin",subtype="in_prompts"

Sin(2) ~> Sin(2)

NM(Sin(4`rad)) ~> -0.7568024953

NM(Sin(7`deg)) ~> 0.1218693434

%/mathpiper





%mathpiper,name="Sin",subtype="automatic_test"

Verify(NM(Sin(1)), 0.8414709848);

%/mathpiper