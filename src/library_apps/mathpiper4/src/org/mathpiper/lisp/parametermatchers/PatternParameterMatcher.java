/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp.parametermatchers;

import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;

// Abstract class for matching an expression tree to a pattern
public abstract class PatternParameterMatcher {
    private final static NullPatternParameterMatcher sNullMatcher = new NullPatternParameterMatcher();
    private PatternParameterMatcher iNextMatcher;
    
    PatternParameterMatcher() {
        if (!(this instanceof NullPatternParameterMatcher))
            iNextMatcher = new NullPatternParameterMatcher();
    }
    
    public abstract boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons aExpressionTree, Cons[] arguments) throws Throwable;
    
    public abstract boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons[] aExpressionTree, int aLocation, Cons[] arguments) throws Throwable;

    public abstract String getType();
    
    public void setNextMatcher(PatternParameterMatcher aMatcher) {
        if (aMatcher == null) {
            iNextMatcher = new NullPatternParameterMatcher();
        } else {
            iNextMatcher = aMatcher;
        }
    }
    
    public PatternParameterMatcher getNextMatcher() {
        if (iNextMatcher == null) {
            return sNullMatcher;
        } else {
            return iNextMatcher;
        }
    }
    
    PatternParameterMatcher getNextMatcherRaw() {
        return iNextMatcher;
    }
}
