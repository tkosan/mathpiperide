%mathpiper,def="PadLeft"
Retract("PadLeft", All);

5 ## PadLeft(number_Number?, totalDigits_Integer?) --> PadLeft(number, totalDigits, 0);


10 ## PadLeft(number_Number?, totalDigits_Integer?, padValue_) -->
{
    Local(integerString, padAmount, resultString);
    
    integerString := ToString(number);
    
    padAmount := totalDigits - Length(integerString);
    
    Decide(padAmount >? 0,
        resultString := ListToString(FillList(padValue, padAmount), "") + integerString,
        resultString := integerString );
}

%/mathpiper




%mathpiper_docs,name="PadLeft",categories="Programming Procedures,Input/Output",access="experimental"
*CMD PadLeft --- converts a number into a string which has a specified width
*STD
*CALL
        PadLeft(number,stringWidth)
        PadLeft(number,stringWidth, padValue)

*PARMS

{number} -- an integer or a decimal number to convert to a string

{stringWidth} -- the width of the string

{padValue} a value to use as the pad instead of 0

*DESC

This procedure converts a number into a string which has a specified width.  If the number
would normally be converted into a string with fewer characters than this width, zero (or
 {padValue} is added to the left side of the string to make it the specified width.

*E.G.
/%mathpiper,title=""

Echo(PadLeft(.1,3));
Echo(PadLeft(20,3));
Echo(PadLeft(5,3));
Echo(PadLeft(5,3,"_"));

/%/mathpiper

    /%output,preserve="false"
      Result: True
      
      Side Effects:
      0.1
      020
      005
      __5
.   /%/output
%/mathpiper_docs



