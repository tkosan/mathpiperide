/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.mathpiper.builtin.BigNumber;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.worksheets.ScreenCapturePanel;


public class ImageFromFileView extends BuiltinProcedure {

    private Map defaultOptions;
    
    public void plugIn(Environment aEnvironment)  throws Throwable
    {
	this.functionName = "ImageFromFileView";
	
        aEnvironment.getBuiltinFunctions().put("ImageFromFileView", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
	
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if(! Utility.isSublist(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.

        Object imageFileArgument = arguments.car();
        
        if(! (imageFileArgument instanceof String)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        String imagePathString = (String) imageFileArgument;
        
        
        Cons options = arguments.cdr();

        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        

        imagePathString = Utility.stripEndQuotesIfPresent(imagePathString);

        imagePathString = Utility.stripEndDollarSigns(imagePathString);

        JLabel imageLabel = new JLabel();
        
        BufferedImage image = ImageIO.read(new File(imagePathString));
        
        ImageIcon icon = new ImageIcon(image);

        imageLabel.setIcon(icon);

        JPanel screenCapturePanel = new ScreenCapturePanel();
        
        screenCapturePanel.add(imageLabel);
 
        JPanel jPanel = new JPanel();
        jPanel.setBackground(Color.WHITE);
        jPanel.add(screenCapturePanel);

        JavaObject response = new JavaObject(jPanel);

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));


    }//end method.


}//end class.





/*
%mathpiper_docs,name="ImageFromFileView",categories="Programming Procedures,Visualization"
*CMD ImageFromFileView --- display an image from a file

*CALL
    ImageFromFileView(expression, option, option, option...)

*PARMS
{expression} -- a string that contains the path to the file


*DESC
Display an image from a file.
 
*E.G.
In> Show(ImageFromFileView("/home/afile.png"))
Result: java.awt.Component


*SEE Show
%/mathpiper_docs
*/



