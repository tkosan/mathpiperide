package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class CurrentIntegrator extends Meter {

    protected String chargeString = "0.0 C";

    public CurrentIntegrator(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        if(uid != null)
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        secondary = "Charge";
        secondaryUnitSymbol = "C";
        primary = "Charge";
        primaryUnitSymbol = "C";
    }
    
    public void reset() {
        super.reset();
        setChargeString("0.00 C");
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        String valueAndUnit = Component.addUnitOperator(getChargeString(), isValueVisible(), isUnitVisible());
        
        if(isValueVisible())
        {
            sg.drawString(valueAndUnit.split("`")[0], Math.round((x1 + x2) * 0.5 + 3) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 3));
        }
        
        if(isUnitVisible())
        {
            sg.drawString(valueAndUnit.split("`")[1], Math.round((x1 + x2) * 0.5 + 16) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 16));
        }
    }

    public String getChargeString() {
        return chargeString;
    }

    public void setChargeString(String chargeString) {
        this.chargeString = chargeString;
    }
}
