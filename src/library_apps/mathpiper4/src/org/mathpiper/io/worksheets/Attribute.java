package org.mathpiper.io.worksheets;

public class Attribute {
    
    private String name;
    private String value;
    private int index;
    
    public Attribute(String name, String value, int index)
    {
        this.name = name;
        this.value = value;
        this.index = index;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String newName)
    {
        this.name = newName;
    }

    public String getValue() {
        return value;
    }
    
    public void setValue(String newValue) {
        value = newValue;
    }

    public int getIndex() {
        return index;
    }
    
    public void setIndex(int newIndex)
    {
        this.index = newIndex;
    }
    
    public String toString()
    {
        return "Name: " + name + ", Value: " + value + ", Index: " + index;
    }
}
