import java.io.File;
import org.mathpiper.io.worksheets.MPWSFile;
import java.io.FieldInputStream;

buffer.save(view,null);
org.gjt.sp.jedit.io.VFSManager.waitForRequests();

startTime = java.lang.System.nanoTime();

// Evaluate all of the mathpiper_grade folds in a worksheet.

File mpwFile = new File(buffer.getPath());
fileStream = new FileInputStream(mpwFile);
foldsMap = org.mathpiper.io.worksheets.MPWSFile.getFoldsMap( "Problem", fileStream, "mathpiper", "problem");

if(foldsMap.size() == 0)
{
	fileStream = new FileInputStream(mpwFile);
	foldsMap = org.mathpiper.io.worksheets.MPWSFile.getFoldsMap( "Problem", fileStream, "uasm65", "problem");
}
fileStream.close();

if(foldsMap.size() == 0)
{
    org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(),"There are no problem folds in the worksheet.");
    return;
}

File mpwFile = new File(buffer.getPath());
fileStream = new FileInputStream(mpwFile);
gradesFoldsMap = org.mathpiper.io.worksheets.MPWSFile.getFoldsMap( "Grade", fileStream, "mathpiper_grade");

if(gradesFoldsMap.size() == 0)
{
	fileStream = new FileInputStream(mpwFile);
	gradesFoldsMap = org.mathpiper.io.worksheets.MPWSFile.getFoldsMap( "Grade", fileStream, "uasm65_grade");
}
fileStream.close();

if(foldsMap.size() == 0)
{
    org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(),"There are no grading folds in the worksheet.");
    return;
}

currentCaretPosition = textArea.getCaretPosition();

interpreter = org.mathpiper.interpreters.Interpreters.getSynchronousInterpreter();

interpreter.evaluate("LoadScript(\"{?possibleProblemPoints := 0; ?totalProblemPoints := 0; ?foldResult? := None;} ;\");", false);

i = 1;

resultString = "";

totalWorksheetPoints = 0;
oldTotalProblemPoints = 0;
oldPossibleProblemPoints = 0;

totalWorksheetProblems = 0;
totalWorksheetProblemsCorrect = 0;

public static int compareNatural(String a, String b) {
    // From https://stackoverflow.com/questions/7270447/java-string-number-comparator.
    int la = a.length();
    int lb = b.length();
    int ka = 0;
    int kb = 0;
    while (true) {
        if (ka == la)
            return kb == lb ? 0 : -1;
        if (kb == lb)
            return 1;
        if (a.charAt(ka) >= '0' && a.charAt(ka) <= '9' && b.charAt(kb) >= '0' && b.charAt(kb) <= '9') {
            int na = 0;
            int nb = 0;
            while (ka < la && a.charAt(ka) == '0')
                ka++;
            while (ka + na < la && a.charAt(ka + na) >= '0' && a.charAt(ka + na) <= '9')
                na++;
            while (kb < lb && b.charAt(kb) == '0')
                kb++;
            while (kb + nb < lb && b.charAt(kb + nb) >= '0' && b.charAt(kb + nb) <= '9')
                nb++;
            if (na > nb)
                return 1;
            if (nb > na)
                return -1;
            if (ka == la)
                return kb == lb ? 0 : -1;
            if (kb == lb)
                return 1;
        }
        if (a.charAt(ka) != b.charAt(kb))
            return a.charAt(ka) - b.charAt(kb);
        ka++;
        kb++;
    }
}

treeSet = new java.util.TreeSet(new java.util.Comparator() {
        public int compare(left, right) {
            compareNatural(left, right);
        }
});

treeSet.addAll(gradesFoldsMap.keySet());

for(foldKey: treeSet)
{
	
//System.out.println("AAA " + foldKey);

	fold = gradesFoldsMap.get(foldKey);
	foldType = fold.getType();

	if(foldType.equals("mathpiper_grade"))
	{
		if (fold.getAttributes().containsKey("name")) {
		    nameAttribute = fold.getAttributeValue("name");
		}
		else
		{
			org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(),"An problem fold is missing a \"name\" attribute.");
		}

		assessmentUnits = fold.getAttributeValue("assessment_unit");
		if(assessmentUnits == null)
		{
		    assessmentUnits = "passes";
		}
		
		problemFold = foldsMap.get(nameAttribute);
		
		
		
		resultString += "\n***** " + nameAttribute + " *****\n\n";
		
		if(problemFold == null)
		{
			resultString += "FAIL: The fold \"" + nameAttribute + "\" is missing. " + "\n\n";
			continue;
		}
		
		if(org.mathpiper.ide.mathpiperplugin.MathPiperPlugin.getHaltButtonState() == true)
		{
			org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(),"A MathPiper evaluation is currently in progress.");
			return;
		}
		
		totalWorksheetProblems++;

		problemCode = problemFold.getContents(); 
		problemCode = problemCode.replace("\\","\\\\");
		problemCode = problemCode.replace("\"","\\\"");
		
		evaluationResponse = interpreter.evaluate("?foldCodeString := \"'{\n" + problemCode + "\n};\";", false);
		
		if(evaluationResponse.isExceptionThrown())
		{
			exception = evaluationResponse.getException();
			resultString += "FAIL: The fold \"" + nameAttribute + "\" threw an exception. " + " 0 " + assessmentUnits + "\n\n";
			continue;
		}
		
		
		codeText = fold.getContents();
		
        if(fold.getAttributes().get("base_six_four") != null && fold.getAttributeValue("base_six_four").equalsIgnoreCase("true"))
        {
            codeText = new java.lang.String(org.mathpiper.builtin.library.base64.Base64.decode(codeText.trim()));
        }
				
		codeText = codeText.replace("\\","\\\\");
		codeText = codeText.replace("\"","\\\"");
		
		evaluationResponse = interpreter.evaluate("FoldPrechecks(3000);", true);
 
		if(evaluationResponse.isExceptionThrown())
		{
			org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(), "The FoldPrechecks procedure has the following error: " + evaluationResponse.getException());
			return;
		}
				
		prechecksSideEffects = evaluationResponse.getSideEffects();
		
		
        evaluationResponse = interpreter.evaluate("FoldGradePointsTotal(\"'{" + codeText + "}\");", true);

        if(evaluationResponse.isExceptionThrown())
        {
            org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(), "The FoldGradePointsTotal procedure has the following error: " + evaluationResponse.getException());
            org.mathpiper.ide.mathpiperplugin.MathPiperPlugin.setHaltButtonState(false);
            return;
        }
        
		
		evaluationResponse = interpreter.evaluate("LoadScript(\" {" + codeText + "}; \");",false);
		
		
		if(evaluationResponse.isExceptionThrown())
		{
			exception = evaluationResponse.getException();
			org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(), exception.getMessage());
			return;
		}
		
		resultString += prechecksSideEffects + evaluationResponse.getSideEffects();
		
		possibleProblemPoints = Integer.parseInt(interpreter.getEnvironment().getGlobalState().get("?possibleProblemPoints").toString());

		totalProblemPoints = Integer.parseInt(interpreter.getEnvironment().getGlobalState().get("?totalProblemPoints").toString());
		
		if(totalProblemPoints == 0)
		{
			diffTotalProblemPoints = 0;
		}
		else
		{
			diffTotalProblemPoints = totalProblemPoints - oldTotalProblemPoints;
		}
		
		totalWorksheetPoints += diffTotalProblemPoints;
		
		diffPossibleProblemPoints = possibleProblemPoints - oldPossibleProblemPoints;
		
		if(diffTotalProblemPoints == diffPossibleProblemPoints)
		{
		    totalWorksheetProblemsCorrect++;
		}
			
		totalPoints = "\n ====> " + diffTotalProblemPoints + "/" + diffPossibleProblemPoints + " " + assessmentUnits + " total, Percent: " + Math.round(Math.floor(((new Integer(diffTotalProblemPoints).doubleValue() / new Integer(diffPossibleProblemPoints).doubleValue()) * 100)));
		
		oldTotalProblemPoints = totalProblemPoints;
		
		oldPossibleProblemPoints = possibleProblemPoints;
		
		resultString += totalPoints + "\n\n";
		
	} 
	else if(foldType.equals("uasm65_grade"))
	{

		if (fold.getAttributes().containsKey("name")) {
		    nameAttribute = (String) fold.getAttributeValue("name");
		}
		else
		{
			org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(),"An problem fold is missing a \"name\" attribute.");
		}
		
		assessmentUnits = fold.getAttributeValue("assessment_unit");
		if(assessmentUnits == null)
		{
		    assessmentUnits = "passes";
		}
		
		problemFold = foldsMap.get(nameAttribute);
		
		
		
		resultString += "\n***** " + nameAttribute + " *****\n\n";
		
		if(problemFold == null)
		{
			resultString += "FAIL: The fold \"" + nameAttribute + "\" is missing. " + "\n\n";
			continue;
		}
		
		if(org.mathpiper.ide.mathpiperplugin.MathPiperPlugin.getHaltButtonState() == true)
		{
			org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(),"A MathPiper evaluation is currently in progress.");
			return;
		}
		
		totalWorksheetProblems++;

		problemCode = problemFold.getContents(); 
		problemCode = problemCode.replace("\\","\\\\");
		problemCode = problemCode.replace("\"","\\\"");
		
		evaluationResponse = interpreter.evaluate("?foldCode := \"" + problemCode + "\";", false);
		
		if(evaluationResponse.isExceptionThrown())
		{
			exception = evaluationResponse.getException();
			resultString += "FAIL: The fold \"" + nameAttribute + "\" threw an exception. " + " 0 " + assessmentUnits + "\n\n" + exception.getMessage();
			continue;
		}
		
		
		codeText = fold.getContents();
		
        if(fold.getAttributes().get("base_six_four") != null && fold.getAttributeValue("base_six_four").equalsIgnoreCase("true"))
        {
            codeText = new java.lang.String(org.mathpiper.builtin.library.base64.Base64.decode(codeText.trim()));
        }
				
		codeText = codeText.replace("\\","\\\\");
		codeText = codeText.replace("\"","\\\"");
		
		
        evaluationResponse = interpreter.evaluate("FoldGradePointsTotal(\"'{" + codeText + "}\");", true);

        if(evaluationResponse.isExceptionThrown())
        {
            org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(), "The FoldGradePointsTotal procedure has the following error: " + evaluationResponse.getException());
            org.mathpiper.ide.mathpiperplugin.MathPiperPlugin.setHaltButtonState(false);
            return;
        }
        

		evaluationResponse = interpreter.evaluate("LoadScript(\" {" + codeText + "}; \");",false);
		
		
		if(evaluationResponse.isExceptionThrown())
		{
			exception = evaluationResponse.getException();
			org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(), exception.getMessage());
			return;
		}
		
		resultString += evaluationResponse.getSideEffects();
		
		possibleProblemPoints = Integer.parseInt(interpreter.getEnvironment().getGlobalState().get("?possibleProblemPoints").toString());

		totalProblemPoints = Integer.parseInt(interpreter.getEnvironment().getGlobalState().get("?totalProblemPoints").toString());
		
		
		if(totalProblemPoints == 0)
		{
			diffTotalProblemPoints = 0;
		}
		else
		{
			diffTotalProblemPoints = totalProblemPoints - oldTotalProblemPoints;
		}
		
		totalWorksheetPoints += diffTotalProblemPoints;
		
		diffPossibleProblemPoints = possibleProblemPoints - oldPossibleProblemPoints;
			
		if(diffTotalProblemPoints == diffPossibleProblemPoints)
		{
		    totalWorksheetProblemsCorrect++;
		}
		
		totalPoints = "\n ====> " + diffTotalProblemPoints + "/" + diffPossibleProblemPoints + " " + assessmentUnits + " total, Percent: " + Math.round(Math.floor(((new Integer(diffTotalProblemPoints).doubleValue() / new Integer(diffPossibleProblemPoints).doubleValue()) * 100)));
		
		oldTotalProblemPoints = totalProblemPoints;
		
		oldPossibleProblemPoints = possibleProblemPoints;
		
		resultString += totalPoints + "\n\n";
		
	}
		
}

resultString += "\n\n======================================================================";

// resultString += "\n\n ====> " + totalWorksheetPoints + "/" + possibleProblemPoints + " " + assessmentUnits + " total, PERCENT: " + Math.round(Math.floor(((new Integer(totalWorksheetPoints).doubleValue() / new Integer(possibleProblemPoints).doubleValue()) * 100)));
resultString += "\n\n ====> " + totalWorksheetProblemsCorrect + "/" + totalWorksheetProblems + " problems correct total, PERCENT: " + Math.round(Math.floor(((new Integer(totalWorksheetProblemsCorrect).doubleValue() / new Integer(totalWorksheetProblems).doubleValue()) * 100)));

textArea.setCaretPosition(buffer.getLength());


if(! textArea.getText().endsWith("\n\n\n"))
{
	buffer.insert(buffer.getLength(),"\n");
	
	if(! textArea.getText().endsWith("\n\n\n"))
	{
		buffer.insert(buffer.getLength(), "\n");
		
		if(! textArea.getText().endsWith("\n\n\n"))
		{
			buffer.insert(buffer.getLength(), "\n");
		}
	}

	textArea.setCaretPosition(buffer.getLength());
}

elapsedTime = java.lang.System.nanoTime() - startTime;

elapsedTimeSeconds = elapsedTime / 1000000000;
	
buffer.insert(buffer.getLength(), "%output,preserve=\"false\"\n" + resultString + "\n\n\nElapsed Time: " + elapsedTimeSeconds + " seconds\n\n.%/output\n");

textArea.setCaretPosition(buffer.getLength());

Thread t = new Thread()
{
	 public void run()
	 {
		buffer.save(view,null);
		
		java.awt.Toolkit.getDefaultToolkit().beep();
	 }
};
t.start();

