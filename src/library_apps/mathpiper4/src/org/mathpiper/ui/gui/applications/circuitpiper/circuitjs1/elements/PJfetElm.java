package org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements;

import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.StringTokenizer;

public class PJfetElm extends JfetElm {

    public PJfetElm(int xx, int yy) {
        super(xx, yy, true);
    }
    
    public PJfetElm(int xa, int ya, int xb, int yb, int f, StringTokenizer st) {
        super(xa, ya, xb, yb, f, st);
    }

    public Class getDumpClass() {
        return JfetElm.class;
    }
}