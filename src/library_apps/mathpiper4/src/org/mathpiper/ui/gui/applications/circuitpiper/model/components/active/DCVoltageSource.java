package org.mathpiper.ui.gui.applications.circuitpiper.model.components.active;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Stack;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.mathpiper.ui.gui.applications.circuitpiper.view.SchematicSymbol;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class DCVoltageSource extends VoltageSource {
    
    public static int componentCounter = 1;

    public DCVoltageSource(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, uid, circuit);
        
        init();
        
        primaryValue = 4.56;
        enteredPrimaryValue = "" + siToValue.get(siPrefix) * primaryValue;
    }
    
    public DCVoltageSource(int x, int y, String uid, Stack<String> attributes, Circuit circuit) throws Exception {
        super(x, y, uid, circuit);
        
        init();
        
        handleAttribute(attributes);
    }

    public void init() {
        setPreselectedPrimaryPrefix(getSiPrefix() + getPrimaryUnitSymbol());
        
        symbol = new SchematicSymbol();
        symbol.type = "DC Voltage Source";
        symbol.templateHeadTerminalPoint = new Point2D.Double(0, 0);
        symbol.templateHeadLeadPoint = new Point2D.Double(0, 0);
        symbol.templateTailTerminalPoint = new Point2D.Double(30, 0);
        symbol.templateTailLeadPoint = new Point2D.Double(30, 0);
        
        // Plus sign.
        symbol.template.put("1", new Line2D.Double(0, -7, 4, -7));
        symbol.template.put("2", new Line2D.Double(2, -5, 2, -9));
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
            int x1 = getHeadTerminal().getX();
            int x2 = getTailTerminal().getX();
            int y1 = getHeadTerminal().getY();
            int y2 = getTailTerminal().getY();
            int rise = y2 - y1;
            int run = x2 - x1;
            int  distanceBetweenTerminalsSquared = (rise * rise + run * run);
            double divisor = Math.sqrt( distanceBetweenTerminalsSquared) / 16.0;
            if ( distanceBetweenTerminalsSquared < 16 * 16) {
                divisor = 1.0;
            }
            double middleX1 = x1 + run / 2.0 - run / 4.0 / divisor;
            double middleY1 = y1 + rise / 2.0 - rise / 4.0 / divisor;
            double middleX2 = x2 - run / 2.0 + run / 4.0 / divisor;
            double middleY2 = y2 - rise / 2.0 + rise / 4.0 / divisor;

            sg.drawLine(x1, y1,  middleX1,  middleY1);
            sg.drawLine( middleX2,  middleY2, x2, y2);
            sg.drawLine( (middleX1 - rise / divisor),  (middleY1 + run / divisor),  (middleX1 + rise / divisor),
                     (middleY1 - run / divisor));
            sg.drawLine( (middleX2 - rise / divisor / 2),  (middleY2 + run / divisor / 2),
                     (middleX2 + rise / divisor / 2),  (middleY2 - run / divisor / 2));

            symbol.drawComponent(getHeadTerminal().getX(), getHeadTerminal().getY(), getTailTerminal().getX(), getTailTerminal().getY(), sg);

        
        
        if(this.circuit.isCirSim && circuit.circuitPanel.isNotMovingAny())
        {
            this.getCircuitElm().doDots(sg);
        }
    }
    
    public String toString()
    {
        return super.toString() + " " + this.getPrimaryValue();
    }
    
}
