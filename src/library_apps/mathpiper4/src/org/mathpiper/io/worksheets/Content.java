package org.mathpiper.io.worksheets;

import org.mathpiper.io.worksheets.MPWSFile2.HintType;

public class Content {
    protected int startLineNumber;
    protected int endLineNumber;
    protected Object contents;
    protected Object originalContents;
    protected boolean isPrint = true;
    protected HintType hintType = HintType.NONE;

    public int getStartLineNumber() {
        return startLineNumber;
    }

    public void setStartLineNumber(int startLineNumber) {
        this.startLineNumber = startLineNumber;
    }

    public int getEndLineNumber() {
        return endLineNumber;
    }

    public void setEndLineNumber(int endLineNumber) {
        this.endLineNumber = endLineNumber;
    }

    public boolean isPrint() {
        return isPrint;
    }

    public void setIsPrint(boolean isPrint) {
        this.isPrint = isPrint;
    }
    
    public HintType getHintType() {
        return hintType;
    }

    public void setHintType(HintType hintType) {
        this.hintType = hintType;
    }
}
