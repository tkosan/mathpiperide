/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp.unparsers;

import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.io.MathPiperOutputStream;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;
import org.mathpiper.lisp.Operator;
import org.mathpiper.lisp.collections.OperatorMap;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.rulebases.ListedRulebase;
import org.mathpiper.lisp.rulebases.MultipleArityRulebase;
import org.mathpiper.lisp.rulebases.SingleArityRulebase;

public class MathPiperUnparser extends LispUnparser {

    StringBuilder spaces = new StringBuilder();
    public static int KMaxPrecedence = 60000;
    OperatorMap iPrefixOperators;
    OperatorMap iInfixOperators;
    OperatorMap iPostfixOperators;
    OperatorMap iBodiedProcedures;
    Environment iCurrentEnvironment;
    private boolean isCompact;

    //private List<Cons> visitedLists = new ArrayList<Cons>();
    public MathPiperUnparser(OperatorMap aPrefixOperators,
            OperatorMap aInfixOperators,
            OperatorMap aPostfixOperators,
            OperatorMap aBodiedOperators) {
        iPrefixOperators = aPrefixOperators;
        iInfixOperators = aInfixOperators;
        iPostfixOperators = aPostfixOperators;
        iBodiedProcedures = aBodiedOperators;
    }
    
    
    @Override
    public String unparse(int aStackTop, Cons aExpression,  Environment aEnvironment, boolean isCompact, boolean isFullParentheses, boolean isNoParentheses, boolean isMultilineArguments) throws Throwable
    {
        String output = super.unparse(aStackTop, aExpression, aEnvironment, isCompact, isFullParentheses, isNoParentheses, isMultilineArguments);
        
        // todo:tk:find a better way to handle the space in front of the unary minus operator. Perhaps use a pattern in "Operator".
        //output = output.replace("  ", " ");
        output = output.replace("  '", " '");
        output = output.replaceAll("^ -", "-");
        output = output.replaceAll("\\( -", "\\(-");
        output = output.replace("^ -", "^-");
        output = output.replace("/ -", "/-");
        output = output.replace("* -", "*-");
        output = output.replace(" ;", ";");
        output = output.replace("};", "}");
        
        if(isCompact)
        {
            output = output.replace("\n", " ");
        }
               
        // Remove outermost parentheses.
        /*
        todo:tk:this removes needed code if the expression is not a mathematical expression.
        if(isFullParentheses && output.length() >= 2)
        {
            output = output.substring(1, output.length() - 1);
        }
        */
            
        return output;
    }

    @Override
    public void print(int aStackTop, Cons aExpression, MathPiperOutputStream aOutput, Environment aEnvironment, boolean isCompact, boolean isFullParentheses, boolean isNoParentheses, boolean isMultilineArguments) throws Throwable {
        iCurrentEnvironment = aEnvironment;

        this.isCompact = isCompact;
        
        this.isFullParentheses = isFullParentheses;
        
        this.isNoParentheses = isNoParentheses;
        
        this.isMultilineArguments = isMultilineArguments;

        printHelper(aEnvironment, aStackTop, aExpression, aOutput, KMaxPrecedence, false);

        //visitedLists.clear();
    }


    void printHelper(Environment aEnvironment, int aStackTop, Cons aExpression, MathPiperOutputStream aOutput, int iPrecedence, boolean isSingleLineCodeSequence) throws Throwable {

        if(aExpression == null) 
        {
            LispError.lispAssert(aEnvironment, aStackTop);
        }

        String tokenName;
        if (aExpression.car() instanceof String) {
            tokenName = (String) aExpression.car();
            boolean bracket = false;
            if (iPrecedence < KMaxPrecedence &&
                    tokenName.charAt(0) == '-' &&
                    (MathPiperTokenizer.isDigit(tokenName.charAt(1)) || tokenName.charAt(1) == '.')) {
                //Code for (-1)/2 .
                bracket = true;
            }
            if (bracket && ! isNoParentheses) {
                writeToken(aOutput, "(");
            }
            writeToken(aOutput, tokenName);
            if (bracket && ! isNoParentheses) {
                writeToken(aOutput, ")");
            }
            return;
        }

        if (aExpression.car() instanceof BuiltinContainer) {
            //TODO display generic class
            writeToken(aOutput, ((BuiltinContainer) aExpression.car()).getObject().getClass().toString());
            return;
        }

        Cons subList = (Cons) aExpression.car();

        if(subList == null) LispError.throwError(aEnvironment, aStackTop, LispError.UNPRINTABLE_TOKEN, "");

        if (subList == null) {
            writeToken(aOutput, "( )");
        } else if (subList.car() instanceof BuiltinContainer)
        {
            writeToken(aOutput, subList.car().toString());
        }else {
            int length = Utility.listLength(aEnvironment, aStackTop, subList);
            tokenName = (String) subList.car();
            Operator prefix = (Operator) iPrefixOperators.map.get(tokenName);
            Operator infix = (Operator) iInfixOperators.map.get(tokenName);
            Operator postfix = (Operator) iPostfixOperators.map.get(tokenName);
            Operator bodied = (Operator) iBodiedProcedures.map.get(tokenName);
            Operator operator = null;
            
            if (length != 2) {
                prefix = null;
                postfix = null;
            }
            if (length != 3) {
                infix = null;
            }
            if (prefix != null) {
                operator = prefix;
            }
            if (postfix != null) {
                operator = postfix;
            }
            if (infix != null) {
                operator = infix;
            }

            if (operator != null) {

                Cons left = null;
                Cons right = null;

                if (prefix != null) {
                    right = subList.cdr();
                } else if (infix != null) {
                    left = subList.cdr();
                    right = subList.cdr().cdr();
                } else if (postfix != null) {
                    left = subList.cdr();
                }


                if (isFullParentheses || (iPrecedence < operator.iPrecedence && !tokenName.equals("Else"))) { //todo:tk:change < to <= to show more parentheses. Is needed for expressions such as '(3*((x+1)*(x-1))).
                    if(! isNoParentheses)
                    {
                        writeToken(aOutput, "(");
                    }
                } else {
                    //Vladimir?    aOutput.write(" ");
                }

                if (left != null) {
                    
                    boolean isAddParenthesesForDivision = ! isNoParentheses && ! isFullParentheses && 
                            (tokenName.equals("/") && Utility.functionType(left).equals("/")) ||
                            (tokenName.equals("/$") && Utility.functionType(left).equals("/$"));

                    if (isAddParenthesesForDivision) {
                        //Code for In> Hold((3/2)/(1/2)) Result> (3/2)/(1/2) .
                        writeToken(aOutput, "(");
                    }

                    printHelper(aEnvironment, aStackTop, left, aOutput, operator.iLeftPrecedence, false);

                    if (isAddParenthesesForDivision ) {
                        //Code for In> Hold((3/2)/(1/2)) Result> (3/2)/(1/2) .
                        writeToken(aOutput, ")");
                    }
                }


                if(tokenName.equals("Else"))
                {
                    writeToken(aOutput, "\n" + spaces.toString());
                }
                else
                {
                    writeToken(aOutput, operator.getSpaceLeft());
                }

                writeToken(aOutput, tokenName);

                writeToken(aOutput, operator.getSpaceRight());


                if (right != null) {
                    
                    boolean isAddParenthesesForDivision = ! isNoParentheses && 
                            (tokenName.equals("/") && Utility.functionType(right).equals("/")) ||
                            (tokenName.equals("/$") && Utility.functionType(right).equals("/$"));

                    if (isAddParenthesesForDivision) {
                        //Code for In> Hold((3/2)/(1/2)) Result> (3/2)/(1/2) .
                        writeToken(aOutput, "(");
                    }

                    //WriteToken(aOutput, operator.spaceRight);


                    printHelper(aEnvironment, aStackTop, right, aOutput, operator.iRightPrecedence, false);

                    if (isAddParenthesesForDivision ) {
                        //Code for In> Hold((3/2)/(1/2)) Result> (3/2)/(1/2) .
                        writeToken(aOutput, ")");
                    }
                }

                if (isFullParentheses || (iPrecedence < operator.iPrecedence && !tokenName.equals("Else"))) { //todo:tk:change < to <= to show more parentheses.
                    if(! isNoParentheses)
                    {
                        writeToken(aOutput, ")");
                    }
                }

            } else {

                Cons  consTraverser =  subList.cdr();

               /*
                   Removing complex number output notation formatting until the problem with Solve(x^3 - 2*x - 7 == 0,x) is resolved.

                   if (functionOrOperatorName == iCurrentEnvironment.iComplexAtom.car()) {

                    Print(consTraverser.getPointer(), aOutput, KMaxPrecedence);

                    consTraverser.goNext(); //Point to second argument.

                    if (!consTraverser.car().toString().startsWith("-")) {
                        writeToken(aOutput, "+");
                    }

                    Print(consTraverser.getPointer(), aOutput, KMaxPrecedence);

                    writeToken(aOutput, "*I");

                } else */
                if (tokenName.equals(iCurrentEnvironment.iListAtom.car())) {

                    /*
                    Cons atomCons = (Cons) subList.getCons();
                    if (visitedLists.contains(atomCons)) {
                    writeToken(aOutput, "{CYCLE_LIST}");
                    return;

                    } else {

                    visitedLists.add(atomCons);*/

                    writeToken(aOutput, "[");

                    while (consTraverser != null) {
                        printHelper(aEnvironment, aStackTop, consTraverser, aOutput, KMaxPrecedence, false);
                        consTraverser = consTraverser.cdr();
                        if (consTraverser != null) {
                            
                                Object o = consTraverser.car();
                                
                                if(o instanceof AtomCons)
                                {
                                    Object o2 = ((AtomCons) o).car();
                                    
                                    if(o2 instanceof String)
                                    {
                                        if(((String) o2).equals("->") )
                                        {
                                            writeToken(aOutput, ", "); 
                                        }
                                        else
                                        {
                                            writeToken(aOutput, ",");
                                        }
                                    }
                                }
                                else
                                {
                                    writeToken(aOutput, ",");
                                }
                        }
                    }//end while.

                    writeToken(aOutput, "]");

                    // }//end else.
                } else if (tokenName.equals(iCurrentEnvironment.iSequenceAtom)) // Program block brackets.
                {
                    if(! isSingleLineCodeSequence)
                    {                    
                        aOutput.write("\n" + spaces.toString());
                    }

                    writeToken(aOutput, "{");
                    
                    if(! isSingleLineCodeSequence)
                    {
                        aOutput.write("\n");
                        spaces.append("    ");
                    }

                    while (consTraverser != null) {
                        if(! isSingleLineCodeSequence)
                        {
                            aOutput.write(spaces.toString());
                        }
                        
                        printHelper(aEnvironment, aStackTop, consTraverser, aOutput, KMaxPrecedence, isSingleLineCodeSequence);
                        
                        if(! isSingleLineCodeSequence)
                        {
                            writeToken(aOutput, ";");
                            aOutput.write("\n");
                        }
                        else
                        {
                            if(consTraverser.cdr() != null)
                            {
                                writeToken(aOutput, "; ");
                            }
                            else
                            {
                                writeToken(aOutput, ";");
                            }
                        }
                        
                        consTraverser = consTraverser.cdr();
                        
                        if(consTraverser != null && ! isSingleLineCodeSequence )
                        {
                            if(consTraverser.car() != null)
                            {
                                Object o = consTraverser.car();
                                if(o instanceof AtomCons)
                                {
                                    Object o2 = ((AtomCons) o).car();
                                    
                                    if(o2 instanceof String)
                                    {
                                        if(! ((String) o2).equals(":=") && ! ((String) o2).equals("::"))
                                        {
                                            aOutput.write("\n"); 
                                        }
                                    }
                                }

                            }

                        }
                    }

                    if(! isSingleLineCodeSequence)
                    {
                        spaces.delete(0, 4);
                        aOutput.write(spaces.toString());
                    }
                    
                    writeToken(aOutput, "}");
                    //aOutput.write("\n");

                } else if (tokenName.equals(iCurrentEnvironment.iNthAtom)) {
                    printHelper(aEnvironment, aStackTop, consTraverser, aOutput, 0, false);
                    consTraverser = consTraverser.cdr();
                    writeToken(aOutput, "[");
                    printHelper(aEnvironment, aStackTop,  consTraverser, aOutput, KMaxPrecedence, false);
                    writeToken(aOutput, "]");
                } else {
                    boolean bracket = false;
                    if (bodied != null) {
                        //printf("%d > %d\n",iPrecedence, bodied.iPrecedence);
                        if (iPrecedence < bodied.iPrecedence) {
                            bracket = true;
                        }
                    }
                    
                    if (bracket) {
                        writeToken(aOutput, "(");
                    }
                    
                    if (tokenName != null) {
                        writeToken(aOutput, tokenName); //Print function name.
                    } else {
                        printHelper(aEnvironment, aStackTop, subList, aOutput, 0, false);
                    }
                    
                    writeToken(aOutput, "("); //Print the opening parentheses of the function argument list.
                    
                    int charsSinceNewline = aOutput.getCharsSinceNewline();

                    Cons counter = consTraverser;
                    int nr = 0;

                    while (counter != null) { //Count arguments.
                        counter = counter.cdr();
                        nr++;
                    }

                    if (bodied != null) {
                        nr--;
                    }
                    
                    MultipleArityRulebase multipleArityRulebase = aEnvironment.getMultipleArityRulebase(aStackTop, tokenName, false);
                    
                    SingleArityRulebase rulebase = null;
                    
                    if(multipleArityRulebase != null)
                    {
                        rulebase = multipleArityRulebase.getUserFunction(nr, aStackTop, aEnvironment);
                    }
                    
                    if(rulebase != null && rulebase instanceof ListedRulebase)
                    {
                        while (nr-- != 1) {
                            printHelper(aEnvironment, aStackTop, consTraverser, aOutput, KMaxPrecedence, true); //Print argument.

                            consTraverser = consTraverser.cdr();

                            if (nr != 0) {
                                
                                if(this.isMultilineArguments)
                                {
                                    writeToken(aOutput, ","); //Print the comma which is between arguments.
                                    aOutput.write("\n" + makeSpaces(charsSinceNewline));
                                }
                                else
                                {
                                    writeToken(aOutput, ", "); //Print the comma which is between arguments.
                                }
                            }
                        }
                        
                        /*
                        todo:tk:this code will not properly unparse a listed procedure that
                        only contains one optional argument. This is due to a bug in ListedRulebase
                        that does not place a single optional argument in a list like it does for
                        multiple optional arguments.
                        */
                        if(Utility.isList(consTraverser))
                        {
                            consTraverser = ((Cons) consTraverser.car()).cdr();
                        }
                        
                        while (consTraverser != null)
                        {
                            printHelper(aEnvironment, aStackTop, consTraverser, aOutput, KMaxPrecedence, true); //Print argument.

                            consTraverser = consTraverser.cdr();

                            if (consTraverser != null) {  
                                
                                if(this.isMultilineArguments)
                                {
                                    writeToken(aOutput, ","); //Print the comma which is between arguments.
                                    aOutput.write("\n" + makeSpaces(charsSinceNewline));
                                }
                                else
                                {
                                    writeToken(aOutput, ", "); //Print the comma which is between arguments.
                                }
                            }
                        }
                    }
                    else
                    {
                        while (nr-- != 0) {
                            printHelper(aEnvironment, aStackTop, consTraverser, aOutput, KMaxPrecedence, true); //Print argument.

                            consTraverser = consTraverser.cdr();

                            if (nr != 0) {
                                
                                if(this.isMultilineArguments)
                                {
                                    writeToken(aOutput, ","); //Print the comma which is between arguments.
                                    aOutput.write("\n" + makeSpaces(charsSinceNewline));
                                }
                                else
                                {
                                    writeToken(aOutput, ", "); //Print the comma which is between arguments.
                                }
                            }
                        }
                    }

                    writeToken(aOutput, ")"); //Print the closing parenthses of the function argument list.

                    if (consTraverser != null) {
                        printHelper(aEnvironment, aStackTop, consTraverser, aOutput, bodied.iPrecedence, false);
                    }

                    if (bracket) {
                        writeToken(aOutput, ")"); 
                    }
                }
            }
        }//end sublist if.
    }

    void writeToken(MathPiperOutputStream aOutput, String aString) throws Throwable {
        /*if (MathPiperTokenizer.isAlNum(iPrevLastChar) && (MathPiperTokenizer.isAlNum(aString.charAt(0)) || aString.charAt(0)=='_'))
        {
        aOutput.write(" ");
        }
        else if (MathPiperTokenizer.isSymbolic(iPrevLastChar) && MathPiperTokenizer.isSymbolic(aString.charAt(0)))
        {
        aOutput.write(" ");
        }*/
        
        /* todo:tk: unparses strings that contain newlines correctly, but mangles indentation on code in general.
        if(aString.startsWith("\"") && aString.endsWith("\"") && aString.contains("\n"))
        {
            aString = aString.replaceAll(" +", " ");
            aString = aString.replaceAll("\n", "\n" + spaces.toString());
        }
        */
 
        if(aString.startsWith("\"") && aString.endsWith("\""))
        {
            String addEscapes = aString.substring(1, aString.length()-1);
            addEscapes = addEscapes.replaceAll("(?<![\\\\])\"", "\\\\\"");
            aString = "\"" + addEscapes + "\"";
        }

        aOutput.write(aString);
    }
    
    private String makeSpaces(int numberOfSpaces)
    {
        StringBuilder sb = new StringBuilder();
        
        for(int index = 0; index < numberOfSpaces; index++)
        {
            sb.append(" ");
        }
        
        return sb.toString();
    }
}