%mathpiper,def="Ln"

2 ## Ln(0)                    --> -Infinity;
2 ## Ln(1)                    --> 0;
2 ## Ln(Infinity)                    --> Infinity;
2 ## Ln(Undefined)                   --> Undefined;

/* 2 ## Ln(-Infinity)                    --> 0; */
2 ## Ln(x_NegativeNumber?)::NumericMode?() --> Complex(Ln(-x), Pi);
3 ## Ln(x_Number?)::(NumericMode?() &? x>=?1) --> InternalLnNum(x);
4 ## Ln(Exp(x_))              --> x;

3 ## Ln(Complex(r_, i_)) --> Complex(Ln(Abs(Complex(r,i))), Arg(Complex(r,i)));
4 ## Ln(x_NegativeNumber?) --> Complex(Ln(-x), Pi);
5 ## Ln(x_Number?)::(NumericMode?() &? x<?1) --> - InternalLnNum(DivideN(1, x));

10 ## Ln(xx_`uu_) --> Ln(UnitsStripAll(NotationStandard(xx`uu)));

Ln(xlist_List?) --> MapSingle("Ln",xlist);

%/mathpiper



%mathpiper_docs,name="Ln",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD Ln --- natural logarithm
*STD
*CALL
        Ln(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure calculates the natural logarithm of "x". This is the
inverse function of the exponential function, {Exp}, i.e. $Ln(x) = y$ implies that $Exp(y) = x$. For complex
arguments, the imaginary part of the logarithm is in the interval
(-$Pi$,$Pi$}. This is compatible with the branch cut of {Arg}.

This procedure is threaded, meaning that if the argument {x} is a
list, the procedure is applied to all entries in the list.

*EXAMPLES
*SEE Exp, Arg
%/mathpiper_docs





%mathpiper,name="Ln",subtype="in_prompts"

Ln(1) ~> 0

Ln(3) ~> Ln(3)

NM(Ln(3)) ~> 1.098612289

Ln(Exp(_x)) ~> _x

%/mathpiper





%mathpiper,name="Ln",subtype="automatic_test"

Verify(Ln(1), 0);

Verify(Ln(3), Ln(3));

Verify(Ln(Exp(_x)), _x);

%/mathpiper