package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.scilab.forge.mp.jlatexmath.TeXConstants;
import org.scilab.forge.mp.jlatexmath.TeXFormula;
import org.scilab.forge.mp.jlatexmath.TeXIcon;

public class Text extends GraphicObject implements Scalable
{
    private double defaultSize = 30;
    private double size = defaultSize;
    //private double yAdjust;
    private Point point;
    private TeXFormula texFormula;
    private double initialYAdjust;

    public Text(String label, Point point, PlotPanel plotPanel)
    {
        this.label = label;
        this.point = point;
        
        String texString = label;
        texString = Utility.stripEndQuotesIfPresent(texString);
        texString = "\\text{\\textbf{" + texString + "}}";
        texFormula = new TeXFormula(texString);
        
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));
    }
    
    public void scale(double scale)
    {
        size *= scale;
    }
    
    public void resetScale()
    {
        size = defaultSize;         
    }

    public void paint(ScaledGraphics sg, PlotPanel plotPanel)
    {
        if (isVisible)
        {
            boolean isDebug = false;
            
            Color oldColor = sg.getColor();

            sg.setColor(Color.BLACK);
            
            sg.drawLatex(texFormula, size, plotPanel.toPixelsX(point.getXCoordinate()), plotPanel.toPixelsY(point.getYCoordinate()), isDebug);
            
            //sg.drawScaledString(point.getLabel(), plotPanel.toPixelsX(point.getXCoordinate()), plotPanel.toPixelsY(point.getYCoordinate()), plotPanel.viewScale);

            if(isDebug)
            {
                int diameter = 3;
                sg.setColor(Color.RED);
                sg.fillOval(plotPanel.toPixelsX(point.getXCoordinate()) - diameter / 2.0, plotPanel.toPixelsY(point.getYCoordinate()) - diameter / 2.0, diameter, diameter);
            }
            
            sg.setColor(oldColor);
        }
    }
    
    public double getTextWidth(PlotPanel plotPanel)
    {
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));

        return plotPanel.toScaleX(icon.getTrueIconWidth());
    }
    
    public double getTextHeight(PlotPanel plotPanel)
    {
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));
        return plotPanel.toScaleY(icon.getTrueIconHeight());
    }
    
    public double getTextBaseline(PlotPanel plotPanel)
    {
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));
        return plotPanel.toScaleY(icon.getBaseLine());
    }

}
