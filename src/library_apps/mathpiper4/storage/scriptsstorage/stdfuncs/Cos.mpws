%mathpiper,def="Cos;CosMap"

1 # CosMap( _n )_(Not?(RationalOrNumber?(n))) <-- ListToFunction([ToAtom("Cos"),n*Pi]);
2 # CosMap( _n )_(n<?0) <-- CosMap(-n);
2 # CosMap( _n )_(n>?2) <-- CosMap(Modulo(n,2));
3 # CosMap( _n )_(n>?1) <-- CosMap(2-n);
4 # CosMap( _n )_(n>?1/2) <-- -CosMap(1-n);

5 # CosMap( 0 ) <-- 1;
5 # CosMap( 1/6 ) <-- Sqrt(3)/2;
5 # CosMap( 1/4 ) <-- Sqrt(2)/2;
5 # CosMap( 1/3 ) <-- 1/2;
5 # CosMap( 1/2 ) <-- 0;
5 # CosMap( 2/5 ) <-- (Sqrt(5)-1)/4;

10 # CosMap(_n) <-- ListToFunction([ToAtom("Cos"),n*Pi]);



2 # Cos(x_Number?)_NumericMode?() <-- CosNum(x);
4 # Cos(ArcCos(_x))           <-- x;
4 # Cos(ArcSin(_x)) <-- Sqrt(1-x^2);
4 # Cos(ArcTan(_x)) <-- 1/Sqrt(1+x^2);
5 # Cos(- _x)_(Not? Constant?(x))                 <-- Cos(x);
6 # (Cos(x_Constant?))_(NegativeNumber?(NM(Eval(x))))   <-- Cos(-x);
// must prevent it from looping

110 # Cos(Complex(_r,_i)) <--
    (Exp(I*Complex(r,i)) + Exp(- I*Complex(r,i))) / (2) ;

6 # Cos(x_Infinity?) <-- Undefined;
6 # Cos(Undefined) <-- Undefined;

200 # Cos(v_CanBeUni(Pi))_(Not?(NumericMode?()) And? Degree(v,Pi) <? 2 And? Coef(v,Pi,0) =? 0) <--
      CosMap(Coef(v,Pi,1));

400 # Cos(x_RationalOrNumber?) <--
    {
     Local(ll);
     ll:= FloorN(NM(Eval(x/Pi)));
     Decide(Even?(ll),x:=(x - Pi*ll),x:=(-x + Pi*(ll+1)));
     ListToFunction([Cos,x]);
     };

400 # Cos(x_RationalOrNumber?) <--
    {
     Local(ll);
     ll:= FloorN(NM(Eval(Abs(x)/Pi)));
     Decide(Even?(ll),x:=(Abs(x) - Pi*ll),x:=(-Abs(x) + Pi*(ll+1)));
     ListToFunction([Cos,x]);
     };




Cos(xlist_List?) <-- MapSingle("Cos",xlist);


%/mathpiper



%mathpiper_docs,name="Cos",categories="Mathematics Functions;Trigonometry (Symbolic)"
*CMD Cos --- trigonometric cosine function
*STD
*CALL
        Cos(x)

*PARMS

{x} -- argument to the function, in radians

*DESC

This function represents the trigonometric function cosine. MathPiper leaves 
expressions alone even if x is a number, trying to keep the result as 
exact as possible. The floating point approximations of these functions 
can be forced by using the {N} function.

MathPiper knows some trigonometric identities, so it can simplify to exact
results even if {N} is not used. This is the case, for instance,
when the argument is a multiple of $Pi$/6 or $Pi$/4.

These functions are threaded, meaning that if the argument {x} is a
list, the function is applied to all entries in the list.

*E.G.

In> Cos(1)
Result: Cos(1);

In> NM(Cos(1),20)
Result: 0.5403023058681397174;

In> Cos(Pi/4)
Result: Sqrt(1/2);

*SEE Sin, Tan, ArcSin, ArcCos, ArcTan, NM, Pi
%/mathpiper_docs



%mathpiper,name="Cos",subtype="automatic_test"

Verify(Cos(0),1);
Verify(Cos(2*Pi), 1);
Verify(Cos(4*Pi), 1);
Verify(NM(Cos(Pi/6)), NM(Sqrt(3/4)));
NumericEqual(NM(Cos(Pi/2),BuiltinPrecisionGet()+1), 0,BuiltinPrecisionGet());

%/mathpiper


