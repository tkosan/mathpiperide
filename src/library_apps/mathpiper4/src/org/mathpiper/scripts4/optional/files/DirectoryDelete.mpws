%mathpiper,def="DirectoryDelete"

DirectoryDelete(fileOrPath) :=
{
	Local(index, files);
	
	If(String?(fileOrPath))
	{
	    fileOrPath := File(fileOrPath);
	}
	
	If(JavaAccess(fileOrPath, "exists"))
	{
		files := DirectoryFiles(JavaAccess(fileOrPath, "getPath"));
		
		For(index := 1, index <=? Length(files), index++)
		{
			If(JavaAccess(files[index], "isDirectory"))
			{
				DirectoryDelete(files[index]);
			}
			Else
			{
				JavaCall(files[index], "delete");
			}
		}
		
		JavaAccess(fileOrPath, "delete");
	}
	Else
	{
	    False;
	}
}

%/mathpiper





%mathpiper_docs,name="DirectoryDelete",categories="Programming Procedures,Input/Output",access="experimental"

*CMD DirectoryDelete --- Deletes a directory

*CALL DirectoryDelete(fileOrPath)
        

*PARMS 
{fileOrPath} -- The path of the directory that is to be deleted or a Java 'File' object.

*DESC
This procedure deletes the specified directory. It returns "True" if the specified directory
was deleted or "False" if it was not deleted.

*E.G.
In> DirectoryDelete("/home/temp_dir")
Result: True
%/mathpiper_docs

