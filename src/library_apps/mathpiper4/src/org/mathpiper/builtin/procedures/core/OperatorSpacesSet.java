/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Operator;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class OperatorSpacesSet extends BuiltinProcedure
{

    private OperatorSpacesSet()
    {
    }

    public OperatorSpacesSet(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {              
        Cons arg2 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 2);
        if( arg2 == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        LispError.checkIsString(aEnvironment, aStackTop, arg2, aStackTop);
        String xfix = (String) arg2.car();
        if( xfix == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        xfix = Utility.stripEndQuotesIfPresent(xfix);
        
        Cons arg3 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 3);
        if( arg3 == null) LispError.checkArgument(aEnvironment, aStackTop, 3);
        LispError.checkIsString(aEnvironment, aStackTop, arg3, aStackTop);
        String leftSpaces = (String) arg3.car();
        if( leftSpaces == null) LispError.checkArgument(aEnvironment, aStackTop, 3);
        
        Cons arg4 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 4);
        if( arg4 == null) LispError.checkArgument(aEnvironment, aStackTop, 4);
        LispError.checkIsString(aEnvironment, aStackTop, arg4, aStackTop);
        String rightSpaces = (String) arg4.car();
        if( rightSpaces == null) LispError.checkArgument(aEnvironment, aStackTop, 4);
        
        Operator op = null;
        
        if(xfix.equals("Prefix"))
        {
            op = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iPrefixOperators, 1);
            
            if (op == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator < " + op + " > is is not a prefix operator.");
            }
        }
        else if (xfix.equals("Infix"))
        {
            op = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iInfixOperators, 1);
            
            if (op == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator < " + op + " > is is not an infix operator.");
            }          
        }
        else if (xfix.equals("Postfix"))
        {
            op = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iPostfixOperators, 1);
            
            if (op == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator < " + op + " > is is not a postfix operator.");
            }
        }
        else
        {
            LispError.throwError(aEnvironment, aStackTop, "The argument < " + xfix + " > is invalid.");
        }
        

        op.setSpaceLeft(Utility.stripEndQuotesIfPresent(leftSpaces));
        
        op.setSpaceRight(Utility.stripEndQuotesIfPresent(rightSpaces));
            
        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}



/*
%mathpiper_docs,name="OperatorSpacesSet",categories="Programming Procedures,Miscellaneous,Built In"
*CMD OperatorSpacesSet --- specify the spaces that are on each side of an operator when it is printed

*CALL
	OperatorSpacesSet(operator, type, spaceBefore, spaceAfter);

*PARMS

{operator} -- string, the name of an operator
{type} -- "Prefix", "Infix" or "Postfix"
{spaceBefore} -- a string that contains zero or more spaces
{spaceAfter} -- a string that contains zero or more spaces

*DESC

This procedure specifies the number of spaces that are on each side of
an operator when it is printed.

*EXAMPLES

*SEE LeftSpacesGet,RightSpacesGet,LeftPrecedenceGet,RightPrecedenceGet,LeftPrecedenceSet,RightPrecedenceSet,RightAssociativeSet,OperatorsGet
%/mathpiper_docs




%mathpiper,name="OperatorSpacesSet",subtype="in_prompts"

Infix("foo", 20) ~> True

1 foo 2 ~> 1 foo 2

OperatorSpacesSet("foo", "Infix", "", "  ") ~> True

1 foo 2 ~> 1foo  2

%/mathpiper
*/