package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.mathpiper.ui.gui.ScreenCapture;

public class GraphPanelController extends MathPanelController {

    //private JSlider zoomSlider;
    private JButton centerButton;
    private JButton resetButton;
    private JSlider channel1PhaseSlider;
    private JSlider xGraphScaleSlider;
    private JSlider yGraphScaleSlider;

    private final PlotPanel plotPanel;

    public GraphPanelController(final ViewPanel viewPanel, double initialValue) {
        super(viewPanel, initialValue);

        plotPanel = (PlotPanel) viewPanel;
        

        /*
        zoomSlider = new JSlider(JSlider.HORIZONTAL,  -100, 100, 0);
        zoomSlider.addChangeListener(this);
        zoomSlider.setPaintLabels(true);
        this.add(new JLabel("Zoom"));
        this.add(zoomSlider);
         */
        xGraphScaleSlider = new JSlider(JSlider.HORIZONTAL, 0, 1000, 100);
        xGraphScaleSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                JSlider source = (JSlider) e.getSource();

                int intValue = (int) source.getValue();
                PlotPanel plotPanel = (PlotPanel) viewPanel;
                plotPanel.setXGraphMax(plotPanel.getXGraphMaxInitial() * Math.pow(10, intValue / 100.0) / 2);
                plotPanel.setXGraphMin(plotPanel.getXGraphMinInitial() * Math.pow(10, intValue / 100.0) / 2);
                plotPanel.repaint();
            }
        });
        xGraphScaleSlider.setPaintLabels(true);
        this.add(xGraphScaleSlider);
        
        
        JPanel xZoomPanel = new JPanel();
        xZoomPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        xZoomPanel.add(new JLabel("X Zoom"));
        xZoomPanel.add(xGraphScaleSlider);
        this.add(xZoomPanel);

        
        yGraphScaleSlider = new JSlider(JSlider.HORIZONTAL, 0, 1000, 100);
        yGraphScaleSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                JSlider source = (JSlider) e.getSource();

                int intValue = (int) source.getValue();
                PlotPanel plotPanel = (PlotPanel) viewPanel;
                plotPanel.setYGraphMax(plotPanel.getYGraphMaxInitial() * Math.pow(10, intValue / 100.0) / 2);
                plotPanel.setYGraphMin(plotPanel.getYGraphMinInitial() * Math.pow(10, intValue / 100.0) / 2);
                plotPanel.repaint();
            }
        });
        yGraphScaleSlider.setPaintLabels(true);
        JPanel yZoomPanel = new JPanel();
        yZoomPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        yZoomPanel.add(new JLabel("Y Zoom"));
        yZoomPanel.add(yGraphScaleSlider);
        this.add(yZoomPanel);
        
        
        centerButton = new JButton("Center Graph");
        centerButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                PlotPanel plotPanel = (PlotPanel) viewPanel;
                plotPanel.center();
                plotPanel.repaint();
            }
        });
        this.add(centerButton);
        
        
        resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                PlotPanel plotPanel = (PlotPanel) viewPanel;
                plotPanel.resetGraph();
                plotPanel.repaint();
            }            
        });
        this.add(resetButton);
        
        
        JCheckBox hideAxesCheckBox = new JCheckBox("Hide Axes");
        hideAxesCheckBox.setSelected(false);
        hideAxesCheckBox.addItemListener(new ItemListener() {

                public void itemStateChanged(ItemEvent ie) {
                    if (ie.getStateChange() == ItemEvent.SELECTED) {
                        plotPanel.setShowAxes(false);
                    } 
                    else 
                    {
                        plotPanel.setShowAxes(true);
                    }
                }
        });
        this.add(hideAxesCheckBox);
    
 
        JButton imageButton = new JButton("Image");
        imageButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    JFileChooser saveImageFileChooser = new JFileChooser();

                    int returnValue = saveImageFileChooser.showSaveDialog(plotPanel);

                    if (returnValue == JFileChooser.APPROVE_OPTION)
                    {
                        File exportImageFile = saveImageFileChooser.getSelectedFile();

                        Thread t = new Thread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                try
                                {
                                    plotPanel.center();
                                    boolean oldAxes = plotPanel.isShowAxes();
                                    plotPanel.setShowAxes(false);
                             
                                    BufferedImage diagramImage = plotPanel.getImageOfDiagram();
                                    
                                    plotPanel.setShowAxes(oldAxes);

                                    ScreenCapture.writeImage(diagramImage, exportImageFile.getAbsolutePath());
                                    //ScreenCapture.showImage(diagramImage);
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }

                        });
                        
                        t.start();
                    }
                }
                catch (Throwable ex)
                {
                    ex.printStackTrace();
                }
            }            
        });
        this.add(imageButton);
        
    }
    
    public void removeScaleSlider()
    {
        super.removeScaleSlider();
    }
}
