%mathpiper,def="NonNegativeNumber?"

NonNegativeNumber?(x):= Number?(x) &? x >=? 0;

%/mathpiper



%mathpiper_docs,name="NonNegativeNumber?",categories="Programming Procedures,Predicates"
*CMD NonNegativeNumber? --- tests if a number is a positive or 0

*CALL
        
NonNegativeNumber?(n)

*PARMS

{n} -- number to be tested

*DESC


{NonNegativeNumber?(n)} evaluates to True if {n} is a positive value, or is equal to 0. If {n} is not a number, the procedure will evaluate to {False}.

*E.G.

In> NonNegativeNumber?(1)
Result: True

In> NonNegativeNumber?(1.0001)
Result: True

In> NonNegativeNumber?(0)
Result: True

In> NonNegativeNumber?(-1)
Result: False

*SEE Number?, NegativeNumber?, NegativeInteger?, NonNegativeInteger?, PositiveNumber? 
%/mathpiper_docs



%mathpiper,name="NonNegativeNumber?",subtype="automatic_test"

Verify(NonNegativeNumber?(0), True);

Verify(NonNegativeNumber?(1.09), True);

Verify(NonNegativeNumber?(-1.5), False);

%/mathpiper