%mathpiper,def="Sin;SinMap"


1 # SinMap( _n )_(Not?(RationalOrNumber?(n))) <-- ListToFunction([ToAtom("Sin"),n*Pi]);
2 # SinMap( _n )_(n<?0) <-- -SinMap(-n);
2 # SinMap( _n )_(n>?2) <-- SinMap(Modulo(n,2));
3 # SinMap( _n )_(n>?1) <-- SinMap(n-2);
4 # SinMap( _n )_(n>?1/2) <-- SinMap(1-n);

5 # SinMap( n_Integer? ) <-- 0;
5 # SinMap( 1/6 ) <-- 1/2;
5 # SinMap( 1/4 ) <-- Sqrt(2)/2;
5 # SinMap( 1/3 ) <-- Sqrt(3)/2;
5 # SinMap( 1/2 ) <-- 1;
5 # SinMap( 1/10) <-- (Sqrt(5)-1)/4;

10 # SinMap(_n) <-- ListToFunction([ToAtom("Sin"),n*Pi]);




2 # Sin(x_Number?)_NumericMode?() <-- SinNum(x);
4 # Sin(ArcSin(_x))           <-- x;
4 # Sin(ArcCos(_x)) <-- Sqrt(1-x^2);
4 # Sin(ArcTan(_x)) <-- x/Sqrt(1+x^2);
5 # Sin(- _x)_(Not? Constant?(x))                 <-- -Sin(x);
6 # (Sin(x_Constant?))_(NegativeNumber?(NM(Eval(x))))   <-- -Sin(-x);

// must prevent it from looping
6 # Sin(x_Infinity?)                 <-- Undefined;
6 # Sin(Undefined) <-- Undefined;

110 # Sin(Complex(_r,_i)) <--
    (Exp(I*Complex(r,i)) - Exp(- I*Complex(r,i))) / (I*2) ;

200 # Sin(v_CanBeUni(Pi))_(Not?(NumericMode?()) And? Degree(v,Pi) <? 2 And? Coef(v,Pi,0) =? 0) <--
{
  SinMap(Coef(v,Pi,1));
};


Sin(xlist_List?) <-- MapSingle("Sin",xlist);

%/mathpiper



%mathpiper_docs,name="Sin",categories="Mathematics Functions;Trigonometry (Symbolic)"
*CMD Sin --- trigonometric sine function
*STD
*CALL
        Sin(x)

*PARMS

{x} -- argument to the function, in radians

*DESC

This function represents the trigonometric function sine. MathPiper leaves 
expressions alone even if x is a number, trying to keep the result as 
exact as possible. The floating point approximations of these functions 
can be forced by using the {N} function.

MathPiper knows some trigonometric identities, so it can simplify to exact
results even if {N} is not used. This is the case, for instance,
when the argument is a multiple of $Pi$/6 or $Pi$/4.

These functions are threaded, meaning that if the argument {x} is a
list, the function is applied to all entries in the list.

*E.G.

In> Sin(1)
Result: Sin(1);

In> NM(Sin(1),20)
Result: 0.84147098480789650665;

In> Sin(Pi/4)
Result: Sqrt(2)/2;

*SEE Cos, Tan, ArcSin, ArcCos, ArcTan, NM, Pi
%/mathpiper_docs





%mathpiper,name="Sin",subtype="automatic_test"

Verify(NM(Sin(a)),Sin(a));

Verify(Sin(2*Pi), 0);
Verify(Sin(3*Pi/2)+1, 0);
Verify(Sin(Pi/2), 1);

%/mathpiper