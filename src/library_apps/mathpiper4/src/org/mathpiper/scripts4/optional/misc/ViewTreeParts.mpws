%mathpiper,def="TreeParts;ViewTreeParts"

Retract("TreeParts", All);
Retract("ViewTreeParts", All);

RulebaseListedHoldArguments("TreeParts", ["tree", "optionsList"]);
RulebaseListedHoldArguments("ViewTreeParts", ["tree", "optionsList"]);

//Handle no options call.
5 ## TreeParts(tree_) --> TreeParts(tree, []);
5 ## ViewTreeParts(tree_) --> ViewTreeParts(tree, []);


//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
10 ## TreeParts(tree_, optionsList_List?) -->
{

    Local(list,
        result, 
        options, 
        treeScale, 
        latexScale, 
        showPositions, 
        function, 
        path, 
        pattern, 
        process,
        arcsHighlight?,
        arcsAutoHighlight?,
        resizable?,
        showDepths?,
        showPositions?, 
        rootNodeHighlight?, 
        nodesHighlight?, 
        leafNodesHighlight?,
        includeExpression?,
        fontSize,
        positionHighlight,
        pathNumbers?,
        code?,
        debug?,
        width,
        height,
        center?,
        nodeHighlight);
    

    options := OptionsToAssociationList(optionsList);

    treeScale := Decide(Number?(options["Scale"]), options["Scale"], 2.0);
    fontSize := Decide(Number?(options["FontSize"]), options["FontSize"], 30.0);
    showPositions? := Decide(Boolean?(options["ShowPositions"]), options["ShowPositions"], False);
    showDepths? := Decide(Boolean?(options["ShowDepths"]), options["ShowDepths"], False);
    resizable? := Decide(Boolean?(options["Resizable"]), options["Resizable"], False);
    arcsHighlight? := Decide(Boolean?(options["ArcsHighlight"]), options["ArcsHighlight"], False);
    arcsAutoHighlight? := Decide(Boolean?(options["ArcsAutoHighlight"]), options["ArcsAutoHighlight"], True);
    nodesHighlight? := Decide(Boolean?(options["NodesHighlight"]), options["NodesHighlight"], False);
    leafNodesHighlight? := Decide(Boolean?(options["LeafNodesHighlight"]), options["LeafNodesHighlight"], False);
    rootNodeHighlight? := Decide(Boolean?(options["RootNodeHighlight"]), options["RootNodeHighlight"], False);
    includeExpression? := Decide(Boolean?(options["IncludeExpression"]), options["IncludeExpression"], True);
    pattern := options["Pattern"];
    path := options["Path"];
    nodeHighlight := options["NodeHighlight"];
    pathNumbers? := Decide(Boolean?(options["PathNumbers"]), options["PathNumbers"], False);
    code? := Decide(Boolean?(options["Code"]), options["Code"], False);
    debug? := Decide(Boolean?(options["Debug"]), options["Debug"], False);

    If(path !=? None &? !? List?(path))
    {
        ExceptionThrow("", "The \"Path\" option must be a list."); 
    }

    tree := Copy(tree);

    If(!? code?)
    {
        tree := ObjectToMeta(tree);
    }
    
    
    process := [
        [
            "function",
            Lambda([trackingList,positionString,node], 
            {   
                MetaSet(node,"HighlightColor","ORANGE");

                node; 
            })
        ]
    ];
    function := /` '(TreeProcess("expression", "pattern", @process, Position:"position"));

    If(path !=? None)
    {
        TreeView(tree, Process:function, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathHighlight:path, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?);
    }
    Else If(pattern !=? None)
    {
        list := [
            ["track",[]],
            
            ["function",
                Lambda([trackingList, positionString, node], 
                {
                    Append!(trackingList["track"], [ToString(node), positionString]);
                    
                    MetaSet(node,"HighlightColor","ORANGE");
                    
                    node; 
                })
            ]
        ];
        
        ForEach(entry, list["track"])
        {
            Echo("Subtree: " + entry[1] + ", Position: " + entry[2]);
        }
        
        result := TreeProcess(MetaToObject(tree), pattern, list);
        
        TreeView(result, Process:function, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?);
    }    
    Else If(leafNodesHighlight?)
    {
        Local(leafPositions);
        
        leafPositions := PositionsLeaves(tree);
        ForEach(leafPosition, leafPositions)
        {
            tree := Mark(tree, leafPosition, a_Atom?, "ORANGE");
        }
        
        TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?);
    }
    Else If(rootNodeHighlight?)
    {
        tree := Mark(tree, "", a_Atom?, "ORANGE");
        
        TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?);
    }
    Else If(nodeHighlight !=? None)
    {
        If(Atom?(nodeHighlight))
        {
            nodeHighlight := [nodeHighlight];
        }
        
        ForEach(position, nodeHighlight)
        {
            tree := Mark(tree, position, a_Atom?, "ORANGE");
        }
        
        TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?);
    }
    Else
    {
        TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, NodesHighlight:nodesHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?);
    }
    
}

10 ## ViewTreeParts(tree_, optionsList_List?) -->
{
    Local(options, width, height, center?);
    
    options := OptionsToAssociationList(optionsList);
    
    width := Decide(Integer?(options["Width"]), options["Width"], Null);
    height := Decide(Integer?(options["Height"]), options["Height"], Null);
    center? := Decide(Boolean?(options["Center"]), options["Center"], False);
    
    Show(Eval(/`ListToProcedure(Concat(['TreeParts, ' ' @tree],optionsList))),returnContent:True, Width:width, Height:height, Center:center?);
}


//Handle a single option call because the option does not come in a list for some reason.
20 ## TreeParts(tree_, singleOption_) --> TreeParts(tree, [singleOption]);
20 ## ViewTreeParts(tree_, singleOption_) --> ViewTreeParts(tree, [singleOption]);


//ViewTreeParts('((a+b)* (c+d+a)), Pattern:(a_ + b_));

//ViewTreeParts('((a+b)* (c+d+a)),  ShowDepths:True);

//ViewTreeParts('((a+b)* (c+d+a)), Path:["11","12"]);

//ViewTreeParts('((a+b)* (c+d+3)), ArcsHighlight:True);

//ViewTreeParts('((a+b)* (c+d+3)), LeafNodesHighlight:True);

//ViewTreeParts('((a+b)* (c+d+3)), RootNodeHighlight:True);

//TreeParts('((a+b)* (c+d+3)), NodesHighlight:True);

%/mathpiper



%mathpiper_docs,name="ViewTreeParts",categories="Programming Procedures,Visualization"
*CMD ViewTreeParts --- Displays a Java GUI component containing an expression in tree form.

*CALL
    ViewTreeParts(expression)

    ViewTreeParts(expression, option, option, ...)


*PARMS

{expression} -- an expression (which may be in string form) to display as an expression tree

{options}:

{Scale} -- Alters the scale of the tree. (Defaults to 2.0)

{FontSize} -- Alters the size of the font. (Defaults to 30.0)

{NodeHighlight} -- Highlights a node with a specified tree path. (Path is a string with each node position separated by a comma)

{ShowDepths} -- If set to True, displays each node's distance from the root node of the tree.

{ShowPositions} -- If set to True, displays each branch's position in relation to its parent node.

{Resizable} -- If set to True, allows for resizing of both the linear view and the tree view of {expression}.

{ArcsHighlight} -- If set to True, highlights each arc between nodes.

{NodesHighlight} -- If set to True, highlights each node of the tree.

{LeafNodesHighlight} -- If set to True, highlights each leaf node of the tree.

{RootNodeHighlight} -- If set to True, highlights the root node of the tree

{IncludeExpression} -- If set to True, displays the linear view of {expression} at the top of window. (Defaults to True)

{Code} -- If set to True, the expression is rendered using code symbols instead of mathematical symbols.

{Pattern} -- Highlights nodes in the tree that match the provided pattern.



*DESC
Returns a Java GUI component that contains an expression rendered as an expression tree.
Options are entered using the : operator. For example, here is how to disable {Resizable} option: {Resizable: False}.

Any number and combination of options may be used, however some may not be effective when paired with others, for example: {NodesHighlight: True} and {LeafNodesHighlight: True}

 
*E.G.
In> ViewTreeParts("(1 + a_) / b_ * c_ == 13", Scale: 3.0, FontSize: 35.0, NodesHighlight:True)
Result: class javax.swing.Box

In> ViewTreeParts('((1 + 12) / 2 * 5), Pattern:(a_Integer?::(a >? 3)), FontSize: 35.0, Code:True)
Result: class javax.swing.Box

In> ViewTreeParts('((1 + 12) / 2 * 5), NodeHighlight:"1,1,2")
Result: class javax.swing.Box

In> ViewTreeParts('((1 + 12) / 2 * 5))
Result: class javax.swing.Box


*SEE TreeView

%/mathpiper_docs
