package org.mathpiper.lisp;


public class ProfileData
{
    public Long totalTime = 0l;
    public Long enterTime = 0l;
    public String procedureName;
    
    public String toString()
    {
        long ms = totalTime / 1000000;
        return procedureName + " " + ms;
    }
}
