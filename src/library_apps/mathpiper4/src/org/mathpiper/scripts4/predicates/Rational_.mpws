%mathpiper,def="Rational?"

/* changed definition of Rational?, Nobbi 030529
Procedure("Rational?",["aLeft"]) Type(aLeft) = "/";

Procedure("RationalNumeric?",["aLeft"])
    Type(aLeft) = "/" &?
    Number?(aLeft[1]) &?
    Number?(aLeft[2]);

RationalOrNumber?(x_) --> (Number?(x) |? RationalNumeric?(x));

10 ## RationalOrInteger?(x_Integer?) --> True;
10 ## RationalOrInteger?(x_Integer? / y_Integer?) --> True;
20 ## RationalOrInteger?(x_) --> False;

*/

10 ## Rational?(x_Integer?) --> True;
10 ## Rational?(x_Integer? / y_Integer?) --> True;
10 ## Rational?(-(x_Integer? / y_Integer?)) --> True;
60000 ## Rational?(x_) --> False;

%/mathpiper



%mathpiper_docs,name="Rational?",categories="Programming Procedures,Predicates"
*CMD Rational? --- test whether argument is a rational
*STD
*CALL
        Rational?(expr)

*PARMS

{expr} -- expression to test

*DESC

This command tests whether the expression "expr" is a rational
number, i.e. an integer or a fraction of integers.

*EXAMPLES

*SEE Numerator, Denominator
%/mathpiper_docs





%mathpiper,name="Rational?",subtype="in_prompts"

Rational?(5) -> True

Rational?(2/7) -> True

Rational?(0.5) -> False

Rational?('a/'b) -> False

Rational?('x + 1/'x) -> False

%/mathpiper





%mathpiper,name="Rational?",subtype="automatic_test"

Verify(Rational?(2),True);
Verify(Rational?(2/5),True);
Verify(Rational?(-2/5),True);
Verify(Rational?(2.0/5),False);
Verify(Rational?(Pi/2),False);

%/mathpiper