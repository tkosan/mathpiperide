/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import org.mathpiper.builtin.BigNumber;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;

import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.LayoutTechnique;
import org.mathpiper.ui.gui.VerticalFlow;
import org.mathpiper.ui.gui.VerticalLine;

public class Show extends BuiltinProcedure implements WindowFocusListener
{

    private Map defaultOptions;

    private boolean initialFocusGained = false;

    public void plugIn(Environment aEnvironment) throws Throwable
    {
        this.functionName = "Show";

        aEnvironment.getBuiltinFunctions().put("Show", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Title", null);
        defaultOptions.put("returnContent", false);
        defaultOptions.put("Width", null);
        defaultOptions.put("Height", null);
        defaultOptions.put("Center", false);
        defaultOptions.put("Menu", true);
        defaultOptions.put("ScrollPane", true);
        defaultOptions.put("VisibleDuration", new BigNumber("-1.0", 10, 10));
        defaultOptions.put("VerticalFlow?", false);
        defaultOptions.put("VerticalFlowHeight", new BigNumber("1000.0", 10, 10));
        defaultOptions.put("Border?", false);
        defaultOptions.put("Bordercolor", Color.BLACK);

    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {

        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if (!Utility.isSublist(arguments))
        {
            LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
        }

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.

        Cons dataList = null;

        if (Utility.isSublist(arguments))
        {
            dataList = (Cons) arguments.car(); //Grab the first member of the list arguments list.

            dataList = dataList.cdr(); //Strip List tag.
        }
        else
        {
            dataList = arguments.copy(false);
        }

        Cons options = arguments.cdr();

        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);


        final JFrame frame = new JFrame();

        frame.addWindowFocusListener(this);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        frame.setBackground(Color.WHITE);
        contentPane.setBackground(Color.WHITE);

        //frame.setAlwaysOnTop(false);
        frame.setTitle((String) userOptions.get("title"));
        frame.setResizable(true);
        frame.pack();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = userOptions.get("Width") == null ? screenSize.width / 2 : ((BigNumber) userOptions.get("Width")).toInt();
        int height = userOptions.get("Height") == null ? screenSize.height / 2 : ((BigNumber) userOptions.get("Height")).toInt();
        frame.setSize(width, height);

        final int visibleDuration = ((BigNumber) userOptions.get("VisibleDuration")).toInt();

        if ((Boolean) userOptions.get("Center"))
        {
            frame.setLocationRelativeTo(null);
        }

        LayoutTechnique componentsPanel;

        if ((Boolean) userOptions.get("VerticalFlow?"))
        {            
            componentsPanel = new VerticalFlow(((BigNumber) userOptions.get("VerticalFlowHeight")).toDouble());
        }
        else
        {
            componentsPanel = new VerticalLine(((Boolean) userOptions.get("Border?")), ((Color) userOptions.get("BorderColor")));
        }
        
        final LayoutTechnique componentsPanelFinal = componentsPanel;
                
        componentsPanel.setList(aEnvironment, aStackTop, dataList);

        if ((Boolean) userOptions.get("ScrollPane"))
        {
            JScrollPane scrollPane = new JScrollPane(componentsPanelFinal, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.getVerticalScrollBar().setUnitIncrement(16);
            contentPane.add(scrollPane);
        }
        else
        {
            contentPane.add(componentsPanelFinal);
        }

        if ((Boolean) userOptions.get("Menu"))
        {
            JMenu fileMenu = new JMenu("File");

            JMenuItem saveAsImageAction = new JMenuItem();
            saveAsImageAction.setText("Save As Image");
            saveAsImageAction.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent ae)
                {
                    org.mathpiper.ui.gui.Utility.saveImageOfComponent(componentsPanelFinal);
                }
            });
            fileMenu.add(saveAsImageAction);

            JMenuBar menuBar = new JMenuBar();

            menuBar.add(fileMenu);

            frame.setJMenuBar(menuBar);
        }

        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                frame.setVisible(true);

                if (visibleDuration != -1.0)
                {

                    final Timer timer = new Timer();

                    timer.schedule(new TimerTask()
                    {
                        public void run()
                        {
                            timer.cancel();

                            frame.setVisible(false);
                        }
                    }, visibleDuration);
                }

            }
        });

        JavaObject response = null;

        if (((Boolean) userOptions.get("returnContent")) == true)
        {
            response = new JavaObject(componentsPanelFinal);
        }
        else
        {
            response = new JavaObject(frame);
        }

        while (!initialFocusGained)
        {
            try
            {
                Thread.sleep(50);
            } catch (InterruptedException ie)
            {
                // Eat the exception.
            }
        }

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));

    }//end method.

    public void windowGainedFocus(WindowEvent e)
    {
        initialFocusGained = true;
    }

    public void windowLostFocus(WindowEvent e)
    {

    }

}//end class.

/*
 %mathpiper_docs,name="Show",categories="Programming Procedures,Visualization"
 *CMD Show --- displays GUI components
 *CORE
 *CALL
 Show(component, option, option, option...)

 *PARMS

 {component} -- a Java GUI component

 {Options:}

 {Title} -- the title of the window


 *DESC

 Displays a GUI window that contains a Java GUI component. These GUI components 
 are usually returned from XXXView functions and charting functions.



 *E.G.
 In> Show(TreeView( '(a*(b+c) == a*b + a*c)))
 Result: class javax.swing.JFrame

 *SEE TreeView, LatexView

 %/mathpiper_docs
 */
