%mathpiper,def="TreeDepth"

TreeDepth(expression) :=
{
    Local(allPositions, deepest, length);
    
    allPositions := PositionsPattern2(expression, a_Atom?);

    deepest := 0;
    
    ForEach(position, allPositions)
    {
        length := Length(Remove(StringToList(position), ","));
        If(length >? deepest)
        {
            deepest := length;
        }
    }
    
    deepest;
}

%/mathpiper





%mathpiper_docs,name="TreeDepth",categories="Programming Procedures,Expression Trees"
*CMD TreeDepth --- determine the depth of an expression tree

*CALL
    TreeDepth(expression)

*PARMS

{expression} -- an expression

*DESC

Determine the depth of an expression tree.

*E.G.
In> TreeDepth('(2 + (_b*_c) / _b - 3))
Result: 4
%/mathpiper_docs





%mathpiper,name="TreeDepth",subtype="automatic_test"

Verify(TreeDepth('(2 + (_b*_c) / _b - 3)), 4);

%/mathpiper
