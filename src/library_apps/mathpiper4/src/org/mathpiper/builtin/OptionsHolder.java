
package org.mathpiper.builtin;

import java.util.Map;

public interface OptionsHolder {
    Map getOptions();
}
