%mathpiper,def="EigenValues"

// diagonal matrices will be caught by UpperTriangular?
10 # EigenValues(matrix_UpperTriangular?) <-- Diagonal(matrix);
10 # EigenValues(matrix_LowerTriangular?) <-- Diagonal(matrix);

20 # EigenValues(matrix_Matrix?) <-- Roots(CharacteristicEquation(matrix,xx));

%/mathpiper



%mathpiper_docs,name="EigenValues",categories="Mathematics Functions;Linear Algebra"
*CMD EigenValues --- get eigenvalues of a matrix
*STD
*CALL
        EigenValues(matrix)

*PARMS

{matrix} -- a square matrix

*DESC

EigenValues returns the eigenvalues of a matrix.
The eigenvalues x of a matrix M are the numbers such that
$M*v=x*v$ for some vector.

It first determines the characteristic equation, and then factorizes this
equation, returning the roots of the characteristic equation
Det(matrix-x*identity).

*E.G.

In> M:=[[1,2],[2,1]]
Result: [[1,2],[2,1]];

In> EigenValues(M)
Result: [3,-1];

*SEE EigenVectors, CharacteristicEquation
%/mathpiper_docs