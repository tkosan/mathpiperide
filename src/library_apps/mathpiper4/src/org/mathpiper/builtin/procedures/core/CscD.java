/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

/**
 *
 * @author christopher
 */
public class CscD extends BuiltinProcedure
{

    private CscD()
    {
    }

    public CscD(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        BigNumber x;

        x = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 1);

        double xDouble = x.toDouble();

        double result = 1 / Math.sin(xDouble);

        if(Double.isNaN(result))
        {
            LispError.raiseError("The result is NaN.", aStackTop, aEnvironment);
        }

        BigNumber z = new BigNumber(aEnvironment.getPrecision());

        z.setTo(result);

        setTopOfStack(aEnvironment, aStackTop, new org.mathpiper.lisp.cons.NumberCons(z));
    }
}//end class.




/*
%mathpiper_docs,name="CscD",categories="Mathematics Procedures,Numeric,Trigonometry (Numeric)"
*CMD CscD --- double-precision math function
*CORE
*CALL
	CscD(x)

*PARMS
{x} -- a number

*DESC
A double precision version of the Csc function.

*EXAMPLES
*SEE SinD, CosD, TanD, SecD, CotD, ArcSinD, ArcCosD, ArcTanD, ArcCscD, ArcSecD, ArcCotD
%/mathpiper_docs





%mathpiper,name="CscD",subtype="in_prompts"

CscD(.7) ~> 1.552270327

CscD(-1.2) ~> -1.072916378

%/mathpiper





%mathpiper,name="CscD",subtype="automatic_test"

Verify(CscD(.7), 1.552270327);

Verify(CscD(-1.2), -1.072916378);

%/mathpiper
*/