%mathpiper,def="V;InVerboseMode"

LocalSymbols(Verbose) {

  Assign(Verbose,False);


  Function("V",[aNumberBody])
  {
    Local(prevVerbose,result);
    Assign(prevVerbose,Verbose);
    Assign(Verbose,True);
    Assign(result,Eval(aNumberBody));
    Assign(Verbose,prevVerbose);
    result;
  };


  Function("InVerboseMode",[]) Verbose;

}; // LocalSymbols(Verbose)

HoldArgument("V",aNumberBody);
UnFence("V",1);

%/mathpiper





%mathpiper_docs,name="V;InVerboseMode",categories="Programming Functions;Input/Output"
*CMD V, InVerboseMode --- set verbose output mode
*STD
*CALL
        V(expression)
        InVerboseMode()

*PARMS

[expression] -- expression to be evaluated in verbose mode

*DESC

The procedure {V(expression)} will evaluate the expression in
verbose mode. Various parts of MathPiper can show extra information
about the work done while doing a calculation when using {V}.

In verbose mode, {InVerboseMode()} will return {True}, otherwise
it will return {False}.

*E.G. notest

In> OldSolve([x+2==0],[x])
Result: [[-2]];

In> V(OldSolve([x+2==0],[x]))
        Entering OldSolve
        From  x+2==0  it follows that  x  = -2
           x+2==0  simplifies to  True
        Leaving OldSolve
Result: [[-2]];

In> InVerboseMode()
Result: False

In> V(InVerboseMode())
Result: True

*SEE Echo, NM, OldSolve
%/mathpiper_docs