package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;


public abstract class GraphicObject
{
    protected String label;
    
    protected boolean isShowLabel = true;
    
    protected boolean isVisible = true;

    protected Color highlightColor = new Color(243, 128, 43);
    
    protected boolean isSelected = false;
    
    protected Color color = Color.BLUE;
    
    protected String style = null;
    
    public String getLabel()
    {
        return label;
    }
    
    public String setLabel(String label)
    {
        return this.label = label;
    }
    
    abstract public void paint(ScaledGraphics sg, PlotPanel plotPanel);

    public Color getColor()
    {
        return color;
    }

    public boolean isShowLabel()
    {
        return isShowLabel;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public void showLabel(boolean isShowLabel)
    {
        this.isShowLabel = isShowLabel;
    }

    public void setVisible(boolean isVisible)
    {
        this.isVisible = isVisible;
    }

    public String getStyle()
    {
        return style;
    }

    public void setStyle(String style)
    {
        this.style = style;
    }
}
