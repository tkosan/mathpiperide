v.01

The directory that this file is in contains an introduction
to computer programming course which uses the MathPiper
language. All of the assignments in this course are
automatically graded.

Each problem contains a %mathpiper fold into which
a solution program is typed. Beneath each of these
folds is a %mathpiper_grade fold that provides
immediate feedback on the code that is in the
%mathpiper fold it is associated with.

Module 1: Using MathPiperIDE as a text editor and a calculator.
Module 2: MathPiperIDE worksheets and programming fundamentals.
Module 3: Lists, random integers, making decisions, and loops.
Module 4: Predicate procedures, using loops with lists.
Module 5: Review for exam 1.
Exam 1.
Module 6: Procedures, and breaking large problems into smaller ones.
Module 7: Additional MathPiper library procedures, drawing points.
Module 8: Debugging and drawing points, review for exam 2.
Exam 2.
Module 9: Trigonometry and drawing circles with points.
Module 10: Drawing points, review for exam 3.
Exam 3.