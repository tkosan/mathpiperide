%mathpiper,def="NearRational"

//////////////////////////////////////////////////
/// NearRational, GuessRational
//////////////////////////////////////////////////

/// find rational number with smallest num./denom. near a given number x
/// See: HAKMEM, MIT AI Memo 239, 02/29/1972, Item 101C

10 ## NearRational(x_) --> NearRational(x, Floor(1/2*BuiltinPrecisionGet()));

15 ## NearRational(x_RationalOrNumber?, prec_Integer?) --> 
{
        Local(x1, x2, i,  oldprec);
        oldprec := BuiltinPrecisionGet();
    BuiltinPrecisionSet(prec + 8);        // 8 guard digits (?)
        x1 := ContFracList(NM(Eval(x+10^(-prec))));
        x2 := ContFracList(NM(Eval(x-10^(-prec))));

    /*
    Decide(Verbose?(),
      [
         Echo("NearRational: x      = ", NM(Eval(x           ))));
         Echo("NearRational: xplus  = ", NM(Eval(x+10^(-prec)))));
         Echo("NearRational: xmin   = ", NM(Eval(x-10^(-prec)))));
         Echo("NearRational: Length(x1) = ", Length(x1)," ",x1));
         Echo("NearRational: Length(x2) = ", Length(x2)," ",x1));
      ]
    );
    */
    
        // find where the continued fractions for "x1" and "x2" differ
        // prepare result in "x1" and length of result in "i"
        For (i:=1, i<=?Length(x1) &? i<=?Length(x2) &? x1[i]=?x2[i], i++ ) True;
        Decide(
                i>?Length(x1),
                // "x1" ended but matched, so use "x2" as "x1"
                x1:=x2,
                Decide(
                        i>?Length(x2),
                // "x2" ended but matched, so use "x1"
                        True,
                // neither "x1" nor "x2" ended and there is a mismatch at "i"
                // apply recipe: select the smalest of the differing terms
                        x1[i]:=Minimum(x1[i],x2[i])
                )
        );
        // recipe: x1dd 1 to the lx1st term unless it's the lx1st in the originx1l sequence
        //Ayal added this line, i could become bigger than Length(x1)!
        //Decide(Verbose?(), Echo(["NearRational: using ", i, "terms of the continued fraction"]));
        Decide(i>?Length(x1),i:=Length(x1));
        x1[i] := x1[i] + Decide(i=?Length(x1), 0, 1);
        BuiltinPrecisionSet(oldprec);
        ContFracEval(Take(x1, i));
}


20 ## NearRational(z_, prec_Integer?)::
      (And?(Im(z)!=?0,RationalOrNumber?(Im(z)),RationalOrNumber?(Re(z)))) -->
{
    Local(rr,ii);
    rr := Re(z);
    ii := Im(z);
    Complex( NearRational(rr,prec), NearRational(ii,prec) );
}

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="NearRational",categories="Mathematics Procedures,Numbers (Operations)"
*CMD NearRational --- find optimal rational approximations
*STD
*CALL
    NearRational(x)
    NearRational(x, digits)
    NearRational(z)
    NearRational(z, digits)

*PARMS

{x} -- a number to be approximated (must be already evaluated to floating-point)
{z} -- a complex number to be approximated (Re and Im as above)

{digits} -- desired number of decimal digits (integer)

*DESC

The procedures {GuessRational(x)} and {NearRational(x)} attempt to find "optimal"
rational approximations to a given value {x}. The approximations are "optimal"
in the sense of having smallest numerators and denominators among all rational
numbers close to {x}. This is done by computing a continued fraction
representation of {x} and truncating it at a suitably chosen term.  Both
procedures return a rational number which is an approximation of {x}.

Unlike the procedure {Rationalize()} which converts floating-point numbers to
rationals without loss of precision, the procedures {GuessRational()} and
{NearRational()} are intended to find the best rational that is <i>approximately</i>
equal to a given value.

The procedure {NearRational(x)} is useful if one needs to
approximate a given value, i.e. to find an "optimal" rational number
that lies in a certain small interval around a certain value {x}. This
procedure takes an optional second parameter {digits} which has slightly
different meaning: it specifies the number of digits of precision of
the approximation; in other words, the difference between {x} and the
resulting rational number should be at most one digit of that
precision. The parameter {digits} also defaults to half of the current
precision.

*E.G.

Start with a rational number and obtain a floating-point approximation:

In> x:=NM(956/1013)
Result: 0.9437314906

In> Rationalize(x)
Result: 4718657453/5000000000;
The first 10 terms of this continued fraction correspond to the correct 
continued fraction for the original rational number.

In> NearRational(x)
Result: 218/231;

This procedure found a different rational number closeby because the precision was not high enough.
In> NearRational(x, 10)
Result: 956/1013;

*SEE BracketRational, GuessRational, ContFrac, ContFracList, Rationalize
%/mathpiper_docs

    %output,preserve="false"
      
.   %/output


