package org.mathpiper.io;

import java.io.InputStream;
import java.io.InputStreamReader;


/*

The file is read into a string because it has an UTF-8 character encoding and
RandomAccessFile can't be used with files that have a UTF-8 character encoding.

*/

public class ClasspathFileInputStream extends MathPiperInputStream {

    private StringBuilder fileStringBuilder = new StringBuilder();

    private StringInputStream stringInputStream;


    public ClasspathFileInputStream(String aFileName, InputStatus aStatus) throws Throwable {
	
	super(aStatus);

        InputStream inputStream = ClasspathFileInputStream.class.getClassLoader().getResourceAsStream(aFileName);
        
        InputStreamReader stream = new InputStreamReader(inputStream);
        
	int c;

	while (true) {
	    c = stream.read();

	    if (c == -1)

		break;

	    fileStringBuilder.append((char) c);
	}

	stringInputStream = new StringInputStream(fileStringBuilder.toString(), aStatus);
    }



    public char next() throws Throwable {
	return stringInputStream.next();
    }



    public char peek() throws Throwable {
	return stringInputStream.peek();
    }
    
    
    public char peekPeek() throws Throwable {
	return stringInputStream.peekPeek();
    }


    public boolean endOfStream() {
	return stringInputStream.endOfStream();
    }



    public String substring(int firstPosition, int lastPosition) {
	return stringInputStream.substring(firstPosition, lastPosition);
    }



    public int position() {
	return stringInputStream.position();
    }



    public void setPosition(int aPosition) {
	stringInputStream.setPosition(aPosition);
    }



    public String toString() {
	return ("File: " + iStatus.getSourceName());
    }

}
