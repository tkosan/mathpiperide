/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *  
 */
public class Reverse__ extends BuiltinProcedure
{

    private Reverse__()
    {
    }

    public Reverse__(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons reversed = aEnvironment.iListAtom.copy(false);
        reversed.setCdr(Utility.reverseList(aEnvironment, ((Cons) getArgument(aEnvironment, aStackTop, 1).car()).cdr()));
        setTopOfStack(aEnvironment, aStackTop, SublistCons.getInstance(reversed));
    }
}



/*
%mathpiper_docs,name="Reverse!",categories="Programming Procedures,Lists (Operations),Built In"
*CMD Reverse! --- reverse a list destructively
*CORE
*CALL
	Reverse!(list)

*PARMS

{list} -- list to reverse

*DESC

This command reverses "list" in place, so that the original is
destroyed. This means that any variable bound to "list" will now have
an undefined content, and should not be used any more.
The reversed list is returned.

Destructive commands are faster than their nondestructive
counterparts. {Reverse} is the non-destructive version of
this function.

*EXAMPLES

*SEE FlatCopy, Reverse
%/mathpiper_docs





%mathpiper,name="Reverse!",subtype="in_prompts"


lst := '[a,b,c,13,19] ~> '[a,b,c,13,19]

revlst := Reverse!(lst) ~> '[19,13,c,b,a]


Notice that the original list has been destroyed.

lst ~> '[a]

%/mathpiper





%mathpiper,name="Reverse!",subtype="automatic_test"

Verify(Reverse!('[a,b,c]),'[c,b,a]);

%/mathpiper

*/