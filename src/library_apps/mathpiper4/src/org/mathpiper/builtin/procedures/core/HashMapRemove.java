package org.mathpiper.builtin.procedures.core;

import java.util.HashMap;
import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.AtomCons;


public class HashMapRemove extends BuiltinProcedure
{
    
    private HashMapRemove()
    {
    }

    public HashMapRemove(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons evaluated = getArgument(aEnvironment, aStackTop, 1);

        BuiltinContainer gen = (BuiltinContainer) evaluated.car();
        if( gen == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        Object o = gen.getObject();
        
        if(! (o instanceof HashMap)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        
        HashMap<Object,Cons> hashMap = (HashMap) o;

        Cons key = getArgument(aEnvironment, aStackTop, 2);

        if( key == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        //if(! (key.car() instanceof String)) LispError.checkArgument(aEnvironment, aStackTop, 2);

        Object key2 = key.car();
        
        Object result = hashMap.remove(key2);
        
        if(result == null)
        {
            setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(10, "Null"));
        }
        else
        {
            setTopOfStack(aEnvironment, aStackTop, (Cons) result);
        }
    }
}//end class.



/*
%mathpiper_docs,name="HashMapRemove",categories="Programming Procedures,Built In"
*CMD HashMapRemove --- remove the mapping for the given key from the given hash map

*CALL
	HashMapRemove(hashMap, key)

*PARMS

{hashMap} -- a hash map
{key} -- an atom

*DESC
This procedure removes the mapping for the given key from the given hash map if it is present.

*SEE HashMapCreate

%/mathpiper_docs
*/
