%mathpiper,def="Plot3DS;Plot3DSgetdata;Plot3DSadaptive"

//////////////////////////////////////////////////
/// Plot3DS --- adaptive three-dimensional surface plotting
//////////////////////////////////////////////////

/// definitions of backends
//LoadScriptOnce("plots.rep/backends_3d.mpi");

/*
        Plot3DS is an interface for various backends (Plot3DS...). It calls
Plot3DSgetdata to obtain the list of points and values, and then it calls
Plot3DS<backend> on that data.

        Algorithm for Plot3DSgetdata:
        1) Split the given square into Quotient(Sqrt(points)+1, 2) subsquares, and split each subsquare into 4 parts.
        2) For each of the parts: evaluate procedure values and call Plot3DSadaptive
        3) concatenate resulting lists and return
*/

        LocalSymbols(var, func, xrange, yrange, option, optionslist, xdelta, ydelta, optionshash, cx, cy, fc, allvalues)
{

// declaration of Plot3DS with variable number of arguments
Procedure() Plot3DS("func");
Procedure() Plot3DS("func", "xrange", "yrange");
Procedure() Plot3DS("func", "xrange", "yrange", "options", ...);


/// interface routines
1 ## Plot3DS(func_) --> (Apply("Plot3DS", [func, -5::5, -5::5]));
2 ## Plot3DS(func_, xrange_, yrange_) --> (Apply("Plot3DS", [func, xrange, yrange, []]));
3 ## Plot3DS(func_, xrange_, yrange_, option_Procedure?):: (Type(option) =? ":" ) --> (Apply("Plot3DS", [func, xrange, yrange, [option]]));

/// Plot a single procedure
5 ## Plot3DS(func_, xrange_, yrange_, optionslist_List?)::(!? List?(func)) --> (Apply("Plot3DS", [[func], xrange, yrange, optionslist]));

/// Top-level 3D plotting routine:
/// plot several procedures sharing the same ranges and other options
4 ## Plot3DS(funclist_List?, xrange_, yrange_, optionslist_List?) -->
{
        Local(var, func, xdelta, ydelta, optionshash, cx, cy, fc, allvalues);
        // this will be a list of all computed values
        allvalues := [];
        optionshash := Apply("OptionsListToHash", [optionslist]);
        // this will be a string - name of independent variable
        optionshash["xname"] := "";
        optionshash["yname"] := "";
        // this will be a list of strings - printed forms of procedures being plotted
        optionshash["zname"] := [];
        // parse range
        /*
        Default plotting range is {-5::5} in both coordinates.
        A range can also be specified with a variable name, e.g. {x= -5::5} (note the mandatory space separating "{=}" and "{-}").
        The variable name {x} should be the same as that used in the procedure {f(x,y)}.
        If ranges are not given with variable names, the first variable encountered in the procedure {f(x,y)} is associated with the first of the two ranges.
        Decide(
                Type(xrange) =? "==",        // variable also specified -- ignore for now, store in options
                {
                        // store alternative variable name
                        optionshash["xname"] := ToString(xrange[1]);
                        xrange := xrange[2];
                }
        );
        Decide(
                Type(yrange) =? "==" ,        // variable also specified -- ignore for now, store in options
                {
                        // store alternative variable name
                        optionshash["yname"] := ToString(yrange[1]);
                        yrange := yrange[2];
                }
        );
        Verify(PipeToString()Write(Plot3DS(x1,x1: -1::1,x2: -1::1,output: data,points: 2)), result);
        */
        Decide(
                Type(xrange) =? ":",        // simple range
                xrange := NM(Eval([xrange[1], xrange[2]]))
        );
        Decide(
                Type(yrange) =? ":",        // simple range
                yrange := NM(Eval([yrange[1], yrange[2]]))
        );
        // set default option values
        Decide(
                optionshash["points"] =? None,
                optionshash["points"] := 10        // default ## of points along each axis
        );
        Decide(
                optionshash["xpoints"] =? None,
                optionshash["xpoints"] := optionshash["points"]
        );
        Decide(
                optionshash["ypoints"] =? None,
                optionshash["ypoints"] := optionshash["points"]
        );

        Decide(
                optionshash["depth"] =? None,
                optionshash["depth"] := 2
        );
        Decide(
                optionshash["precision"] =? None,
                optionshash["precision"] := 0.0001
        );
        Decide(
                optionshash["hidden"] =? None |? !? Boolean?(optionshash["hidden"]),
                optionshash["hidden"] := True
        );
        Decide(
                optionshash["output"] =? None |? String?(optionshash["output"]) &? Plot3DSoutputs()[optionshash["output"]] =? None,
                optionshash["output"] := Plot3DSoutputs()["default"]
        );
        // a "filename" parameter is required when using data file
        Decide(
                optionshash["output"] =? "datafile" &? optionshash["filename"] =? None,
                optionshash["filename"] := "output.data"
        );
        optionshash["used depth"] := optionshash["depth"];
        // we will divide each subsquare in 4 parts, so divide number of points by 2 now
        optionshash["xpoints"] := NM(Eval(Quotient(optionshash["xpoints"]+1, 2)));
        optionshash["ypoints"] := NM(Eval(Quotient(optionshash["ypoints"]+1, 2)));
        // in case it is not a simple number but an unevaluated expression
        optionshash["precision"] := NM(Eval(optionshash["precision"]));
        // store range in options
        optionshash["xrange"] := [xrange[1], xrange[2]];
        optionshash["yrange"] := [yrange[1], yrange[2]];
        // compute the separation between grid points
        xdelta := NM(Eval( (xrange[2] - xrange[1]) / (optionshash["xpoints"]) ) );
        ydelta := NM(Eval( (yrange[2] - yrange[1]) / (optionshash["ypoints"]) ) );
        // check that the input parameters are valid (all numbers)
        Check(NumericList?([xrange[1], xrange[2], optionshash["xpoints"], optionshash["ypoints"], optionshash["precision"]]),
                "Plot3DS: Error: plotting ranges "
                +(PipeToString()Write(xrange, yrange))
                +" and/or the number of points "
                +(PipeToString()Write(optionshash["xpoints"], optionshash["ypoints"]))
                +" and/or precision "
                +(PipeToString()Write(optionshash["precision"]))
                +" is not numeric"
        );
        // loop over procedures in the list
        ForEach(func, funclist)
        {
                func := MetaToObject(func);
                
                // obtain name of variable
                var := VarList(func);        // variable names in a list
                
                Check(Length(var)<=?2, "Plot3DS: Error: expression is not a procedure of at most two variables: "
                        +(PipeToString()Write(func))
                );
                // Allow plotting of constant procedures
                Decide(Length(var)=?0, var:=[_dummy, _dummy]);
                Decide(Length(var)=?1, var:=[var[1], _dummy]);
                // store variable name if not already done so
                Decide(
                        optionshash["xname"] =? "",
                        optionshash["xname"] := ToString(var[1])
                );
                Decide(
                        optionshash["yname"] =? "",
                        optionshash["yname"] := ToString(var[2])
                );
                // store procedure name in options
                Append!(optionshash["zname"], PipeToString()Write(func));
                // compute the first point to see if it's okay
                cx := xrange[1]; cy := yrange[1];
                fc := NM(Eval(Apply([var, func], [cx, cy])));
                Check(Number?(fc) |? fc=?Infinity |? fc=? -Infinity |? fc=?Undefined, 
                        "Plot3DS: Error: cannot evaluate procedure "
                        +(PipeToString()Write(func))
                        +" at point "
                        +(PipeToString()Write(cx, cy))
                        +" to a number, instead got "
                        +(PipeToString()Write(fc))
                        +""
                );
                // compute all other data points
                Append!(allvalues, RemoveRepeated(HeapSort( Plot3DSgetdata(func, var, [cx, cy, fc], [xdelta, ydelta], optionshash), Hold([[x,y],x[1]<?y[1] |? x[1] =? y[1] &? x[2] <=? y[2] ] ) )) );
                Decide(Verbose?(), Echo(["Plot3DS: using ", Length(allvalues[Length(allvalues)]), " points for procedure ", func]), True);
                Decide(Verbose?(), Echo(["Plot3DS: max. used ", 2^(optionshash["depth"] - optionshash["used depth"]), "subdivisions for ", func]), True);
        }
        // call the specified output backend
        Apply(Plot3DSoutputs()[optionshash["output"]], [allvalues, optionshash]);
}

HoldArgumentNumber("Plot3DS", 3, 2);
HoldArgumentNumber("Plot3DS", 3, 3);
HoldArgumentNumber("Plot3DS", 4, 2);
HoldArgumentNumber("Plot3DS", 4, 3);
HoldArgumentNumber("Plot3DS", 4, 4);

/// this is the middle-level plotting routine; it generates the initial
/// grid, calls the adaptive routine, and gathers data points.
/// func must be just one procedure (not a list).
/// initvalues is a list with values [x,y,f]; deltas is a list [deltax, deltay].
Plot3DSgetdata(func_, var_, initvalues_, deltas_, optionshash_) -->
{
        Local(ii, jj, xa, ya, fa, xb, yb, fb, result, rowcache);
        // compute all grid points in the 0th row in the y direction;
        // store this array in a temporary cache;
        // also store this in the final list ("result");
        // then, go in the y direction and compute the 1th row; call the adaptive routine and add all points it gives along the way. update the row cache along the way.
        // in cases when depth=0, the adaptive routine gives no extra points, and we must make sure that the "result" array contains the grid in exact order
        rowcache := [initvalues];
        For(ii:=1, ii<=?optionshash["ypoints"], ii++)
        {
                ya := NM(Eval(initvalues[2]+ii*deltas[2]));
                Append!(rowcache, [initvalues[1], ya, NM(Eval(Apply([var, func], [initvalues[1], ya])))]);
        }
        result := rowcache;
        // now loop over the x direction
        For(ii:=1, ii<=?optionshash["xpoints"], ii++)
        {
                // start the next row
                // the 0th point
                xa := NM(Eval(initvalues[1]+ii*deltas[1]));
                ya := initvalues[2];
                fa := NM(Eval(Apply([var, func], [xa, ya])));
                Append!(result, [xa, ya, fa]);
                // now loop at each grid point in y direction
                For(jj:=1, jj<=?optionshash["ypoints"], jj++)
                {        // now we need to plot the data inside the following square:
                        //        p  b
                        //        r  a
                        // xa, ya, fa are the values at the point a; the points p and r are stored as rowcache[jj+1] and rowcache[jj]. We just need to compute the point q and update the rowcache with the value at point a, and update xa, ya, fa also with b.
                        yb := NM(Eval(initvalues[2] + jj*deltas[2]));
                        fb := NM(Eval(Apply([var, func], [xa, yb])));
                        result := Concat(result, Plot3DSadaptive(func, var, [rowcache[jj][1], ya, xa, yb, rowcache[jj][3], rowcache[jj+1][3], fa, fb], optionshash["depth"],
                                // since we are dividing into "points" subintervals, we need to relax precision
                                optionshash["precision"] * optionshash["xpoints"] * optionshash["ypoints"], optionshash ));
                        // update rowcache
                        rowcache[jj] := [xa, ya, fa];
                        ya := yb;
                        fa := fb;
                        Append!(result, [xa, ya, fa]);
                }
        }

        result;
}

//////////////////////////////////////////////////
/// Plot3DSadaptive --- core routine to collect data
//////////////////////////////////////////////////
/*
        Plot3DSadaptive returns a list of triples of coordinates [ [x1,y1,z1], [x2,y2,z2],...] inside a given square. The corners of the square are not returned (they are already computed).
        All arguments except f() and var must be numbers. var is a two-element list containing the independent variables. The "square" argument contains the values of the procedure that have been already computed -- we don't want to recompute them once more.
        square = [x1, y1, x2, y2, f11, f12, f21, f22]

        So the routine will return the list f13, f31, f33, f32, f23 and any points returned by recursive calls on subsquares.
        See the Algo book for the description of the algorithm.
*/

10 ## Plot3DSadaptive(func_, var_, square_, 0, epsilon_, optionshash_) --> [];
20 ## Plot3DSadaptive(func_, var_, [x1_, y1_, x2_, y2_, f11_, f12_, f21_, f22_], depth_, epsilon_, optionshash_) -->
{
        Local(x3, y3, f13, f31, f33, f32, f23, result);

        // if we are here, it means we used one more recursion level
        optionshash["used depth"] := depth-1;
        // bisection
        x3 := NM(Eval((x1+x2)/2));
        y3 := NM(Eval((y1+y2)/2));
        // compute new values
        // use the confusing Map semantics: the list of all x's separately from the list of all y's
        f13 := NM(Eval(Apply([var, func], [x1, y3])));
        f31 := NM(Eval(Apply([var, func], [x3, y1])));
        f33 := NM(Eval(Apply([var, func], [x3, y3])));
        f32 := NM(Eval(Apply([var, func], [x3, y2])));
        f23 := NM(Eval(Apply([var, func], [x2, y3])));
        result := [[x1,y3,f13], [x3, y1, f31], [x3, y3, f33], [x3, y2, f32], [x2, y3, f23]];
/*
y2        12  32  22

        13  33  23

y1        11  31  21

        x1      x2
*/
        Decide(
                // condition for the values not to oscillate too rapidly
                signchange(f11,f13,f12) + signchange(f13,f12,f32) + signchange(f12,f32,f22) <=? 2 &? signchange(f22,f23,f21) + signchange(f23,f21,f31) + signchange(f21,f31,f11) <=? 2 &? 
                
                // condition for the values not to change too rapidly
                NM(Eval(Abs( (f11-f23)/2-(f12-f21)/3+(f22-f13)/6+2*(f32-f33)/3 )))
                        <=? NM(Eval( epsilon*(        // the expression here will be nonnegative because we subtract the minimum value
                        // cubature normalized to 1
                (f11 + f12 + f21 + f22)/12 + 2*f33/3
                - Minimum([f11, f12, f21, f22, f13, f31, f33, f32, f23]) ) ) )
                ,
                // okay, do not refine any more
                result,
                // not okay, need to refine more
                Concat(
                        // first, give the extra points,
                        result,
                        // then perform recursive calls on four quarters of the original square; relax precision by factor of 4
                        Plot3DSadaptive(func, var, [x1, y1, x3, y3, f11, f13, f31, f33], depth-1, epsilon*4, optionshash),
                        Plot3DSadaptive(func, var, [x1, y3, x3, y2, f13, f12, f33, f32], depth-1, epsilon*4, optionshash),
                        Plot3DSadaptive(func, var, [x3, y1, x2, y3, f31, f33, f21, f23], depth-1, epsilon*4, optionshash),
                        Plot3DSadaptive(func, var, [x3, y3, x2, y2, f33, f32, f23, f22], depth-1, epsilon*4, optionshash)
                )
        );
}

}        // LocalSymbols()


%/mathpiper



%mathpiper_docs,name="Plot3DS",categories="Mathematics Procedures,Visualization"
*CMD Plot3DS --- three-dimensional (surface) plotting
*STD
*CALL
        Plot3DS(f(x,y))
        Plot3DS(f(x,y), a::b, c::d)
        Plot3DS(f(x,y), a::b, c::d, option: value)
        Plot3DS(f(x,y), a::b, c::d, option: value, ...)
        Plot3DS(list, ...)

*PARMS

{f(x,y)} -- unevaluated expression containing two variables (procedure to be plotted)

{list} -- list of procedures to plot

{a}, {b}, {c}, {d} -- numbers, plotting ranges in the $x$ and $y$ coordinates

{option} -- atom, option name

{value} -- atom, number or string (value of option)

*DESC
The routine {Plot3DS} performs adaptive plotting of a procedure
of two variables in the specified ranges.
The result is presented as a surface given by the equation $z=f(x,y)$.
Several procedures can be plotted at once, by giving a list of procedures.
Various plotting options can be specified.
Output can be directed to a plotting program (the default is to use
{data}), to a list of values.

The procedure parameter {f(x,y)} must evaluate to a MathPiper expression containing
at most two variables. (The variables do not have to be called {x} and {y}.)
Also, {NM(f(x,y))} must evaluate to a real (not complex) numerical value when given 
numerical values of the arguments {x}, {y}.
If the procedure {f(x,y)} does not satisfy these requirements, an error is raised.

Several procedures may be specified as a list but they have to depend on the same 
symbolic variables, for example, {[f(x,y), g(y,x)]}, but not {[f(x,y), g(a,b)]}.
The procedures will be plotted on the same graph using the same coordinate ranges.

If you have defined a procedure which accepts a number but does not
accept an undefined variable, {Plot3DS} will fail to plot it.
Use {NumericProcedure} to overcome this difficulty.

Data files are created in a temporary directory {/tmp/plot.tmp/} unless otherwise requested.
File names
and other information is printed if {Verbose?()} returns {True} on using {Verbose()}.

The current algorithm uses Newton-Cotes cubatures and some heuristics for error 
estimation (see <*mathpiperdoc://Algo/3/1/*>).
The initial rectangular grid of {xpoints+1}*{ypoints+1} points is refined within any 
rectangle where the integral
of $f(x,y)$ is not approximated to the given precision by
the existing grid.

Options are of the form {option: value}. Currently supported option names
are "points", "xpoints", "ypoints", "precision", "depth", "output", "filename", "xrange", 
"yrange", "zrange". Option values
are either numbers or special unevaluated atoms such as {data}.
If you need to use the names of these atoms
in your script, strings can be used (e.g. {output="data"}). Several option/value pairs 
may be specified (the procedure {Plot3DS} has a variable number of arguments).

*        {xrange}, {yrange}: optionally override coordinate ranges. Note that {xrange} 
is always the first variable and {yrange} the second variable, regardless of the actual variable names.
*        {zrange}: the range of the $z$ axis to use for plotting, e.g.
{zrange=0::20}. If no range is specified, the default is usually to
leave the choice to the plotting backend. Automatic choice based on actual values may
give visually inadequate plots if the procedure has a singularity.
*        {points}, {xpoints}, {ypoints}: initial number of points (default 10 each) -- at least that
many points will be plotted in each coordinate.
The initial grid of this many points will be
adaptively refined.
If {points} is specified, it serves as a default for both {xpoints} and {ypoints} this value may be overridden by {xpoints} and {ypoints} values.
*        {precision}: graphing precision (default $0.01$). This is interpreted as the relative precision of computing the integral of $f(x,y)-Minimum(f(x,y))$ using the grid points. For a smooth, non-oscillating procedure this value should be roughly 1/(number of screen pixels in the plot).
*        {depth}: max. refinement depth, logarithmic (default 3) -- means there will be at most $2^depth$ extra points per initial grid point (in each coordinate).
*        {output}: name of the plotting backend. Supported names: {data} (default).
The {data} backend will return the data as a list of triples such as {{{x1, y1, z1}, {x2, y2, z2}, ...}}.

Other options may be supported in the future.

The current implementation can deal with a singularity within the plotting range only if the procedure {f(x,y)} returns {Infinity}, {-Infinity} or
{Undefined} at the singularity.
If the procedure {f(x,y)} generates a numerical error and fails at a
singularity, {Plot3DS} will fail only if one of the grid points falls on the
singularity.
(All grid points are generated by bisection so in principle the
endpoints and the {xpoints}, {ypoints} parameters could be chosen to avoid numerical
singularities.)

The {filename} option is optional if using graphical backends, but can be used to specify the location of the created data file.

*WIN32

Same limitations as {Plot2D}.

*E.G. notest

In> Plot3DS(a*b^2)
Result: True;

In> Verbose(Plot3DS(Sin(x)*Cos(y),x: 0::20, y: 0::20,depth: 3))
        CachedConstant: Info: constant Pi is being 
          recalculated at precision 10
        CachedConstant: Info: constant Pi is being
          recalculated at precision 11
        Plot3DS: using 1699  points for procedure Sin(x)*Cos(y)
        Plot3DS: max. used 8 subdivisions for Sin(x)*Cos(y)
        Plot3DSdatafile: created file /tmp/plot.tmp/data1
Result: True;


*SEE Verbose, NumericProcedure, Plot2D
%/mathpiper_docs





%mathpiper,name="Plot3DS",subtype="automatic_test"

/* I stringified the results for now, as that is what the tests used to mean. The correct way to deal with this
 * would be to compare the resulting numbers to accepted precision.
 */
{
  Local(result);
  result:="[[[-1,-1,-1],[-1,0,-1],[-1,1,-1],[0,-1,0],[0,0,0],[0,1,0],[1,-1,1],[1,0,1],[1,1,1]]]";
  Verify(PipeToString()Write(Plot3DS(_a,-1::1,-1::1,output: data,points: 2)), result);
}

%/mathpiper