%mathpiper,def="StandardDeviation"

StandardDeviation(list) := 
{
    Check(List?(list), "Argument must be a list.");

    Sqrt(UnbiasedVariance(list));
}


%/mathpiper




%mathpiper_docs,name="StandardDeviation",categories="Mathematics Procedures,Statistics & Probability"
*CMD StandardDeviation --- calculates the standard deviation of a list of values
*STD
*CALL
        StandardDeviation(list)

*PARMS

{list} -- list of values

*DESC

This procedure calculates the standard deviation of a list of values.

*EXAMPLES
*SEE Variance, UnbiasedVariance
%/mathpiper_docs





%mathpiper,name="StandardDeviation",subtype="in_prompts"

StandardDeviation([73,94,80,37,57,94,40,21,7,26]) ~> Sqrt(88009/90)

NM(StandardDeviation([73,94,80,37,57,94,40,21,7,26])) ~> 31.27103737

%/mathpiper





%mathpiper,name="StandardDeviation",subtype="automatic_test"

Verify(StandardDeviation([73,94,80,37,57,94,40,21,7,26]), Sqrt(88009/90));

Verify(NM(StandardDeviation([73,94,80,37,57,94,40,21,7,26])), 31.27103737);

Verify(NM(StandardDeviation([-73,94,-80,37,57,-94,-40,21,7,-24])), 63.05420948);

%/mathpiper