/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

package org.mathpiper.io.worksheets;

public class OutsideFold extends Content {
    
    public OutsideFold(int startLineNumber, int endLineNumber, String contents) throws Throwable {

        this.startLineNumber = startLineNumber;
        
        this.endLineNumber = endLineNumber;

        this.contents = contents;
    }

    public String getContents() {
        return contents.toString();
    }

    public String toString()
    {
        if(isPrint)
        {
            return contents.toString();
        }
        else
        {
            return "";
        }
    }
}
