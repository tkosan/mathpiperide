package org.mathpiper.builtin.procedures.optional;

import java.util.ArrayList;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;

public class StringFormat extends BuiltinProcedure {

    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "StringFormat";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {

        Cons cons = (Cons) getArgument(aEnvironment, aStackTop, 1).car();
        cons = cons.cdr();
        
        Object formatString = cons.car();
        
        if(! (formatString instanceof String)) LispError.raiseError("The first argument must be a string.", aStackTop, aEnvironment);
        
        ArrayList javaList = new ArrayList();
        
        cons = cons.cdr();
        
        while (cons != null) {
            
            if(cons instanceof NumberCons)
            {
                String item = (String) cons.car();
                item = Utility.stripEndQuotesIfPresent(item);
                
                Object number;
                
                if(item.contains("."))
                {
                    number = Double.parseDouble(item);
                }
                else
                {
                    number = Integer.parseInt(item);
                }
                
                javaList.add(number);
                
            }
            else if(cons instanceof AtomCons)
            {
                String item = (String) cons.car();
                item = Utility.stripEndQuotesIfPresent(item);
                javaList.add(item);
            }
            else
            {
                LispError.raiseError("The argument " + cons.toString() + " is not supported yet.", aStackTop, aEnvironment);
            }

            cons = cons.cdr();
        }
        
        String formattedString = String.format((String)formatString, javaList.toArray());

        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), formattedString));
    }
}
/*
%mathpiper_docs,name="StringFormat",categories="Programming Procedures,Strings,Built In"
*CMD StringFormat --- format a string using format specifiers

*CALL
StringFormat(formatString, expr, ...)

*PARMS

{formatString} -- a string that contains format specifiers and literals
{expr} -- expressions that are associated with the format specifiers

*DESC
{Format String}

Composed of format specifiers and literals. Arguments are required only if there are format specifiers in the
format string. Format specifiers include flags, width specifiers, precision specifiers, and conversion characters in the following
sequence:

{% [flags] [width] [.precision] conversion-character}

(brackets indicate optional parameters)

{Flags}

|table
|-|left-justify (right-justify is the default)
|+|place a plus sign {+} or minus sign {-} sign to the left of a number
|0|zero-pad numbers (blank padding is the fefault)
|,|add commas to numbers that are greater than 1000
| |a space will place a minus sign to the left of a number if it is negative
|/table

{Width}

Specifies the field width for outputting the argument and represents the minimum number of characters to
be written to the output. Include space for expected commas and a decimal point in the determination of
the width for numerical values.

{Precision}

Used to restrict the output depending on the conversion. It specifies the number of digits of precision when
outputting floating-point values or the length of a substring to extract from a String. Numbers are rounded
to the specified precision.

{Conversion-Characters}

|table
|d|integer number
|f|decimal number
|s|String
|S|Upper case string
|h|hashcode
|n|newline
|/table

*EXAMPLES

*SEE Printf
%/mathpiper_docs





%mathpiper,name="StringFormat",subtype="in_prompts"

StringFormat("%d + %.3f == %f", 1, 2.2, 3.2) ~> "1 + 2.200 == 3.200000";

%/mathpiper





%mathpiper,name="StringFormat",subtype="automatic_test"

Verify(StringFormat("%d + %.3f == %f", 1, 2.2, 3.2), "1 + 2.200 == 3.200000");

%/mathpiper
 */
