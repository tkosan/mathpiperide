package org.mathpiper.ui.gui.applications.circuitpiper.view;

public class Help
{
    
}

/*
To create a new schematic, open CircuitPiper, select a component (such as a resistor) from the "Component" pulldown menu, click near a grid point in the schematic area to attach one end of the resistor, move the mouse and click on another grid point to place the other end of the resistor. Other components are placed in a similar manner.

The CircuitPiper application consists of 4 frames titled "Schematic", "Main Multimeter", "Instruments", and "Adjusters". The sizes and locations of these frames can be adjusted.

If you right click on a component, a popup menu appears which will enable you to change its value.

Components can be moved by dragging their terminals (the large black circles).

To cancel the placement of a component, press the <Esc> key or right click to bring up a popup menu that has a "Cancel" menu item.

After a circuit is complete, press the "Run" button to simulate it.

Circuits can be saved to disc using "File -> Save" and "File -> Open".

The text of a circuit's file can be obtained by selecting "File -> Export".

If the text of a circuit is in the copy/paste buffer, it can be pasted into the window that is displayed by "File -> Import" in order to import the circuit into the simulator.

When a circuit is running, or after it has been run for a bit then stopped, hovering the mouse over any component or terminal will show its electrical measurements in the "Main Multimeter" frame.

The "Resize" slider adjusts the zoom level of the schematic area.

The "Sim Speed" slider adjusts how fast the simulation runs. Running the simulation faster makes it move through time quicker but it also makes it less accurate.
*/