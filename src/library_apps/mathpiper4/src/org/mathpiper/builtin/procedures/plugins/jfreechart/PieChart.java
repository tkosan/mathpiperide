/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.plugins.jfreechart;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.RadialGradientPaint;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.HorizontalAlignment;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;


public class PieChart extends BuiltinProcedure {

    private Map defaultOptions;


    public void plugIn(Environment aEnvironment)  throws Throwable
    {
        aEnvironment.getBuiltinFunctions().put("PieChart", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("title", null);
        defaultOptions.put("legend", true);
        defaultOptions.put("tooltips", true);
        defaultOptions.put("urls", false);
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {

        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if(! Utility.isSublist(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "");

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.

        if(! Utility.isList(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.NOT_A_LIST, "");

        Cons dataList = (Cons) arguments.car(); //Grab the first member of the list.

        Cons options = arguments.cdr();

        Map userOptions = ChartUtility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);

        PieDataset dataset = ChartUtility.associationListToIPieDataset(aEnvironment, aStackTop, dataList, userOptions);

        JFreeChart chart = ChartFactory.createPieChart(
                (String) userOptions.get("title"),
                dataset,
                //(Integer) userOptions.get("percentDiffForMaxScale"),
                //((Boolean) userOptions.get("greenForIncrease")).booleanValue(),
                ((Boolean) userOptions.get("legend")).booleanValue(),
                ((Boolean) userOptions.get("tooltips")).booleanValue(),

                ((Boolean) userOptions.get("urls")).booleanValue()
                //((Boolean) userOptions.get("subTitle")).booleanValue(),
                //((Boolean) userOptions.get("showDifference")).booleanValue()
        );

        TextTitle t = chart.getTitle();

        t.setFont(new Font("Arial", Font.BOLD, 26));

        PiePlot plot = (PiePlot) chart.getPlot();
       
        plot.setBackgroundPaint(null);
        plot.setInteriorGap(0.04);
        plot.setOutlineVisible(false);
        plot.setSectionOutlinesVisible(true);
        plot.setLabelFont(new Font("Courier New", Font.BOLD, 20));
        plot.setLabelLinkStroke(new BasicStroke(2.0f));


        if(userOptions.get("subTitle") != null)
        {
           TextTitle subTitle = new TextTitle(userOptions.get("subTitle").toString());
           chart.addSubtitle(subTitle);
        }

        if (chart == null) {
            setTopOfStack(aEnvironment, aStackTop, Utility.getFalseAtom(aEnvironment));
            return;
        } else {
            setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, new JavaObject(new ChartPanel(chart))));
            return;
        }
    }
    
    private static RadialGradientPaint createGradientPaint(Color c1, Color c2) {
        Point2D center = new Point2D.Float(0, 0);
        float radius = 200;
        float[] dist = {0.0f, 1.0f};
        return new RadialGradientPaint(center, radius, dist,
                new Color[] {c1, c2});
    }
    
}//end class.







/*
%mathpiper_docs,name="PieChart",categories="Mathematics Procedures,Visualization"
*CMD PieChart --- displays a graphic pie chart

*CALL
    PieChart(associationList, option, option, option...)


*PARMS

{accociationList} -- an association list that contains the key/value pairs

{Options:}

{title} -- a string that specifies a title for chart

{title} -- a string that specifies a subtitle for chart

{ledgend} -- a boolean that indicates if a ledgend should be provides

{tooltips} -- a boolean that indicates if tooltip information should be provided


*DESC

This procedure creates a pie chart. Options are entered using the : operator.
For example, here is how to set the {title} option: {title: "Example Title"}.

*E.G.
In> list := [["Pears", 27.8], ["Oranges", 16.8], ["Apples", 17.1], ["Others", 55.3]]
Result: [["Pears",27.8],["Oranges",16.8],["Apples",17.1],["Others",55.3]]

In> Show(PieChart(list, title: "Fruit"))
Result: class javax.swing.JFrame

*SEE Show, BarChart, LineChart, Histogram, ScatterPlot, CumulativePlot
%/mathpiper_docs
*/
