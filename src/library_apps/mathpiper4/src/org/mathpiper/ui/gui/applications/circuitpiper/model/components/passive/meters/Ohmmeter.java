package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters;

import java.awt.Color;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.OhmMeterElm;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Resistor;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class Ohmmeter extends Meter {

    private static String ohmStringInitial = "??? " + Resistor.ohm;
    protected String ohmString = ohmStringInitial;

    public Ohmmeter(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        if(uid != null)
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        primary = "resistance";
    }
    
    public void reset() {
        super.reset();
        setOhmString(Ohmmeter.ohmStringInitial);
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);

        String valueAndUnit = Component.addUnitOperator(getOhmString(), isValueVisible(), isUnitVisible());

        if(isValueVisible())
        {
            sg.drawString(valueAndUnit.split("`")[0], Math.round((x1 + x2) * 0.5 + 3) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 3));
        }
        
        if(isUnitVisible())
        {
            sg.drawString(valueAndUnit.split("`")[1], Math.round((x1 + x2) * 0.5 + 16) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 16));
        }
        
        if(this.circuit.isCirSim && circuit.circuitPanel.isNotMovingAny())
        {
            getCircuitElm().drawDots(sg, x1, y1, middleX1, middleY1, -getCircuitElm().curcount);
            getCircuitElm().drawDots(sg, x2, y2, middleX2, middleY2, getCircuitElm().curcount);
        }
    }

    public String getOhmString() {
        if(this.circuit.isCirSim)
        {
            OhmMeterElm ohmMeterElm = (OhmMeterElm) this.circuitElm;
            
            return ohmMeterElm.getMeterValue();
        }

        return ohmString;
    }

    public void setOhmString(String ohmString) {
        this.ohmString = ohmString;
    }
}
