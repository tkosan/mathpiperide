// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
//Copyright (C) 2008 Ted Kosan
/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
import org.gjt.sp.jedit.textarea.Selection.Range;
import org.gjt.sp.jedit.textarea.Selection;

void publish_exercise_worksheet()
{
	hintFoldOffset = -1;
	isObfuscate = false;
	
	path = buffer.getPath();
	
	oldPath = path;
	
	f = new File(path.substring(0, path.lastIndexOf(java.io.File.separator)) + "/dist");

	if(!(f.exists() && f.isDirectory())) { 
		//org.gjt.sp.jedit.Macros.message(jEdit.getActiveView(),"A dist directory does not exist.");
		//return;
		f.mkdirs();
	}
	
	newPath = path.substring(0, path.lastIndexOf(java.io.File.separator)) + "/dist" + path.substring(path.lastIndexOf(java.io.File.separator), path.length());
	
	newPath = newPath.substring(0, newPath.lastIndexOf(".")) + "_firstname_lastname_idnumber_" + buffer.getLineText(0).trim() + ".lg.mpws";
	
	
	//System.out.println("XXX " + newPath);

	buffer.save(view, newPath, true);
	
	// Insert the name of the worksheet file into the worksheet. 
	endOfFirstLineOffset = textArea.getLineStartOffset(1);
	buffer.insert(endOfFirstLineOffset, "\n" + textArea.getBuffer().getName() + "\n");
	
	
	org.gjt.sp.jedit.io.VFSManager.waitForRequests();
	
	Macros.getMacro("MathPiperIDE/Remove_Unpreserved_Folds").invoke(view);

	org.gjt.sp.jedit.io.VFSManager.waitForRequests();
	
	lineCount = buffer.getLineCount();
	
	//System.out.println("VVVV " + lineCount);

	stack = new java.util.Stack();
	
	for(int lineNumber = 0; lineNumber < lineCount; lineNumber++)
	{
		//System.out.println("TTTTT " + buffer.getLineText(lineNumber));
		
		if(buffer.isFoldStart(lineNumber))
		{		
			if(buffer.getLineText(lineNumber).trim().contains("subtype=\"problem\""))
			{
				//System.out.println("SSSSS" + buffer.getLineText(lineNumber));
				
				foldType = buffer.getLineText(lineNumber).split(",")[0].trim();
				
				foldInfo = new java.util.ArrayList();
				foldInfo.add(foldType);
				foldInfo.add(buffer.getLineStartOffset(lineNumber));
				foldInfo.add(buffer.getLineStartOffset(lineNumber + 1));
				foldInfo.add(buffer.getLineText(lineNumber).trim().contains("Problem 0"));
				//inOutputFold = true;
				stack.push(foldInfo);
			}
			else if(buffer.getLineText(lineNumber).trim().contains("subtype=\"hint\""))
			{
				hintFoldOffset = buffer.getLineEndOffset(lineNumber);
			}
			else if((buffer.getLineText(lineNumber).trim().contains("%mathpiper_grade") ||
                                    buffer.getLineText(lineNumber).trim().contains("%uasm65_grade") ||
				    buffer.getLineText(lineNumber).trim().contains("subtype=\"obfuscate\"")) && 
				    ! buffer.getLineText(lineNumber).trim().contains("subtype=\"no_obfuscate\""))
			{
				isObfuscate = true;
				
				endIndex = buffer.getLineEndOffset(lineNumber);
				buffer.insert(endIndex-1, ",base_six_four=\"true\"");
				
				foldType = buffer.getLineText(lineNumber).split(",")[0].trim();
				foldInfo = new java.util.ArrayList();
				foldInfo.add(foldType);
				foldInfo.add(buffer.getLineStartOffset(lineNumber));
				foldInfo.add(buffer.getLineStartOffset(lineNumber + 1));
				foldInfo.add(buffer.getLineText(lineNumber).trim().contains("Problem 0"));
				//inOutputFold = true;
				stack.push(foldInfo);
			}
			else if(buffer.getLineText(lineNumber).trim().contains("%todo"))
			{
				textArea.setCaretPosition(buffer.getLineStartOffset(lineNumber));
				textArea.selectFold();
				fold  = textArea.getSelection()[0];
				foldStartOffset = fold.getStart();
				foldEndOffset = fold.getEnd() + 1;
				buffer.remove(foldStartOffset, foldEndOffset - foldStartOffset);	
				lineCount = buffer.getLineCount();
				lineNumber = textArea.getCaretLine();
			}
		}
		
		if(buffer.isFoldEnd(lineNumber) && ! stack.empty() )
		{
			if(buffer.getLineText(lineNumber).trim().contains("%/"))
			{
				//System.out.println("EEEEEE" + buffer.getLineText(lineNumber));
				//inOutputFold = false;
				
				startFoldInfo = stack.pop();
				
				startFoldType = foldInfo.get(0);
				foldStartOffset = foldInfo.get(1);
				foldContentStartOffset = foldInfo.get(2);
				isProblem0Fold = foldInfo.get(3);
				
				endFoldType = buffer.getLineText(lineNumber);
				
				if(endFoldType.charAt(0) == '.')
				{
					endFoldType = endFoldType.substring(1,endFoldType.length());
				}
				endFoldType = endFoldType.split(",")[0].trim();
				endFoldType = endFoldType.replace("/","");
				
				if(endFoldType.equalsIgnoreCase(startFoldType) )
				{
					
					foldContentEndOffset = buffer.getLineEndOffset(lineNumber - 1);

					if(isObfuscate == true)
					{
						rangeSelection = new Range(foldContentStartOffset,foldContentEndOffset);
						textArea.setSelection(rangeSelection);
						code  = textArea.getSelectedText();
						base64 = codeToBase64(code);
						buffer.remove(foldContentStartOffset, foldContentEndOffset - foldContentStartOffset);
						buffer.insert(foldContentStartOffset, "\n" + base64 + "\n\n");
						isObfuscate = false;
					}
					else if(isProblem0Fold)
					{	
						
						buffer.remove(foldContentStartOffset, foldContentEndOffset - foldContentStartOffset);
						buffer.insert(foldContentStartOffset, "\n[\n[\"firstName\", \"xxx\"],\n[\"lastName\", \"yyy\"],\n[\"ssuIDNumber\", \"000000\"],\n];\n\n");
					}
					else
					{
						if(hintFoldOffset != -1)
						{
							rangeSelection = new Range(foldContentStartOffset,foldContentEndOffset);
							textArea.setSelection(rangeSelection);
							code  = textArea.getSelectedText();
							base64 = codeToBase64(code);
							hintString = "\nHint(\"" + base64 + "\");\n";
							foldContentStartOffset += hintString.length();
							foldContentEndOffset += hintString.length();
							buffer.insert(hintFoldOffset, hintString);
							hintFoldOffset = -1;
						}
					                                       
						buffer.remove(foldContentStartOffset, foldContentEndOffset - foldContentStartOffset);
						
						if(startFoldType.equals("%mathpiper"))
						{
							lineCommentSymbol = "//";
						}
						else if(startFoldType.equals("%uasm65"))
						{
							lineCommentSymbol = ";";
						}
						
						buffer.insert(foldContentStartOffset, "\n" + lineCommentSymbol + " Enter your program into this fold and then EVALUATE THIS FOLD.\n\n");
					}
					
					textArea.setCaretPosition(foldContentStartOffset);
					lineNumber = textArea.getCaretLine();
					lineCount = buffer.getLineCount();
				}
				else
				{
					stack.push(startFoldInfo);
				}
			}//end if.
		}//end if.
	}//end for
	
	buffer.save(view,null);
	
	org.gjt.sp.jedit.io.VFSManager.waitForRequests();
	
	jEdit.openFile(view, oldPath);
	
	org.gjt.sp.jedit.io.VFSManager.waitForRequests();
	
}//end.

codeToBase64(code)
{
	base64 = org.mathpiper.builtin.library.base64.Base64.encodeToString(code.getBytes(), false);
	
	return base64;
}

publish_exercise_worksheet();