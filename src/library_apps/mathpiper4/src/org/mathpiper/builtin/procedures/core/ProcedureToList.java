/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *  
 */
public class ProcedureToList extends BuiltinProcedure
{

    private ProcedureToList()
    {
    }

    public ProcedureToList(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        if(! (getArgument(aEnvironment, aStackTop, 1).car() instanceof Cons)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        Cons head = aEnvironment.iListAtom.copy(false);
        Cons operatorCons = (Cons) getArgument(aEnvironment, aStackTop, 1).car();
        
        /*
        if(! ((String) operatorCons.car()).startsWith("_"))
        {
            operatorCons.setCar("_".concat(((String)operatorCons.car())));
        }
        */
        
        head.setCdr(operatorCons);
        setTopOfStack(aEnvironment, aStackTop, SublistCons.getInstance(head));
    }
}



/*
%mathpiper_docs,name="ProcedureToList",categories="Programming Procedures,Lists (Operations),Built In"
*CMD ProcedureToList --- convert a procedure to a list
*CORE
*CALL
	ProcedureToList(expr)

*PARMS

{expr} -- expression to be converted

*DESC
Source: ProcedureToList.java
The parameter "expr" is expected to be a compound object, i.e. not
an atom. It is evaluated and then converted to a list. The car entry
in the list is the top-level operator in the evaluated expression and
the other entries are the arguments to this operator. Finally, the
list is returned.

*EXAMPLES

*SEE List, ListToProcedure, Atom?
%/mathpiper_docs





%mathpiper,name="ProcedureToList",subtype="in_prompts"

ProcedureToList(Cos('x)) ~> ['Cos,'x]

ProcedureToList(3*_a) ~> ['*,3,_a]

%/mathpiper





%mathpiper,name="ProcedureToList",subtype="automatic_test"

// ProcedureToList and ListToProcedure coredumped when their arguments were invalid.
Verify(ProcedureToList(Sqrt(_x)),'[Sqrt,_x]);
Verify(ProcedureToList('(a(b,c))),'[a,b,c]);
{
  Local(exception);

  exception := False;
  ExceptionCatch(ProcedureToList(1.2), "", exception := ExceptionGet());
  Verify(exception =? False, False);
};

%/mathpiper
*/



