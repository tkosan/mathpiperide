package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;

public class AdjusterPanel extends JPanel
{
    private Component component;
    
    public AdjusterPanel(Component component)
    {
        this.component = component;
        
        this.setLayout(new BorderLayout());
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Options");
        
        JMenuItem removeMenuItem = new JMenuItem("Remove");
        removeMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                component.removeAdjusterPanel(AdjusterPanel.this);
            }
        });
        menu.add(removeMenuItem);
        
        menuBar.add(menu);
        
        this.add(menuBar, BorderLayout.NORTH);
        
        Border blackline = BorderFactory.createLineBorder(Color.black);
        
        TitledBorder titledBorder = BorderFactory.createTitledBorder(blackline, "Potentimeter " + component.getID());
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        this.setBorder(titledBorder);
    }
}
