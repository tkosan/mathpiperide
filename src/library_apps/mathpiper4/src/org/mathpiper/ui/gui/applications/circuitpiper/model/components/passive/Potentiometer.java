package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.Point;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.PotElm;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.NTerminalComponent;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.mathpiper.ui.gui.applications.circuitpiper.view.SchematicSymbol;



public final class Potentiometer extends NTerminalComponent {

    private SchematicSymbol symbol;
       
    public Potentiometer(int x, int y, String uid, Circuit circuit) {
        super(x, y, uid, circuit);
        
        calculatedTerminals = new Terminal[1];
        calculatedPoints = new Point2D.Double[1];
        postsConstraint = 2;
        
        if(uid == null)
        {
            componentSubscript = Resistor.componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= Resistor.componentCounter)
                {
                    Resistor.componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        
        symbol = new SchematicSymbol();
        symbol.type = "Potentiometer";
        symbol.templateHeadTerminalPoint = new Point2D.Double(0, 0);
        symbol.templateHeadLeadPoint = new Point2D.Double(0, 0);
        symbol.templateTailTerminalPoint = new Point2D.Double(30, 0);
        symbol.templateTailLeadPoint = new Point2D.Double(30, 0);

        // Zigzag.
        symbol.template.put("1", new Line2D.Double(0, 0, 2.5, 5));
        symbol.template.put("2", new Line2D.Double(2.5, 5, 7.5, -5));
        symbol.template.put("3", new Line2D.Double(7.5, -5, 12.5, 5));
        symbol.template.put("4", new Line2D.Double(12.5, 5, 17.5, -5));
        symbol.template.put("5", new Line2D.Double(17.5, -5, 22.5, 5));
        symbol.template.put("6", new Line2D.Double(22.5, 5, 27.5, -5));
        symbol.template.put("7", new Line2D.Double(27.5, -5, 30, 0));
        
        // Plus sign.
        symbol.template.put("98", new Line2D.Double(0, -7, 4, -7));
        symbol.template.put("", new Line2D.Double(2, -5, 2, -9));

        init();
    }
    
    public void init() {
        setPrimarySymbol("R");
    }
    
    protected void calculateGeometry()
    {
        super.calculateGeometry();
        
        if(getHeadTerminal() == null || getTailTerminal() == null)
        {
            return;
        }
        
        setback = (mirrorOrientation  == 1) ? 5.5 : -5.5;
        setback2 = (mirrorOrientation  == 1) ? 30.0 : -30.0;
        calculatedPoints[0] = new Point2D.Double(centerX - (setback2 * cosine), centerY + (setback2 * sine));
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        calculateGeometry();
        
        symbol.drawComponent(getHeadTerminal().getX(), getHeadTerminal().getY(), getTailTerminal().getX(), getTailTerminal().getY(), sg);


        // Base line.
        drawArrowLine(sg, 
                centerX - (setback2 * cosine),
                 centerY + (setback2 * sine),
                 centerX - (setback * cosine),
                 centerY + (setback * sine), 7, 3.);

        if(circuit.isCirSim && circuit.circuitPanel.isNotMovingAny())
        {
            PotElm ce = (PotElm) getCircuitElm();
            
            ce.drawDots(sg, new Point((int) Math.round(calculatedPoints[0].x), (int) Math.round(calculatedPoints[0].y)), new Point((int) Math.round(centerX - (setback * cosine)), (int) Math.round(centerY + (setback * sine))), ce.curcount3);

            ce.drawDots(sg, new Point(headTerminalX, headTerminalY), new Point((int) Math.round(centerX), (int) Math.round(centerY)), ce.curcount1);
            
            ce.drawDots(sg, new Point(tailTerminalX, tailTerminalY), new Point((int) Math.round(centerX), (int) Math.round(centerY)), ce.curcount2);
        }
    }
    
    public String getDisplayLabel() throws Exception
    {
        return "(" + super.getDisplayLabel() + ")";
    }

    public String toString()
    {
        return super.toString();
    }
}
