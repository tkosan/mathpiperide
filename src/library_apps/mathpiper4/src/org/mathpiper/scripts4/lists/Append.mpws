%mathpiper,def="Append"

Procedure("Append",["list", "element"])
{
  Check(List?(list), "The first argument must be a list.");

  Insert(list,Length(list)+1,element);
}


%/mathpiper



%mathpiper_docs,name="Append",categories="Programming Procedures,Lists (Operations)"
*CMD Append --- append an entry at the end of a list
*STD
*CALL
        Append(list, expr)

*PARMS

{list} -- list to append "expr" to

{expr} -- expression to append to the list

*DESC

The expression "expr" is appended at the end of "list" and the
resulting list is returned.

Note that due to the underlying data structure, the time it takes to
append an entry at the end of a list grows linearly with the length of
the list, while the time for prepending an entry at the beginning is
constant.

*EXAMPLES

*SEE Concat, Append!
%/mathpiper_docs





%mathpiper,name="Append",subtype="in_prompts"

ll := [_a,Pi,3,[1,2,3]] ~> [_a,Pi,3,[1,2,3]]

Append(ll,1) ~> [_a,Pi,3,[1,2,3],1]

ll ~> [_a,Pi,3,[1,2,3]]

ll := Append(ll,"PKHG") ~> [_a,Pi,3,[1,2,3],"PKHG"]

ll ~> [_a,Pi,3,[1,2,3],"PKHG"]

%/mathpiper





%mathpiper,name="Append",subtype="automatic_test"

Local(list);

list := [1,2,3];

Verify(Append(list, 4), [1,2,3,4]);

Verify(list, [1,2,3]);

Verify(Append([_a,_b,_c],_d),[_a,_b,_c,_d]);

%/mathpiper