/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/**
 *
 *
 */
public class StemAndLeaf extends BuiltinProcedure
{

    public void plugIn(Environment aEnvironment) throws Throwable
    {
        this.functionName = "StemAndLeaf";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons argument = (Cons) getArgument(aEnvironment, aStackTop, 1).car();

        if (!Utility.isList(argument))
        {
            LispError.throwError(aEnvironment, aStackTop, "The argument must be a list");
        }

        int[] data = JavaObject.lispListToJavaIntArray(aEnvironment, aStackTop, argument);

        Map<Integer, List<Integer>> plot = createPlot(data);

        aEnvironment.iCurrentOutput.write(plot(plot));

        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }

    private Map<Integer, List<Integer>> createPlot(int... data)
    {
        // Obtained from http://www.rosettacode.org/wiki/Stem-and-leaf_plot#Java
        Map<Integer, List<Integer>> plot = new TreeMap<Integer, List<Integer>>();
        int highestStem = -1; //for filling in stems with no leaves
        for (int datum : data)
        {
            int leaf = datum % 10;
            int stem = datum / 10; //integer division
            if (stem > highestStem)
            {
                highestStem = stem;
            }
            if (plot.containsKey(stem))
            {
                plot.get(stem).add(leaf);
            }
            else
            {
                LinkedList<Integer> list = new LinkedList<Integer>();
                list.add(leaf);
                plot.put(stem, list);
            }
        }
        if (plot.keySet().size() < highestStem + 1 /*highest stem value and 0*/)
        {
            for (int i = 0; i <= highestStem; i++)
            {
                if (!plot.containsKey(i))
                {
                    LinkedList<Integer> list = new LinkedList<Integer>();
                    plot.put(i, list);
                }
            }
        }
        return plot;
    }

    private String plot(Map<Integer, List<Integer>> plot)
    {
        StringBuffer sb = new StringBuffer();

        for (Map.Entry<Integer, List<Integer>> line : plot.entrySet())
        {
            Collections.sort(line.getValue());
            int key = line.getKey();
            String lineString = line.getValue().toString();
            lineString = lineString.replace("[", "");
            lineString = lineString.replace("]", "");
            lineString = lineString.replace(",", "");
            sb.append(key + ((key < 10) ? "  " : " ") + "| " + lineString + "\n");
        }

        return sb.toString();
    }


}//end class.

/*
 %mathpiper_docs,name="StemAndLeaf",categories="Programming Procedures,Visualization,Built In",access="experimental"
 *CMD StemAndLeaf --- prints a stem and leaf plot

 *CALL
 StemAndLeaf(data)

 *PARMS
 {data} -- a list of integers

 *DESC
 prints a stem and leaf plot.

 *E.G.
 In> StemAndLeaf(RandomIntegerList(100,1,50))
 Result: True
 Side Effects:
 0  | 1 1 1 2 3 3 4 5 5 5 6 6 6 7 8
 1  | 0 1 1 1 1 3 3 3 4 4 5 6 7 8 8 8 8
 2  | 0 0 1 1 2 2 3 3 3 3 4 4 5 5 5 6 6 6 6 6 6 8 8 9 9
 3  | 0 1 2 2 3 3 3 4 4 4 4 5 5 6 6 6 7 7 8 8 9
 4  | 0 1 2 2 2 3 3 3 4 4 5 5 6 6 8 8 9 9 9

 %/mathpiper_docs
 */
