package org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements;

import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.StringTokenizer;

public class CapacitanceMeterElm extends CurrentElm {

    public CapacitanceMeterElm(int xx, int yy) {
        super(xx, yy);
    }

    public CapacitanceMeterElm(int xa, int ya, int xb, int yb, int f,
            StringTokenizer st) {
        super(xa, ya, xb, yb, f, st);
    }

    int getDumpType() {
        return 1000;
    }

    public void setPoints() {
        super.setPoints();
        calcLeads(26);
    }

    
    public String getMeterValue()
    {
        return "todo";
    }

}
