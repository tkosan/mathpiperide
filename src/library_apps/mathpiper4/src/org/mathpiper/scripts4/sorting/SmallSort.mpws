%mathpiper,def="SmallSort"

/// This is a fast in-place sorting of a short list (or array!)
/// It is used to speed up the final steps of a HeapSort.
/// SmallSort sorts up to 3 elements, HeapSort sorts 4 and more elements

SmallSort(list_, first_, last_, compare_):: (last=?first) --> list;

SmallSort(list_, first_, last_, compare_):: (last=?first+1) -->
{
        Local(temp);
        temp := list[first];
        Decide(
                Apply(compare,[temp,list[last]]),
                list,
                {
                        list[first] := list[last];
                        list[last] := temp;
                }        //Swap(list, first, last)
        );
        list;
}

SmallSort(list_, first_, last_, compare_):: (last=?first+2) -->
{
        Local(temp);
        temp := list[first];
        Decide(
                Apply(compare,[list[first+1],temp]),
                {
                        list[first] := list[first+1];
                        list[first+1] := temp;
                }        //Swap(list, first, first+1)        // x>y, z
        );
        // x<y, z
        temp := list[last];
        Decide(
                Apply(compare,[list[first],temp]),
                Decide(        // z>x<y
                        Apply(compare,[list[first+1],temp]),
                        list,
                        {
                                list[last] := list[first+1];
                                list[first+1] := temp;
                        }        //Swap(list, first+1, last)        // 1, 3, 2
                ),
                {        // 2, 3, 1 -> 1, 2, 3
                        list[last] := list[first+1];
                        list[first+1] := list[first];
                        list[first] := temp;
                }
        );
        list;
}

%/mathpiper





%mathpiper_docs,name="SmallSort",categories="Programming Procedures,Sorting,Private"
*CMD SmallSort --- sort a small list

*CALL
        SmallSort(list, first, last, compare)

*PARMS

{list} -- list to sort

{first} -- index of where to begin sorting the elements of {list}

{last} -- index of where to stop sorting the elements of {list}

{compare} -- procedure used to compare elements of {list}


*DESC

This command returns {list} after it is sorted using {compare} to
compare elements. It is used to speed up the final steps of a {HeapSort}.
The procedure requires four arguments.

The first argument is the list to be sorted. {SmallSort} is for lists of three 
or less elements. For a larger list, use {HeapSort}.

The second argument is the position in the list in which to begin sorting the elements.

The third argument is the position in the list to stop sorting the elements.

The fourth argument is the compare procedure which should be used to sort the list.
The compare procedure can either be a string which contains the name of a procedure 
or a pure procedure.

The original {list} is left unmodified.


*EXAMPLES
*SEE SmallSortIndexed, HeapSort, HeapSortIndexed
%/mathpiper_docs





%mathpiper,name="SmallSort",subtype="in_prompts"

SmallSort([3,1,2], 1, 3, "<?") ~> [1,2,3]

SmallSort([3,1,2], 1, 3, ">?") ~> [3,2,1]

%/mathpiper





%mathpiper,name="SmallSort",subtype="automatic_test"

Verify(SmallSort([5,-4,3], 1, 3, "<?"), [-4,3,5]);

Verify(SmallSort([5,-4,3], 1, 3, ">?"), [5,3,-4]);

%/mathpiper