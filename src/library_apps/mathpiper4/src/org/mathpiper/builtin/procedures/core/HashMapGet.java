package org.mathpiper.builtin.procedures.core;

import java.util.HashMap;
import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.BuiltinObjectCons;

/**
 *
 *  
 */
public class HashMapGet extends BuiltinProcedure
{
    
    private HashMapGet()
    {
    }

    public HashMapGet(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    { 
        Cons evaluated = getArgument(aEnvironment, aStackTop, 1);

        BuiltinContainer gen = (BuiltinContainer) evaluated.car();
        if( gen == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        Object o = gen.getObject();
        
        if(! (o instanceof HashMap)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        
        HashMap<Object,Cons> hashMap = (HashMap) o;

        Cons key = getArgument(aEnvironment, aStackTop, 2);

        if( key == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        //if(! (key.car() instanceof String)) LispError.checkArgument(aEnvironment, aStackTop, 2);

        Object key2 = key.car();
        
        Object result = hashMap.get(key2);
        
        if(result == null)
        {
            setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(10, "Null"));
        }
        else
        {
            if(result instanceof Cons)
            {
                setTopOfStack(aEnvironment, aStackTop, (Cons) result);
            }
            else
            {
                setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, new JavaObject(result)));
            }
        }
    }
}



/*
%mathpiper_docs,name="HashMapGet",categories="Programming Procedures,Built In"
*CMD HashMapGet --- returns the value that is associated with the given key in the given hash map

*CALL
	HashMapGet(hashMap, key)

*PARMS

{hashMap} -- a hash map
{key} -- an atom

*DESC
This procedure returns the value that is associated with the given key in the given hash map.

*SEE HashMapCreate

%/mathpiper_docs
*/
