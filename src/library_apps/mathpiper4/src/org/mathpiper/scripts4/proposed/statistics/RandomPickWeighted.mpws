%mathpiper,def="RandomPickWeighted"

RandomPickWeighted(list) :=
{

    Check(List?(list), "Argument must be a list.");
    
    Local(element, probability, probabilities, item, items, lastWeight, randomNumber, result);
    
    probabilities := 0;
    
    items := [];
    
    lastWeight := 0;
    
    
    
    //Make sure that the probabilities sum to 1.
    ForEach(element,list)
    {        
        probability := element[2];

        probabilities := probabilities + probability;
    }
    
    Check(probabilities =? 1, "The probabilities must sum to 1.");
    
    
    
    //Place items in a list and associate it with a subrange in the range between 0 and 1.
    ForEach(element,list)
    {
        probability := element[2];
        
        item := element[1];
        
        items := Append(items, [item, [lastWeight, lastWeight := lastWeight + NM(probability)]] );
    }
    
    
    
    //Pick the item which is in the randomly determined range.
    randomNumber := Random();
    
    ForEach(itemData,items)
    {
        Decide(randomNumber >=? itemData[2][1] &? randomNumber <=? itemData[2][2], result := itemData[1] );
    }
    
    
    
    result;

}

%/mathpiper








%mathpiper_docs,name="RandomPickWeighted",categories="Mathematics Procedures,Statistics & Probability",access="experimental"
*CMD RandomPickWeighted --- randomly pick an element from a list using a given weight
*STD
*CALL
        RandomPickWeighted(list)

*PARMS

{list} -- a list which contains elements and their respective weights

*DESC
Randomly picks an element from the given list with a probability which is determined by the element's weight. 

*E.G.

In> RandomPickWeighted([[_HEADS,1/2],[_TAILS,1/2]]);
Result: _HEADS


In> RandomPickWeighted([[_HEADS,.5],[_TAILS,.5]]);
Result: _TAILS


In> RandomPickWeighted([[_DOOR1,2/8], [_DOOR2,1/8], [_DOOR3,5/8]])
Result: _DOOR1


In> RandomPickWeighted([[_DOG,.2], [_CAT,.3], [_BIRD,.1], [_MOUSE,.15], [_TURTLE,.25]])
Result: _TURTLE


In> RandomPickWeighted([[23,5/32],[56,10/32],[87,8/32],[92,6/32],[15,3/32]])
Result: 15

*SEE RandomPick, RandomPickVector
%/mathpiper_docs








%mathpiper,scope="nobuild",subtype="manual_test"

RandomPickWeighted([[HEADS,1/2],[TAILS,1/2]]);

%/mathpiper



