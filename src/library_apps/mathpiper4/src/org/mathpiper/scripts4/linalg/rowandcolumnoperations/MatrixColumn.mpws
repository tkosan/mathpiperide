%mathpiper,def="MatrixColumn"

Procedure("MatrixColumn",["matrix", "col"])
{
  Local(m);
  m:=matrix[1];

  Check(col >? 0, "MatrixColumn: column index out of range");
  Check(col <=? Length(m), "MatrixColumn: column index out of range");

  Local(i,result);
  result:=[];
  For(i:=1,i<=?Length(matrix),i++)
    Append!(result,matrix[i][col]);

  result;
}

%/mathpiper




%mathpiper_docs,name="MatrixColumn",categories="Mathematics Procedures,Linear Algebra"
*CMD MatrixColumn --- obtain the column of a matrix
*STD
*CALL
        MatrixColumn(matrix,column)

*PARMS
{matrix} -- a matrix
{column} -- the index of a matrix column

*DESC
Returns the column of a matrix which is specified by {column}.

*E.G.
In> aa := [[1,2], [3,4]];
Result: [[1,2],[3,4]]

In> MatrixColumn(aa,1)
Result: [1,3]

*SEE MatrixRow
%/mathpiper_docs

