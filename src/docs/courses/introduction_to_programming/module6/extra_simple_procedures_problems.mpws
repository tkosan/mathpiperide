%group,name="Problem 2b",description="Build a list of perfect squares and their diagonal length."
========================================================================================================
Problem 2b

%todo
- Add some information on how the Pythagorean theorem works.
- A student said "This is the first problem that confused me
real bad." Replace it with a problem that determines
the length of the longest of three lists (to prepare the 
student for the "mix three lists" problem.
%/todo


a) Create a procedure named
"perfectSquaresProcedure" that takes two arguments:
"startNum" which represents the first number
that is used, and "repeatTotal" which represents
the number of elements in the resulting list. Use
a variable named "index", one While loop, and one
Append! procedure to build the following list of
perfect squares and return it as a result:

[1,4,9,16,25,36,49,64,81,100]

Hint: Instead of squaring the loop index and assigning
the result back to the loop index, square the loop
index and assign the result to another 
number := index^2;

Test this procedure by temporarily placing the
following code in the bottom of the fold:

perfectSquaresProcedure(1, 10);


b) Create a procedure named "pythagoreanProcedure" that
takes a list as its only argument. It should Use a
variable named "index", one While loop, one
Append! procedure, one NM() Numeric Mode procedure
and one Sqrt() Square Root procedure. Build the
following list by finding the diagonal length of
each perfect square (starting at 1) using the pythagorean theorem
on each element of the list provided and return it
as a result:

[1.414213562,5.656854249,12.72792206,22.62741700,35.35533906,50.91168825,69.29646456,90.50966799,114.5512986,141.4213562]

Note: 
NM(Sqrt(1^2 + 1^2)) evaluates to 1.414213562
NM(Sqrt(4^2 + 4^2)) evaluates to 5.65685425


Test this procedure by temporarily placing the following code
in the bottom of the fold:

pythagoreanProcedure(perfectSquaresProcedure(1, 10));


c) Create a no parameter procedure named
"mainProcedure" that uses no loops, the
"perfectSquaresProcedure", and the "pythagoreanProcedure"
procedures to build
the following list and return it as a result:

[[1,4,9,16,25,36,49,64,81,100],[1.414213562,5.656854249,12.72792206,22.62741700,35.35533906,50.91168825,69.29646456,90.50966799,114.5512986,141.4213562]]


Place the following line of code at the end of your code:

mainProcedure();


%mathpiper,name="Problem 2b",subtype="problem",unassign_all="true",globalStateShow="true",truncate="6000",timeout="5000"

Procedure("perfectSquaresProcedure", ["startNum", "repeatTotal"])
{
    Local(squaresList, index, number);
    
    squaresList := [];
    
    index := startNum;
    
    While(index <? startNum + repeatTotal)
    {    
        number := index^2;
        
        Append!(squaresList, number);
        
        index := (index + 1);
    }
    squaresList;
}

Procedure("pythagoreanProcedure", ["inputList"])
{
    Local(index, resultList, number, hypotenuse);
    
    resultList := [];
    
    index := 1;
    
    While(index <=? Length(inputList))
    {
        number := inputList[index];
        
        hypotenuse := NM(Sqrt(number^2 + number^2));
        
        Append!(resultList, hypotenuse);
        
        index := (index + 1);
    }
    
    resultList;
}

Procedure("mainProcedure", [])
{
    Local(squaresList, pythagoreanList);
    
    squaresList := perfectSquaresProcedure(1, 10);
    
    pythagoreanList := pythagoreanProcedure(squaresList);
    
    [squaresList, pythagoreanList];
}

mainProcedure();

%/mathpiper







%mathpiper_grade,name="Problem 2b"

FoldGrade("MathPiper version >= 313", 1, True)
{
    StringToNumber(Version()) >=? 313;
}

LocalSymbols(foldCode2, errorMessage, globalsProblem, procedureName, definedProcedureNames, arguments, parametersMadeLocal, globalsUsed, localPositions, localProcedure, locals, localVariables, procedureProcedure, proceduresPositions, variables)
{
    foldCode2 := ?foldCode;
    
    proceduresPositions := PositionsPattern(foldCode2, 'Procedure(aa_, bb_) cc_);
    
    definedProcedureNames := [];
    
    globalsUsed := [];
        
    FoldGrade("No global variables are used inside of a procedure", 1, False)
    {
        ForEach(procedurePosition, proceduresPositions)
        {
            procedureProcedure := PositionGet(foldCode2, procedurePosition);
            Append!(definedProcedureNames, ToAtom(procedureProcedure[1]));
            variables := VarList(procedureProcedure /: [Local(aa__) -> 'Local()]);
            variables := Difference(variables, definedProcedureNames);
            arguments := MapSingle("ToAtom", procedureProcedure[2]);
            localPositions := PositionsPattern(procedureProcedure, 'Local(aa__));
            localVariables := [];
                locals := [];
            
            ForEach(localPosition, localPositions)
            {
                localProcedure := PositionGet(procedureProcedure, localPosition);
                locals := Rest(ProcedureToList(localProcedure));
                ForEach(local, locals)
                {
                    Append!(localVariables, local);
                }
            }
            
            globalsProblem := Difference(variables, Concat(arguments, locals));
            
            If(globalsProblem !=? [])
            {
                Append!(globalsUsed, [globalsProblem, procedureProcedure[1]]);
            }

        }
        
        errorMessage := "";
        
        If(globalsUsed !=? [])
        {
            ForEach(globalsProblem, globalsUsed)
            {
            
                If(Length(globalsProblem[1]) =? 1)
                {
                    errorMessage +:= "    The global variable " + globalsProblem[1] + " is used in procedure \"" + globalsProblem[2] + "\"." + Nl();
                }
                Else
                {
                    errorMessage +:= "    The global variables " + globalsProblem[1] + " are used in procedure \"" + globalsProblem[2] + "\"." + Nl();
                }
            }
            
            StringTrim(errorMessage);
        }
        Else
        {
            True;
        }
    }
}

FoldGrade("\"mainProcedure()\" is the last expression in the fold", 1, False)
{
    Local(mainPosition);
    
    mainPosition := Length(?foldCode[1]);

    If(Procedure?(?foldCode[1][mainPosition]) &? ?foldCode[1][mainPosition][0] =? 'mainProcedure)
    {
        Local(resultMessage);
        resultMessage := True;
        If(?foldCode[1][mainPosition] !=? 'mainProcedure())
        {
            resultMessage := "The call to \"mainProcedure\" must have zero arguments.";
        }
        
        ?foldCode[1][mainPosition] := /` '('(@ ?foldCode[1][mainPosition]));
        resultMessage;
    }
    Else
    {
        False;
    }

}

{
    Local(procedures, procedureName, parameters, body);

    procedures := ProceduresGet(?foldCode);
    
    If(procedures !=? [])
    {
        { // squares
        
            Local(procedure);
            
            procedureName := "perfectSquaresProcedure";
            
            Echo(procedureName + ":");
        
            Retract(procedureName, All);
            
            procedure := procedures[procedureName];
            
            If(procedure !=? None)
            {
                 FoldGrade("The procedure does not throw an exception when defined", 1, True)
                {
                    ExceptionCatch(
                    {
                        /`( Procedure(@procedureName, @procedure["parameters"]) @procedure["body"] );
                        True;
                    },
                    "",
                    {
                        ExceptionGet()["message"];
                    });
                }

                FoldGrade("The procedure has two formal parameters", 1, False)
                {
                    Length(procedure["parameters"]) =? 2;
                }
    
                FoldGrade("The result is not in the code as a literal", 1, True)
                {
                    Local(values);
                    
                    values := SubtreesPattern(?foldCode, a_List? );
            
                    !? Contains?(values, "[1,4,9,16,25,36,49,64,81,100]");
                }

                FoldGrade("The procedure returns a correct result", 1, True)
                {
                    Local(procedureResult, correctValue);
        
                    correctValue := [1,4,9,16,25,36,49,64,81,100];
                    
                    procedureResult := ExceptionCatch(/`( Apply(Lambda(@procedure["parameters"], @procedure["body"]), [1,10]) ), "", ExceptionGet()["message"]);
                    
                    If(procedureResult !=? correctValue)
                    {
                        procedureResult;
                    }
                    Else
                    {
                        True;
                    }
                }

                FoldGrade("One loop is used", 1, False)
                {
                    Local(procedureNames, loopCount);
                    procedureNames := ProcedureListAll(procedure["body"]);
                    loopCount := Count(procedureNames,"While") + Count(procedureNames,"Until") + Count(procedureNames, "For");
                    loopCount =? 1;
                }
    
                FoldGrade("One \"Append!\" procedure is used", 1, True)
                {
                    Local(procedureNames, procedureCount);
                    procedureNames := ProcedureListAll(procedure["body"]);
                    procedureCount := Count(procedureNames,"Append!");
                    procedureCount =? 1;
                }
    
                FoldGrade("The \"Local\" procedure is used", 1, False)
                {
                    Local(procedureNames);
                    procedureNames := ProcedureList(procedure["body"]);
                    Contains?(procedureNames,"Local");
                }
            }
            Else
            {
                FoldGrade("The procedure name is correct", 0, True)
                {
                    False;
                }
            }
        }
        
        
        { // pythagorean
        
            Local(procedure);
            
            procedureName := "pythagoreanProcedure";
            
            Echo(procedureName + ":");
        
            Retract(procedureName, All);
            
            procedure := procedures[procedureName];
            
            If(procedure !=? None)
            {            
            
                FoldGrade("The procedure does not throw an exception when defined", 1, True)
                {
                    ExceptionCatch(
                    {
                        /`( Procedure(@procedureName, @procedure["parameters"]) @procedure["body"] );
                        True;
                    },
                    "",
                    {
                        ExceptionGet()["message"];
                    });
                }

                FoldGrade("The procedure has one formal parameter", 1, False)
                {
                    Length(procedure["parameters"]) =? 1;
                }
    
                FoldGrade("The result is not in the code as a literal", 1, True)
                {
                    Local(values);
                    
                    values := SubtreesPattern(?foldCode, a_List? );
            
                    !? Contains?(values, "[1.414213562,5.656854250,12.72792206,22.62741700,35.35533906,50.91168825,69.29646455,90.50966799,114.5512986,141.4213562]");
                }

                FoldGrade("The procedure returns a correct result", 1, True)
                {
                    Local(procedureResult, correctValue);
        
                    correctValue := [1.414213562,5.656854250,12.72792206,22.62741700,35.35533906,50.91168825,69.29646455,90.50966799,114.5512986,141.4213562];
                    
                    procedureResult := ExceptionCatch(/`( Apply(Lambda(@procedure["parameters"], @procedure["body"]), [[1,4,9,16,25,36,49,64,81,100]]) ), "", ExceptionGet()["message"]);
                    
                    If(procedureResult !=? correctValue)
                    {
                        procedureResult;
                    }
                    Else
                    {
                        True;
                    }
                }

                FoldGrade("One loop is used", 1, False)
                {
                    Local(procedureNames, loopCount);
                    procedureNames := ProcedureListAll(procedure["body"]);
                    loopCount := Count(procedureNames,"While") + Count(procedureNames,"Until") + Count(procedureNames, "For");
                    loopCount =? 1;
                }
    
                FoldGrade("One \"Append!\" procedure is used", 1, True)
                {
                    Local(procedureNames, procedureCount);
                    procedureNames := ProcedureListAll(procedure["body"]);
                    procedureCount := Count(procedureNames,"Append!");
                    procedureCount =? 1;
                }
    
                FoldGrade("One \"NM\" procedure is used", 1, True)
                {
                    Local(procedureNames, procedureCount);
                    procedureNames := ProcedureListAll(procedure["body"]);
                    procedureCount := Count(procedureNames,"NM");
                    procedureCount =? 1;
                }
    
                FoldGrade("One \"Sqrt\" procedure is used", 1, True)
                {
                    Local(procedureNames, procedureCount);
                    procedureNames := ProcedureListAll(procedure["body"]);
                    procedureCount := Count(procedureNames,"Sqrt");
                    procedureCount =? 1;
                }
    
                FoldGrade("The \"Local\" procedure is used", 1, False)
                {
                    Local(procedureNames);
                    procedureNames := ProcedureList(procedure["body"]);
                    Contains?(procedureNames,"Local");
                }
            }
            Else
            {
                FoldGrade("The procedure name is correct", 0, True)
                {
                    False;
                }
            }
        }
        
        
        { // mainProcedure
        
            Local(procedure);

            procedureName := "mainProcedure";
            
            Echo(procedureName + ":");
        
            Retract(procedureName, All);
            
            procedure := procedures[procedureName];
            
            If(procedure !=? None)
            {
            
                FoldGrade("The procedure does not throw an exception when defined", 1, True)
                {
                    ExceptionCatch(
                    {
                        /`( Procedure(@procedureName, @procedure["parameters"]) @procedure["body"] );
                        True;
                    },
                    "",
                    {
                        ExceptionGet()["message"];
                    });
                }

                FoldGrade("The procedure has zero formal parameters", 1, False)
                {
                    Length(procedure["parameters"]) =? 0;
                }

                FoldGrade("The result is not in the code as a literal", 1, True)
                {
                    Local(values);
                    
                    values := SubtreesPattern(?foldCode, a_List? );
            
                    !? Contains?(values, "[1,4,9,16,25,36,49,64,81,100]") &?
                    !? Contains?(values, "[1.414213562,5.656854250,12.72792206,22.62741700,35.35533906,50.91168825,69.29646455,90.50966799,114.5512986,141.4213562]") &?
                    !? Contains?(values, "[[1,4,9,16,25,36,49,64,81,100],[1.414213562,5.656854250,12.72792206,22.62741700,35.35533906,50.91168825,69.29646455,90.50966799,114.5512986,141.4213562]]");
                }    

                FoldGrade("The procedure returns a correct result", 1, True)
                {
                    Local(procedureResult, correctValue);
        
                    correctValue := [[1,4,9,16,25,36,49,64,81,100],[1.414213562,5.656854250,12.72792206,22.62741700,35.35533906,50.91168825,69.29646455,90.50966799,114.5512986,141.4213562]];
                    
                    procedureResult := ExceptionCatch(/`( Apply(Lambda(@procedure["parameters"], @procedure["body"]), []) ), "", ExceptionGet()["message"]);
                    
                    If(procedureResult !=? correctValue)
                    {
                        procedureResult;
                    }
                    Else
                    {
                        True;
                    }
                }

                FoldGrade("No loops are used", 1, False)
                {
                    Local(procedureNames, loopCount);
                    procedureNames := ProcedureListAll(procedure["body"]);
                    loopCount := Count(procedureNames,"While") + Count(procedureNames,"Until") + Count(procedureNames, "For");
                    loopCount =? 0;
                }
    
                FoldGrade("The \"perfectSquaresProcedure\" procedure is called", 1, False)
                {
                    Local(procedureNames);
                    procedureNames := ProcedureList(procedure["body"]);
                    Contains?(procedureNames,"perfectSquaresProcedure");
                }

                FoldGrade("The \"pythagoreanProcedure\" procedure is called", 1, False)
                {
                    Local(procedureNames);
                    procedureNames := ProcedureList(procedure["body"]);
                    Contains?(procedureNames,"pythagoreanProcedure");
                }
            }
            Else
            {
                FoldGrade("The procedure name is correct", 0, True)
                {
                    False;
                }
            }
        }    
    }
    Else
    {
        FoldGrade("At least one procedure is defined in the fold", 0, True)
        {
            False;
        }
    }

    FoldGrade("The code follows the MathPiper code style brace alignment guidelines", 1, False)
    {
        BraceTest(PipeFromString(?foldCodeString) ParseMathPiper());
    }

    FoldGrade("The code follows the MathPiper code style indentation guidelines", 1, False)
    {
        IndentationTest(PipeFromString(?foldCodeString) ParseMathPiper());
    }
}

%/mathpiper_grade

%/group
