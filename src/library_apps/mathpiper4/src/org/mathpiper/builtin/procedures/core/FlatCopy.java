/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *  
 */
public class FlatCopy extends BuiltinProcedure
{

    private FlatCopy()
    {
    }

    public FlatCopy(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        
        Cons copied = Utility.flatCopy(aEnvironment, aStackTop, (Cons) getArgument(aEnvironment, aStackTop, 1).car());
        setTopOfStack(aEnvironment, aStackTop, SublistCons.getInstance(copied));
    }
}



/*
%mathpiper_docs,name="FlatCopy",categories="Programming Procedures,Lists (Operations),Built In"
*CMD FlatCopy --- copy the top level of a list
*CORE
*CALL
	FlatCopy(list)

*PARMS

{list} -- list to be copied

*DESC

A copy of "list" is made and returned. The list is not recursed
into, only the car level is copied. This is useful in combination
with the destructive commands that actually modify lists in place (for
efficiency).

*EXAMPLES

*SEE
%/mathpiper_docs





%mathpiper,name="FlatCopy",subtype="in_prompts"

The following shows a possible way to define a command that reverses a
list nondestructively.

lst := '[_a,_b,_c,_d,_e] ~> '[_a,_b,_c,_d,_e]

Reverse!(FlatCopy(lst)) ~> '[_e,_d,_c,_b,_a]

lst ~> '[_a,_b,_c,_d,_e]

%/mathpiper





%mathpiper,name="FlatCopy",subtype="automatic_test"

//Reverse and FlatCopy (and some friends) would segfault in the past if passed a string as argument.
//I am not opposed to overloading these functions to also work on strings per se, but for now just
//check that they return an error in stead of segfaulting.
//
Verify(ExceptionCatch(FlatCopy("abc"), "", Exception), Exception);

{
  Local(l);
  l:=[1,2,3];
  Delete!(l,1);
  Verify(l,[2,3]);
  Insert!(l,1,1);
  Verify(l,[1,2,3]);
  l[1] := 2;
  Verify(l,[2,2,3]);
  l[1] := 1;
  Delete!(l,3);
  Verify(l,[1,2]);
  Insert!(l,3,3);
  Verify(l,[1,2,3]);
  Delete!(FlatCopy(l),1);
  Verify(l,[1,2,3]);
}

%/mathpiper
*/
