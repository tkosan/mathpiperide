/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp.stacks;

import java.util.Stack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.*;

import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.variables.LocalVariable;
import org.mathpiper.lisp.variables.LocalVariableFrame;

/** 
 * Implements a stack of pointers to CONS that can be used to pass
 * arguments to functions, and receive results back.
 */
public class ArgumentStack {

    //ConsArray iArgumentStack;
    
    public static int stackPointer = 0;
    
    Cons[] iArgumentStack;
    int iStackTopIndex;
    
    LocalVariableFrame[] iArgumentStack2;
    int iStackTopIndex2;

    //TODO appropriate constructor?
    public ArgumentStack(Environment aEnvironment, int aStackSize) {
        iArgumentStack = new Cons[aStackSize];
        iStackTopIndex = 0;
        
        iArgumentStack2 = new LocalVariableFrame[aStackSize];
        iStackTopIndex2 = 0;
        //printf("STACKSIZE %d\n",aStackSize);
    }

    public int getStackTopIndex() {
        return iStackTopIndex;
    }

    public void raiseStackOverflowError(int aStackTop, Environment aEnvironment) throws Throwable {
        LispError.raiseError("Argument stack reached maximum. Please extend argument stack with --stack argument on the command line.", aStackTop, aEnvironment);
    }

    public void pushArgumentOnStack(Cons aCons, int aStackTop, Environment aEnvironment) throws Throwable {
        if (iStackTopIndex >= iArgumentStack.length) {
            raiseStackOverflowError(aStackTop, aEnvironment);
        }
        iArgumentStack[iStackTopIndex] = aCons;
        iStackTopIndex++;
    }
    
    public void pushLocalFrame(Environment aEnvironment, int aStackTop, LocalVariableFrame localVariableFrame) throws Throwable
    {
        if (iStackTopIndex2 >= iArgumentStack2.length) {
            raiseStackOverflowError(aStackTop, aEnvironment);
        }
        
        iArgumentStack2[iStackTopIndex2] = localVariableFrame;
        iStackTopIndex2++; 
    }
    
    public void popLocalFrame(Environment aEnvironment, int aStackTop) throws Throwable {
        if(iStackTopIndex2 == 0) LispError.lispAssert(aEnvironment, aStackTop);
        
        iStackTopIndex2--;
        
        if(Evaluator.iStackTraced && iArgumentStack2[iStackTopIndex2].stackPointer != -1)
        {
            stackPointer--;
        }
        
        iArgumentStack2[iStackTopIndex2] = null;
    }
    
    public LocalVariableFrame getLocalFrame(Environment aEnvironment, int aStackTop) throws Throwable
    {
        return (LocalVariableFrame) iArgumentStack2[iStackTopIndex2-1];
    }

    public void pushNulls(int aNr, int aStackTop, Environment aEnvironment) throws Throwable {
        if (iStackTopIndex + aNr > iArgumentStack.length) {
            raiseStackOverflowError(aStackTop, aEnvironment);
        }
        iStackTopIndex += aNr;
    }

    public Cons getElement(int aPos, int aStackTop, Environment aEnvironment) throws Throwable {
        if(aPos < 0 || aPos >= iStackTopIndex) LispError.lispAssert(aEnvironment, aStackTop);
        return iArgumentStack[aPos];
    }

    public void setElement(int aPos, int aStackTop, Environment aEnvironment, Cons cons) throws Throwable {
        if(aPos < 0 || aPos >= iStackTopIndex) LispError.lispAssert(aEnvironment, aStackTop);
        iArgumentStack[aPos] = cons;
    }

    public void popTo(int aTop, int aStackTop, Environment aEnvironment) throws Throwable {
        if(aTop > iStackTopIndex) LispError.lispAssert(aEnvironment, aStackTop);
        while (iStackTopIndex > aTop) {
            iStackTopIndex--;
            iArgumentStack[iStackTopIndex] = null;
        }
    }

    public void reset(int aStackTop, Environment aEnvironment) throws Throwable {
        this.popTo(0, aStackTop, aEnvironment);
    }//end method.
    
    

    public String dumpArgumentStack(int aStackTop, Environment aEnvironment, boolean isSource) throws Throwable {

        StringBuilder stringBuilder = new StringBuilder();

        int functionBaseIndex = 0;


        while (functionBaseIndex <= aStackTop) {

            if(functionBaseIndex == 0)
            {
                stringBuilder.append("\n\n========================================= Start Of Built In Function Stack Trace\n");
            }
            else
            {
                stringBuilder.append("-----------------------------------------\n");
            }

            Cons argumentCons = getElement(functionBaseIndex, aStackTop, aEnvironment);


            if(argumentCons instanceof Cons)
            {
                int argumentCount = Utility.listLength(aEnvironment, aStackTop, (Cons) argumentCons);
                Cons  cons = argumentCons;

                stringBuilder.append(functionBaseIndex + ": " + cons.stackPointer + ", ");
                if(isSource)
                {
                    stringBuilder.append(Utility.printMathPiperExpression(aStackTop, cons, aEnvironment, -1, false, false, false, false));
                }
                else
                {
                    Object car = cons.car();

                    if(car instanceof String)
                    {
                        stringBuilder.append(car);
                    }
                    else
                    {
                        stringBuilder.append(((Cons) cons.car()).car());
                    }
                }
                stringBuilder.append("\n");

                cons = cons.cdr();

                while(cons != null)
                {
                    if(isSource)
                    {
                        stringBuilder.append("    arg-> " + Utility.printMathPiperExpression(aStackTop, cons, aEnvironment, -1, false, false, false, false));
                        stringBuilder.append("\n");
                    }

                    cons = cons.cdr();
                }

                functionBaseIndex = functionBaseIndex + argumentCount;
            }


            

        }//end while.

        stringBuilder.append("========================================= End Of Built In Function Stack Trace\n\n");

        return stringBuilder.toString();
        
    }//end method.
    
    
    
    
    public String dumpBuiltinNameCallStack(int aStackTop, Environment aEnvironment)
    {
        StringBuilder stringBuilder = new StringBuilder();
        
        int stackIndex = 0;
        
        Stack stack = BuiltinProcedureEvaluator.builtinNamesCallStack;
        
        while(stackIndex < stack.size())
        {
            stringBuilder.append(stack.elementAt(stackIndex));
            stringBuilder.append("\n");
            stackIndex++;
        }
        
        return stringBuilder.toString();
    }
    
    
    
    public String dumpLocalVariablesFrame(Environment aEnvironment, int aStackTop, boolean isSource) throws Throwable {

        /*
            if (iLocalVariablesFrame == null)
                    LispError.throwError(this, aStackTop, LispError.INVALID_STACK, "");
        */

            //LocalVariableFrame localVariablesFrame = iArgumentStack.getLocalFrame(this, aStackTop);

            //LocalVariableFrame localVariableFrame = localVariablesFrame;

            int frameIndex = 0;

            StringBuilder stringBuilder = new StringBuilder();



            while (frameIndex < this.iStackTopIndex2) {

                    LocalVariableFrame localVariableFrame = this.iArgumentStack2[frameIndex];

                    
                    if(localVariableFrame.stackPointer != -1)
                    {
                        String functionName = localVariableFrame.getFunctionName();

                        if (frameIndex == 0) {
                                stringBuilder.append("\n\n========================================= Start Of User Function Stack Trace\n");
                        } else {
                                stringBuilder.append("-----------------------------------------\n");
                        }
                    
                        stringBuilder.append(frameIndex + ": ");
                        stringBuilder.append(functionName);
                        stringBuilder.append(", " + localVariableFrame.stackPointer);
                        stringBuilder.append("\n");


                        LocalVariable localVariable = localVariableFrame.iFirst;

                        // stringBuilder.append("Local variables: ");

                        while (localVariable != null) {

                            if(isSource)
                            {
                                stringBuilder.append("   var-> ");
                                stringBuilder.append(localVariable.iVariable);
                                stringBuilder.append(": ");
                                Cons value = localVariable.iValue;
                                String valueString = Utility.printMathPiperExpression(aStackTop, value, aEnvironment, -1, false, false, false, false);
                                stringBuilder.append(valueString);
                                stringBuilder.append("\n");
                            }
                                /*
                                 * if(value != null) {
                                 * localVariablesStringBuilder.append(value.trim
                                 * ().replace("  ","").replace("\n", "") ); } else {
                                 * localVariablesStringBuilder.append("unbound"); }//end else.
                                 * 
                                 * 
                                 * localVariablesStringBuilder.append(", ");
                                 */

                            localVariable = localVariable.iNext;
                        }// end while.
                    }
                    
                    frameIndex++;

            }// end while

            stringBuilder.append("========================================= End Of User Function Stack Trace\n\n");

            return stringBuilder.toString();

            /*
             * StringBuilder stringBuilder = new StringBuilder();
             * 
             * int functionBaseIndex = 0;
             * 
             * int functionPositionIndex = 0;
             * 
             * 
             * while (functionBaseIndex <= aStackTop) {
             * 
             * if(functionBaseIndex == 0) { stringBuilder.append(
             * "\n\n========================================= Start Of Stack Trace\n"
             * ); } else {
             * stringBuilder.append("-----------------------------------------\n");
             * }
             * 
             * ConsPointer consPointer = getElement(functionBaseIndex, aStackTop,
             * aEnvironment);
             * 
             * int argumentCount = Utility.listLength(aEnvironment, aStackTop,
             * consPointer);
             * 
             * ConsPointer argumentPointer = new ConsPointer();
             * 
             * Object car = consPointer.getCons().car();
             * 
             * ConsPointer consTraverser = new ConsPointer( consPointer.getCons());
             * 
             * stringBuilder.append(functionPositionIndex++ + ": ");
             * stringBuilder.append(Utility.printMathPiperExpression(aStackTop,
             * consTraverser, aEnvironment, -1)); stringBuilder.append("\n");
             * 
             * consTraverser.goNext(aStackTop, aEnvironment);
             * 
             * while(consTraverser.getCons() != null) { stringBuilder.append("   " +
             * functionPositionIndex++ + ": "); stringBuilder.append("-> " +
             * Utility.printMathPiperExpression(aStackTop, consTraverser,
             * aEnvironment, -1)); stringBuilder.append("\n");
             * 
             * consTraverser.goNext(aStackTop, aEnvironment); }
             * 
             * 
             * functionBaseIndex = functionBaseIndex + argumentCount;
             * 
             * }//end while.
             * 
             * stringBuilder.append(
             * "========================================= End Of User Function Stack Trace\n\n"
             * );
             * 
             * return stringBuilder.toString();
             */

    }// end method.


    
    
    public String dumpBoth(Environment aEnvironment, int aStackTop, boolean isSource) throws Throwable
    {

        int frameIndex = 0;

        int stackIndex = 0;

        Stack<ProcedureCallInfo> stack = BuiltinProcedureEvaluator.builtinNamesCallStack;

        LocalVariableFrame localVariableFrame;

        StringBuilder stringBuilder = new StringBuilder();

        while (frameIndex < this.iStackTopIndex2 || stackIndex < stack.size())
        {
            if (frameIndex < this.iStackTopIndex2 && this.iArgumentStack2[frameIndex].stackPointer == -1)
            {
                frameIndex++;
                continue;
            }

            if (frameIndex < this.iStackTopIndex2 && stackIndex < stack.size())
            {
                localVariableFrame = iArgumentStack2[frameIndex];

                ProcedureCallInfo procedureCallInfo = stack.elementAt(stackIndex);

                if (localVariableFrame.stackPointer < procedureCallInfo.stackPointer)
                {
                    this.writeLocal(aEnvironment, aStackTop, stringBuilder, localVariableFrame, isSource);
                    frameIndex++;
                }
                else
                {
                    if (stackIndex != stack.size())
                    {
                        this.writeBuiltin(stringBuilder, procedureCallInfo);
                        stackIndex++;
                    }
                }
            }
            else if (frameIndex < this.iStackTopIndex2 && stackIndex == stack.size())
            {
                localVariableFrame = iArgumentStack2[frameIndex];

                this.writeLocal(aEnvironment, aStackTop, stringBuilder, localVariableFrame, isSource);
                frameIndex++;
            }
            else
            {
                ProcedureCallInfo procedureCallInfo = stack.elementAt(stackIndex);
                stringBuilder.append("B " + procedureCallInfo.functionName);
                stringBuilder.append("\n");
                stackIndex++;
            }
        }

        return stringBuilder.toString();
    }

    private void writeLocal(Environment aEnvironment, int aStackTop, StringBuilder stringBuilder, LocalVariableFrame localVariableFrame, boolean isSource) throws Throwable
    {
        String functionName = localVariableFrame.getFunctionName();

        //stringBuilder.append(frameIndex + ": ");
        stringBuilder.append("U " + functionName);
        stringBuilder.append(", " + localVariableFrame.stackPointer);
        stringBuilder.append(", " + localVariableFrame.sourcePath);
        stringBuilder.append(", " + localVariableFrame.lineNumber);
        stringBuilder.append("\n");

        if (isSource)
        {
            LocalVariable localVariable = localVariableFrame.iFirst;

            while (localVariable != null)
            {

                stringBuilder.append("   var-> ");
                stringBuilder.append(localVariable.iVariable);
                stringBuilder.append(": ");
                Cons value = localVariable.iValue;
                String valueString = Utility.printMathPiperExpression(aStackTop, value, aEnvironment, -1, true, false, false, false);
                stringBuilder.append(valueString);
                stringBuilder.append("\n");

                localVariable = localVariable.iNext;
            }
        }

    }

    private void writeBuiltin(StringBuilder stringBuilder, ProcedureCallInfo procedureCallInfo)
    {
        stringBuilder.append("B " + procedureCallInfo.functionName);
        stringBuilder.append("\n");
    }

    
    
}//end class.

