%mathpiper,def="LowerTriangular?"

LowerTriangular?(A_Matrix?) --> (UpperTriangular?(Transpose(A)));

%/mathpiper



%mathpiper_docs,name="LowerTriangular?",categories="Programming Procedures,Predicates"
*CMD LowerTriangular? --- test for a lower triangular matrix
*STD
*CALL
        LowerTriangular?(A)

*PARMS

{A} -- a matrix

*DESC

A lower triangular matrix is a square matrix which has all zero entries above the diagonal.

{LowerTriangular?(A)} returns {True} if {A} is a lower triangular matrix and {False} otherwise.

*E.G.
In> LowerTriangular?(IdentityMatrix(5))
Result: True

In> LowerTriangular?([[1,2],[0,1]])
Result: False

A non-square matrix cannot be triangular:

In> LowerTriangular?([[1,2,3],[0,1,2]])
Result: False

*SEE UpperTriangular?, Diagonal?
%/mathpiper_docs





%mathpiper,name="LowerTriangular",subtype="automatic_test"

Verify(LowerTriangular?(IdentityMatrix(5)) , True);

Verify(LowerTriangular?([[1,2],[0,1]]), False);

Verify(LowerTriangular?([[1,2,3],[0,1,2]]), False);

%/mathpiper