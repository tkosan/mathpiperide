package org.mathpiper.lisp.parsers.subl.helpers;

import java.util.*;

/*
MIT License

Copyright (c) 2020 Joseph Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
 * File: StringHelpers.java
 * 
 * This file is for some helpers to work with strings and vectors of strings.
 * 
 * 
 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
 * @since 2012-11-01
 * @version 2012-11-01
 */

public class StringHelpers{

	/**
	 * Function: join
	 *
	 * This function is used to implodde a generic list together
	 * with the specified "glue" string
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 * 
	 * @param s 	Any List whose elements support the toString method
	 * @param glue	A string with which to implode the list element strings
	 * 
	 * @return 		The string of list elements stringified and joined
	 */	
	public static String join(List s, String glue){
		String newString = "";
		for ( int i = 0; i < s.size() - 1; i++ ){
			newString = newString + s.get(i).toString() + glue;
		}
		return newString + s.get(s.size() - 1).toString();
	}

	/**
	 * Function: vectorize
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 * 
	 * @param s A string
	 * @return A vector where each element is a signle character of the string
	 */	
	public static Vector <String> vectorize(String s){
		Vector <String> v = new Vector <String> ();
		int i;
		for ( i = 0; i < s.length() - 1; i ++ ){
			v.add(s.substring(i,i+1));
		}
		if ( i > 0 ){
			v.add(s.substring(i));
		}
		return v;
	}
}