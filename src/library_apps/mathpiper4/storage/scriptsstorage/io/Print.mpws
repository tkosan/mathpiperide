%mathpiper,def="Print;PrintArg;PrintArgComma;PrintArgBlock"


/* A reference print implementation. Expand at own leisure.
 *
 * This file implements Print, a scripted expression printer.
 */


/* 60000 is the maximum precedence allowed for operators */
10 # Print(_x) <--
{
  Print(x,60000);
  NewLine();
  DumpErrors();
};

/* Print an argument within an environment of precedence n */
10 # Print(x_Atom?,_n) <-- Write(x);
10 # Print(_x,_n)_(Infix?(Type(x))And? ArgumentsCount(x) =? 2) <--
{
  Local(bracket);
  bracket:= (PrecedenceGet(Type(x)) >? n);
  Decide(bracket,WriteString("("));
  Print(x[1],LeftPrecedenceGet(Type(x)));
  Write(x[0]);
  Print(x[2],RightPrecedenceGet(Type(x)));
  Decide(bracket,WriteString(")"));
};

10 # Print(_x,_n)_(Prefix?(Type(x)) And? ArgumentsCount(x) =? 1) <--
{
  Local(bracket);
  bracket:= (PrecedenceGet(Type(x)) >? n);
  Write(x[0]);
  Decide(bracket,WriteString("("));
  Print(x[1],RightPrecedenceGet(Type(x)));
  Decide(bracket,WriteString(")"));
};

10 # Print(_x,_n)_(Postfix?(Type(x))And? ArgumentsCount(x) =? 1) <--
{
  Local(bracket);
  bracket:= (PrecedenceGet(Type(x)) >? n);
  Decide(bracket,WriteString("("));
  Print(x[1],LeftPrecedenceGet(Type(x)));
  Write(x[0]);
  Decide(bracket,WriteString(")"));
};

20 # Print(_x,_n)_(Type(x) =? "List") <--
{
  WriteString("[");
  PrintArg(x);
  WriteString("]");
};

20 # Print(_x,_n)_(Type(x) =? "Block") <--
{
  WriteString("[");
  PrintArgBlock(Rest(FunctionToList(x)));
  WriteString("]");
};
20 # Print(_x,_n)_(Type(x) =? "Nth") <--
{
  Print(x[1],0);
  WriteString("[");
  Print(x[2],60000);
  WriteString("]");
};

100 # Print(x_Function?,_n) <--
 {
   Write(x[0]);
   WriteString("(");
   PrintArg(Rest(FunctionToList(x)));
   WriteString(")");
 };


/* Print the arguments of an ordinary function */
10 # PrintArg([]) <-- True;

20 # PrintArg(_list) <--
{
  Print(First(list),60000);
  PrintArgComma(Rest(list));
};
10 # PrintArgComma([]) <-- True;
20 # PrintArgComma(_list) <--
{
  WriteString(",");
  Print(First(list),60000);
  PrintArgComma(Rest(list));
};


18 # Print(Complex(0,1),_n)   <-- {WriteString("I");};
19 # Print(Complex(0,_y),_n)  <-- {WriteString("I*");Print(y,4);};
19 # Print(Complex(_x,1),_n)  <-- {Print(x,7);WriteString("+I");};
20 # Print(Complex(_x,_y),_n) <-- {Print(x,7);WriteString("+I*");Print(y,4);};


/* Tail-recursive printing the body of a compound statement */
10 # PrintArgBlock([]) <-- True;
20 # PrintArgBlock(_list) <--
{
   Print(First(list),60000);
   WriteString(";");
   PrintArgBlock(Rest(list));
};








%/mathpiper