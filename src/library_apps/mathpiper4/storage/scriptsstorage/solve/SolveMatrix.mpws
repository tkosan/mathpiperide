%mathpiper,def="SolveMatrix"

Function("SolveMatrix",[matrix,vector])
{
  Decide(InVerboseMode(),Tell("   SolveMatrix",[matrix,vector]));
  Local(perms,indices,inv,det,n);
  n:=Length(matrix);
  indices:=BuildList(i,i,1,n,1);
  perms:=PermutationsList(indices);
  inv:=ZeroVector(n);
  det:=0;
  ForEach(item,perms)
  {
    Local(i,lc);
    lc := LeviCivita(item);
    det:=det+Product(i,1,n,matrix[i][item[i] ])* lc;
    For(i:=1,i<=?n,i++)
        {
         inv[i] := inv[i]+
           Product(j,1,n,
           Decide(item[j] =? i,vector[j ],matrix[j][item[j] ]))*lc;
        };
  };
  Check(det !=? 0, "Math", "Zero determinant");
  (1/det)*inv;
};

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="SolveMatrix",categories="Mathematics Functions;Linear Algebra;Solvers (Symbolic)"
*CMD SolveMatrix --- solve a linear system
*STD
*CALL
        SolveMatrix(M,v)

*PARMS

{M} -- a matrix

{v} -- a vector

*DESC

{SolveMatrix} returns the vector $x$ that satisfies
the equation $M*x = v$. The determinant of $M$ should be non-zero.

*E.G.

In> A := [[1,2], [3,4]];
Result: [[1,2],[3,4]];

In> v := [5,6];
Result: [5,6];

In> x := SolveMatrix(A, v);
Result: [-4,9/2];

In> A * x;
Result: [5,6];

*SEE Inverse, Solve, PSolve, Determinant
%/mathpiper_docs