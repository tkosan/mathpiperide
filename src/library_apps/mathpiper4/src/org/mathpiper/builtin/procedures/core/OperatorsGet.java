/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Operator;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *  
 */
public class OperatorsGet extends BuiltinProcedure
{

    private OperatorsGet()
    {
    }

    public OperatorsGet(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        ArrayList<Operator> operatorsList = new ArrayList();
        operatorsList.addAll(aEnvironment.iInfixOperators.toList());
        operatorsList.addAll(aEnvironment.iPrefixOperators.toList());
        operatorsList.addAll(aEnvironment.iPostfixOperators.toList());

        Cons listAtomCons = aEnvironment.iListAtom.copy(false);
        
        Comparator<Operator> comparator = Comparator.comparing(Operator::getiPrecedence).thenComparing(Operator::getSymbol);
 
        Collections.sort(operatorsList, comparator);
        
        Cons pointerCons = listAtomCons;

	for (Operator operator : operatorsList) {
            String symbolName = operator.getSymbol();
            
            if(! symbolName.endsWith("$"))
            {
                Cons elementCons = AtomCons.getInstance(aEnvironment.getPrecision(), "\"" + symbolName + "\"");
                pointerCons.setCdr(elementCons);
                pointerCons = elementCons;
            }
	}

        Cons list = SublistCons.getInstance(listAtomCons);

	setTopOfStack(aEnvironment, aStackTop, list);
    }
}



/*
%mathpiper_docs,name="OperatorsGet",categories="Programming Procedures,Miscellaneous,Built In"
*CMD OperatorsGet --- returns a list of all currently defined operators

*CALL
	OperatorsGet()

*DESC
This procedure returns a list of all the currently defined operators sorted by
precedence (high to low) then by name.

*E.G.
In> OperatorsGet()
Result: ["'","@","/`",":*:","`","++","--",":","<<",">>","∠","^","+","-",".","/","X","bin","hex","o","*","***","+","-","~~","||","!==","!=?","<=>","<=?","<>","<?","==","=?",">=?",">?","@@","!?","..","/@","&?","|?","AddTo","#","->?","==?","Where","''","::","/``","##","-->","->","/:","/::","+:=","-:=",":=","Else"]

*SEE Operator?,LeftPrecedenceGet,RightPrecedenceGet,LeftPrecedenceSet,RightPrecedenceSet,RightAssociativeSet,OperatorSpacesSet,LeftSpacesGet,RightSpacesGet
%/mathpiper_docs




todo:tk:the ">>" documentation symbol will not work with this example in an "in_prompts" fold
because it clashes with the ">>" operator.





%mathpiper,name="OperatorsGet",subtype="automatic_test"

Verify(OperatorsGet(), ["'","/`","@",":*:","`","++","--",":","<<",">>","∠","^","+","-",".","/","X","bin","hex","o","*","***","+","-","~~","||","!==","!=?","<=>","<=?","<>","<?","==","=?",">=?",">?","@@","!?","..","/@","&?","|?","AddTo","#","->?","==?","Where","''","/``","::","##","\\\\","-->","->","/:","/::","+:=","-:=",":=","Else"]);

%/mathpiper
*/

