package org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.fet;
import java.awt.Color;
import java.awt.geom.Point2D;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.Point;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.JfetElm;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.Transistor;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;


public final class TransistorJFETP extends Transistor {
    
    protected Terminal baseTerminal;
    
    public TransistorJFETP(int x, int y, String uid, Circuit circuit) {
        super(x, y, uid, circuit);

        init();
    }
    
    public void init() {
        super.init();
    }
    
    protected void calculateGeometry()
    {
        super.calculateGeometry();
        
        if(getHeadTerminal() == null || getTailTerminal() == null)
        {
            return;
        }
        
        radius2 = 5;
        hypotenuse = Math.sqrt(rise * rise + run * run); // Distance between terminals.
        centerX = (headTerminalX + tailTerminalX) / 2;
        centerY = (headTerminalY + tailTerminalY) / 2;
        sine = run / hypotenuse;
        cosine = rise / hypotenuse;
        headX = headTerminalX + run / 2.0 - radius2 * sine;
        headY = headTerminalY + rise / 2.0 - radius2 * cosine;
        tailX = tailTerminalX - run / 2.0 + radius2 * sine;
        tailY = tailTerminalY - rise / 2.0 + radius2 * cosine;
        circleOffset = 5 * mirrorOrientation;
        shaftScale = 8.0;
        shaftScale2 = 5.0;
        setback = (mirrorOrientation  == 1) ? 8.0 : -8.0;
        setback2 = (mirrorOrientation  == 1) ? 30.0 : -30.0;
        setback3 = (mirrorOrientation  == 1) ? 16.0 : -16.0;
        calculatedPoints[0] = new Point2D.Double(centerX - (setback2 * cosine), centerY + (setback2 * sine));
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        calculateGeometry();
        
        if (hypotenuse >= 2 * radius) {
            sg.drawLine(headTerminalX, headTerminalY, headX, headY);
            sg.drawLine(tailX, tailY, tailTerminalX, tailTerminalY);
            
            sg.drawOval( (centerX - (circleOffset * cosine) - radius ),  (centerY + (circleOffset * sine) - radius), 2 * radius, 2 * radius);
        } else {
            // sg.drawOval( (centerX - hypotenuse / 2),  (centerY - hypotenuse / 2),  hypotenuse,  hypotenuse); //todo:tk
        }
        if (hypotenuse > 0) {
            
            // Vertical line.
            double oldLineWidth = sg.getLineWidth();
            sg.setLineWidth(3);
            sg.drawLine(centerX - (setback * cosine) + shaftScale * sine,
                     centerY + (setback * sine) + shaftScale * cosine,
                     centerX - (setback * cosine) - shaftScale * sine,
                     centerY + (setback * sine) - shaftScale * cosine);
            sg.setLineWidth(oldLineWidth);
            
            // Top line.
            sg.drawLine(headX,
                     headY,
                     centerX - (setback * cosine) - shaftScale2 * sine,
                     centerY + (setback * sine) - shaftScale2 * cosine);

            // Bottom line.
            sg.drawLine(centerX - (setback * cosine) + shaftScale2 * sine, 
                    centerY + (setback * sine) + shaftScale2 * cosine, 
                    tailX, 
                    tailY);
            
            // Base line.        
            drawArrowLine(sg, 
                    centerX - (setback * cosine),
                    centerY + (setback * sine),
                    centerX - (setback3 * cosine),
                    centerY + (setback3 * sine), 
                    6 ,2);
            
            sg.drawLine(centerX - (setback3 * cosine),
                    centerY + (setback3 * sine),
                    centerX - (setback2 * cosine),
                    centerY + (setback2 * sine));
            
            if(circuit.isCirSim && circuit.circuitPanel.isNotMovingAny())
            {
                JfetElm ce = (JfetElm) getCircuitElm();
                
                // draw dots
                ce.drawDots(sg, new Point((int) Math.round(calculatedPoints[0].x), (int) Math.round(calculatedPoints[0].y)), new Point((int) Math.round(centerX - (setback3 * cosine)), (int) Math.round(centerY + (setback3 * sine))), ce.curcountg);
                ce.drawDots(sg, new Point(headTerminalX, headTerminalY), new Point((int) Math.round(headX), (int) Math.round(headY)), -ce.curcounts);
                ce.drawDots(sg, new Point(tailTerminalX, tailTerminalY), new Point((int) Math.round(tailX), (int) Math.round(tailY)), ce.curcountd);
            }
        }
      
    }
    
    public String toString()
    {
        return super.toString();
    }
}
