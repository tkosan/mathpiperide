/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *
 */
public class StringCompareNatural extends BuiltinProcedure {

    private StringCompareNatural() {
    }

    public StringCompareNatural(String functionName) {
        this.functionName = functionName;
    }

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        //public static int compareNatural(String a, String b) {
        Cons argument1 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 1);

        Cons argument2 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 2);

        String a;
        String b;
        
        a = (String) argument1.car();
        b = (String) argument2.car();
        if( a == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        if( b == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        
        int result = Utility.compareNatural(a,b);
        
        BuiltinProcedure.setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "" + result));

    }
    
    
}



/*
%mathpiper_docs,name="StringCompareNatural",categories="Programming Procedures,Predicates,Strings,Built In"
*CMD StringCompareNatural --- compares strings naturally instead of lexicographically

*CALL
	StringCompareNatural(string1, string2)

*PARMS
{string1}, {string2} -- strings

*DESC
Compare strings naturally instead of lexicographically. Lexicongraphic
comparison considers "a10" to be less than "a2", while natural comparison
considers "a2" to be less than "a10". Leading zeros immediately before
numbers in a string are ignored.

{String1 < String2} returns a negative number

{String1 = String2} returns 0

{String1 > String2} returns a positive number

*EXAMPLES

*SEE LessThan?, GreaterThan?, Equal?
%/mathpiper_docs





%mathpiper,name="StringCompareNatural",subtype="in_prompts"

StringCompareNatural("a", "a") ~> 0
StringCompareNatural("a", "b") ~> -1
StringCompareNatural("b", "a") ~> 1
LessThan?("a2", "a10") ~> False
StringCompareNatural("a2", "a10") ~> -1

%/mathpiper





%mathpiper,name="StringCompareNatural",subtype="automatic_test"

Verify(StringCompareNatural("a", "a"), 0);
Verify(StringCompareNatural("a", "b"), -1);
Verify(StringCompareNatural("b", "a"), 1);
Verify(StringCompareNatural("a2", "a10"), -1);

%/mathpiper

 */
