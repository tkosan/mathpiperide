package org.mathpiper.ui.gui;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;


class VariableWidthLine extends JComponent
{
    private int thickness = 1;
    
    public VariableWidthLine(String name)
    {
        this.setName(name);
    }
    
    public VariableWidthLine(String name, int thickness)
    {
        this.setName(name);
        this.thickness = thickness;
    }
    
    public void paintComponent(Graphics g) {
            
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(thickness));
        g2.drawLine(0, 0, g.getClipBounds().width, 0);
    }
    
    public int getHeight()
    {
        return thickness;
    }
}
