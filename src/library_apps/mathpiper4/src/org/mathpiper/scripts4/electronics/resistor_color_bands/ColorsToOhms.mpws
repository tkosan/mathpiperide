%mathpiper,def="ColorsToOhms"

ColorsToOhms(list) :=
{
    Local(listLength, resistorRatedValue, tolerance);
    
    Check(List?(list) &? UnFlatten(MapSingle(Lambda([element], Color?(element)), list), "&?", True), "The argument must be a list.");

    listLength := Length(list);
    
    If(listLength =? 3)
    {
        resistorRatedValue :=  NM((ResistorColorToNumber(list[1]) * 10 + ResistorColorToNumber(list[2])) * 10 ^ ResistorColorToNumber(list[3])) ` Ω;
        
        tolerance := .2;
    }
    Else If(listLength =? 4)
    {
        resistorRatedValue :=  NM((ResistorColorToNumber(list[1]) * 10 + ResistorColorToNumber(list[2])) * 10 ^ ResistorColorToNumber(list[3])) ` Ω;
        
        tolerance := ResistorToleranceColorToNumber(list[4]);
    }
    Else If(listLength =? 5)
    {
        resistorRatedValue := NM((ResistorColorToNumber(list[1]) * 100 + ResistorColorToNumber(list[2]) * 10 + ResistorColorToNumber(list[3])) * 10 ^ ResistorColorToNumber(list[4])) ` Ω;

        tolerance := ResistorToleranceColorToNumber(list[5]);    
    }
    Else
    {
        ExceptionThrow("", "Resistors with " + listLength + " color bands is not supported.");
    }
    
    [
        resistorRatedValue,
        tolerance`1 # %,
        resistorRatedValue - tolerance * resistorRatedValue,
        resistorRatedValue + tolerance * resistorRatedValue
    ];
}

%/mathpiper





%mathpiper_docs,name="ColorsToOhms",categories="Programming Procedures,Electronics"
*CMD ColorsToOhms --- convert resistor color code colors to a value

*CALL
        ColorsToOhms(colors)

*PARMS

{colors} -- a list of resistor color code colors

*DESC

This procedure will convert 3-band, 4-band, and 5-band resistor color 
code colors to a value.

*EXAMPLES

*SEE OhmsToColors
%/mathpiper_docs





%mathpiper,name="ColorsToOhms",subtype="in_prompts"

ColorsToOhms([Orange, Blue, Gold]) ~> [3.6`Ω,20`%,2.88`Ω,4.32`Ω]

ColorsToOhms([Brown, Blue, Green, Silver]) ~> [1600000`Ω,10`%,1440000`Ω,1760000`Ω]

ColorsToOhms([Orange, Blue, Gold, Red]) ~> [3.6`Ω,2`%,3.528`Ω,3.672`Ω]

ColorsToOhms([Red, Green, Blue, Brown, Gold]) ~> [2560`Ω,5`%,2432`Ω,2688`Ω]

%/mathpiper





%mathpiper,name="ColorsToOhms",subtype="automatic_test"

Verify(ColorsToOhms([Orange, Blue, Gold]), [3.6`Ω,20`%,2.88`Ω,4.32`Ω]);

Verify(ColorsToOhms([Brown, Blue, Green, Silver]), [1600000`Ω,10`%,1440000`Ω,1760000`Ω]);

Verify(ColorsToOhms([Orange, Blue, Gold, Red]), [3.6`Ω,2`%,3.528`Ω,3.672`Ω]);

Verify(ColorsToOhms([Red, Green, Blue, Brown, Gold]), [2560`Ω,5`%,2432`Ω,2688`Ω]);

%/mathpiper
