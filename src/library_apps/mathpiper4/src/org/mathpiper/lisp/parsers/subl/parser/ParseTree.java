package org.mathpiper.lisp.parsers.subl.parser;

import java.util.Vector;

/*
MIT License

Copyright (c) 2020 Joseph Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
 * File: ParseTree.java
 * 
 * This files contains the main tree structure of the Lisp program.
 * 
 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
 * @since 2012-11-01
 * @version 2012-11-01
 *
 */

public class ParseTree{
	public TreeNode root;
	
	/**
	 * Function: ParseTree
	 * 
	 * Constructor(Vector <String> s)
	 * 
	 * Uses the given vector to create the root. It calls the
	 * TreeNode factory constructor
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 *
	 * @param s A vector representing the outermost expression
	 * 
	 * @throws Exception If node creation fails
	 *
	 */
	public ParseTree(Vector <String> s) throws Exception{
		root = TreeNode.create(s);
	}

	/**
	 * Function: evaluate
	 * 
	 * Evaluates the root node and returns the result as a string
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 * 
	 * @return The string of the executed statement
	 *
	 * @throws Exception If evaluation fails
	 *
	 */
	protected String evaluate() throws Exception{
		String rtn = root.evaluate().toString();
		return rtn;
	}

	/**
	 * Function: print
	 * 
	 * Stringifies and returns the parsetree as-is
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 *
	 * @deprecated used only for early debuggin
	 * 
	 * @return String of the root node
	 *
	 */
	protected String print(){
		String result = root.toString();
		return result;
	}
}
