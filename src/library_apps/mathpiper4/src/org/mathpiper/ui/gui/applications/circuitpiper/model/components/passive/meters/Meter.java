package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters;

import java.awt.Color;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public abstract class Meter extends Component {

    public static int METER_SIZE = 27;
    
    public static int componentCounter = 1;
    
       protected int x1;
       protected int x2;
       protected int y1;
       protected int y2;
       protected int rise;
       protected int run;
       protected double distance;
       protected double xm;
       protected double ym;
       protected double middleX1;
       protected double middleY1;
       protected double middleX2;
       protected double middleY2;

    public Meter(int x, int y, Circuit circuit) {
        super(x, y, circuit);
        componentSubscript = componentCounter + "";
        componentCounter++;
        primarySymbol = "M";
    }
    
    public void reset()
    {
        setSecondaryValue(0);
        setFullValue("");
        setCalculatedValue((Double) 0.0);
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        x1 = getHeadTerminal().getX();
        x2 = getTailTerminal().getX();
        y1 = getHeadTerminal().getY();
        y2 = getTailTerminal().getY();
        rise = y2 - y1;
        run = x2 - x1;
        distance = Math.sqrt(rise * rise + run * run);
        xm = (x1 + x2) / 2;
        ym = (y1 + y2) / 2;
        
        middleX1 = (x1 + run / 2.0 - METER_SIZE * run / distance);
        middleY1 = (y1 + rise / 2 - METER_SIZE * rise / distance);
        middleX2 = (x2 - run / 2.0 + METER_SIZE * run / distance);
        middleY2 = (y2 - rise / 2.0 + METER_SIZE * rise / distance);
        
        if (distance >= 2 * METER_SIZE) {
            double oldLineWidth = sg.getLineWidth();
            sg.setLineWidth(4);
            Color oldColor = sg.getColor();
            sg.setColor(Color.RED);
            this.drawArrowLine(sg, middleX1, middleY1, x1, y1, 11, 4);
            sg.setColor(oldColor);
            drawArrowLine(sg, middleX2, middleY2, x2, y2, 11, 4);
            sg.setLineWidth(oldLineWidth);
            sg.drawOval( (xm - METER_SIZE),  (ym - METER_SIZE), 2 * METER_SIZE, 2 * METER_SIZE);
            sg.drawString("+",  (x1 + run / 2.0 - (METER_SIZE + 10) * run / distance - 10 * rise / distance),
                         (y1 + rise / 2 - (METER_SIZE + 10) * rise / distance + 10 * run / distance));
        } 
        else
        {
            sg.drawOval( (xm - distance / 2),  (ym - distance / 2),  distance,  distance);
        }
        
        sg.drawString(getID(),  xm - 10,  ym - 10);
    }
    
    public int getLabelDistance()
    {
        return 30;
    }
}
