%mathpiper,def="DirectoryFiles"

DirectoryFiles(filename) :=
{
    Local(filesList, directory, array, fileCount, file, fileNumber);
    
    Check(FileExists?(filename), "The file <" + filename + "> does not exist.");
    
    filesList := [];
    
    directory := JavaNew("java.io.File", filename);
    
    Check(JavaAccess(directory, "isDirectory"), "The file <" + filename + "> is not a directory.");
    
    array := JavaCall(directory, "listFiles");
    
    fileCount := JavaAccess("java.lang.reflect.Array","getLength",array);
    
    For(fileNumber := 0, fileNumber <? fileCount, fileNumber++)
    {
        file := JavaCall("java.lang.reflect.Array","get", array, fileNumber);
        Append!(filesList, file);
    }
    
    filesList;
}

%/mathpiper





%mathpiper_docs,name="DirectoryFiles",categories="Programming Procedures,Input/Output",access="experimental"

*CMD DirectoryFiles --- Returns a list of all the files in a directory.

*CALL DirectoryFiles(filename)
        

*PARMS 
{filename} -- The filename that you enter.

*DESC
This procedure returns a list of all the files in a directory.

*E.G.
In> DirectoryFiles("/")
Result: [class java.io.File,class java.io.File,class java.io.File]
%/mathpiper_docs

