package org.mathpiper.lisp.stacks;


public class ProcedureCallInfo {
    public String functionName;
    public int stackPointer;
    
    public ProcedureCallInfo(String functionName, int stackPointer)
    {
        this.functionName = functionName;
        this.stackPointer = stackPointer;
    }
}
