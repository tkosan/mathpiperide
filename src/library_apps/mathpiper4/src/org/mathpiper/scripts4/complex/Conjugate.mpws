%mathpiper,def="Conjugate"

LocalSymbols(a,x)
{
Procedure("Conjugate",["a"])
  SubstituteApply(a,[[x],Type(x)=?"Complex"],[[x],Complex(x[1],-(x[2]))]);
} // LocalSymbols(a,x)

%/mathpiper



%mathpiper_docs,name="Conjugate",categories="Mathematics Procedures,Numbers (Complex)"
*CMD Conjugate --- complex conjugate
*STD
*CALL
        Conjugate(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure returns the complex conjugate of "x". The complex
conjugate of $a + I*b$ is $a - I*b$. This procedure assumes that all
unbound variables are real.

*E.G.

In> Conjugate(2)
Result: 2;

In> Conjugate(Complex(3,5))
Result: Complex(3,-5);

*SEE Complex, Re, Im
%/mathpiper_docs





%mathpiper,name="Conjugate",subtype="automatic_test"

// the following broke evaluation (dr)
Verify(Conjugate([_a]),[_a]);

%/mathpiper