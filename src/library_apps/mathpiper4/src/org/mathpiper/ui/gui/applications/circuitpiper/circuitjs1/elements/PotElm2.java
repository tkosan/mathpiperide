package org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements;

import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.Point;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.StringTokenizer;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.NTerminalComponent;

public class PotElm2 extends PotElm
{
    public PotElm2(int xx, int yy) {
        super(xx, yy);
    }
    
    public PotElm2(int xa, int ya, int xb, int yb, int f, StringTokenizer st) {
        super(xa, ya, xb, yb, f, st);
    }
    
    public void setPoints()
    {
        super.setPoints();
        
        NTerminalComponent nTerminalComponent = (NTerminalComponent) this.component;
        
        point1 = new Point(x2, y2);

        point2 = new Point(x1, y1);

        post3 = nTerminalComponent.calculatedPointsToGridPoints()[0];
    }
    
    public String getDisplayLabel()
    {
        return getShortUnitText(this.maxResistance, "\u03a9");
    }
}
