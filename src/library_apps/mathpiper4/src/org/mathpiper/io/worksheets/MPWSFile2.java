/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
package org.mathpiper.io.worksheets;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.mathpiper.builtin.library.base64.Base64;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.Interpreters;
import org.mathpiper.io.StringInputStream;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.parsers.MathPiperParser;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;


public class MPWSFile2 {
    
    enum HintType 
    { 
        ALL, NAMES_ONLY, NONE; 
    } 
    
    private File mpwsFile;
    private List<Fold> topLevelFolds = new ArrayList<Fold>();
    private List<OutsideFold> outsideFolds = new ArrayList<OutsideFold>();
    InputStream inputStream;
    private String versionLabel;
    private static HintType hintType = HintType.NONE;
    private static boolean isPublish = false;
    
    private DefaultMutableTreeNode rootNode;
    private DefaultTreeModel m_model;
    private JTree jTree;
    private int currentLevel = 1;
    private int totalLevels = 4;
    private int numberOfProblems = 0;
    private int numberOfProblemsWithDescriptions = 0;
    
    public MPWSFile2() {

    }
        
    public MPWSFile2(InputStream inputStream) {
        
        this.inputStream = inputStream;

    }
    
    public MPWSFile2(File mpwsFile) throws Exception
    { 
        this(new FileInputStream(mpwsFile));
        
        this.mpwsFile = mpwsFile;
    }

    public List<Fold> getTopLevelFolds()
    {
        return topLevelFolds;
    }
    
    public Map<String, Fold> getFoldsMap(String sourceName) throws Throwable {
        return getFoldsMap(sourceName, null, null);
    }//end method.

    
    public Map<String, Fold> getFoldsMap(String sourceName, String type) throws Throwable {
        return getFoldsMap(sourceName, type, null);
    }//end method.        

    
    public Map<String, Fold> getFoldsMap(String sourceName, String foldType, String foldSubtype) throws Throwable {
        Map<String, Fold> namedFolds = new HashMap<String, Fold>();

        for (Fold fold : topLevelFolds) {
           
            Set<String> keys = fold.getAttributes().keySet();

            for (String key : keys) {

                if (key.equalsIgnoreCase("name")) {
                    
                    String foldName = fold.getAttributeValue(key);
                    
                    if(foldType == null && foldSubtype == null)
                    {
                        namedFolds.put(foldName, fold);
                    }
                    else if(foldType != null && foldSubtype == null)
                    {
                        String currentFoldType = fold.getType();
                        
                        if(currentFoldType != null && currentFoldType.equals(foldType))
                        {
                            namedFolds.put(foldName, fold);
                        }
                    }
                    else if(foldType != null && foldSubtype != null)
                    {
                        String currentFoldType = fold.getType();
                        
                        String currentFoldSubtype = fold.getAttributeValue("subtype");
                        
                        if(currentFoldType != null && currentFoldSubtype != null && currentFoldType.equals(foldType) && currentFoldSubtype.equals(foldSubtype))
                        {
                            namedFolds.put(foldName, fold);
                        }
                    }
                }
            }
        }

        return namedFolds;
    }
    
    private void createOutsideFold(int lineCounter, StringBuilder outsideFoldContents, GroupFold groupFold, List outsideFolds) throws Throwable
    {
        OutsideFold outsideFold = new OutsideFold((lineCounter - countLines(outsideFoldContents.toString())), lineCounter-1 , outsideFoldContents.toString());

        if(groupFold == null)
        {
            outsideFolds.add(outsideFold);
        }
        else
        {
            groupFold.addOutsideFold(outsideFold);
        }

        outsideFoldContents.delete(0, outsideFoldContents.length());
    }
    
    public void scanSourceFile(String sourceName) throws Throwable {
        scanSourceFile(sourceName, false);
    } 

   
    public void scanSourceFile(String sourceName, boolean isReverse) throws Throwable {

        //Uncomment for debugging.
        /*
         if (getFileName.equals("Factors.mpw")) {
         int xxx = 1;
         }//end if.*/
        

        StringBuilder foldContents = new StringBuilder();
        StringBuilder outsideFoldContents = new StringBuilder();
        String foldHeader = "";
        boolean isInFold = false;
        boolean isInOutputFold = false;
        int lineCounter = 0;
        int startLineNumber = -1;
        List foldNames = new ArrayList();
        GroupFold groupFold = null;


        DataInputStream in = new DataInputStream(inputStream);
        
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            
            String line;
            
            //Read File Line By Line
            while ((line = br.readLine()) != null)
            {

                //System.out.println(line);
                lineCounter++;
                
                if(lineCounter == 1 && line.startsWith("v."))
                {
                    this.versionLabel = line.trim();
                }

                if (!line.matches("^ */%/.*$") && !line.matches("^ *\\. */%/.*$") && line.contains("%/")) {
                    // Handle end of a fold.
                    
                    if(outsideFoldContents.length() != 0)
                    { 
                        createOutsideFold(lineCounter, outsideFoldContents, groupFold, outsideFolds);
                    }
                    
                    String tempLine = line;

                    if (tempLine.startsWith(".")) {
                        tempLine = line.substring(1);

                        if (tempLine.trim().startsWith("%/output")) {
                            isInOutputFold = false;
                        }
                    }

                    if (!isInOutputFold && tempLine.trim().startsWith("%/")) {
                        
                        if(! line.trim().startsWith("%/group"))
                        {
                            if (isInFold == false) {
                                throw new Exception("Opening fold tag missing in file " + sourceName + " on line " + lineCounter);
                            }

                            Fold fold = new Fold(startLineNumber, lineCounter, foldHeader, foldContents.toString());

                            String foldType = fold.getType();

                            String foldSubtype = fold.getAttributeValue("subtype");

                            String foldName = fold.getAttributeValue("name");

                            if(foldName != null)
                            {
                                if(foldNames.contains(foldType + ":" + foldName + ":" + foldSubtype))
                                {
                                    throw new Exception("Duplicate fold name:type:subtype " + foldName + ":" + foldType + ":" + foldSubtype + " in file: " + sourceName);
                                }

                                foldNames.add(foldType + ":" + foldName + ":" + foldSubtype);
                            }

                            foldContents.delete(0, foldContents.length());
                            
                            if(groupFold == null)
                            {
                                topLevelFolds.add(fold);
                            }
                            else
                            {
                                groupFold.addFold(fold);
                            }
                            
                            isInFold = false;
                        }
                        else if(line.trim().startsWith("%/group"))
                        {
                            groupFold.setEndLineNumber(lineCounter);
                            groupFold = null;
                        }
                    }

                } else if (!isInOutputFold && line.trim().startsWith("%") && line.trim().length() > 2 && line.trim().charAt(1) != ' ' ) {
                    // Handle start of a fold.
                    
                    if(outsideFoldContents.length() != 0)
                    { 
                        createOutsideFold(lineCounter, outsideFoldContents, groupFold, outsideFolds);
                    }
                    
                    if(line.trim().startsWith("%group"))
                    {
                        startLineNumber = lineCounter;
                        foldHeader = line.trim();
                        groupFold = new GroupFold(startLineNumber, -1, foldHeader);
                        topLevelFolds.add(groupFold);
                    }
                    else
                    {
                        if (isInFold == true) {
                            throw new Exception("Closing fold tag missing in file " + sourceName + " on line " + lineCounter);
                        }

                        if (line.trim().startsWith("%output")) {
                            isInOutputFold = true;
                        }

                        startLineNumber = lineCounter;
                        foldHeader = line.trim();
                        isInFold = true;
                    }
                } else if (isInFold == true) {
                    foldContents.append(line);
                    foldContents.append("\n");
                }
                else
                {
                    outsideFoldContents.append(line);
                    outsideFoldContents.append("\n");
                }
            }
            
            if(outsideFoldContents.length() != 0)
            { 
                createOutsideFold(lineCounter+1, outsideFoldContents, groupFold, outsideFolds);
            }

            if (isInFold == true) {
                throw new Exception("Opening or closing fold tag missing in file " + sourceName + " on line " + lineCounter);
            }

            if(isReverse) Collections.reverse(topLevelFolds);
        }
        finally
        {
            in.close();
        }
    }
    
    public List<GroupFold> getGroupFolds()
    {
        List<GroupFold> groupFolds = new ArrayList<GroupFold>();
        
        Iterator<Fold> foldsIterator = topLevelFolds.iterator();
        
        while(foldsIterator.hasNext())
        {
            Fold fold = foldsIterator.next();
            
            if(fold.type.equalsIgnoreCase("group"))
            {
                groupFolds.add((GroupFold)fold);
            }
                
        }
        
        return groupFolds;
    }
    
    
    public void processFoldsPublish() throws Throwable
    {
        //Interpreter interpreter = Interpreters.getSynchronousInterpreter();
        
        List<GroupFold> groupFolds = this.getGroupFolds();
        
        for(GroupFold groupFold : groupFolds)
        {
            List<Fold> contentFolds = groupFold.getFolds();
            
            Fold hintFold = null;
            
            for(Fold fold : contentFolds)
            {
                if(fold.getAttributeValue("subtype") != null && fold.getAttributeValue("subtype").equals("hint"))
                {
                    // Process hint folds that are in the original file.
                    
                    hintFold = fold;
                }
                
                if(fold.getAttributeValue("subtype") != null && fold.getAttributeValue("subtype").equals("problem"))
                {   
                    if(hintFold != null)
                    {
                        String foldContents = fold.getContents();
                        String base64 = Base64.encodeToString(foldContents.getBytes(), false);
                        hintFold.setContents("\nHint(\"" + base64 + "\");\n\n");
                        hintFold = null;
                    }
                                        
                    if(fold.getAttributeValue("formula") != null && fold.getAttributeValue("formula").equals("true"))
                    {
                        fold.originalContents = fold.contents;
                        
                        String foldContents = fold.getContents();

                        foldContents = foldContents.replaceAll("Given\\(\\)\\s+\\{[\\s|\\u0000-\\u007C|\\u007E-\\uFFFF]+\\}", "Given()\n    {\n\n    }");
                        foldContents = foldContents.replaceAll("Lookup\\(\\)\\s+\\{\\s+}", "");
                        foldContents = foldContents.replaceAll("Lookup\\(\\)\\s+\\{[\\s|\\u0000-\\u007C|\\u007E-\\uFFFF]+\\}", "Lookup()\n    {\n\n    }");
                        // foldContents = foldContents.replaceAll("Formulas\\(\\)\\s+\\{\\s+}", "");
                        foldContents = foldContents.replaceAll("Formulas\\(\\)\\s+\\{[\\s|\\u0000-\\u007C|\\u007E-\\uFFFF]+\\}", "Formulas()\n    {\n\n    }");
                        foldContents = foldContents.replaceAll("(?m)^([ \\t]*\\r?\\n){2,}", "\n");
                        foldContents = foldContents.replaceAll("(?<=\\})\\s+(?=\\})", "\n");

                        foldContents = foldContents.replaceAll("Problem\\(ID:[a-zA-Z0-9\\.\\\"]+\\)", "Problem()"); //todo:tk:this code needs to be generalized.

                        //EvaluationResponse response = interpreter.evaluate(
                       //"{code := PipeFromString(\"" + foldContents + "\") ParseMathPiper(); UnparseMathPiper(code /: [Given() body_ -> '(Given()\n{\n\n}\n\n), Formulas() body_ -> '(Formulas()\n{\n\n}\n\n), Evaluations() body_ -> '(Evaluations()\n{\n\n}\n\n)]);}");

                        //if (response.isExceptionThrown()) {
                        //    throw response.getException();  
                        //}

                        //fold.setContents("\n" + response.getResult().substring(1, response.getResult().length()-1) + "\n\n");
                        
                        //foldContents = foldContents.replace("\\","\\\\");
                        //foldContents = foldContents.replace("\"","\\\"");
                        
                        fold.setContents(foldContents);
                    }
                    else
                    {
                        //fold.setContents("\n// Enter your program into this fold.\n\n");

                        throw new Exception("todo:tk:Attempting to process a fold that does not contain a \"formula\" attribute or which has a false \"formula\" attribute. " + fold.getType() + "" + fold.getAttributesString());
                    }
                }
                
                if(fold.type.equals("mathpiper_grade"))
                {
                    String foldContents = fold.getContents();
                    foldContents = Base64.encodeToString(foldContents.getBytes(), false);
                    fold.setContents("\n" + foldContents + "\n\n");
                    
                    fold.addAttribute("base_six_four", "true", fold.getNumberOfAttributes());
                }
            }
        }
    }
    
    
    public void processFoldsUpdateCircuitsFormulas() throws Throwable
    {   
        List<GroupFold> groupFolds = this.getGroupFolds();
        
        //int id = 50;
        
        for(GroupFold groupFold : groupFolds)
        {
            // Don't replace Description and Schematic procedures in etffa groups.
            Attribute etffaAttribute1 = groupFold.getAttributes().get("etffa");
            Attribute etffaAttribute2 = groupFold.getAttributes().get("etffa_name");
            String isEtffa = "False";
            if(etffaAttribute1 != null || etffaAttribute2 != null)
            {
                isEtffa = "True";
            }

            Attribute nameAttribute = groupFold.getAttributes().get("name");

            if(nameAttribute.getValue().startsWith("Problem "))
            {
                nameAttribute.setName("old_name");
                groupFold.getAttributes().put("old_name", nameAttribute);
                for(Attribute attribute : groupFold.getAttributes().values())
                {
                    attribute.setIndex(attribute.getIndex() + 1);
                }

                //Attribute newNameAttribute = new Attribute("name", String.format("z%04d", id), 0);
                Attribute newNameAttribute = new Attribute("name", "", 0);

                //id += 50;

                groupFold.getAttributes().put("name", newNameAttribute);
            }
    
            List<Fold> contentFolds = groupFold.getFolds();
            
            for(Fold fold : contentFolds)
            {
                if(fold.getAttributeValue("subtype") != null && fold.getAttributeValue("subtype").equals("problem"))
                {     
                    if(fold.getAttributeValue("formula") != null && fold.getAttributeValue("formula").equals("true"))
                    {
                        System.out.println(fold.getAttributeValue("name"));
                        
                        fold.originalContents = fold.contents;
                        
                        String foldContents = fold.getContents();
                        
                        foldContents = foldContents.replace("\\","\\\\");
                        foldContents = foldContents.replace("\"","\\\"");

                        Interpreter interpreter = Interpreters.getSynchronousInterpreter();
                        Interpreters.addOptionalFunctions(interpreter.getEnvironment(), "org/mathpiper/builtin/procedures/optional/");
                        EvaluationResponse response = interpreter.evaluate(
                       "{LoadScriptFile(\"/home/tkosan/git/mathpiper/experimental/automatic_grading/etem1111/formulabase/formulabase_electronics.mpws\"); code := PipeFromString(\"" + foldContents + "\") ParseMathPiper(); code := updateCircuitProblemFormulas(code, " + isEtffa + ");}"); //

                        if (response.isExceptionThrown()) {
                            System.out.println(response.getSideEffects() + " In Fold: " + fold.getAttributeValue("name"));
                            throw response.getException();  
                        }
                        
                        //System.out.println(response.getResult());

                        fold.setContents("\n" + response.getResult().substring(1, response.getResult().length()-1) + "\n\n");
                    }
                    else
                    {
                        throw new Exception("todo:tk:Attempting to process a fold that does not contain a \"formula\" attribute or which has a false \"formula\" attribute.");
                    }
                }
            }
        }
    }

    
    public static String printContent(Iterator<Fold> foldsIterator, Iterator<OutsideFold> outsideFoldsIterator)
    {
        StringBuilder sb = new StringBuilder();

        Fold fold;
        if(foldsIterator.hasNext())
        {
            fold = foldsIterator.next();
        }
        else
        {
            fold = null;
        }
        
        OutsideFold outsideFold;
        if(outsideFoldsIterator.hasNext())
        {
            outsideFold = outsideFoldsIterator.next();
        }
        else
        {
            outsideFold = null;
        }

        while(true)
        {   
            if(fold != null)
            {
                fold.setHintType(hintType);
            }
            
            if(isPublish && fold != null && (fold.type.equals("todo") || fold.type.equals("output") || fold.type.equals("text")))
            {
                fold.setIsPrint(false);
            }
            
            if(fold == null && outsideFold == null)
            {
                break;
            }

            if(fold != null && outsideFold == null)
            {
                sb.append(fold.toString());
                if(foldsIterator.hasNext())
                {
                    fold = foldsIterator.next();
                }
                else
                {
                    fold = null;
                }
                continue;
            }

            if(fold == null && outsideFold != null)
            {
                sb.append(outsideFold.toString());
                if(outsideFoldsIterator.hasNext())
                {
                    outsideFold = outsideFoldsIterator.next();
                }
                else
                {
                    outsideFold = null;
                }
                
                continue;
            }

            if(fold.getStartLineNumber() < outsideFold.getStartLineNumber())
            {
                sb.append(fold.toString());

                if(foldsIterator.hasNext())
                {
                    fold = foldsIterator.next();
                }
                else
                {
                    fold = null;
                }
            }
            else
            {
                sb.append(outsideFold.toString());

                if(outsideFoldsIterator.hasNext())
                {
                    outsideFold = outsideFoldsIterator.next();
                }
                else
                {
                    outsideFold = null;
                }
            }
        }
        
        return sb.toString();
    }
    
    
    public void publish(String outputFilename, boolean isAddFilename) throws Throwable
    {
        PrintWriter out = new PrintWriter(outputFilename);
        
        if(isAddFilename)
        {
            out.println(this.mpwsFile.getName());
            out.println();
        }
        
        /*
        if(outsideFolds.size() != 0)
        {
            OutsideFold lastOutsideFold = this.outsideFolds.get(outsideFolds.size()-1);

            if(lastOutsideFold != null && lastOutsideFold.toString().trim().equals(""))
            {
                outsideFolds.remove(lastOutsideFold);
            }
        }
        */
        
        String outputString = this.toString();
        
        out.print(outputString);
        
        if(! outputString.endsWith("\n\n"))
        {
            // At least two blank lines should be at the bottom of a
            // worksheet to provide a place where the output from grading
            // a the whole worksheet can be placed.
            if(outputString.endsWith("\n"))
            {
                out.print("\n");
            }
            else
            {
                out.print("\n\n");
            }
        }

        out.close();
        
        System.out.println("    " + outputFilename);
    }

    public String getVersionLabel() {
        return versionLabel;
    }
    
    public void publishFilter(String[] onlyPublishArray, String[] onlyAttributesArray) throws Exception
    {
        int problemIndex = 1;
        
        Fold lastOnlyPublishTopLevelFold = null;
        
        for(Fold fold : topLevelFolds)
        {
            if(fold.getType().equals("todo"))
            {
                continue;
            }
                                    
            if(onlyPublishArray != null && ! Arrays.asList(onlyPublishArray).contains(fold.getAttributeValue("name")))
            {
                fold.setIsPrint(false);
                
                for(Content outsideFolds : this.outsideFolds)
                {
                    if(outsideFolds.startLineNumber == fold.endLineNumber + 1)
                    {
                        outsideFolds.setIsPrint(false);
                    }
                }
            }
            else
            {
                lastOnlyPublishTopLevelFold = fold;
                
                fold.setOnlyAttributesArray(onlyAttributesArray);
                fold.changeAttribute("name", "Problem " + problemIndex);
                
                if(fold instanceof GroupFold)
                {
                    GroupFold groupFold = (GroupFold) fold;
                    
                    for(Fold innerFold : groupFold.getFolds())
                    {
                        if(innerFold.getType().equals("todo"))
                        {
                            continue;
                        }
                                    
                        innerFold.setOnlyAttributesArray(onlyAttributesArray);
                        innerFold.changeAttribute("name", "Problem " + problemIndex);
                    }
                }
                
                problemIndex++;
            }
        }
        
        for(Content outsideFolds : this.outsideFolds)
        {
            if(lastOnlyPublishTopLevelFold != null && outsideFolds.startLineNumber == lastOnlyPublishTopLevelFold.endLineNumber + 1)
            {
                outsideFolds.setIsPrint(false);
            }
        }
    }
    
    public void addTopLevelFold(Fold fold)
    {
        topLevelFolds.add(fold);
    }
    
    public void addOutsideFold(OutsideFold outsideFold)
    {
        outsideFolds.add(outsideFold);
    }


    public String toString()
    {
        return printContent(topLevelFolds.iterator(), outsideFolds.iterator());
    }
    
    
    
    // ================ Static methods
    
    private static int countLines(String str)
    {
        int lineCounter = 0;
        int charIndex = 0;
        
        while(charIndex < str.length())
        {
            if(str.charAt(charIndex) == '\n')
            {
                lineCounter++;
            }
            
            charIndex++;
        }
        
        return  lineCounter;
    }
    
    public static boolean isPublish()
    {
        return isPublish;
    }
    
    public static void appendToFile(String path, String data) throws Exception
    {
        java.nio.file.Files.write(
        java.nio.file.Paths.get(path), 
        data.getBytes(),
        java.nio.file.StandardOpenOption.CREATE,
        java.nio.file.StandardOpenOption.APPEND);
    }
    
    public static void publishWorksheet(String filename, boolean isAddFilename)
    {
        publishWorksheet(filename, null, isAddFilename, null, null);
    }
    
    public static void publishWorksheet(String filename, HintType hintType, boolean isAddFilename)
    {
        publishWorksheet(filename, hintType, isAddFilename, null, null);
    }
    
    public static void publishWorksheet(String filename, HintType hintType, boolean isAddFilename, String[] onlyPublishArray, String[] onlyAttributesArray)
    {
        String path = filename.substring(0, filename.lastIndexOf("/"));
        
        String file = filename.substring(filename.lastIndexOf("/") + 1, filename.length()-5); // Filename without .mpws extension.
        
        File mpwsFile = new File(filename);

        try {
            
            File outputFileDirectory  = new File(path + "/dist");
            
            if(!outputFileDirectory.exists())
            {
                outputFileDirectory.mkdirs();
            }
            
            
            MPWSFile2 mpwsFile2 = new MPWSFile2(mpwsFile);
            
            MPWSFile2.hintType = hintType;
            
            MPWSFile2.isPublish = true;
            
            mpwsFile2.scanSourceFile(mpwsFile.getPath());
            
            mpwsFile2.publishFilter(onlyPublishArray, onlyAttributesArray);
            
            mpwsFile2.processFoldsPublish();
            
            String outputDirectoryPath = path + "/dist/" + file + "_firstname_lastname_idnumber_" + mpwsFile2.versionLabel + ".lg.mpws";
            
            mpwsFile2.publish(outputDirectoryPath, false);
            
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    
    public static void updateCircuitsFormulas(String filename)
    {
        String path = filename.substring(0, filename.lastIndexOf("/"));
        
        String file = filename.substring(filename.lastIndexOf("/") + 1, filename.length()-5); // Filename without .mpws extension.
        
        File mpwsFile = new File(filename);

        try {
            
            File outputFileDirectory  = new File(path + "/dist");
            
            if(!outputFileDirectory.exists())
            {
                outputFileDirectory.mkdirs();
            }
            
            
            MPWSFile2 mpwsFile2 = new MPWSFile2(mpwsFile);
            
            MPWSFile2.isPublish = false;
            
            mpwsFile2.scanSourceFile(mpwsFile.getPath());
            
            mpwsFile2.processFoldsUpdateCircuitsFormulas();
            
            String outputDirectoryPath = path + "/dist/" + file + ".converted.mpws";
            
            mpwsFile2.publish(outputDirectoryPath, false);
            
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    
    public static MPWSFile2 loadProblembase(String directoryName) throws Throwable
    {
        List<Path> paths = new ArrayList();
                
        Files.find(Paths.get(directoryName), 9999, (p, bfa) -> bfa.isRegularFile()
                && p.getFileName().toString().matches(".*_problems\\.mpws")).forEach(paths::add);
        
        List<Path> removeDist = new ArrayList();
        
        for(Path path : paths)
        {
            if(path.toString().contains("/dist"))
            {
                removeDist.add(path);
            }
        }
        
        if(paths.removeAll(removeDist));
        
        Collections.sort(paths, new Comparator<Path>() {
            public int compare(Path a, Path b) {
                return Utility.compareNatural(a.getFileName().toString(), b.getFileName().toString());
            }
        });
        
        MPWSFile2 problemsbase = new MPWSFile2();
        
        for(Path path : paths)
        {
            //System.out.println(path.getFileName());
            
            File mpwsFile = path.toFile();
            
            MPWSFile2 mpwsFile2 = new MPWSFile2(mpwsFile);
            
            try
            {
            mpwsFile2.scanSourceFile(path.toString());
            
                for(GroupFold groupFold : mpwsFile2.getGroupFolds())
                {
                    groupFold.setInFile(path.toString());
                    problemsbase.addTopLevelFold(groupFold);
                }      
            }
            catch (Throwable t)
            {
                System.err.println(path.toString());
                throw t;
            }
        }
        
        return problemsbase;
    }
    
    
    public Map getProblembase(String directoryName) throws Throwable
    {
        Map problembaseMap = new HashMap();
        
        Interpreter interpreter = Interpreters.getSynchronousInterpreter();
        
        MPWSFile2 problembase = loadProblembase(directoryName);
        
        int index = 1;
        
        for(GroupFold groupFold : problembase.getGroupFolds())
        {
            List<Fold> problemFolds = groupFold.getFoldsByAttribute("subtype", "problem");
            
            if(problemFolds.size() != 1)
            {
                throw new Exception("The group fold named " + groupFold.getAttributeValue("name") + " has more than one problem fold.");
            }
            
            Fold problemFold = problemFolds.get(0);
            
            String foldContents = problemFold.getContents();
            
            Environment environment = interpreter.getEnvironment();
            StringInputStream newInput = new StringInputStream(foldContents, environment.iInputStatus);
            MathPiperParser parser = new MathPiperParser(new MathPiperTokenizer(), newInput, environment, environment.iPrefixOperators, environment.iInfixOperators, environment.iPostfixOperators, environment.iBodiedProcedures);

            Cons pattern = parser.parse(-1);
        
            Map<String, Object> attributesMap = new HashMap();
            
            for(Attribute attribute : groupFold.attributes.values())
            {
                attributesMap.put("\"" + attribute.getName() + "\"", "\"" + attribute.getValue() + "\"");
            }
            
            attributesMap.put("\"code\"", pattern);
            
            String uniqueName = groupFold.getAttributeValue("name");
            if(uniqueName.startsWith("P"))
            {
                uniqueName += ("-" + index++);
            }
            
            problembaseMap.put("\"" + uniqueName + "\"", attributesMap);
        }       
        
        return problembaseMap;
    }
    
    
    public static MPWSFile2 getProblemsByAttribute(MPWSFile2 problemsbase, String attributeName, String attributeValue) throws Throwable
    {
        // Can be used to return a new worksheet that contains problems of a given type.
        
        MPWSFile2 filteredProblems = new MPWSFile2();
        
        int lineNumber = 1;
        
        // Pattern schematicPattern = Pattern.compile("Schematic\\(\\)([\\S\\s]*?)}\\s");
        
        for(Fold groupFold : problemsbase.getGroupFolds())
        {
            if(groupFold.getAttributeValue(attributeName) != null && groupFold.getAttributeValue(attributeName).equals(attributeValue))
            {
                int groupFoldLength = groupFold.getEndLineNumber() - groupFold.getStartLineNumber();
                
                groupFold.setStartLineNumber(lineNumber);
                lineNumber += groupFoldLength;
                
                /*
                
                StringBuilder sb = new StringBuilder(fold.toString());
            
                Matcher schematicMatcher = schematicPattern.matcher(sb);

                if(schematicMatcher.find() == true)
                {
                    System.out.println(schematicMatcher.start());
                    System.out.println(schematicMatcher.end() );
                    System.out.println(sb.substring(schematicMatcher.start(), schematicMatcher.end()));
                    System.out.println();
                    sb.delete(schematicMatcher.start(), schematicMatcher.end());
                    
                    fold.setContents(sb.toString());
                }
                
                
                lineNumber += countLines(sb.toString());
                fold.setEndLineNumber(lineNumber);
                
            
                lineNumber++;
                */
                

                groupFold.setEndLineNumber(lineNumber);
                
                lineNumber++;                
                
                filteredProblems.addTopLevelFold(groupFold);
                 
                OutsideFold outsideFold = new OutsideFold(-1, -1, "\n\n\n\n\n\n\n");
                
                outsideFold.setStartLineNumber(lineNumber);
                lineNumber += countLines(groupFold.toString());
                outsideFold.setEndLineNumber(lineNumber);
                
                lineNumber++;
                 
                filteredProblems.addOutsideFold(outsideFold);
            }
        }
        
        return filteredProblems;
    }
    
    
    public static MPWSFile2 addGivenComment(MPWSFile2 inFile) throws Throwable
    {
        Interpreter interpreter = Interpreters.getSynchronousInterpreter();
        Interpreters.addOptionalFunctions(interpreter.getEnvironment(), "org/mathpiper/builtin/procedures/optional/");
        
        for(GroupFold groupFold : inFile.getGroupFolds())
        {
            for(Fold fold : groupFold.getFolds())
            {
                String foldContents = fold.getContents();

                if(foldContents != null)
                {
                    foldContents = foldContents.replace("\"","\\\"");
                        
                    EvaluationResponse response = interpreter.evaluate(
                    "{code := PipeFromString(\"" + foldContents + "\") ParseMathPiper(); positions := PositionsPattern(code, 'Given() body_); If(positions !=? []) VarList(PositionGet(code, First(positions)));}");

                    if (response.isExceptionThrown()) {
                        throw new Exception("The group fold named " + groupFold.getAttributeValue("name") + " has the following exception: " + response.getException());  
                    }

                    String given = response.getResult();

                    String newFoldContents = fold.getContents().replace("Given()", "// Use variables " + given + ".\n    Given()");
                    
                    fold.setContents(newFoldContents);
                }
            }
        
        }
        
        return inFile;
    }
    
    
    private void createJTree(int level, DefaultMutableTreeNode parentTreeNode) throws Exception
    {  
        numberOfProblems = 0;
        numberOfProblemsWithDescriptions = 0;
        
        char categoryLetter = 'a';
        categoryLetter--;
        DefaultMutableTreeNode categoryNode = null;
        DefaultMutableTreeNode uncategorizedNodes = new DefaultMutableTreeNode();
        uncategorizedNodes.setUserObject("Uncategorized");
        
        List<GroupFold> groupFolds = getGroupFolds();
        
        Collections.sort(groupFolds, new Comparator<GroupFold>() {
            public int compare(GroupFold a, GroupFold b) {
                return Utility.compareNatural(a.getAttributeValue("name"), b.getAttributeValue("name"));
            }
        });
        
        String tag = "";
        
        for(GroupFold groupFold: groupFolds)
        {
            numberOfProblems++;
            
            tag = "";

            String groupFoldName = groupFold.getAttributeValue("name");
            
            if(! groupFoldName.substring(0, 1).matches("[a-z]"))
            {
                DefaultMutableTreeNode uncategorizedNode = new DefaultMutableTreeNode();
                uncategorizedNode.setUserObject(groupFoldName + ": " + groupFold.getInFile().substring(groupFold.getInFile().lastIndexOf("/") + 1));
                uncategorizedNodes.add(uncategorizedNode);
                continue;
            }
            
            if(groupFoldName.charAt(0) != categoryLetter)
            {
                categoryLetter++;
                categoryNode = new DefaultMutableTreeNode();
                categoryNode.setUserObject(categoryLetter + ": " + groupFold.getInFile().substring(groupFold.getInFile().lastIndexOf("/") + 1));
                
                DefaultMutableTreeNode pathInfoNode = new DefaultMutableTreeNode();
                pathInfoNode.setUserObject("Path");
                DefaultMutableTreeNode pathNode = new DefaultMutableTreeNode();
                pathNode.setUserObject(groupFold.getInFile());
                pathInfoNode.add(pathNode);
                categoryNode.add(pathInfoNode);
                
                parentTreeNode.add(categoryNode);
            }
            
            DefaultMutableTreeNode childTreeNode = new DefaultMutableTreeNode();
            childTreeNode.setUserObject(groupFoldName);
            categoryNode.add(childTreeNode);
                       
            Pattern descriptionPattern = Pattern.compile("(?<=Description\\(\\)\\s+\\{)[\\s|\\u0000-\\u007C|\\u007E-\\uFFFF]+(?=\\})");
            
            String groupFoldString = groupFold.toString();
            groupFoldString = groupFoldString.trim();
            groupFoldString = groupFoldString.replace("\"", "");
            groupFoldString = groupFoldString.replace(";", "");
            groupFoldString = groupFoldString.replaceAll("\\s+", " ");
            
            StringBuilder sb = new StringBuilder(groupFoldString);
            
            Matcher descriptionMatcher = descriptionPattern.matcher(sb);

            if(descriptionMatcher.find() == true)
            {
                String descriptionContents = sb.substring(descriptionMatcher.start(), descriptionMatcher.end());
                
                if(! descriptionContents.trim().equals(""))
                {
                    numberOfProblemsWithDescriptions++;
                }
                
                DefaultMutableTreeNode descriptionNode = new DefaultMutableTreeNode();
                
                if(descriptionContents.trim().equals(""))
                {
                    tag += "D";
                }
                
                descriptionNode.setUserObject(descriptionContents.replace("\n", " "));
                
                childTreeNode.add(descriptionNode);
            }
            
            if(groupFold.getAttributes().get("ide") == null )
            {
                tag += "P";
            }
            
            childTreeNode.setUserObject(childTreeNode.getUserObject() + " " + tag);
        }
        
        parentTreeNode.add(uncategorizedNodes);
    }
    
    
    public void setTreeExpandedState(JTree tree, boolean expanded)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getModel().getRoot();
        setNodeExpandedState(tree, node, expanded);
    }

    
    public void setNodeExpandedState(JTree tree, DefaultMutableTreeNode node, boolean expanded)
    {
        ArrayList list = Collections.list(node.children());

        for (Object node2 : list)
        {
            setNodeExpandedState(tree, (DefaultMutableTreeNode) node2, expanded);
        }
        
        if (!expanded && node.isRoot())
        {
            return;
        }
        
        TreePath path = new TreePath(node.getPath());
        
        if (expanded)
        {
            tree.expandPath(path);
        }
        else
        {
            tree.collapsePath(path);
        }
    }
    
    
    public void showTree() throws Throwable
    {
        rootNode = new DefaultMutableTreeNode("Problems");
        m_model = new DefaultTreeModel(rootNode);

        createJTree(0, rootNode);

        jTree = new JTree(rootNode);
        jTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        
        MouseListener ml = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int selRow = jTree.getRowForLocation(e.getX(), e.getY());
                TreePath node = jTree.getPathForLocation(e.getX(), e.getY());

                //<snip>


            }
        };
        jTree.addMouseListener(ml);

        JFrame frame = new JFrame();

        frame.setTitle("Titles");

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel treePanel = new JPanel();

        treePanel.setLayout(new BorderLayout());

        treePanel.add(jTree);

        JScrollPane scrollPane = new JScrollPane(treePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        
        

        frame.add(scrollPane);
        
        JPanel infoPanel = new JPanel();
        JTextField textField = new JTextField();
        String infoString = "Descriptions: " + this.numberOfProblemsWithDescriptions+ ", Problems: " + this.numberOfProblems + ", Complete: " + String.format("%.2f%%", ((double)numberOfProblemsWithDescriptions/(double)numberOfProblems) * 100.0); 
        textField.setText(infoString);
        infoPanel.add(textField);
        frame.add(infoPanel, BorderLayout.SOUTH);
        
        
        JPanel buttonsPanel = new JPanel();
        
        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(ae ->
        {
            jTree.clearSelection();
            
            currentLevel = 1;
            setTreeExpandedState(jTree, false);

            String searchText = JOptionPane.showInputDialog(null, "Enter text", "Search", JOptionPane.QUESTION_MESSAGE);

            Enumeration e = rootNode.breadthFirstEnumeration();

            List<TreePath> treePaths = new ArrayList();

            while (e.hasMoreElements())
            {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();

                if (node.getUserObject().toString().toLowerCase().contains(searchText.toLowerCase()))
                {
                    TreeNode[] nodes = m_model.getPathToRoot(node);
                    TreePath path2 = new TreePath(nodes);
                    treePaths.add(path2);
                    //jTree.scrollPathToVisible(path2);
                }
            }

            if (treePaths.size() > 0)
            {
                jTree.setSelectionPaths(treePaths.toArray(new TreePath[treePaths.size()]));
            }
            else
            {
                JOptionPane.showMessageDialog(null, "The text \"" + searchText + "\" was not found.");
            }
        });
        buttonsPanel.add(searchButton);
        
        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(ae ->
        {
            jTree.clearSelection();
            
            currentLevel = 1;
            setTreeExpandedState(jTree, false);
        });
        buttonsPanel.add(resetButton);

        JButton clearButton = new JButton("Clear Selection");
        clearButton.addActionListener(ae ->
        {
            jTree.clearSelection();
        });
        buttonsPanel.add(clearButton);
        
        JButton collapseBtn = new JButton("Collapse All");
        collapseBtn.addActionListener(ae ->
        {
            currentLevel = 1;
            setTreeExpandedState(jTree, false);
        });
        buttonsPanel.add(collapseBtn);
        
        JButton expandBtn = new JButton("Expand All");
        expandBtn.addActionListener(ae ->
        {
            currentLevel = totalLevels;
            setTreeExpandedState(jTree, true);
        });
        buttonsPanel.add(expandBtn);
        
        JButton decreaseLevelButton = new JButton("Level -");
        decreaseLevelButton.addActionListener(ae ->
        {
            if(currentLevel != 1)
            {
                currentLevel--;
                setLevel(currentLevel);
            }
        });
        buttonsPanel.add(decreaseLevelButton);
        
        JButton increaseLevelButton = new JButton("Level +");
        increaseLevelButton.addActionListener(ae ->
        {
            if(currentLevel < totalLevels)
            {
                currentLevel++;
                setLevel(currentLevel);
            }
        });
        buttonsPanel.add(increaseLevelButton);
        
        frame.add(buttonsPanel, BorderLayout.NORTH);
        frame.pack();
        frame.setSize(800, 800);
        frame.setLocationByPlatform(true);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    
    
    private void setLevel(int level)
    {
        DefaultMutableTreeNode currentNode = rootNode.getNextNode();
        do
        {
            if (currentNode.getLevel() == level)
            {
                if(level > currentLevel)
                {
                    jTree.expandPath(new TreePath(currentNode.getPath()));
                }
                else
                {
                    jTree.collapsePath(new TreePath(currentNode.getPath()));
                }
            }
            currentNode = currentNode.getNextNode();
        }
        while (currentNode != null);
    }


    public static void main(String[] args) throws Throwable {

        // todo:tk:sort the problems by their new names.
        /*
        MPWSFile2 problembase = loadProblembase("/home/tkosan/git/mathpiper/experimental/automatic_grading/etem1111/chapters"); //Find all problems that are in a given chapter.
        MPWSFile2 chapterExamples = getProblemsByAttribute(problembase, "etffa", "ch6e");
        
        MPWSFile2 chapterExamplesWithGiven = addGivenComment(chapterExamples);
        chapterExamplesWithGiven.publish("/home/tkosan/git/mathpiper/experimental/automatic_grading/etem1111/chapters/dist/etffa_chapter_6_problems.mpws", false);
        // */

        
        
        // Update a worksheet.
        // todo:tk:create "old_name" attribite.
        //String filename = "/home/tkosan/git/mathpiper/experimental/automatic_grading/etem1111/chapters/chapter_26/chapter_26_odd_practice_problems.mpws";
        //updateCircuitsFormulas(filename)



        // Publish a synthesized worksheet.
        /*
        String filename = "/home/tkosan/git/mathpiper/experimental/automatic_grading/etem1111/chapters/dist/etffa_chapter_10_problems.mpws";
        publishWorksheet(filename, HintType.ALL, true, null, new String[] {"name", "subtype", "unassign_all", "formula", "base_six_four"});
        // */
        
        
        /*
        String filename = "/home/tkosan/atmp2/gradebook_colton/practice_a_evaluate_boolean_expressions_colton_wilkes_297762_v.14.lg.mpwslog";
        File file = new File(filename);
        
        MPWSFile2 mp2 = new MPWSFile2(file);
        
        mp2.scanSourceFile("TEST", true);
        */
        
        
        MPWSFile2 problembase = loadProblembase("/home/tkosan/git/mathpiper/experimental/automatic_grading/etem1111/chapters"); //Find all problems that are in a given chapter.
        problembase.showTree();
    }

}
