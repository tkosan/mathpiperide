/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

/**
 *
 *  
 */
public class ArcCscD extends BuiltinProcedure
{

    private ArcCscD()
    {
    }

    public ArcCscD(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        BigNumber x;
        
        x = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 1);

        double xDouble = x.toDouble();

        double result = Math.asin(1 / xDouble);

        if(Double.isNaN(result))
        {
            LispError.raiseError("The argument must have a value that is not between -1 and 1.", aStackTop, aEnvironment);
        }

        BigNumber z = new BigNumber(aEnvironment.getPrecision());

        z.setTo(result);

        setTopOfStack(aEnvironment, aStackTop, new org.mathpiper.lisp.cons.NumberCons(z));
    }
}//end class.




/*
%mathpiper_docs,name="ArcCscD",categories="Mathematics Procedures,Numeric,Trigonometry (Numeric)"
*CMD ArcCscD --- double-precision math function
*CORE
*CALL
	ArcCscD(x)

*PARMS
{x} -- a number

*DESC
A double precision version of the ArcCsc function.


*EXAMPLES
*SEE SinD, CosD, TanD, CscD, SecD, CotD, ArcSinD, ArcCosD, ArcTanD, ArcSecD, ArcCotD
%/mathpiper_docs





%mathpiper,name="ArcCscD",subtype="in_prompts"

ArcCscD(1) ~> 1.570796327

ArcCscD(-1.2) ~> -0.9851107833

%/mathpiper





%mathpiper,name="ArcCscD",subtype="automatic_test"

Verify(ArcCscD(1), 1.570796327);

Verify(ArcCscD(-1.2), -0.9851107833);

%/mathpiper
*/
