/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.io.StringOutputStream;
import org.mathpiper.lisp.Environment;
import org.mathpiper.io.MathPiperOutputStream;



public class FilterSideEffects extends BuiltinProcedure
{

    private FilterSideEffects()
    {
    }

    public FilterSideEffects(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        StringBuffer oper = new StringBuffer();
        StringOutputStream newOutput = new StringOutputStream(oper);
        MathPiperOutputStream previous = aEnvironment.iCurrentOutput;
        aEnvironment.iCurrentOutput = newOutput;
        
        try
        {
            // Evaluate the body
            setTopOfStack(aEnvironment, aStackTop, aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, getArgument(aEnvironment, aStackTop, 1)));
            
        } catch (Throwable e)
        {
            throw e;
        } finally
        {
            aEnvironment.iCurrentOutput = previous;
        }
    }
}



/*
%mathpiper_docs,name="FilterSideEffects",categories="Programming Procedures,Input/Output,Built In"
*CMD FilterSideEffects --- remove side effect output

*CALL
    FilterSideEffects() body

*PARMS

{body} -- expression to be evaluated

*DESC

The expressions in "body" are evaluated and any side effects
that are produced by these expressions is removed.

*EXAMPLES

%/mathpiper_docs





%mathpiper,name="FilterSideEffects",subtype="in_prompts"

FilterSideEffects() {Echo("Hello"); 3;} ~> 3

%/mathpiper
*/
