/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
// Copied from MathPiper (http://mathpiper.org)

package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.*;
import java.awt.RenderingHints.Key;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import javax.swing.JLabel;
import org.mathpiper.ui.gui.worksheets.DynamicIcon;
import org.scilab.forge.mp.jlatexmath.TeXConstants;
import org.scilab.forge.mp.jlatexmath.TeXFormula;
import org.scilab.forge.mp.jlatexmath.TeXIcon;



public class ScaledGraphics {

    private double viewScale = 1;
    public Graphics iG = null;
    private Graphics2D iG2D = null;
    int prevGray = -1;
    float prevSetFontSize = -1;
    FontMetrics metrics = null;

    public ScaledGraphics(Graphics g) {
        setGraphics(g);
    }
    
    public void setGraphics(Graphics g)
    {
        iG = g;
        
        if (g instanceof Graphics2D) {
            iG2D = (Graphics2D) g;
        }
    }

    public void setLineWidth(double aThickness) {
        if (iG2D != null) {
            iG2D.setStroke(new BasicStroke((float) aThickness, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND)); //BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        }
    }
    
    public double getLineWidth() {
        if (iG2D != null) {
            BasicStroke stroke = (BasicStroke) iG2D.getStroke();
            return stroke.getLineWidth();
        }
        
        return 1;
    }

    public void drawLine(double x0, double y0, double x1, double y1) {
        iG.drawLine((int) Math.round(x0 * viewScale), (int) Math.round(y0 * viewScale), (int) Math.round(x1 * viewScale), (int) Math.round(y1 * viewScale));
    }


    public void drawArc(double x,double y,double width,double height,int startAngle,int arcAngle) {
        //iG.drawLine((int) Math.round(x0 * viewScale), (int) Math.round(y0 * viewScale), (int) Math.round(x1 * viewScale), (int) Math.round(y1 * viewScale));
        iG.drawArc((int) Math.round(x * viewScale), (int) Math.round(y * viewScale), (int) Math.round(width * viewScale), (int) Math.round(height * viewScale), startAngle, arcAngle);
    }

    public void drawRect(double x, double y, double width, double height) {
        iG.drawRect((int) Math.round(x * viewScale), (int) Math.round(y * viewScale), (int) Math.round(width * viewScale), (int) Math.round(height * viewScale));
    }
    
    public void drawOval(double x, double y, double width, double height) {
        iG.drawOval((int) Math.round(x * viewScale), (int) Math.round(y * viewScale), (int) Math.round(width * viewScale), (int) Math.round(height * viewScale));
    }

    public void fillRect(double x, double y, double width, double height) {
        iG.fillRect((int) Math.round(x * viewScale), (int) Math.round(y * viewScale), (int) Math.round(width * viewScale), (int) Math.round(height * viewScale));
    }    
    
    public void fillOval(double x, double y, double width, double height) {
        iG.fillOval((int) Math.round(x * viewScale), (int) Math.round(y * viewScale), (int) Math.round(width * viewScale), (int) Math.round(height * viewScale));
    }
    
    public void fillArc(double x,double y,double width,double height,int startAngle,int arcAngle) {
        //iG.drawLine((int) Math.round(x0 * viewScale), (int) Math.round(y0 * viewScale), (int) Math.round(x1 * viewScale), (int) Math.round(y1 * viewScale));
        iG.fillArc((int) Math.round(x * viewScale), (int) Math.round(y * viewScale), (int) Math.round(width * viewScale), (int) Math.round(height * viewScale), startAngle, arcAngle);
    }
    
    public void fillPolygon(double[] xPoints, double[] yPoints, int numberOfPoints)
    {
        int[] scaledXPoints = new int[numberOfPoints];
        int[] scaledYPoints = new int[numberOfPoints];
        
        for(int index = 0; index < numberOfPoints; index++)
        {
            scaledXPoints[index] = (int) Math.round(xPoints[index] * viewScale);
            scaledYPoints[index] = (int) Math.round(yPoints[index] * viewScale);
        }
        
        iG.fillPolygon(scaledXPoints, scaledYPoints, numberOfPoints);
    }
    
    public void setClip(double x, double y, double width, double height) {
        iG.setClip((int) Math.round(x * viewScale), (int) Math.round(y * viewScale), (int) Math.round(width * viewScale), (int) Math.round(height * viewScale));
    }

    public void clip(double x, double y, double width, double height) {
        Rectangle2D r = new Rectangle();
        r.setRect((x * viewScale), (y * viewScale), (width * viewScale), (height * viewScale));
        iG2D.clip(r);
    }
    
    public void setGray(int aGray) {
        if (prevGray != aGray) {
            prevGray = aGray;
            iG.setColor(new Color(aGray, aGray, aGray));
        }
    }

    public void drawString(String text, double x, double y) {
        double normalFontSize = getFontSize();

        double scaledFontSize = normalFontSize;

        setFontSize(scaledFontSize);

        if(iG2D != null)
        {
            iG2D.drawString(text, (int) Math.round(x * viewScale), (int) Math.round(y * viewScale));
        }
        
        setFontSize(normalFontSize);
    }

    public void drawScaledString(String text, double x, double y, double scale) {
        double normalFontSize = getFontSize();

        double scaledFontSize = normalFontSize * scale;

        setFontSize(scaledFontSize);

        if(iG2D != null)
        {
            iG2D.drawString(text, (int) Math.round(x * viewScale), (int) Math.round(y * viewScale));
        }

        setFontSize(normalFontSize);
    }
    
    
    public void drawLatex(TeXFormula texFormula, double size, double x, double y)
    {
        drawLatex(texFormula,  size, x, y, false);
    }
    
    public void drawLatex(TeXFormula texFormula, double size, double x, double y, boolean isDebug)
    {
	//Obtain image example.
	//http://forge.scilab.org/index.php/p/jlatexmath/source/tree/8b1b0250b95fe80c0e245db2671a817f299ecaf7/examples/Basic/Example1.java

	TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * viewScale));

	JLabel jl = new JLabel();

	jl.setForeground(new Color(0, 0, 0));
	
	icon.paintIcon(jl, iG, (int)(x * viewScale), (int)(y * viewScale));
	
        if(isDebug)
        {
            Color oldColor = this.getColor();
            setColor(Color.RED);
            
            double oldLineWidth = this.getLineWidth();
            this.setLineWidth(1.0);
            
            iG.drawRect((int)(x * viewScale), (int)(y * viewScale), (int)(icon.getIconWidth()), (int)(icon.getIconHeight()));
            
            this.setColor(oldColor);
            this.setLineWidth(oldLineWidth);
        }

    }
    
    
    public void drawIcon(DynamicIcon icon, double x, double y) // todo:tk:this method needs more work.
    {
	JLabel jl = new JLabel();

	jl.setForeground(new Color(0, 0, 0));
	
	icon.paintIcon(jl, iG, (int)(x * viewScale), (int)(y * viewScale));
    }
    

    public void setFontSize(double aSize) {
        this.prevSetFontSize = (int) Math.round(viewScale * aSize);
        
                        
        Font font = new Font("Monospaced", Font.PLAIN, 12);
        
        iG.setFont(font.deriveFont(this.prevSetFontSize));
        
        if(iG.getFontMetrics() != null)
        {
            metrics = iG.getFontMetrics();
        }
        else
        {
            Canvas canvas = new Canvas();
            metrics = canvas.getFontMetrics(font);
        }
        
    }

    public double getFontSize() {
        return  (prevSetFontSize / viewScale);
    }

    public double getScaledTextWidth(String text) {
        java.awt.geom.Rectangle2D textBoundingRectangle = metrics.getStringBounds(text, iG);
        return  (textBoundingRectangle.getWidth() / viewScale);
    }

    public double getScaledTextHeight(String text) {
        java.awt.geom.Rectangle2D textBoundingRectangle = metrics.getStringBounds(text, iG);
        return  (textBoundingRectangle.getHeight() / viewScale);
    }

    public double getTextWidth(String text) {
        java.awt.geom.Rectangle2D textBoundingRectangle = metrics.getStringBounds(text, iG);
        return  textBoundingRectangle.getWidth();
    }

    public double getTextHeight(String text) {
        java.awt.geom.Rectangle2D textBoundingRectangle = metrics.getStringBounds(text, iG);
        return  textBoundingRectangle.getHeight();
    }

    public double getAscent() {
        return (metrics.getAscent() / viewScale);
    }

    public double getDescent() {
        return  (metrics.getDescent() / viewScale);
    }

    public void setViewScale(double aViewScale) {
        viewScale = aViewScale;
    }

    public void setColor(Color color) {
        if (iG != null) {
            iG.setColor(color);
        }

    }
    
    public Color getColor()
    {
        if(iG != null)
        {
            return iG.getColor();
        }
        else
        {
            return null;
        }
    }
    
    public static int fontForSize(int aSize) {

        if (aSize > 3) {
            aSize = 3;
        }

        if (aSize < 0) {
            aSize = 0;
        }

        switch (aSize) {

            case 0:
                return 6;

            case 1:
                return 8;

            case 2:
                return 12;

            case 3:
                return 16;

            default:
                return 16;
        }//end switch.
        
    }//end method.

    public void setRenderingHint(Key a, Object b)
    {
        if(iG2D != null)
        {
            this.iG2D.setRenderingHint(a, b);
        }
    }
    
    public void setRenderingHints(Map <?,?> hints)
    {
        if(iG2D != null)
        {
            iG2D.setRenderingHints(hints);
        }
    }
            
    public void setPaint(Color color)
    {
        if(iG2D != null)
        {
            this.iG2D.setPaint(color);
        }
    }

    public void fill(Shape shape)
    {   
        if(iG2D != null)
        {
            Rectangle rect = shape.getBounds();
        
            iG2D.fill(new Arc2D.Double(rect.getX() * viewScale, rect.getY() * viewScale, rect.getWidth() * viewScale, rect.getHeight() * viewScale, 0, 360, Arc2D.CHORD));
        }
    }
    
    public void addRenderingHints(Map <?, ?> map)
    {
        if(iG2D != null)
        {
            iG2D.addRenderingHints(map);
        }
    }
    
    public void setBackground(Color color)
    {
        if(iG2D != null)
        {
            iG2D.setBackground(color);
        }
    }
    
    public void draw(Shape shape)
    { 
        if(iG2D != null)
        {
            Rectangle rect = shape.getBounds();

            if(shape instanceof Arc2D.Double)
            {
                iG2D.draw(new Arc2D.Double(rect.getX() * viewScale, rect.getY() * viewScale, rect.getWidth() * viewScale, rect.getHeight() * viewScale, 0, 360, Arc2D.CHORD));
            }
            else if(shape instanceof Line2D.Double)
            {
                Line2D.Double line = (Line2D.Double) shape;
                
                iG2D.draw(new Line2D.Double(line.x1 * viewScale, line.y1 * viewScale, line.x2 * viewScale, line.y2 * viewScale));
            }
            else if(shape instanceof Path2D.Double)
            {
                AffineTransform transform = new AffineTransform();
                transform.scale(viewScale, viewScale);
                
                iG2D.draw(transform.createTransformedShape(shape));
            }
        }
    }
    
    public void setStroke(Stroke stroke)
    {
        if(iG2D != null)
        {
            iG2D.setStroke(stroke);
        }
    }
    
    public Stroke getStroke()
    {
        if(iG2D != null)
        {
            return iG2D.getStroke();
        }
        
        return null;
    }
    
    public void rotate(double angle)
    {
        if(iG2D != null)
        {
            iG2D.rotate(angle);
        }
    }
    
    public void rotate(double arg0, double arg1, double arg2)
    {
        if(iG2D != null)
        {
            iG2D.rotate(arg0, arg1, arg2);
        }
    }
    
    public void translate(double x, double y)
    {
        if(iG2D != null)
        {
            iG2D.translate(x, y);
        }
    }
    
    public void scale(double x, double y)
    {
        if(iG2D != null)
        {
            iG2D.scale(x, y);
        }
    }
    
    public void setTransform(AffineTransform transform)
    {
        if(iG2D != null)
        {
            iG2D.setTransform(transform);
        }
    }
    
    public AffineTransform getTransform()
    {
        if(iG2D != null)
        {
            return iG2D.getTransform();
        }
        else
        {
            return null;
        }
    }
    
    public Font getFont()
    {
        if(iG2D != null)
        {
            return iG2D.getFont();
        }
        else
        {
            return null;
        }
    }
    
}
