%mathpiper,def="Sec"

RulebaseHoldArguments("Sec",["x"]);

5 ## Sec(x_Number?)::NumericMode?() --> SecD(x);
5 ## Sec(x_Number?`1)::NumericMode?() --> SecD(x);

10 ## Sec(x_Number?`rad)::NumericMode?() --> SecD(x);
10 ## Sec(x_Number?`radian)::NumericMode?() --> SecD(x);

15 ## Sec(x_Number?`deg)::NumericMode?() --> SecD(x * Pi/180);
15 ## Sec(x_Number?`degree)::NumericMode?() --> SecD(x * Pi/180);

20 ## Sec(xx_Number?`uu_)::NumericMode?() --> SecD(UnitsStripAll(NotationStandard(xx`uu)));

%/mathpiper





%mathpiper_docs,name="Sec",categories="Mathematics Procedures,Trigonometry (Symbolic)"

*CMD Sec --- trigonometric function secant

*CALL
        Sec(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure represents the trigonometric function secant.

*EXAMPLES
*SEE Sin, Cos, Tan, Cot, Sec, Csc, ArcSin, ArcCos, ArcTan, ArcCot, ArcSec, ArcCsc
%/mathpiper_docs





%mathpiper,name="Sec",subtype="in_prompts"

Sec(2) ~> Sec(2)

NM(Sec(4`rad)) ~> -1.529885656

NM(Sec(7`deg)) ~> 1.007509825

%/mathpiper





%mathpiper,name="Sec",subtype="automatic_test"

Verify(NM(Sec(1)), 1.850815718);

%/mathpiper