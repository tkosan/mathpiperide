/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import java.math.BigDecimal;
import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Environment;

/**
 *
 *  
 */
public class NotationDecimal extends BuiltinProcedure
{

    private NotationDecimal()
    {
    }

    public NotationDecimal(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        BigNumber in = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 1);

        String out = new BigDecimal(in.numToString(aStackTop, 10)).toPlainString();
        
        if(! out.contains("."))
        {
            out = out + ".0";
        }
        
        setTopOfStack(aEnvironment, aStackTop, new org.mathpiper.lisp.cons.NumberCons(out, in.getPrecision()));
    }
}

/*
%mathpiper_docs,name="NotationDecimal",categories="Programming Procedures,Numbers (Notations),Built In"
*CMD NotationDecimal --- convert a number to decimal notation

*CALL
	NotationDecimal(number)

*PARMS

{number} -- a number

*DESC
This procedure converts a number to decimal notation. Its main use is
to convert numbers that are in scientific notation (such as 2e3) to
decimal notation.

*EXAMPLES

%/mathpiper_docs




%mathpiper,name="NotationDecimal",subtype="in_prompts"

NotationDecimal(2e3) ~> 2000.0

%/mathpiper




%mathpiper,name="NotationDecimal",subtype="automatic_test"

Verify(NotationDecimal(2e3), 2000.0);

%/mathpiper
*/
