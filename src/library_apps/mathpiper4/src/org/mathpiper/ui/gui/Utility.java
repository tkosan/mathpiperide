package org.mathpiper.ui.gui;

import java.io.File;
import javax.swing.JComponent;
import javax.swing.JFileChooser;

public class Utility {
    
    private static File lastFile = null;

    public static void saveImageOfComponent(JComponent component) {
        JFileChooser saveImageFileChooser = new JFileChooser();
        

        if(lastFile != null)
        {
            saveImageFileChooser.setSelectedFile(lastFile);
        }

        int returnValue = saveImageFileChooser.showSaveDialog(component);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File exportImageFile = saveImageFileChooser.getSelectedFile();
            try {
                lastFile = exportImageFile;
                ScreenCapture.createImage(component, exportImageFile.getAbsolutePath());
            } catch (java.io.IOException ioe) {
                ioe.printStackTrace();
            }//end try/catch.

        }
    }

}
