package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;
import org.mathpiper.lisp.cons.SublistCons;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.scilab.forge.mp.jlatexmath.TeXConstants;
import org.scilab.forge.mp.jlatexmath.TeXFormula;
import org.scilab.forge.mp.jlatexmath.TeXIcon;

public class Formula extends GraphicObject implements Scalable
{
    private double defaultSize = 30;
    private double size = defaultSize;
    private double yAdjust;
    private Point point;
    private TeXFormula texFormula;
    private double initialYAdjust;
    
    public Formula(Environment environment, Integer integer, Point point,  PlotPanel plotPanel) throws Throwable
    {   
        this(environment, new NumberCons(new org.mathpiper.builtin.BigNumber(integer.toString(), 10, 10)), point,  plotPanel);
    }

    public Formula(Environment environment, Cons expressionCons, Point point, PlotPanel plotPanel) throws Throwable
    {
        this.point = point;

        //Evaluate Hold function.
        Cons holdAtomCons = AtomCons.getInstance(environment.getPrecision(), "Hold");
        holdAtomCons.setCdr(Cons.deepCopy(environment, -1, expressionCons));
        Cons holdSubListCons = SublistCons.getInstance(holdAtomCons);
        Cons holdInputExpression = holdSubListCons;

        //Obtain LaTeX version of the expression.
        Cons head = SublistCons.getInstance(AtomCons.getInstance(environment.getPrecision(), "UnparseLatex"));
        ((Cons) head.car()).setCdr(holdInputExpression);
        Cons result = environment.iLispExpressionEvaluator.evaluate(environment, -1, head);
        String texString = (String) result.car();
        texString = Utility.stripEndQuotesIfPresent(texString);
        texString = texString.substring(1, texString.length());
        texString = texString.substring(0, texString.length() - 1);
        texString = texString.replace("==", "=");
        texString = texString.replace("^", "^\\wedge");
        texFormula = new TeXFormula(texString);
        
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));
        yAdjust = initialYAdjust = -(icon.getTrueIconHeight() * icon.getBaseLine());
    }
    
    public void scale(double scale)
    {
        size *= scale;
        yAdjust *= scale;
    }
    
    public void resetScale()
    {
        size = defaultSize;
        yAdjust = initialYAdjust;         
    }

    public void paint(ScaledGraphics sg, PlotPanel plotPanel)
    {
        if (isVisible)
        {
            boolean isDebug = false;
            
            Color oldColor = sg.getColor();

            sg.setColor(Color.BLACK);
            
            sg.drawLatex(texFormula, size, plotPanel.toPixelsX(point.getXCoordinate()), plotPanel.toPixelsY(point.getYCoordinate()) + yAdjust, isDebug);
            
            //sg.drawScaledString(point.getLabel(), plotPanel.toPixelsX(point.getXCoordinate()), plotPanel.toPixelsY(point.getYCoordinate()), plotPanel.viewScale);

            if(isDebug)
            {
                int diameter = 3;
                sg.setColor(Color.RED);
                sg.fillOval(plotPanel.toPixelsX(point.getXCoordinate()) - diameter / 2.0, plotPanel.toPixelsY(point.getYCoordinate()) - diameter / 2.0, diameter, diameter);
            }
            
            sg.setColor(oldColor);
        }
    }
    
    public double getFormulaWidth(PlotPanel plotPanel)
    {
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));

        return plotPanel.toScaleX(icon.getTrueIconWidth());
    }
    
    public double getFormulaHeight(PlotPanel plotPanel)
    {
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));
        return plotPanel.toScaleY(icon.getTrueIconHeight());
    }
    
    public double getFormulaBaseline(PlotPanel plotPanel)
    {
        TeXIcon icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float)(size * plotPanel.viewScale));
        return plotPanel.toScaleY(icon.getBaseLine());
    }

}