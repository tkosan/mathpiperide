package org.mathpiper.io.worksheets;

import java.util.ArrayList;
import java.util.List;

public class GroupFold extends Fold {
    private List<Fold> folds = new ArrayList<Fold>();
    private List<OutsideFold> outsideFolds = new ArrayList<OutsideFold>();
    private String inFile = null;
    
    public GroupFold(int startLineNumber, int endLineNumber, String header) throws Throwable {
        super(startLineNumber, endLineNumber, header, null);
    }

    public void addFold(Fold fold)
    {
        folds.add(fold);
    }
    
    public void addOutsideFold(OutsideFold outsideFold)
    {
        outsideFolds.add(outsideFold);
    }
    
    public List<Fold> getFolds()
    {
        return folds;
    }
    
    public List<Fold> getFoldsByAttribute(String attributeName, String attributeValue)
    {
        List<Fold> matchedFolds = new ArrayList();
        
        for(Fold fold : folds)
        {
            if(fold.getAttributeValue(attributeName) != null && fold.getAttributeValue(attributeName).equals(attributeValue))
            {
                matchedFolds.add(fold);
            }
        }
        
        return matchedFolds;
    }

    public String getInFile()
    {
        return inFile;
    }

    public void setInFile(String inFile)
    {
        this.inFile = inFile;
    }

    public String toString()
    {
        if(isPrint)
        {
            return "%" + type + getAttributesString() + "\n" +  MPWSFile2.printContent(folds.iterator(), outsideFolds.iterator()) + "%/" + type + "\n";
        }
        else
        {
            return "";
        }
    }
}
