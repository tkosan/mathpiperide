package org.mathpiper.builtin.procedures.core;

import java.util.HashMap;
import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;

/**
 *
 *  
 */
public class HashMapPut extends BuiltinProcedure
{
    
    private HashMapPut()
    {
    }

    public HashMapPut(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons evaluated = getArgument(aEnvironment, aStackTop, 1);

        BuiltinContainer gen = (BuiltinContainer) evaluated.car();
        if( gen == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        Object o = gen.getObject();
        
        if(! (o instanceof HashMap)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        
        HashMap<Object,Cons> hashMap = (HashMap) o;

        Cons key = getArgument(aEnvironment, aStackTop, 2);

        if( key == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        if(! (key.car() instanceof String)) LispError.throwError(aEnvironment, aStackTop, "The key must be an atom.");
        
        Object key2 = key.car();
        
        hashMap.put(key2, getArgument(aEnvironment, aStackTop, 3));

        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}//end class.



/*
%mathpiper_docs,name="HashMapPut",categories="Programming Procedures,Built In"
*CMD HashMapPut --- associates the given key with the given value in the given hash map

*CALL
	HashMapPut(hashMap, key, value)

*PARMS

{hashMap} -- a hash map
{key} -- an atom
{value} -- a value

*DESC
This procedure associates the given key with the given value in the given hash map.

*SEE HashMapCreate

%/mathpiper_docs
*/
