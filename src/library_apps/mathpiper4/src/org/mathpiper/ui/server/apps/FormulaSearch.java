package org.mathpiper.ui.server.apps;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.Interpreters;
import org.mathpiper.lisp.Environment;
import org.nanohttpd.protocols.http.IHTTPSession;
import org.nanohttpd.protocols.http.NanoHTTPD;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.util.ServerRunner;

public class FormulaSearch extends NanoHTTPD {

    private static FileHandler logFile;
    private final static Logger logger = Logger.getLogger(FormulaSearch.class.getName());
    private Interpreter interpreter;
    private int counter = 1;
    private String variables = "";
    private String label = "";
    private String keywords = "";
    private boolean isLiteralMatch = false;
    
    public FormulaSearch() throws IOException {
        super(8080);

        try {
            logFile = new FileHandler("formula_search.log", false);

            logFile.setFormatter(new SimpleFormatter());
            logger.addHandler(logFile);
            logger.setLevel(Level.CONFIG);

            interpreter = Interpreters.getSynchronousInterpreter();

            Environment environment = interpreter.getEnvironment();
            Interpreters.addOptionalFunctions(environment, "org/mathpiper/builtin/procedures/nongwtcore/");
            Interpreters.addOptionalFunctions(environment, "org/mathpiper/builtin/procedures/optional/");

            System.out.println("\nRunning! Point your browsers to http://localhost:8080/ \n");

        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            ServerRunner.executeInstance(new FormulaSearch());
        } catch (IOException ioe) {
            System.err.println("Couldn't start server:\n" + ioe);
        }
    }

    @Override
    public Response serve(IHTTPSession session) {
        Response r = Response.newFixedLengthResponse("Hello");

        logger.log(Level.INFO, "HEADERS: " + session.getHeaders().toString());

        String uri = session.getUri();

        logger.log(Level.INFO, "REQUESTEDURL: " + uri.toString());
        
        StringBuilder sb1 = new StringBuilder();
        
        StringBuilder sb2 = new StringBuilder();

        try {
            switch (uri) {
                case "/":

                    try {
                        EvaluationResponse response;
                        Map<String, List<String>> parms = session.getParameters();
                        
                        if (! parms.isEmpty()) {

                            if(parms.get("variables") != null)
                            {
                                variables = parms.get("variables").get(0);
                            }
                            
                            if(parms.get("label") != null)
                            {
                                label = parms.get("label").get(0);
                            }
                            
                            if(parms.get("keywords") != null)
                            {
                                keywords = parms.get("keywords").get(0);
                            }
                            
                            if(parms.get("isLiteralMatch") != null)
                            {
                                isLiteralMatch = parms.get("isLiteralMatch").get(0).equalsIgnoreCase("true") ? true : false;
                            }
                            




                            response = interpreter.evaluate(
                                    "FormulaSearch('[" + variables + "],\n"
                                    + "Formulabase:Electronics,\n"
                                    + "FormulaLabel:\"" + label + "\",\n"
                                    + "MultilineArguments?:True,\n"
                                    + "LiteralMatch?:\"" + (isLiteralMatch ? "True" : "False") + "\",\n"
                                    + "Units?:False,\n"
                                    + "Keywords:[\"" + keywords + "\"]);");
                            
                            if(response.isExceptionThrown())
                            {
                                sb2.append(response.getException().getMessage());
                            }
                            else
                            {
                                String sideEffects = response.getSideEffects();

                                sb2.append("<pre>\n" + sideEffects + "\n</pre>");
                            }
                        }
                        
                        sb2.append("</body></html>\n");

                    } catch (Exception e) {
                        sb2.append(e.getMessage());
                    }
                    sb1.append("<!DOCTYPE html>\n" +
                            "<html>\n" +
                            "<head>\n" +
                            "	<title>Electronics Formula Search</title>\n" +
                            "	<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"> \n" +
                            "\n" +
                            "<style type=\"text/css\">\n" +
                            "form  { display: table;  }\n" + //border-spacing: 10px; border-collapse: separate;
                            "p     { display: table-row; }\n" +
                            "label { display: table-cell; text-align: right; padding-right: 10px;}\n" +
                            "input { display: table-cell;}\n" +
                            "div { display: table-cell; align: right;}\n" +
                            "</style>\n\n" + 
                            "</head> \n");
                    
                    
                    sb1.append("<body>\n");
                    
                    sb1.append(counter++ + "\n\n");
                    
                    sb1.append(  "<form action='/' method='get'>\n" +
                                "  <p>\n" +
                                "    <label for=\"variables\">Variables </label>\n" +
                                "    <input id=\"variables\" name=\"variables\" type=\"text\", value=\"" + variables + "\">\n" +
                                "  </p>\n" +
                                "  <p>\n" +
                                "    <label for=\"label\">Label </label>\n" +
                                "    <input id=\"label\" name=\"label\" type=\"text\", value=\"" + label + "\">\n" +
                                "  </p>\n" +
                                "  <p>\n" +
                                "    <label for=\"keywords\">Keywords </label>\n" +
                                "    <input id=\"keywords\" name=\"keywords\" type=\"text\", value=\"" + keywords + "\">\n" +
                                "  </p>\n" +
                                "  <p>\n" +
                                "    <label for=\"isLiteralMatch\">Literal Match</label>\n" +
                                "    <input type=\"checkbox\" id=\"isLiteralMatch\" name=\"isLiteralMatch\" value=\"True\" checked>\n" +
                                "  </p>\n" +
                                "  <div></div>" +
                                "  <div>\n" +
                                "    <input type=\"submit\" value=\"Search\">" +
                                "  </div>\n" +
                                "</form>\n");
                    
                    sb1.append("<br>\n");

                case "/css/style.css":
                    
                case "/script/script.js":
                //inputStream = mAssetManager.open("script/script.js");
                ///return new NanoHTTPD.Response(Response.Status.OK, "text/javascript", inputStream);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

        
        sb1.append(sb2);
        return Response.newFixedLengthResponse(sb1.toString());
    }

}
