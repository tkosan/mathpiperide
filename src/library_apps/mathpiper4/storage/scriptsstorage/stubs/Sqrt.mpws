%mathpiper,def="Sqrt"

0 # Sqrt(0) <-- 0;
0 # Sqrt(Infinity) <--  Infinity;
0 # Sqrt(-Infinity) <-- Complex(0,Infinity);
0 # Sqrt(Undefined) <--  Undefined;
1 # Sqrt(x_PositiveInteger?)_(Integer?(SqrtN(x))) <-- SqrtN(x);
2 # Sqrt(x_PositiveNumber?)_NumericMode?() <-- SqrtN(x);
2 # Sqrt(x_NegativeNumber?) <-- Complex(0,Sqrt(-x));
/* 3 # Sqrt(x_Number?/y_Number?) <-- Sqrt(x)/Sqrt(y); */
3 # Sqrt(x_Complex?)_NumericMode?() <-- x^(1/2);
/* Threading  */
Sqrt(xlist_List?) <-- MapSingle("Sqrt",xlist);

90 # (Sqrt(x_Constant?))_(NegativeNumber?(NM(x))) <-- Complex(0,Sqrt(-x));

110 # Sqrt(Complex(_r,_i)) <-- Exp(Ln(Complex(r,i))/2);


400 # Sqrt(x_Integer?)_Integer?(SqrtN(x)) <-- SqrtN(x);
400 # Sqrt(x_Integer?/y_Integer?)_(Integer?(SqrtN(x)) And? Integer?(SqrtN(y))) <-- SqrtN(x)/SqrtN(y);

%/mathpiper



%mathpiper_docs,name="Sqrt",categories="Mathematics Functions;Calculus Related (Symbolic)"
*CMD Sqrt --- square root
*STD
*CALL
        Sqrt(x)

*PARMS

{x} -- argument to the function

*DESC

This function calculates the square root of "x". If the result is
not rational, the call is returned unevaluated unless a numerical
approximation is forced with the {N} function. This
function can also handle negative and complex arguments.

This function is threaded, meaning that if the argument {x} is a
list, the function is applied to all entries in the list.

*E.G.

In> Sqrt(16)
Result: 4;

In> Sqrt(15)
Result: Sqrt(15);

In> NM(Sqrt(15))
Result: 3.8729833462;

In> Sqrt(4/9)
Result: 2/3;

In> Sqrt(-1)
Result: Complex(0,1);

*SEE Exp, ^, N
%/mathpiper_docs





%mathpiper,name="Sqrt",subtype="automatic_test"

Verify(Sqrt(Infinity),Infinity);

// version 1.0.56: Due to MathBitCount returning negative values sometimes, functions depending on
// proper functioning failed. MathSqrtFloat failed for instance on NM(1/2). It did give the right
// result for 0.5.
NumericEqual(NM(Sqrt(500000e-6),20),NM(Sqrt(0.0000005e6),20),20);
NumericEqual(NM(Sqrt(0.5),20),NM(Sqrt(NM(1/2)),20),20);

%/mathpiper