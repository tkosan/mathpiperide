%mathpiper,def="PositiveInteger?"

PositiveInteger?(x):= Integer?(x) And? x >? 0;

%/mathpiper



%mathpiper_docs,name="PositiveInteger?",categories="Programming Functions;Predicates"
*CMD PositiveInteger? --- test for a positive integer
*STD
*CALL
        PositiveInteger?(n)

*PARMS

{n} -- integer to test

*DESC

This function tests whether the integer {n} is (strictly) positive. The
positive integers are 1, 2, 3, 4, 5, etc. If {n} is not a integer, the
function returns {False}.

*E.G.

In> PositiveInteger?(31);
Result: True;

In> PositiveInteger?(-2);
Result: False;

*SEE NegativeInteger?, NonZeroInteger?, PositiveNumber?
%/mathpiper_docs