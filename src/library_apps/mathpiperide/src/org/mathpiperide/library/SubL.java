package org.mathpiperide.library;

import java.net.*;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class SubL {

    public static String evaluate(String expression) throws Exception {
        
        BufferedReader in;
        PrintWriter out;
        String inputLine;
        Socket socket;
        String domainName = "localhost";
        int port = 3601;

        StringBuilder htmlCode = new StringBuilder();

        InetAddress iAddress = InetAddress.getByName(domainName);

        socket = new Socket(iAddress, port);

        OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.ISO_8859_1);
        out = new PrintWriter(osw);

        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        out.print(expression + "\n");
        out.flush();

        while (true) {
            inputLine = in.readLine();

            if (inputLine == null) {
                break;
            }

            htmlCode.append(inputLine);
            htmlCode.append("\n");

            if (inputLine.contains("200") || inputLine.contains("500")) {
                break;
            }
        }

        socket.close();

        return htmlCode.toString().trim();
    }

    public static void main(String args[]) throws Exception {
        SubL.evaluate("(cyc-query '(#$isa ?X #$Project) #$LogicMOOMt)");
    }
}
