/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

package org.mathpiper.io.worksheets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.mathpiper.builtin.library.base64.Base64;
import org.mathpiper.io.worksheets.MPWSFile2.HintType;

public class Fold extends Content {

    protected String type;
    protected Map<String, Attribute> attributes = new HashMap();
    protected String[] onlyAttributesArray = null;

    public Fold(int startLineNumber, int endLineNumber, String header, Object contents) throws Throwable {

        this.startLineNumber = startLineNumber;
        
        this.endLineNumber = endLineNumber;

        scanHeader(header);
        
        this.contents = contents;
    }

    private void scanHeader(String header) throws Throwable {
        String[] headerParts = header.trim().split(",(?!(?=[^\"]*\"[^\"]*(?:\"[^\"]*\"[^\"]*)*$))");

        Pattern headerPattern = Pattern.compile("%[a-z0-9_]+$|%[a-z0-9_]+( *[,] *[a-zA-Z_]+ *[a-zA-Z0-9_]*= *\"[^\"\\n]*\")+ *$");
        Matcher headerMatcher = headerPattern.matcher(header);

        if(headerMatcher.find() == false) 
        {
            header = header.replace("\"", "\\\"");
            throw new Exception("Header: " + header + "\n\nThere is a syntax error in the fold's header on line " + this.startLineNumber + ". Make sure all values are enclosed in double quotes, and check for missing commas and equals signs.\n\n");
        }

        type = headerParts[0].substring(1, headerParts[0].length());

        for (int index = 1; index < headerParts.length; index++) {
            headerParts[index] = headerParts[index].replaceFirst("=", ",");
            String[] headerPart = headerParts[index].split(",(?!(?=[^\"]*\"[^\"]*(?:\"[^\"]*\"[^\"]*)*$))");
            String attributeName = headerPart[0].trim();
            String attributeValue = headerPart[1].trim().replace("\"", "");
            attributes.put(attributeName, new Attribute(attributeName, attributeValue, index));
        }

    }

    public Map<String,Attribute> getAttributes() {
        return attributes;
    }
    
    public String getAttributeValue(String name)
    {
        Attribute attribute = attributes.get(name);
        
        if(attribute == null)
        {
            return null;
        }
        else
        {
            return attribute.getValue();
        }
    }
    
    public int getNumberOfAttributes()
    {
        return attributes.size();
    }
    
    public void addAttribute(String name, String value, int index)
    {
        Attribute newAttribute = new Attribute(name, value, index);
        
        attributes.put(name, newAttribute);
    }
    
    public void removeAttribute(String name)
    {
        attributes.remove(name);
    }
    
    public void changeAttribute(String name, String newValue) throws Exception
    {
        Attribute attribute = attributes.get(name);
        
        if(attribute == null)
        {
            throw new Exception("Attribute \"" + name + "\" does not exist.");
        }
        
        attribute.setValue(newValue);
    }

    public String getContents() {
        if(contents != null)
        {
            return contents.toString();
        }
        else
        {
            return null;
        }
    }
    
    public void setContents(String newContents)
    {
        this.contents = newContents;
    }

    public String getType() {
        return type;
    }

    public String[] getOnlyAttributesArray()
    {
        return onlyAttributesArray;
    }

    public void setOnlyAttributesArray(String[] onlyAttributesArray)
    {
        this.onlyAttributesArray = onlyAttributesArray;
    }
    
    public String getAttributesString()
    {
        List<Attribute> attributeValues = new ArrayList<Attribute>(attributes.values());

        Collections.sort(attributeValues, new Comparator<Attribute>() {
            @Override
            public int compare(Attribute lhs, Attribute rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs.getIndex() > rhs.getIndex() ? 1 : (lhs.getIndex() < rhs.getIndex()) ? -1 : 0;
            }
        });

        StringBuilder sb = new StringBuilder();

        for (Attribute attribute : attributeValues) {
            
            if(onlyAttributesArray == null || Arrays.asList(onlyAttributesArray).contains(attribute.getName()))
            {
                sb.append(",");
                sb.append(attribute.getName());
                sb.append("=");
                sb.append("\"");
                sb.append(attribute.getValue());
                sb.append("\"");
            }
        }
        
        return sb.toString();
    }
    
    public String toString() {
        
        StringBuilder sb = new StringBuilder();
        
        
        if(isPrint)
        {
            if(hintType != HintType.NONE && 
               this.getAttributeValue("subtype") != null && this.getAttributeValue("subtype").equals("problem") &&
               (this.getAttributeValue("hint") == null || ! this.getAttributeValue("hint").equals("false")))
            {
                sb.append("%mathpiper,name=\"" + this.getAttributeValue("name") + "\",subtype=\"hint\"\n");
                
                String foldContents = originalContents.toString();
                
                foldContents = foldContents.replaceAll("Problem\\(ID:[a-zA-Z0-9\\\"]+\\)", "Problem()"); //todo:tk:this code needs to be generalized.
                
                StringBuilder hintString = new StringBuilder();
                
                if(hintType == HintType.ALL)
                {
                    hintString.append(foldContents);
                }
                else if(hintType == HintType.NAMES_ONLY)
                {
                    List<String> formulaIDsList = new ArrayList<String>();

                    Pattern regex = Pattern.compile("ID: *\"[a-zA-Z0-9\\.]+\"");
                    Matcher regexMatcher = regex.matcher(foldContents);
                    while (regexMatcher.find()) {
                        formulaIDsList.add(regexMatcher.group());
                    }

                    Collections.shuffle(formulaIDsList);

                    for(String label : formulaIDsList)
                    {
                        label = label.replace("ID:", "");
                        label = label.replace("\"", "");
                        hintString.append(label);
                        hintString.append("\n");
                    }
                }
                
                String base64 = Base64.encodeToString(hintString.toString().getBytes(), false);
                sb.append("\nHint(\"" + base64 + "\");\n\n");
                
                    sb.append("%/mathpiper\n\n\n");
                }
                          
            sb.append("%" + type + getAttributesString() + "\n" + contents + "%/" + type + "\n");
        }

        return sb.toString();
    }
}
