package org.mathpiper.ui.gui.worksheets;

public interface Scalable
{
   void scale(double scale);
   void resetScale();
}
