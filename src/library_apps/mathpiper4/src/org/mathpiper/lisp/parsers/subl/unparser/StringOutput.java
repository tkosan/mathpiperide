package org.mathpiper.lisp.parsers.subl.unparser;

/*
MIT License

Copyright (c) 2020 Joseph Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

public class StringOutput {

    private int charsSinceNewline = 0;
    StringBuffer stringBuffer;

    public StringOutput() {
        this.stringBuffer = new java.lang.StringBuffer();
    }


    public void putChar(char aChar) {
        this.stringBuffer.append(aChar);
        
        if(aChar == '\n')
        {
            charsSinceNewline = 0;
        }
        else
        {
            charsSinceNewline++;
        }
    }


    /*public void setStringBuffer(StringBuffer stringBuffer)
    {
    this.stringBuffer = stringBuffer;
    }//end method.*/
    public String toString() {
        if (this.stringBuffer.length() != 0) {
            String outputMessage = this.stringBuffer.toString();
            this.clear();
            return outputMessage;
        } else {
            return null;
        }//end else.


    }//end method.


    public void clear() {
        this.stringBuffer.delete(0, this.stringBuffer.length());
    }


    public void write(String aString) throws Throwable {
        int i;
        for (i = 0; i < aString.length(); i++) {
            putChar(aString.charAt(i));
        }
    }
    
    public int getCharsSinceNewline() {
        return charsSinceNewline;
    }

}
