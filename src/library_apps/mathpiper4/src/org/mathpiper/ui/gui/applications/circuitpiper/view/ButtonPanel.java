package org.mathpiper.ui.gui.applications.circuitpiper.view;

import org.mathpiper.ui.gui.applications.circuitpiper.view.instruments.MultimeterPanel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements.CircuitElm;



/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class ButtonPanel extends JPanel {

    public JToggleButton runButton;
    
    private JCheckBox showTerminalIDsCheckBox;
    private JCheckBox showWireIDsCheckBox;
    private JCheckBox showGridCheckBox;
    private JCheckBox showAllTerminalsCheckBox;
    private JCheckBox showComponentValuesCheckBox;
    private JCheckBox componentHighlightingModeCheckBox;
    public JCheckBox roundingModeCheckBox;
    
    private WrapLayout wrapLayout;
    final String SEPARATOR = "SEPARATOR";
    
    static final String[] componentNames = {"Wire", "Ground", "SEPARATOR", "Resistor", "Capacitor", "Inductor", "SEPARATOR",
        "DC Voltage Source", "AC Voltage Source", "SEPARATOR", "Current Source", "AC Current Source", 
        "SEPARATOR", "VCVS", "VCCS", "CCVS", "CCCS", "SEPARATOR", "Voltmeter", "Ohmmeter", "Ammeter", "Capacitance Meter (todo)",
        "Inductance Meter (todo)", "SEPARATOR", "Current Integrator", "Voltage Integrator", "Block",  "Diode",
        "Transistor NPN", "Transistor PNP", "Transistor JFET N", "Transistor JFET P", "SEPARATOR", 
        "Potentiometer", "Switch", "Transformer", "Logical Package"};

    String[] unsupportedInCirSim = {"Current Integrator", 
        "Voltage Integrator", "AC Current Source", "CCVS", "Block", "Logical Package"};
    
    
    public JComboBox componentList;
    public List<String> componentNamesList;
    
    public ButtonPanel(final CircuitPanel parentCircuitPanel) {
        super();
        
        componentNamesList = new ArrayList<String>(Arrays.asList(componentNames));

        if(parentCircuitPanel.currentCircuit.isCirSim)
        {
            for(String name : unsupportedInCirSim)
            {
                componentNamesList.remove(name);
            }
        }
        
        wrapLayout = new WrapLayout();
        this.setLayout(wrapLayout);
        this.add(new JLabel("Component"));
        componentList = new JComboBox(componentNamesList.toArray(new String[componentNamesList.size()]));
        componentList.setMaximumRowCount(32);
        componentList.setRenderer(new ComboBoxRenderer());
        this.add(componentList);
        componentList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                String componentName = (String) cb.getSelectedItem();
                parentCircuitPanel.setSelectedComponent(componentName);
                parentCircuitPanel.setIsDrawing(false);
                parentCircuitPanel.setIsMovingPoint(false);
                parentCircuitPanel.setHintStarting();
                parentCircuitPanel.repaint();
                parentCircuitPanel.revalidate();
            }
        });
        componentList.setSelectedIndex(0);

        parentCircuitPanel.setSelectedComponent("Wire");

        runButton = new JToggleButton("Run");
        runButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JToggleButton toggleButton = (JToggleButton) e.getSource();
                if(toggleButton.isSelected())
                {
                    Runnable currentDotsAndMultimetersRunnable = null;
                    
                    if(parentCircuitPanel.currentCircuit.isCirSim == true)
                    {
                        parentCircuitPanel.currentCircuit.cirSim.setSimRunning(true);
                        
                        currentDotsAndMultimetersRunnable = new Runnable()
                        {
                            public void run()
                            {
                                parentCircuitPanel.simTimeAndStepPanel.setText("Time: " + CircuitElm.getTimeText(parentCircuitPanel.currentCircuit.cirSim.simulationTime) +
                                     ", Step Size: " + CircuitElm.getTimeText(parentCircuitPanel.currentCircuit.cirSim.timeStep));
                                
                                
                                for(java.awt.Component awtComponent : parentCircuitPanel.instrumentsPanel.getComponents())
                                {
                                    if(awtComponent instanceof MultimeterPanel)
                                    {
                                        ((MultimeterPanel) awtComponent).refresh();
                                    }
                                }
                            }
                        };
                    }
                    else
                    {
                        parentCircuitPanel.isRunning = true;
                    }
                    
                    parentCircuitPanel.readoutTimer = parentCircuitPanel.readoutTimerScheduler.scheduleAtFixedRate(currentDotsAndMultimetersRunnable, 100, 10, java.util.concurrent.TimeUnit.MILLISECONDS);
                }
                else
                {
                    if(parentCircuitPanel.currentCircuit.isCirSim == true)
                    {
                        parentCircuitPanel.currentCircuit.cirSim.setSimRunning(false);
                    }
                    else
                    {
                        parentCircuitPanel.isRunning = false;
                    }
                    
                    if(parentCircuitPanel.readoutTimer != null)
                    {
                        parentCircuitPanel.readoutTimer.cancel(true);
                    }
                }
            }
        });
        this.add(runButton);
        
        // playButton.doClick(); todo:tk.
        
        /*
        JCheckBox probeCheckBox = new JCheckBox("Probe");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        probeCheckBox.setSelected(false);
        probeCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitEnginePanel.isProbe = true;
                }
                else
                {
                    parentCircuitEnginePanel.isProbe = false;
                }
            }
        });
        this.add(probeCheckBox);
        */
        
        showGridCheckBox = new JCheckBox("Grid");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showGridCheckBox.setSelected(true);
        showGridCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitPanel.drawingPanel.isDrawGrid = true;
                    parentCircuitPanel.repaint();
                }
                else
                {
                    parentCircuitPanel.drawingPanel.isDrawGrid = false;
                    parentCircuitPanel.repaint();
                }
            }
        });
        parentCircuitPanel.showGridCheckBox = showGridCheckBox;
        this.add(showGridCheckBox);

        
        showWireIDsCheckBox = new JCheckBox("Wire IDs");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showWireIDsCheckBox.setSelected(false);
        showWireIDsCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitPanel.drawingPanel.isDrawWireLabels = true;
                    parentCircuitPanel.repaint();
                }
                else
                {
                    parentCircuitPanel.drawingPanel.isDrawWireLabels = false;
                    parentCircuitPanel.repaint();
                }
            }
        });
        this.add(showWireIDsCheckBox);
        
        showTerminalIDsCheckBox = new JCheckBox("Terminal IDs");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showTerminalIDsCheckBox.setSelected(false);
        showTerminalIDsCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitPanel.drawingPanel.isDrawTerminalLabels = true;
                    parentCircuitPanel.repaint();
                }
                else
                {
                    parentCircuitPanel.drawingPanel.isDrawTerminalLabels = false;
                    parentCircuitPanel.repaint();
                }
            }
        });
        this.add(showTerminalIDsCheckBox);
        
        showAllTerminalsCheckBox = new JCheckBox("All Terminals");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showAllTerminalsCheckBox.setSelected(true);
        showAllTerminalsCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitPanel.drawingPanel.isShowAllTerminals = true;
                    parentCircuitPanel.repaint();
                }
                else
                {
                    parentCircuitPanel.drawingPanel.isShowAllTerminals = false;
                    parentCircuitPanel.repaint();
                }
            }
        });
        this.add(showAllTerminalsCheckBox);
        
        showComponentValuesCheckBox = new JCheckBox("Values");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        showComponentValuesCheckBox.setSelected(true);
        showComponentValuesCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitPanel.drawingPanel.isDrawComponentValues = true;
                    parentCircuitPanel.repaint();
                }
                else
                {
                    parentCircuitPanel.drawingPanel.isDrawComponentValues = false;
                    parentCircuitPanel.repaint();
                }
            }
        });
        this.add(showComponentValuesCheckBox);
        
        componentHighlightingModeCheckBox = new JCheckBox("Highlight");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        componentHighlightingModeCheckBox.setSelected(false);
        componentHighlightingModeCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitPanel.drawingPanel.isComponentHighlightingMode = true;
                }
                else
                {
                    parentCircuitPanel.drawingPanel.isComponentHighlightingMode = false;
                }
            }
        });
        this.add(componentHighlightingModeCheckBox);
        
        roundingModeCheckBox = new JCheckBox("Round");
        //probeCheckBox.setMnemonic(KeyEvent.VK_C); 
        roundingModeCheckBox.setSelected(false);
        roundingModeCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED)
                {
                    parentCircuitPanel.drawingPanel.isRoundingMode = true;
                }
                else
                {
                    parentCircuitPanel.drawingPanel.isRoundingMode = false;
                }
            }
        });
        this.add(roundingModeCheckBox);
        
  
        /*
        JButton moveLeftButton = new JButton("Left");
        moveLeftButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setX(terminal.getX() - parentCircuitEnginePanel.xDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveLeftButton);
        
        
        JButton moveRightButton = new JButton("Right");
        moveRightButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setX(terminal.getX() + parentCircuitEnginePanel.xDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveRightButton);
        
        
        JButton moveUpButton = new JButton("Up");
        moveUpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setY(terminal.getY() - parentCircuitEnginePanel.yDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveUpButton);
        
        
        JButton moveDownButton = new JButton("Down");
        moveDownButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    for (Terminal terminal : parentCircuitEnginePanel.circuit.myTerminals.values()) {
                       terminal.setY(terminal.getY() + parentCircuitEnginePanel.yDistanceBetweenTerminalsPixels);
                    }               
                    parentCircuitEnginePanel.repaint();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(moveDownButton);
        */
        
  }

    public void init()
    {
        this.componentHighlightingModeCheckBox.setSelected(false);
        this.showAllTerminalsCheckBox.setSelected(true);
        this.showGridCheckBox.setSelected(true);
        this.showTerminalIDsCheckBox.setSelected(false);
        this.showWireIDsCheckBox.setSelected(false);
        this.showComponentValuesCheckBox.setSelected(true);
    }
    
    class ComboBoxRenderer extends JLabel implements ListCellRenderer
    {
        // Obtained from (http://www.java2s.com/Code/Java/Swing-Components/BlockComboBoxExample.htm)
        JSeparator separator;

        public ComboBoxRenderer()
        {
            setOpaque(true);
            setBorder(new EmptyBorder(1, 1, 1, 1));
            separator = new JSeparator(JSeparator.HORIZONTAL);
        }

        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus)
        {
            String str = (value == null) ? "" : value.toString();
            if (SEPARATOR.equals(str))
            {
                return separator;
            }
            if (isSelected)
            {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            }
            else
            {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(list.getFont());
            setText(str);
            return this;
        }
    }

    class BlockComboListener implements ActionListener
    {

        JComboBox combo;

        Object currentItem;

        BlockComboListener(JComboBox combo)
        {
            this.combo = combo;
            combo.setSelectedIndex(0);
            currentItem = combo.getSelectedItem();
        }

        public void actionPerformed(ActionEvent e)
        {
            String tempItem = (String) combo.getSelectedItem();
            if (SEPARATOR.equals(tempItem))
            {
                combo.setSelectedItem(currentItem);
            }
            else
            {
                currentItem = tempItem;
            }
        }
    }
}
