%mathpiper,def="OptionsListToHash"

/// utility procedure: convert options lists of the form
/// "[key=value, key=value]" into a hash of the same form.
/// The argument list is kept unevaluated using "HoldArgumentNumber()".
/// Note that symbolic values of type atom are automatically converted to strings, e.g. ListToHash([a: b]) returns [["a", "b"]]
OptionsListToHash(list) :=
{
        Local(item, result);
        result := [];
        ForEach(item, list)
                Decide(
                        Procedure?(item) &? (Type(item) =? ":" ) &? Atom?(item[1]),
                        result[ToString(item[1])] := Decide(
                                Atom?(item[2]) &? !? Number?(item[2]) &? !? String?(item[2]),
                                ToString(item[2]),
                                item[2]
                        ),
                        Echo(["OptionsListToHash: Error: item ", item, " is not of the format a: b."])
                );
        
        result;
}

HoldArgumentNumber("OptionsListToHash", 1, 1);

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output

