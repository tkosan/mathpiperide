/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.optional;

import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;

/**
 *
 *
 */
public class Delay extends BuiltinProcedure
{


    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "Delay";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        BigNumber milliseconds = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 1);

        Thread.sleep(milliseconds.toLong());

        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}//end class.



/*
%mathpiper_docs,name="Delay",categories="Programming Procedures,Input/Output,Built In"
*CMD Delay --- delays execution of a program for a specified number of milliseconds
*CORE
*CALL
	Delay(ms)

 *PARMS
 {ms} -- the number of milliseconds to delay

*DESC

This function delays execution of a program for the specified number of milliseconds.
The delay can be terminated by pressing the "Halt Evaluation" button.

*E.G.
In> Delay(1000)
Result: True

%/mathpiper_docs
*/
