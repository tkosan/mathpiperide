v.07

%todo
- How much of the material in the module 1-4 worksheets
(if any) should be covered here?
%/todo


Evaluate each of the folds and "in" prompts below
from top to bottom and make sure you understand
how all the code they contain works.


== All of the following are literal values.

In> 4


In> -2


In> 'aa


In> "Hello"


In> [1,6,5,4]




== Two kinds of numbers are integers and decimals.

Integers.

In> 3


Decimals.

In> 3.23454




== The ":=" operator assigns values to variables.

In> aa := 4


In> aa


In> aa + 2




== Strings contain sequences of characters.

In> bb := "Hello there!"


In> bb[5]


In> cc := "5274932"


In> cc[5]


In> StringToNumber(cc[5])


In> "Hi " + "there!"


In> "Number " + 1




== Lists can contain any type of data.

In> cc := RandomIntegerList(50, 100, 175)


In> cc[3]


In> cc[RandomInteger(50)]


In> cc[RandomInteger(Length(cc))]


In> dd := [11,12,13,["a","b"],14,15]


In> dd[4]


In> dd[4][2]




== Printing side effect output with Echo.

In> "Hello"


In> Echo("Hello")


In> Echo("Hello", "there", "number", 5)


In> Echo("Hello" + "there" + "number" + 5)


In> Echo("Hello " + "there " + "number " + 5)




== Code sequence.

In> ee := {11; 22; 33; 44;}


%mathpiper

ee := 
{
    11;
    22;
    33;
    44;
}

%/mathpiper


In> ff := {Echo("Hi"); Echo("There!");}


In> ff




== Boolean (predicate) expressions.

Truth table for AND (&?)
-----------------------
True &? True   -> True  
True &? False  -> False
False &? True  -> False
False &? False -> False
-----------------------


Truth table for OR (|?)
-----------------------
True |? True   -> True  
True |? False  -> True
False |? True  -> True
False |? False -> False
-----------------------


Truth table for NOT (!?)
----------------
!? True -> False  
!? False -> True
----------------


In> True &? False
          

The "!?" operator has high precedence so in the
following code its argument is "True" and not
the result of "True &? True".

In> !? True &? True


In> (!? True) &? True



Parentheses are needed to force the result of
"True &? True" to be the argument to the
"!?" operator.

In> !? (True &? True)



In> True |? False


In> !? True |? True


In> (!? True) |? True


In> !? (True |? False)




== Making decisions with If/Else.

NOTE: SET "FLOWCHART" TO "TRUE" TO VIEW THE
FLOWCHART OF THE CODE BELOW.


%mathpiper,flowchart="false",image_scale="2.0"

If(True)
{
    Echo("Hello");
}


Echo("Done");

%/mathpiper



%mathpiper,flowchart="false",image_scale="2.0"

If(False)
{
    Echo("Hello");
}


Echo("Done");

%/mathpiper



%mathpiper,flowchart="false",image_scale="2.0"

If(True)
{
    Echo("One");
    If(True)
    {
        Echo("Two");
        If(True)
        {
            Echo("Three");
            If(True)
            {
                Echo("Hello");
            }        
        }    
    }
}

%/mathpiper




== Looping with While.

%mathpiper,flowchart="false",image_scale="2.0"

index := 1;

While(index <=? 10)
{
    Echo(index);

    index := (index + 1);
}

%/mathpiper




%mathpiper,flowchart="false",image_scale="2.0"

index := 10;

While(index >=? 1)
{
    Echo(index);

    index := index - 1;
}

%/mathpiper




== Looping with lists.

%mathpiper,flowchart="false",image_scale="2.0"

list := [4,77,73,17,48,82,79,48,70,8];

// list := RandomIntegerList(1000, 1, 500);

listLength := Length(list);

index := 1;

numberFound := False;

While(index <=? listLength)
{
    If(list[index] =? 79)
    {
        numberFound := True;
    }
    
    index := index + 1;
}

If(numberFound =? True)
{
    Echo("79 is in the list");
}
Else
{
    Echo("79 is not in the list");
}

%/mathpiper



%todo

How to decide when to use one If, an If/Else, multiple
sequential Ifs, or multiple If/Else/Ifs.

%/todo

