package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Stack;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.mathpiper.ui.gui.applications.circuitpiper.view.SchematicSymbol;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class Inductor extends Component {
    
    public static int componentCounter = 1;

    public Inductor(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        if(uid == null)
        {
            componentSubscript = componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }

        init();
        primaryValue = 0.00234;
        enteredPrimaryValue = "" + 1/siToValue.get(siPrefix) * primaryValue;
    }
    
    public Inductor(int x, int y, String uid, Stack<String> attributes, Circuit circuit) throws Exception {
        super(x, y, circuit);
        if(uid == null)
        {
            componentSubscript = componentCounter++ + "";
        }
        else
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
 
        init();
         
        handleAttribute(attributes);
    }

    public void init() {
        symbol = new SchematicSymbol();
        symbol.type = "Transformer";
        
        symbol.templateHeadTerminalPoint = new Point2D.Double(0, 0);
        symbol.templateHeadLeadPoint = new Point2D.Double(0, 0);
        symbol.templateTailTerminalPoint = new Point2D.Double(30, 0);
        symbol.templateTailLeadPoint = new Point2D.Double(30, 0);
        
        symbol.template.put("1", SchematicSymbol.makeArc(new Point2D.Double(0,30), new Point2D.Double(3.75, 26.25), new Point2D.Double(0,22.5)));
        symbol.template.put("2", SchematicSymbol.makeArc(new Point2D.Double(0,22.5), new Point2D.Double(3.75, 18.75), new Point2D.Double(0,15)));
        symbol.template.put("3", SchematicSymbol.makeArc(new Point2D.Double(0,15), new Point2D.Double(3.75, 11.25), new Point2D.Double(0,7.5)));
        symbol.template.put("4", SchematicSymbol.makeArc(new Point2D.Double(0,7.5), new Point2D.Double(3.75, 3.75), new Point2D.Double(0,0)));

        /*
        // Plus sign.
        symbol.template.put("5", new Line2D.Double(-5, 27, -9, 27));
        symbol.template.put("6", new Line2D.Double(-7, 25, -7, 29));
*/
        
        symbol.rotateSymbol(15, 15, -270);
        
        setSecondary("Current");
        setPrimary("Inductance");
        setPrimaryUnit("Henry");
        setPrimaryUnitPlural("Henries");
        setPrimarySymbol("L");
        setSiPrefix("m");
        setPrimaryUnitSymbol("H");
        setSecondaryUnitSymbol("A");
        setPreselectedPrimaryPrefix(getSiPrefix() + getPrimaryUnitSymbol());
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        symbol.drawComponent(getHeadTerminal().getX(), getHeadTerminal().getY(), getTailTerminal().getX(), getTailTerminal().getY(), sg);   
        
        if(this.circuit.isCirSim && circuit.circuitPanel.isNotMovingAny())
        {
            this.getCircuitElm().doDots(sg);
        }
    }
    
    public int getLabelDistance()
    {
        return 15;
    }
    
    public String toString()
    {
        return super.toString() +  " " + this.getPrimaryValue();
    }
}
