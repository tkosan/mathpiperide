/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;


public class Operator_ extends BuiltinProcedure
{

    private Operator_()
    {
    }

    public Operator_(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons arg = getArgument(aEnvironment, aStackTop, 1);
        
        if(!( arg instanceof AtomCons)) LispError.checkArgument(aEnvironment, aStackTop, 1);
        
        String operator =  (String) getArgument(aEnvironment, aStackTop, 1).car();
        
        operator = Utility.stripEndQuotesIfPresent(operator);
        
        boolean isInfixOperator = aEnvironment.iInfixOperators.map.containsKey(operator);
        boolean isPrefixOperator = aEnvironment.iPrefixOperators.map.containsKey(operator);
        boolean isPostfixOperator = aEnvironment.iPostfixOperators.map.containsKey(operator);
        
        setTopOfStack(aEnvironment, aStackTop, Utility.getBooleanAtom(aEnvironment, isInfixOperator || isPrefixOperator || isPostfixOperator));
    }
}



/*
%mathpiper_docs,name="Operator?",categories="Programming Procedures,Predicates,Built In"
*CMD Operator? --- test for an operator

*CALL
	Operator?(expr)

*PARMS

{expr} -- expression to test

*DESC

This procedure tests whether "expr" is an operator.

*EXAMPLES

*SEE OperatorsGet,Procedure?
%/mathpiper_docs





%mathpiper,name="Operator?",subtype="in_prompts"

Operator?("<?") ~> True
Operator?('(<?)) ~> True
Operator?("For") ~> False

%/mathpiper
*/