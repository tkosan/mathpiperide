package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class VoltageIntegrator extends Meter {

    protected String magneticFluxString = "0.0 Wb";

    public VoltageIntegrator(int x, int y, String uid, Circuit circuit) {
        
        super(x, y, circuit);
        
        if(uid != null)
        {
            componentSubscript = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        secondary = "Magnetic Flux";
        secondaryUnitSymbol = "Wb";
        primary = "Magnetic Flux";
        primaryUnitSymbol = "Wb";
    }
    
    public void reset() {
        super.reset();
        setMagneticFluxString("0.00 Wb");
    }

    public void draw(ScaledGraphics sg) throws Exception {
        super.draw(sg);
        
        String valueAndUnit = Component.addUnitOperator(getMagneticFluxString(), isValueVisible(), isUnitVisible());
        
        if(isValueVisible())
        {
            sg.drawString(valueAndUnit.split("`")[0], Math.round((x1 + x2) * 0.5 + 3) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 3));
        }
        
        if(isUnitVisible())
        {
            sg.drawString(valueAndUnit.split("`")[1], Math.round((x1 + x2) * 0.5 + 16) - Meter.METER_SIZE, Math.round((y1 + y2) * 0.5 + 16));
        }
    }

    public String getMagneticFluxString() {
        return magneticFluxString;
    }

    public void setMagneticFluxString(String magneticFluxString) {
        this.magneticFluxString = magneticFluxString;
    }
}
