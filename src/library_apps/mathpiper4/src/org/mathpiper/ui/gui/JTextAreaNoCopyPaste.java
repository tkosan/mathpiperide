
package org.mathpiper.ui.gui;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.InputStream;
import javax.swing.JTextArea;


public class JTextAreaNoCopyPaste extends JTextArea
{
    
    public JTextAreaNoCopyPaste() throws Exception
    {
        super();
        setFont(newFont(14.0f));
    }
        
    public JTextAreaNoCopyPaste(String text, int rows, int columns) throws Exception
    {
        super(text, rows, columns);
        setFont(newFont(14.0f));
    }
    
    public void setFontSize(float fontSize) throws Exception
    {
        setFont(newFont(fontSize));
    }
    
    private Font newFont(float fontSize) throws Exception
    {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("org/mathpiper/DejaVuSansMono.ttf");
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font size1Font = Font.createFont(Font.TRUETYPE_FONT, inputStream);
        Font font = size1Font.deriveFont(fontSize);
        ge.registerFont(font);
        return font;
    }
    
    public void copy()
    {
    }
    
    public void paste()
    {
    }
}

