%mathpiper,def="Csc"

RulebaseHoldArguments("Csc",["x"]);

5 ## Csc(x_Number?)::NumericMode?() --> CscD(x);
5 ## Csc(x_Number?`1)::NumericMode?() --> CscD(x);

10 ## Csc(x_Number?`rad)::NumericMode?() --> CscD(x);
10 ## Csc(x_Number?`radian)::NumericMode?() --> CscD(x);

15 ## Csc(x_Number?`deg)::NumericMode?() --> CscD(x * Pi/180);
15 ## Csc(x_Number?`degree)::NumericMode?() --> CscD(x * Pi/180);

20 ## Csc(xx_Number?`uu_)::NumericMode?() --> CscD(UnitsStripAll(NotationStandard(xx`uu)));

%/mathpiper





%mathpiper_docs,name="Csc",categories="Mathematics Procedures,Trigonometry (Symbolic)"

*CMD Csc --- trigonometric function cosecant

*CALL
        Csc(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure represents the trigonometric function cosecant.


*EXAMPLES
*SEE Sin, Cos, Tan, Sec, Cot, ArcSin, ArcCos, ArcTan, ArcCsc, ArcSec, ArcCot
%/mathpiper_docs





%mathpiper,name="Csc",subtype="in_prompts"

Csc(2) ~> Csc(2)

NM(Csc(4`rad)) ~> -1.321348709

NM(Csc(7`deg)) ~> 8.205509051

%/mathpiper





%mathpiper,name="Csc",subtype="automatic_test"

Verify(NM(Csc(1)), 1.188395106);

%/mathpiper
