package org.mathpiper.ui.gui.applications.circuitpiper.view.instruments;

import org.mathpiper.ui.gui.applications.circuitpiper.view.instruments.InstrumentPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;

public class MultimeterPanel extends InstrumentPanel
{
    private Component component;
    private Terminal terminal;
    private JTextArea readoutTextArea;
    
    public MultimeterPanel(Component component)
    {
        this(component.getID());
        
        this.component = component;
    }
    
    public MultimeterPanel(Terminal terminal)
    {
        this("T" + terminal.terminalNumber);
        
        this.terminal = terminal;
    }
    
    public MultimeterPanel(String id)
    {
        this.setLayout(new BorderLayout());
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Options");
        
        JMenuItem removeMenuItem = new JMenuItem("Remove");
        removeMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                if(component != null)
                {
                component.removeInstrumentPanel(MultimeterPanel.this);
                }
                
                if(terminal != null)
                {
                    terminal.removeInstrumentPanel(MultimeterPanel.this);
                }
            }
        });
        
        menu.add(removeMenuItem);
        
        menuBar.add(menu);
        
        this.add(menuBar, BorderLayout.NORTH);
        
        Border blackline = BorderFactory.createLineBorder(Color.black);
        
        TitledBorder titledBorder = BorderFactory.createTitledBorder(blackline, id + " Multimeter");
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        this.setBorder(titledBorder);
      
        readoutTextArea = new JTextArea();
        
        readoutTextArea.setFont(new Font("Monospaced", 0, 18));
        
        this.add(readoutTextArea);
    }
    
    public void setText(String readout)
    {
        this.readoutTextArea.setText(readout);
    }
    
    public void clear()
    {
            readoutTextArea.setText("");
    }
    
    public void refresh()
    {
        if(component != null)
        {
            readoutTextArea.setText(component.getInfo());
        }
        
        if(terminal != null)
        {
            readoutTextArea.setText(terminal.circuit.circuitPanel.getTerminalVoltage(terminal));
        }
    }
}
