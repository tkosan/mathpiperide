%mathpiper,def="PDF"

/* Evaluates distribution dst at point x
   known distributions are:
   1. Discrete distributions
   -- BernoulliDistribution(p)
   -- BinomialDistribution(p,n)
   -- DiscreteUniformDistribution(a,b)
   -- PoissonDistribution(l)
   2. Continuous distributions
   -- ExponentialDistribution(l)
   -- NormalDistrobution(a,s)
   -- ContinuousUniformDistribution(a,b)
   -- tDistribution(m)
   -- GammaDistribution(m)
   -- ChiSquareDistribution(m)

  DiscreteDistribution(domain,probabilities) represent arbitrary
  distribution with finite number of possible values; domain list
  contains possible values such that
  Pr(X=domain[i])=probabilities[i].
  TODO: Should domain contain numbers only?
*/



10 ## PDF(ExponentialDistribution(l_), x_) --> Decide(x<?0,0,l*Exp(-l*x));

10 ## PDF(NormalDistribution(m_, s_), x_) --> Exp(-(x-m)^2/(2*s^2))/Sqrt(2*Pi*s^2); //See http://en.wikipedia.org/wiki/Normal_distribution.

10 ## PDF(ContinuousUniformDistribution(a_, b_), x_)::(a<?b) --> Decide(x <? a |? x >? b,0 ,1/(b-a));

10 ## PDF(DiscreteDistribution( dom_List?, prob_List?), x_)::( Length(dom)=?Length(prob) &? Simplify(Add(prob))=?1) -->
    {
      Local(i);
      i:=Find(dom,x);
      Decide(i =? -1,0,prob[i]);
    }
10 ## PDF( ChiSquareDistribution(m_),x_RationalOrNumber?)::(x <=? 0) --> 0;
20 ## PDF( ChiSquareDistribution(m_), x_) --> x^(m/2-1)*Exp(-x/2)/2^(m/2)/Gamma(m/2);

10 ## PDF(tDistribution(m_), x_) --> Gamma((m+1)/2)*(1+x^2/m)^(-(m+1)/2)/Gamma(m/2)/Sqrt(Pi*m);


%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="PDF",categories="Mathematics Procedures,Statistics & Probability"
*CMD PDF --- probability density procedure
*STD
*CALL
        PDF(dist,x)

*PARMS
{dist} -- a distribution type

{x} -- a value of random variable

*DESC
{PDF}
returns the density procedure at point $x$.

The probability density procedure (PDF) of a continuous distribution is defined as the 
derivative of the (cumulative) distribution procedure.

*EXAMPLES

*SEE CDF, PMF, Expectation
%/mathpiper_docs





%mathpiper,name="PDF",subtype="in_prompts"

NM(PDF(ContinuousUniformDistribution(1.2, 2), 2)) ~> 1.25

NM(PDF(ContinuousUniformDistribution(1.3, 2), 2)) ~> 1.428571429

NM(PDF(ContinuousUniformDistribution(1.3, 3), 2)) ~> 0.5882352941

%/mathpiper





%mathpiper,name="PDF",subtype="automatic_test"

Verify(NM(PDF(ContinuousUniformDistribution(1,4), 1.5)), 0.3333333333);

Verify(NM(PDF(ChiSquareDistribution(3), 4)), 0.1079819332);

%/mathpiper