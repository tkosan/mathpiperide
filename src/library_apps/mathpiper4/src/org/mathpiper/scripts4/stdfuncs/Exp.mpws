%mathpiper,def="Exp"

2 ## Exp(x_Number?)::NumericMode?() --> ExpNum(x);
4 ## Exp(Ln(x_))           --> x;
//110 ## Exp(Complex(r_, i_)) -->  Exp(r)*(Cos(i) + I*Sin(i));
200 ## Exp(0) --> 1;
200 ## Exp(-Infinity) --> 0;
200 ## Exp(Infinity) --> Infinity;
200 ## Exp(Undefined) --> Undefined;

300 ## Exp(xx_`uu_) --> Exp(UnitsStripAll(NotationStandard(xx`uu)));

Exp(xlist_List?) --> MapSingle("Exp",xlist);

%/mathpiper



%mathpiper_docs,name="Exp",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD Exp --- exponential function
*STD
*CALL
        Exp(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure calculates $e$ raised to the power $x$, where $e$ is the
mathematic constant 2.71828... One can use {Exp(1)}
to represent $e$.

This procedure is threaded, meaning that if the argument {x} is a
list, the procedure is applied to all entries in the list.

*EXAMPLES
*SEE Ln, Sin, Cos, Tan, NM
%/mathpiper_docs





%mathpiper,name="Exp",subtype="in_prompts"

Exp(0) ~> 1

Exp(I*Pi) ~> Exp(Complex(0, Pi))

NM(Exp(1)) ~> 2.71828183

%/mathpiper





%mathpiper,name="Exp",subtype="automatic_test"

Verify(Exp(0), 1);

Verify(Exp(I*Pi), Exp(Complex(0, Pi)));

Verify(NM(Exp(1)), 2.71828183);

%/mathpiper