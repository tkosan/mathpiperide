%mathpiper,def="LnExpand"

////////////////////// Log rules stuff //////////////////////

// LnExpand
1 ## LnExpand(Ln(x_Integer?))
                            --> Add(Map([[n,m],m*Ln(n)],Transpose(Factors(x))));
1 ## LnExpand(Ln(a_*b_))     --> LnExpand(Ln(a))+LnExpand(Ln(b));
1 ## LnExpand(Ln(a_/b_))     --> LnExpand(Ln(a))-LnExpand(Ln(b));
1 ## LnExpand(Ln(a_^n_))     --> LnExpand(Ln(a))*n;
2 ## LnExpand(a_)            --> a;

%/mathpiper



%mathpiper_docs,name="LnExpand",categories="Mathematics Procedures,Expression Simplification"
*CMD LnExpand --- expand a logarithmic expression using standard logarithm rules
*STD
*CALL
        LnExpand(expr)

*PARMS

{expr} -- the logarithm of an expression

*DESC

{LnExpand} takes an expression of the form $Ln(expr)$, and applies logarithm
rules to expand this into multiple {Ln} expressions where possible.  An
expression like $Ln(a*b^n)$ would be expanded to $Ln(a)+n*Ln(b)$.

If the logarithm of an integer is discovered, it is factorised using {Factors}
and expanded as though {LnExpand} had been given the factorised form.  So 
$Ln(18)$ goes to $Ln(x)+2*Ln(3)$.

*EXAMPLES
*SEE Ln, LnCombine
%/mathpiper_docs






%mathpiper,name="LnExpand",subtype="in_prompts"

LnExpand(Ln(_a*_b^_n)) ~> Ln(_a) + Ln(_b)*_n

LnExpand(Ln(_a^_m/_b^_n)) ~> Ln(_a)*_m - Ln(_b)*_n

//LnExpand(Ln(60)) ~> 2*Ln(2)+Ln(3)+Ln(5)

//LnExpand(Ln(60/25)) ~> 2*Ln(2)+Ln(3)-Ln(5)

%/mathpiper





%mathpiper,name="LnExpand",subtype="automatic_test"

Verify(LnExpand(Ln(_a*_b^_n)), Ln(_a) + Ln(_b)*_n);

Verify(LnExpand(Ln(_a^_m/_b^_n)), Ln(_a)*_m - Ln(_b)*_n);

%/mathpiper