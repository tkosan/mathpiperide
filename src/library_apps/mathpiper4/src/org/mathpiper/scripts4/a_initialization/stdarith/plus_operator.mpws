%mathpiper,def="+;SumListSide"

/* Addition */

50 ## x_Number? + y_Number? --> AddN(x,y);

60 ## x_String? + y_String? --> ConcatStrings(x, y);
62 ## x_String? + y_ --> ConcatStrings(x, ToString(y));
64 ## x_ + y_String? --> ConcatStrings(ToString(x), y);

100 ## + x_  --> x;

//100 ## x_ + x_   --> 2*x;
//100 ## x_ + n_Constant?*(x_)   --> (n+1)*x;
//100 ## n_Constant?*(x_) + x_   --> (n+1)*x;
//100 ## Sinh(x_)+Cosh(x_)                --> Exp(x);

101 ## x_ + - y_ --> x-y;
101 ## x_ + (- y_)/(z_) --> x-(y/z);
101 ## (- y_)/(z_) + x_  --> x-(y/z);
101 ## (- x_) + y_ --> y-x;
102 ## x_ + y_NegativeNumber? --> x-(-y);
102 ## x_ + y_NegativeNumber? * z_ --> x-((-y)*z);
102 ## x_ + (y_NegativeNumber?)/(z_) --> x-((-y)/z);
102 ## (y_NegativeNumber?)/(z_) + x_  --> x-((-y)/z);

102 ## (x_NegativeNumber?) + y_ --> y-(-x);
// fractions
150 ## n1_ / d_ + n2_ / d_ --> (n1+n2)/d;

//200 ## (x_Number? + y_)::Not?(Number?(y)) --> y+x;
//200 ## ((y_ + x_Number?) + z_)::Not?(Number?(y) |? Number?(z)) --> (y+z)+x;
//200 ## ((x_Number? + y_) + z_Number?)::Not?(Number?(y)) --> y+(x+z);
//200 ## ((x_ + y_Number?) + z_Number?)::Not?(Number?(x)) --> x+(y+z);

// fractions
210 ## x_Number? + (y_Number? / z_Number?) -->(x*z+y)/z;
210 ## (y_Number? / z_Number?) + x_Number? -->(x*z+y)/z;
210 ## (x_Number? / v_Number?) + (y_Number? / z_Number?) -->(x*z+y*v)/(v*z);


//  220 ## + x_List?          --> MapSingle("+",x);        // this rule is never active

220 ## (xlist_List? + ylist_List?)::(Length(xlist)=?Length(ylist)) --> Map("+",[xlist,ylist]);

SumListSide(x_, y_List?) -->
{
    Local(i,result);

    Check(y !=? [], "An arithmetic operation can't be done with an empty list.");

    result:=[];

    For(i:=1,i<=?Length(y),i++)
    { 
        Insert!(result,i,x + y[i]); 
    }

    result;
}

240 ## (x_List? + y_)::Not?(List?(y)) &? Not?(String?(y)) --> SumListSide(y,x);
241 ## (x_ + y_List?)::Not?(List?(x)) &? Not?(String?(x)) --> SumListSide(x,y);

250 ## z_Infinity? + Complex(x_,y_) --> Complex(x+z,y);
250 ## Complex(x_,y_) + z_Infinity? --> Complex(x+z,y);

251 ## z_Infinity? + x_ --> z;
251 ## x_ + z_Infinity? --> z;


250 ## Undefined + y_ --> Undefined;
250 ## x_ + Undefined --> Undefined;


300 ## 0 + x_    --> x;
300 ## x_ + 0    --> x;

%/mathpiper





%mathpiper_docs,name="+",categories="Operators"
*CMD + --- arithmetic addition and string concatenation.
*STD
*CALL
        x+y
        +x

*PARMS

{x} and {y} -- objects for which arithmetic addition is defined or strings

*DESC

The addition operators can work on integers,
rational numbers, complex numbers, vectors, matrices, lists and strings.

These operators are implemented in the standard math library (as opposed
to being built-in). This means that they can be extended by the user.

If either or both arguments are strings, they are concatenated.

*EXAMPLES

*SEE -, *, /, ^
%/mathpiper_docs





%mathpiper,name="+",subtype="in_prompts"

2 + 3 ~> 5

"one " + "two" ~> "one two"

1 + " two" ~> "1 two"

"one " + 2 ~> "one 2"

%/mathpiper





%mathpiper,name="+",subtype="automatic_test"

Verify(1+[3,4],[4,5]);
Verify([3,4]+1,[4,5]);
Verify([1]+[3,4],Hold([1]+[3,4]));
Verify([3,4]+[1],Hold([3,4]+[1]));
Verify([1,2]+[3,4],[4,6]);
Verify(3 + 2 , 5);
Verify(0+_a,_a);
Verify(_a+0,_a);
Verify(2+3,5);
Verify(3*(4+2),18);
Verify((4+2)*3,18);
Verify(-2+3,1);
Verify(-2.01+3.01,1.);

%/mathpiper