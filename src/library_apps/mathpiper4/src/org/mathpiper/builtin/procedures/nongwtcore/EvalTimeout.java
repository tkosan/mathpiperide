/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.nongwtcore;

import java.util.TimerTask;
import java.util.Timer;
import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;


public class EvalTimeout extends BuiltinProcedure
{
    
    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "EvalTimeout";
        aEnvironment.iBuiltinFunctions.put(functionName, new BuiltinProcedureEvaluator(this, 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.



    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        
        BigNumber timeoutTime = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 2);
        		   
        final Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            public void run() {
                timer.cancel();
                
                Environment.haltEvaluationMessage = "Evaluation timeout.";
        
                Environment.haltEvaluation = true;
            }
        }, timeoutTime.toLong());
        
        try
        {
            Cons result = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, getArgument(aEnvironment, aStackTop, 1));
            
            setTopOfStack(aEnvironment, aStackTop, result);
        }
        finally
        {
            timer.cancel();
        }
        
        

    }
}//end class


/*
%mathpiper_docs,name="EvalTimeout",categories="Programming Procedures,Control Flow,Built In"
*CMD Eval --- evaluation an expression for a maximum amount of time
*CORE
*CALL
	Eval(expr, timeout)

*PARMS

{expr} -- expression to evaluate
{timeout} -- timeout time in milliseconds

*DESC
This function is the same as the Eval function, except it will throw an exception
if the timeout time is exceeded.

*E.G.
In> infiniteLoop := '(While(True) 3);
Result: While(True)3

In> EvalTimeout(infiniteLoop, 1000)
Result: Exception
Exception: Error: Evaluation timeout. Starting at index 9.



*SEE Eval
%/mathpiper_docs

 */
