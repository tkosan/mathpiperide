v.02

= Using Trigonometry to Plot Points in Arcs and Circles


== The "arc" Procedure

All of the problems in this week's assignment are
based on the following procedure named "arc". Open
the "DynamicMath" plugin and then evaluate the
following fold in order to see an example of what
this procedure does.

%mathpiper

Procedure("arc", ["centerX", 
                  "centerY", 
                  "radius", 
                  "startAngleDegrees", 
                  "endAngleDegrees", 
                  "angleStepDegrees", 
                  "pointCount"])
{
    Local(angleDegrees, angleRadians);
    
    Check(radius >? 0, "The radius needs to be greater than 0.");
    
    angleDegrees := startAngleDegrees;
    
    While(angleDegrees <=? endAngleDegrees)
    {
        angleRadians := angleDegrees * NM(Pi/180);
        
        GeoGebraPoint("A" + pointCount++, 
                        centerX + (radius * CosD(angleRadians)), 
                        centerY + (radius * SinD(angleRadians)));
        
        angleDegrees +:= angleStepDegrees;
    }
    
    pointCount;
}

GeoGebraClear();

arc(0, 0, 1, 0, 90, 5, 1);

%/mathpiper


When this procedure is called with the arguments
(0, 0, 1, 0, 90, 5, 1), it plots 20 points counterclockwise
from an angle of 0 degrees to 90 degrees.

The "centerX" and "centerY" parameters specify the
center of the arc. The "radius" parameter
specifies the radius of the arc. The
"startAngleDegrees" and "endAngleDegrees"
parameters specify the start angle and end angles
of the arc, respectfully. The "angleStepDegrees"
parameter specifies how many degrees will be
between each point that is plotted. Finally, the
"pointCount" parameter specifies the starting
number of the labels of the points that will be
plotted.

Passing "120" to "endAngleDegrees" draws a longer arc.

In> {GeoGebraClear(); arc(0, 0, 1, 0, 120, 5, 1);}



Passing "10" to "angleStepDegrees" draws fewer points
which have more anglar distance between them.

In> {GeoGebraClear(); arc(0, 0, 1, 0, 120, 10, 1);}



Passing "2" to "radius" will double the size of
the arc.

In> {GeoGebraClear(); arc(0, 0, 2, 0, 120, 5, 1);}



Passing "355" to "endAngleDegrees" draws a complete
circle.

In> {GeoGebraClear(); arc(0, 0, 1, 0, 355, 5, 1);}



== Degrees and Radians

People are taught from a young age that there are
360 degrees (angle units) in a circle. But why 360
degrees and not 100 degrees or 500 degrees? There
are many speculations on how the number "360" came
to be picked as the number of degrees in a circle
on Earth, but we may never know which of them is
the correct reason.

If there are civilizations on other planets in the
universe, it is unlikely that they would use 360
angle units in a circle. Is there an angle unit
that would be universal across all civilizations
in the universe during all time periods? The
answer is "yes" and on Earth it is called the
"radian".

Evaluate the following code to have a browser open
the indicated website (if the "BrowserOpen"
procedure does not work, copy the link into the
URL bar of your browser and open it directly from
there).

In> BrowserOpen("http://patternmatics.org/ssu/etec1120/trigonometry/")

Open the file named "radius.png". It shows a unit
circle, which has a radius of 1. Imagine that the
red line that is drawn on the X axis between 0 and
1 is a string that has been laid on the a radius
line that is at an angle of 0 degrees.

Now "pick up" the string and lay it out
counterclockwise on the circumference of the
circle as far as it will go. This distance is an
angle unit called a radian. This is shown in the
file named "radius_1.png".

The file "radius_2" shows the string (now in
green) being picked up and laid out again as far
as it will go, measuring 2 radians.

The file "radius_3" shows the string (now in
purple) being picked up and laid out again as far
as it will go, measuring 3 radians.

The file "radius_4" shows the string (now in
orange) being picked up and laid out again as far
as it takes to make it to half the circle, which
is 180 degrees. The last orange part of the string
is .14 angle units long.

The 3 laid-out radians plus the .14 length radian
sums to 3.14 radians in half a circle.

Since the radius of a circle is the same across
the whole universe during all time periods, the radian
can be used as a universal angle unit of all civilizations
everywhere across all periods of time.



== The Sine and Cosine Procedures

Inside the body of the "arc" program is a call to
a procedure named "CosD" and a call to a procedure
named "SinD". In order to understand what they do,
open the file that is named
"unit_circle_trigonometry_demo_v.01.gif" which is
at the
"http://patternmatics.org/ssu/etec1120/trigonometry/"
website.

This file shows a unit circle with a point named
"B" that is continuously moved back-and-forth
around the circumference of the circle.

The red vertical line indicates the distance of
point "B" above or below the X axis. Its value is
calculated by dividing the length of this red
vertical line by the length of the radius of the
circle. The "SinD" procedure calculates this value
by accepting an angle of a point on the
circumference of the circle in radians as an
argument and returning the distance that the point
is above or below the X axis.

The blue horizontal line indicates the distance of
point "B" to the left or the right of the Y axis.
Its value is calculated by dividing the length of
this blue horizontal line by the length of the
radius of the circle. The "CosD" procedure
calculates this value by accepting an angle of a
point on the circumference of the circle in
radians as an argument and returning the distance
that the point is to the left or the right of the
Y axis.

Said another way, the "SinD" procedure calculates
the X-coordinate of a point that is on the
circumference of a circle, and the "CosD"
procedure calculates it Y-coordinate.

In the "arc" procedure, the line of code
"angleRadians := angleDegrees * NM(Pi/180);"
converts from degrees to radians because "SinD"
and "CosD" only accept radians.
