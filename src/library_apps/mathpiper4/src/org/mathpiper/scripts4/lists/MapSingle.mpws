%mathpiper,def="MapSingle"

TemplateProcedure("MapSingle",["func", "list"])
{
    Local(mapsingleresult);
    
    Check(List?(list), "The second argument must be a list.");
    
    mapsingleresult:=[];
    
    ForEach(mapsingleitem,list)
    {
        Insert!(mapsingleresult,1,
        Apply(func,[mapsingleitem]));
    }
    
    Reverse!(mapsingleresult);
}

UnFence("MapSingle",2);
HoldArgument("MapSingle","func");

%/mathpiper



%mathpiper_docs,name="MapSingle",categories="Programming Procedures,Lists (Operations)"
*CMD MapSingle --- apply a unary procedure to all entries in a list
*STD
*CALL
        MapSingle(fn, list)

*PARMS

{fn} -- procedure to apply

{list} -- list of arguments

*DESC

The procedure "fn" is successively applied to all entries in
"list", and a list containing the respective results is
returned. The procedure can be given either as a string or as a pure
procedure (see Apply for more information on pure procedures).

The {/@} operator provides a shorthand for
{MapSingle}.

*E.G.

Known procedures of with one argument:
In> MapSingle("Sin",[_a,4,Pi]);
Result: [Sin(_a),Sin(4),Sin(Pi)]

In> MapSingle(Lambda([x],x^2), [(_a+2)^2,2,Pi]); 
Result: [(_a+2)^4,4,Pi^2]

In> MapSingle(Lambda([x],Expand(x^2)), [(_a+2)^2,2,Pi])
Result: [_a^4+8*_a^3+24*_a^2+32*_a+16,4,Pi^2]


*SEE Map, MapArgs, /@, Apply, Lambda
%/mathpiper_docs





%mathpiper,name="MapSingle",subtype="automatic_test"

Verify(MapSingle("Factorial",[1,2,3,4]),[1,2,6,24]);

%/mathpiper