package org.mathpiper.lisp.parsers.subl.parser;;

import java.util.Hashtable;
import java.util.Vector;

/*
MIT License

Copyright (c) 2020 Joseph Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
 * File: TreeNode.java
 * 
 * This is the central data structure for representing Atoms
 * and S-Expressions.  It maintains the string vector of
 * tokens which make up the object and also employs the factory
 * pattern to create new TreeNodes based on whether or not they
 * are valid S-Expressions.
 * 
 * This class also stipulates the evaluate functions and the
 * isList method.
 * 
 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
 * @since 2012-11-01
 * @version 2012-11-01
 *
 */

abstract public class TreeNode {
	protected abstract boolean isList();
	protected Vector <String> tokens = new Vector <String> ();

	/**
	 * Function: create
	 * 
	 * Uses the factory pattern to create an atom if the passed string
	 * vector is appropriate or an S-Expression.
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 *
	 * @param s The string vector of the new TreeNode
	 * 
	 * @return The new TreeNode object
	 * 
	 * @throws Exception If the node is empty or if the constructor fails
	 *
	 */
	static TreeNode create(Vector <String> s) throws Exception{
		if ( s.size() > 0 && s.get(0).matches("[(]") ){
			return new SExpression(s);
		} else if ( s.size() > 0 ){
			return new Atom(s.get(0));
		} else {
			throw new Exception("Tried to create a TreeNode with no data");
		}
	}

	/**
	 * Function: create
	 * 
	 * Adapts the factory pattern to force the creation of an Atom
	 * if the data is a boolean value.
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 *
	 * @param b The boolean value of the Atom-to-be
	 * 
	 * @return The new atom object
	 *
	 */
	static TreeNode create(boolean b) throws Exception {
		return new Atom(b);
	}

	/**
	 * Function: create
	 * 
	 * Adapts the factory pattern to create an Atom if the data
	 * is an integer.
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 *
	 * @param i The integer to use as a literal
	 * 
	 * @return The new Atom
	 *
	 */
	static TreeNode create(int i) throws Exception {
		return new Atom(i);
	}

	/**
	 * Function: evaluate
	 * 
	 * The evaluation of TreeNodes returns a new TreeNode.
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 * 
	 * @return The result of evaluating the TreeNode
	 *
	 */
	abstract TreeNode evaluate() throws Exception;

	/**
	 * Function: evaluate
	 * 
	 * The evaluation of TreeNodes returns a new TreeNode.
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 * 
	 * @param flag Whether or not to take numericals literally
	 * 
	 * @return The result of evaluating the TreeNode
	 *
	 */
	abstract TreeNode evaluate(boolean flag) throws Exception;
	
	/**
	 * Function: evaluate
	 * 
	 * The evaluation of TreeNodes returns a new TreeNode.
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 * 
	 * @param flag Whether or not to take numericals literally
	 * @param env The scoped variables
	 * 
	 * @return The result of evaluating the TreeNode
	 *
	 */
	abstract TreeNode evaluate(boolean flag, Hashtable <String, TreeNode> env) throws Exception;
	
	/**
	 * Function: evaluate
	 * 
	 * The evaluation of TreeNodes returns a new TreeNode.
	 * 
	 * @author Joseph T. Anderson <jtanderson@ratiocaeli.com>
	 * @since 2012-11-01
	 * @version 2012-11-01
	 * 
	 * @param env The scoped variables
	 * 
	 * @return The result of evaluating the TreeNode
	 *
	 */	
	abstract TreeNode evaluate(Hashtable <String, TreeNode> env) throws Exception;
}
