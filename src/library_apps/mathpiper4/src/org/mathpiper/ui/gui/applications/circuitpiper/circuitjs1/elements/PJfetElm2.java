package org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements;

import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.Point;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.StringTokenizer;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.Transistor;

public class PJfetElm2 extends NJfetElm
{
    public PJfetElm2(int xx, int yy) {
        super(xx, yy);
    }
    
    public PJfetElm2(int xa, int ya, int xb, int yb, int f, StringTokenizer st) {
        super(xa, ya, xb, yb, f, st);
    }
    
    public void setPoints()
    {
        super.setPoints();
        
        Transistor transistorComponent = (Transistor) this.component;
        
        drainPoints[0] = new Point(x2, y2);

        sourcePoints[0] = new Point(x1, y1);

        basePoint = transistorComponent.calculatedPointsToGridPoints()[0];
    }
}