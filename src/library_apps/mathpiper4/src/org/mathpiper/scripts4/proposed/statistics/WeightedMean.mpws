%mathpiper,def="WeightedMean"


WeightedMean(list) :=
{

    Check(List?(list), "Argument must be a list.");
    
    Local( values, lastWeight, weights );
    
    values := [];
    
    weights := [];
    
    
    ForEach(element,list)
    {        
        Check(List?(element), "Values and their associated weights must be in a list.");
        
        Check(Length(element) =? 2, "Each value and its associated weight must be in a two element list.");
        
        values := Concat(values, [element[1]]);

        weights := Concat(weights, [element[2]]);
    }
    
    Sum(values * weights)/Sum(weights);

}

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="WeightedMean",categories="Mathematics Procedures,Statistics & Probability",access="experimental"
*CMD WeightedMean --- weighted mean
*STD
*CALL 
        WeightedMean([[value, weight],...])

*PARMS

{value} -- a value.

{weight} -- the weight to associate with the value.

*DESC 
This procedure allows more weight to be associated with certain values and
less weight to others when calculating their mean.

*E.G.
In>  WeightedMean([[92,50], [87,40], [76,10]])
Result: 442/5

In>  NM(WeightedMean([[92,50], [87,40], [76,10]]))
Result: 88.4

*SEE Mean, Median, Mode, GeometricMean
%/mathpiper_docs





%mathpiper,name="WeightedMean",subtype="automatic_test"

Verify(WeightedMean([[92,50], [87,40], [76,10]]), 442/5);

%/mathpiper