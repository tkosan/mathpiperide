/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Evaluator;
import org.mathpiper.lisp.ProfileData;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.unparsers.MathPiperUnparser;


public class Profile extends BuiltinProcedure
{


    private Profile()
    {
    }

    public Profile(Environment aEnvironment, String functionName)
    {
        this.functionName = functionName;

        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "Profile");
    }//end constructor.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Evaluator.iTraced = true;
        Evaluator.isProfiled = true;

        try
        {
            Cons result = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, getArgument(aEnvironment, aStackTop, 1));
        }
        finally
        {
            Evaluator.iTraced = false;
            Evaluator.isProfiled = false;
        }
        Map<String, ProfileData> procedureTimes = Evaluator.procedureTimes;
        
        List<ProfileData> list = new ArrayList<ProfileData>(procedureTimes.values());
        
        Collections.sort(list, new Comparator<ProfileData>() {
            @Override
            public int compare(ProfileData lhs, ProfileData rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs.totalTime < rhs.totalTime ? 1 : (lhs.totalTime > rhs.totalTime) ? -1 : 0;
            }
        });
        
        
        for(ProfileData pd : list)
        {
            aEnvironment.iCurrentOutput.write(pd.toString());
            aEnvironment.iCurrentOutput.write("\n");
        }
        
        
        /*
        Cons listAtomCons = aEnvironment.iListAtom.copy(false);
        Comparator<Operator> comparator = Comparator.comparing(Operator::getiPrecedence).thenComparing(Operator::getSymbol);
        Collections.sort(operatorsList, comparator);
        Cons pointerCons = listAtomCons;
	for (Operator operator : operatorsList) {
            String symbolName = operator.getSymbol();
            if(! symbolName.endsWith("$"))
            {
                Cons elementCons = AtomCons.getInstance(aEnvironment.getPrecision(), "\"" + symbolName + "\"");
                pointerCons.setCdr(elementCons);
                pointerCons = elementCons;
            }
	}
        Cons list = SublistCons.getInstance(listAtomCons);
	setTopOfStack(aEnvironment, aStackTop, list);
        */
        
        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, new JavaObject(procedureTimes)));
    }
}



/*
%mathpiper_docs,name="Profile",categories="Programming Procedures,Built In,Debugging",access="experimental"
*CMD Profile --- measure the time taken by an evaluation

*CALL
	Profile() expr
*PARMS
{expr} -- any expression

*DESC
The procedure {Profile() expr} evaluates the expression {expr} and returns a Java
HashMap that contains the names of all the procedures that were called
during the evaluation along with the amount of time each procedure took
in nanoseconds.

The procedure also prints the names of all of the procedures with their elapsed
times in milliseconds.

Note: each procedure's elapsed time is the combined time for all instances of
that procedure that were called.

*E.G.
In> Profile() 2 * 3
Result: class java.util.HashMap
Side Effects:
Sequence 8901
:= 1878
Eval 1678
Decide 1438
/` 1415
MacroAssign 1259
ApplyFast 810
MacroSubstituteApply 751
LocalSymbols 631
Factor 612
ForEach 574
While 566
Insert! 456
List 427
If 408
JavaCall 404
MacroMapArgs 374
ListToProcedure 372
Concat 367
ReduceToMathPiper 361
MacroMapSingle 360
/: 294
MapSingle 256
RulebaseEvaluateArguments 225
=? 130
Assign 114
Nth 113
And? 103
CheckParametersMadeLocal 102
LocPredicate 100
Equal? 95
Check 92
Not? 70
Apply 67
Else 67
MathPiperToReduce 64
!? 64
&? 49
+ 48
Length 48
JavaAccess 46
ToString 46
PatternsCompile 36
List? 32
Atom? 31
Procedure? 30
PatternCompile 23
Number? 22
First 17
--> 16
Hold 15
DefinePattern 14
Local 14
Rest 13
Or? 11
JavaNew 11
String? 11
Unholdable 10
' 8
ProcedureToList 7
MacroLocal 7
!=? 6
ToAtom 6
$HandleFactor17 5
Generic? 5
Procedure 5
Integer? 5
GreaterThan? 4
Type 4
Association 4
TemplateProcedure 3
JavaToValue 3
|? 3
MathNth 3
RuleEvaluateArguments 2
Reverse! 2
MakeVector 2
Lambda 2
<=? 1
BuiltinAssociation 1
TreeProcess 1
>=? 1
StringTrim 1
StringSubstring 1
<? 1
StringEndsWith? 1
- 1
PatternCreate 0
Boolean? 0
PipeFromString 0
LessThan? 0
AddN 0
Macro 0
* 0
RulePatternEvaluateArguments 0
RulebaseDefined 0
Append! 0
HoldArgument 0
UnFence 0
ExceptionCatch 0
HasProcedure? 0
RulebaseHoldArguments 0
ParseMathPiper 0
ConcatStrings 0
RuleHoldArguments 0
Retract 0
SubtractN 0
-> 0
UnderscoreConstant? 0
Variable? 0
DefinedConstant? 0
MultiplyN 0
MacroRulebaseHoldArguments 0
FlatCopy 0

*SEE Trace, TraceSome, TraceExcept

%/mathpiper_docs
*/