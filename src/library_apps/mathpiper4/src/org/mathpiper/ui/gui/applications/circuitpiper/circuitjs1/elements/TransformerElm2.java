package org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.elements;

import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.Point;
import org.mathpiper.ui.gui.applications.circuitpiper.circuitjs1.StringTokenizer;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.NTerminalComponent;

public class TransformerElm2 extends TransformerElm
{
    public TransformerElm2(int xx, int yy) {
        super(xx, yy);
    }
    
    public TransformerElm2(int xa, int ya, int xb, int yb, int f, StringTokenizer st) {
        super(xa, ya, xb, yb, f, st);
    }
    
    public void setPoints()
    {
        super.setPoints();
        
        NTerminalComponent nTerminalComponent = (NTerminalComponent) this.component;
        
        ptEnds[0] = new  Point(x2, y2);
        ptEnds[1] = nTerminalComponent.calculatedPointsToGridPoints()[0];
        ptEnds[2] =  new Point(x1, y1);
        ptEnds[3] = nTerminalComponent.calculatedPointsToGridPoints()[1];
    }
    
    public String getDisplayLabel()
    {
        return "" + ratio;
    }
}
