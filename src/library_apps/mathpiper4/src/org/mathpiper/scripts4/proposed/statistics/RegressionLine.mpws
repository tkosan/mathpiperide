%mathpiper,def="RegressionLine"

RegressionLine(x,y) :=
{   
    Check(List?(x), "The first argument must be a list.");
    
    Check(List?(y), "The second argument must be a list.");
    
    Check(Length(x) =? Length(y), "The lists for argument 1 and argument 2 must have the same length.");
    
    Local(n,a,b,xMean,yMean,line,result);
    
    n := Length(x);
    
    b := NM((n*Sum(x*y) - Sum(x)*Sum(y))/(n*Sum(x^2)-(Sum(x))^2));
    
    xMean := NM(Mean(x));
    
    yMean := NM(Mean(y));
    
    a := NM(yMean - b*xMean);
    
    line := a + b*Hold(x);
    
    result := [];
    
    result["xMean"] := xMean;
    
    result["yMean"] := yMean;
    
    result["line"] := line;
    
    result["yIntercept"] := a;
    
    result["slope"] := b;
    
    result["count"] := n;
    
    result;
}

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="RegressionLine",categories="Mathematics Procedures,Statistics & Probability",access="experimental"
*CMD RegressionLine --- calculates the correlation coefficient between two lists of values
*STD
*CALL
        RegressionLine(xList,yList)

*PARMS

{xList} -- the list of domain values
{yList} -- the list of range values

*DESC
This procedure calculates the correlation coefficient between two lists of values.

*E.G.
/%mathpiper

x := [4,3,5,2,3,4,3];
y := [83,86,92,78,82,95,80];

RegressionLine(x,y);

/%/mathpiper

    /%output,preserve="false"
      Result: 0.7766185090
.   /%/output
%/mathpiper_docs

    %output,preserve="false"
      
.   %/output


