%mathpiper,def="-:="

RulebaseHoldArguments("-:=",["aLeft", "aRight"]);
UnFence("-:=", 2);
HoldArgument("-:=", "aLeft");
HoldArgument("-:=", "aRight");

RuleHoldArguments("-:=", 2, 0, Number?(Eval(aLeft)))
{
    MacroAssign(aLeft,Eval(SubtractN(Eval(aLeft),Eval(aRight))));
    Eval(aLeft);
}

%/mathpiper





%mathpiper_docs,name="-:=",categories="Operators"
*CMD -:= --- subtract to a variable
*STD
*CALL
        var -:= subVar

*PARMS

{var} -- variable to subtract from
{subVar} -- variable to subtract
*DESC

The variable with name "subVar" is subtracted from var. This operation is a destructive assignment, ie. the 
variable is over written with the result of var - subVar. The expression {x -:= y} is equivalent to
the assignment {x := x - y}.

*EXAMPLES

*SEE :=, --

%/mathpiper_docs





%mathpiper,name="-:=",subtype="in_prompts"

test := 9 ~> 9

test -:= 5 ~> 4

test ~> 4

x := 1 ~> 1

x -:= 3 ~> -2

x ~> -2

%/mathpiper





%mathpiper,name="-:=",subtype="automatic_test"
{
    Local(x, y);

    x := 9;

    y := 3;

    Verify(x -:= 2, 7);

    Verify(x -:= y, 4);
}

%/mathpiper