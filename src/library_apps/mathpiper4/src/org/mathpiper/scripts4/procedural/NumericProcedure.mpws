%mathpiper,def="NumericProcedure;NumericProcedureNumberize;NumericProcedureNumberize"

/* NumericProcedure("newfunc", "oldfunc" [arglist]) will define a wrapper procedure
around  "oldfunc", called "newfunc", which will return "oldfunc(arglist)"
only when all arguments are numbers and will return unevaluated
"newfunc(arglist)" otherwise. */
LocalSymbols(NumericProcedureNumberize)
{
NumericProcedure(newname_String?, oldname_String?, arglist_List?) --> {
        RulebaseEvaluateArguments(newname, arglist);
        RuleEvaluateArguments(newname, Length(arglist), 0,        // check whether all args are numeric
                ListToProcedure(['NumericList?, MapSingle("ToAtom", arglist)])
        )

                /* this is the rule defined for the new procedure.
                // this expression should evaluate to the body of the rule.
                // the body looks like this:
                // NumericProcedureNumberize(oldname(arglist))
                */
                        NumericProcedureNumberize(ListToProcedure([ToAtom("Apply"), oldname,  MapSingle("ToAtom", arglist)]));
                        

}

// this procedure is local to NumericProcedure.
// special handling for numerical errors: return Undefined unless given a number.
10 ## NumericProcedureNumberize(x_Number?) --> x;
20 ## NumericProcedureNumberize(x_Atom?) --> Undefined;
// do nothing unless given an atom

}        // LocalSymbols()

%/mathpiper



%mathpiper_docs,name="NumericProcedure",categories="Programming Procedures,Functional Operators"
*CMD NumericProcedure --- make wrapper for numeric procedures
*STD
*CALL
        NumericProcedure("newname","funcname", [arglist])

*PARMS
{"newname"} -- name of new procedure

{"funcname"} -- name of an existing procedure

{arglist} -- symbolic list of arguments

*DESC
This procedure will define a procedure named "newname"
with the same arguments as an existing procedure named "funcname". The new procedure 
will evaluate and return the expression "funcname(arglist)" only when
all items in the argument list {arglist} are numbers, and return unevaluated otherwise.

This can be useful when plotting procedures defined through other MathPiper routines 
that cannot return unevaluated.

If the numerical calculation does not return a number (for example,
it might return the atom {nan}, "not a number", for some arguments),
then the new procedure will return {Undefined}.

This operator can help the user to program in the style of procedureal programming languages such as Miranda or Haskell.


*EXAMPLES

*SEE RuleEvaluateArguments
%/mathpiper_docs





%mathpiper,name="NumericProcedure",subtype="in_prompts"

PKHG suggestion:

-  /%mathpiper,title="" >>
-  f(x) := Decide( Number?(x), SinN(x),Sin(x)); >>
-  NumericProcedure("f1", "f", ["x"]); >>
-  Echo("indererminate arg _a --> ",f1(_a)); >>
-  Echo("f1(0) = ",f1(0)," and f1(1) = ",f1(1)); >>
-  Plot2D(f1(_x), 0 -> 1); //gives graphic! >>
-  /%/mathpiper >>
- >>
-       /%output,sequence="33",timestamp="2013-10-27 11:45:44.214",preserve="false" >>
-       Result: class org.jfree.chart.ChartPanel >>
- >>
-       Side Effects: >>
-       indererminate arg _a --> f1(_a)  >>
-       f1(0) = 0  and f1(1) = 0.841470984807895 >>
        /%/output

--------------------------------------------------------------------------------

f(x) := NM(Sin(x)) ~> True

NumericProcedure("f1", "f", ["x"]) ~> True

f1(_a) ~> f1(_a)

f1(0) ~> 0

Suppose we need to define a complicated procedure [t(x)]>>
which cannot be evaluated unless [x] is a number:

t(x) := Decide(x<=?0.5, 2*x, 2*(1-x)) ~> True

t(0.2) ~> 0.4

t(x) ~~> Exception
Exception: Error: The variable <x> does not have a value assigned to it. Starting at index 2.

Then, we can use [NumericProcedure()] to define a wrapper [t1(x)] around >>
[t(x)] which will not try to evaluate [t(x)] unless [x] is a number.

NumericProcedure("t1", "t", ["x"]) ~> True

t1("x") ~> t1("x")

t1(0.2) ~> 0.4

Now we can plot the procedure.

Plot2D(t1(_x), -0.1 :: 1.1) ~> class org.jfree.chart.ChartPanel

%/mathpiper





%mathpiper,name="NumericProcedure",subtype="automatic_test"

BuiltinPrecisionSet(10);
Retract("f",All);
Retract("f1",All);
f(x) := NM(Abs(1/x-1));
Verify(f(0), Infinity);
NumericEqual(RoundToN(f(3),BuiltinPrecisionGet()), 0.6666666667,BuiltinPrecisionGet());
NumericProcedure("f1", "f", ["x"]);
Verify(f1(0), Undefined);
NumericEqual(RoundToN(f1(3),BuiltinPrecisionGet()), 0.6666666667,BuiltinPrecisionGet());
Retract("f",All);
Retract("f1",All);
%/mathpiper